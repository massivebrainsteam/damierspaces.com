@extends('admin.layouts.app')

@section('title')
Categories
@endsection

@section('content')
<div class="card">
@include('admin.components.search')
  <div class="card-header">
    <h4 class="card-header-title">
      <a href="{{url('admin/category/0')}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-plus-circle"></i> Add new Category</a>
    </h4>
  </div>

  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Status</th>
          <th>Spaces in this Category</th>
          <th>Last Modified</th>
        </tr>
      </thead>
      <tbody>
        @foreach($categories as $row)
        <tr>
          <td><a href="{{url('admin/category/'.$row->id)}}">{{$row->name}}</a></td>
          <td>{{_badge($row->status)}}</td>
          <td>{{$row->spaces()->count()}}</td>
          <td>{{_date($row->updated_at)}}</td>
        </tr>
        @endforeach

        @if($categories->count() < 1)
        <tr>
          <td colspan="4" class="text-center text-muted">When you create Categories, you will see them here.</td>
        </tr>
        @endif
      </tbody>
    </table>

  </div>

  <div class="mt-3 float-right">
    {{$categories->links() }}
  </div>
</div>

@endsection