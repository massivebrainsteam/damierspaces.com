@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
    {{$category->id > 0 ? 'Update '.$category->name : 'Add New Category'}}
    <a href="/admin/categories" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to All Categories</a>
</div>
@endsection

@section('content')

<div class="col-8 offset-2">

    <form class="mb-4" method="post" action="{{url('admin/category')}}">

        @csrf

        <div class="form-group">
            <label>Name</label>
            <input name="name" type="text" class="form-control" value="{{old('name', $category->name)}}" required>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control" required>

                @foreach(['active', 'inactive'] as $row)
                <option value="{{$row}}" {{old('status', $category->status) == $row ? 'selected' : ''}}>
                    {{ucfirst($row)}}
                </option>
                @endforeach
            </select>
        </div>

        <input type="hidden" name="id" value="{{$category->id}}">

        <button type="submit" class="btn btn-block btn-primary">
            Save Category
        </button>

    </form>
</div>

@endsection
