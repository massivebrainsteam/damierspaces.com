@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
{{$host->name}} - Locations
<a href="/admin/view-host/{{$host->id}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to Host Details</a>
</div>
@endsection

@section('content')

<div class="card">
  <div class="card-header">
    <h4 class="card-header-title">
      <a class="btn btn-outline-primary" href="{{url('admin/location/'.$host->id.'/0')}}"><i class="fe fe-plus-circle"></i> Add new Location</a>
    </h4>
  </div>

  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>Host</th>
          <th>Name</th>
          <th>Country *</th>
          <th>State *</th>
          <th>City *</th>
          <th>Address</th>
          <th>Spaces</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach($locations as $row)
        <tr>
          <td>{{$row->host->name}}</td>
          <td><a href="{{url('admin/location/'.$row->host_id.'/'.$row->id)}}">{{$row->name}}</a></td>
          <td>Nigeria</td>
          <td>Lagos</td>
          <td>Ikeja</td>
          <td>{{$row->address}}</td>
          <td>{{$row->spaces->count()}}</td>
          <td>{{_badge($row->status)}}</td>
        </tr>
        @endforeach

        @if($locations->count() < 1)
        <tr>
          <td colspan="6" class="text-center text-muted">When you create Locations under {{$host->name}}, you will see them here.</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

@endsection