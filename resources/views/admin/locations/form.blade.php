@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
{{$location->id > 0 ? 'Update '.$location->name : 'Add New Location for '.$host->name}}
<a href="/admin/locations/{{$host->id}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to Host Locations</a>
</div>

@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('admin-assets/libs/dropify/css/dropify.min.css')}}">
@endsection

@section('content')

<div class="col-8 offset-2">

    <form class="mb-4" method="post" action="{{url('admin/location')}}" enctype="multipart/form-data">
        
        @csrf

        <div class="form-group">
            <label>Name</label>
            <input name="name" type="text" class="form-control" value="{{old('name', $location->name)}}" required>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Address</label>
            <input name="address" type="text" class="form-control" value="{{old('address', $location->address)}}" required>
            @error('address')
            <div class="invalid-feedback" role="alert">{{ $errors->first('address') }}</div>
            @enderror
        </div>

        <div class="form-group">
            @php($photo_url = $location->getOriginal('photo_url'))
            <label>Location Photo @if($photo_url)
                <a href="{{$photo_url}}" target="_blank">View Current File</a>
            @endif</label>
            <input type="file" class="dropify" name="photo_url" data-default-file="{{$photo_url}}"  data-height="200" data-allowed-file-extensions="jpg jpeg png" data-max-file-size="2M" />
        </div>

        <div class="row">
            <div class="col-12 col-md-12">
                <div class="form-group">
                    <label>Country *</label>
                    <select name="country" id="country" class="form-control" required>
                    </select>
                    @error('country')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('country') }}</div>
                    @enderror
                </div>

            </div>
        </div>

        <div class="row">

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>State *</label>
                    <select name="state" id="state" class="form-control" required>
                    </select> @error('state')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('state') }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>City</label>
                    <input name="city" type="text" class="form-control" value="" required>
                    @error('city')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('city') }}</div>
                    @enderror
                </div>
            </div>

        </div>

        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control" required>

                @foreach(['active', 'inactive'] as $row)
                <option value="{{$row}}" {{old('status', $location->status) == $row ? 'selected' : ''}}>
                    {{ucfirst($row)}}
                </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>Open on Weekends</label>
            <select name="weekends" class="form-control" required>

                @foreach(['yes', 'no'] as $row)
                <option value="{{$row}}" {{old('status', $location->weekends) == $row ? 'selected' : ''}}>
                    {{ucfirst($row)}}
                </option>
                @endforeach
            </select>
        </div>

        <input type="hidden" name="host_id" value="{{$host->id}}">
        <input type="hidden" name="id" value="{{$location->id}}">

        <button type="submit" class="btn btn-block btn-primary">
            Save Location
        </button>

    </form>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('admin-assets/libs/dropify/js/dropify.min.js')}}"></script>
<script type="text/javascript">
    $('.dropify').dropify();
</script>
@endsection