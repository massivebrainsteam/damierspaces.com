@extends('admin.layouts.app')

@section('title')
Attendances
@endsection

@section('content')
<div class="header">
  <div class="header-body">
    <div class="row align-items-center">
      <div class="col">
        {{-- <h1 class="header-title">{{ucwords($title)}}</h1> --}}
      </div>
    </div> 
    <div class="row align-items-center">
      <div class="col">
        <ul class="nav nav-tabs nav-overflow header-tabs">
          <li class="nav-item">
            <a href="{{url('admin/attendances')}}" class="nav-link active">
              Check-In Histories
               <span class="badge badge-pill badge-soft-secondary">10</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('admin/attendances?q=active')}}" class="nav-link">
              Reservations Histories
               <span class="badge badge-pill badge-soft-secondary">10</span>
            </a>
          </li>
      
          
        </ul>

      </div>
    </div>
  </div>
</div>


<div class="card">

  @include('admin.components.search')

  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>Reference</th>
          <th>Space</th>
          <th>Host</th>
          <th>User</th>
          <th>CheckIn At</th>
          <th>CheckOut At</th>
          <th>Time Used</th>
          <th>Package</th>
          <th>Settlement</th>
          <th>Settlement Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach($attendances as $row)
        <tr>
          <td>{{$row->reference}}</td>
          <td>{{$row->space->name}}</td>
          <td>{{$row->space->host->name}}</td>
          <td>{{$row->user->name}}</td>
          <td>{{_date($row->start_at, true)}}</td>
          <td>{{_date($row->end_at, true)}}</td>
          <td>{{$row->time_used}}</td>
          <td>{{$row->user->subscription->package->name}}</td>
          <td>{{_currency($row->settlement)}}</td>
          <td>{{_badge($row->settlement_status)}}</td>
        </tr>
        @endforeach

        @if($attendances->count() < 1)
        <tr>
          <td colspan="8" class="text-center text-muted">When users checkin to spaces, those checkin information will show up here.</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>

  <div class="mt-3 float-right">
    {{$attendances->links() }}
  </div>

</div>

@endsection