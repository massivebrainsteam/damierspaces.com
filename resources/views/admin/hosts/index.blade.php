@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
Hosts
<a href="/admin/host/" class="ml-auto btn-outline-primary btn "><i class="fe fe-user-plus"></i> Add New</a>
</div>
@endsection

@section('content')
<div class="card px-0 mb-3">
    @include('admin.components.search')
</div>

<div class="row mt-4 host-page">

    @foreach($hosts as $row)
    <div class="card mb-3 col-6">
        <div class="card-body">
            <div class="row">
                <div class="col-auto">

                    <a href="#" class="avatar avatar-lg">
                        <img src="{{$row->photo_url}}" alt="{{$row->name}}" class="avatar-img rounded-circle">
                    </a>

                </div>
                <div class="col ml-n2">

                    <h2 class="card-title mb-1">
                        <a href="/admin/view-host/{{$row->id}}">{{$row->name}}</a>
                    </h2>
                    <p class="text-muted">{{$row->address}}</p>
                    <p class="text-primary">Account created: {{$row ->created_at}}</p>


                    <p class="card-text small text-muted mb-1">
                    </p>


                    <div class="card-text mt-3">
                        <p class="border-top mb-1 pt-2">Email: <a href="mailto:{{$row->email}}">{{$row->email}}</a></p>
                        <p class="border-top mb-1 pt-2">Email: <a href="tel:{{$row->phone}}">{{$row->phone}}</a></p>
                        <p class="border-top d-flex align-items-center justify-content-between  mb-1 pt-2">{{$row->spaces->count()}} {{str_plural('Space', $row->spaces->count())}} <a href="{{url('admin/spaces?host_id='.$row->id)}}"> <i class="fe fe-arrow-right"></i></a></p>
                        <p class="border-top d-flex align-items-center justify-content-between  mb-1 pt-2"> {{$row->locations->count()}} {{str_plural('Location', $row->locations->count())}} <a href="{{url('admin/locations/'.$row->id)}}"> <i class="fe fe-arrow-right"></i></a></p>
                        <p class="border-top d-flex align-items-center justify-content-between  mb-1 pt-2">{{_currency($row->attendances()->whereSettlementStatus('pending')->sum('settlement'))}} Pending Settlement <a href="{{url('admin/settlement/'.$row->id)}}"><i class="fe fe-arrow-right"></i></a></p>
                    </div>

                </div>

                <div class="col-auto">

                    {{_badge($row->status)}}

                </div>

                <div class="col-auto">


                    <div class="dropdown">
                        <a href="#" class="dropdown-ellipses dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" data-expanded="false" aria-expanded="false">
                            <i class="fe fe-more-vertical"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 25px, 0px);">

                            <a href="{{url('admin/view-host/'.$row->id)}}" class="dropdown-item">
                                View Host Details
                            </a>

                            <a href="{{url('admin/locations/'.$row->id)}}" class="dropdown-item">
                                View Host's Locations
                            </a>

                            <a href="{{url('admin/spaces?host_id='.$row->id)}}" class="dropdown-item">
                                View Host's Spaces
                            </a>
                            <a href="{{url('admin/host/'.$row->id)}}" class="dropdown-item">
                                Edit Host's basic information
                            </a>
                            <a href="{{url('admin/settlement/'.$row->id)}}" class="dropdown-item">
                                Pay Pending Settlement
                            </a>
                            <a href="{{url('admin/settlements/'.$row->id)}}" class="dropdown-item">
                                View Settlement
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @endforeach

    @if($hosts->count() < 1) <div class="mb-3 col-12 text-center">
        <h1 class="text-center text-muted">When you create Hosts, they will show up here.</h1>
</div>
@endif

<div class="mt-3 col-12 float-right">
    {{$hosts->links() }}
</div>

</div>

@endsection
