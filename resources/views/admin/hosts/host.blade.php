@extends('admin.layouts.app')

@section('title')
<div class="d-flex">

    Host - {{$host->name}}
    <a href="/admin/hosts" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to all Hosts</a>
</div>
{{_badge($host->status)}}
@endsection

@section('content')

    <div class="d-flex flex-wrap">
        <a href="{{url('admin/locations/'.$host->id)}}" class="mr-2 my-3 ml-0 btn btn-sm btn-outline-primary">Manage Locations  <i class="fe fe-chevron-right"></i></a>
        <a href="{{url('admin/spaces?host_id='.$host->id)}}" class="mr-2 my-3 ml-0 btn btn-sm btn-outline-primary">Manage Spaces <i class="fe fe-chevron-right"></i></a>
        <a href="{{url('admin/view-host/'.$host->id)}}" class="mr-2 my-3 ml-0 btn btn-sm btn-outline-primary">Edit Details <i class="fe fe-chevron-right"></i></a>
        <a href="" class="mr-2 my-3 ml-0 btn btn-sm btn-outline-primary">View Settlement <i class="fe fe-chevron-right"></i></a>
    </div>
<div class="row">


    <div class="card col-6 m-r-5">

        <div class="card-header d-flex">

            <h4 class="card-header-title">Basic Information</h4>
            <div class="ml-auto text-right">
                <a href="" class="btn btn-sm btn-outline-primary"><i class="fe fe-edit"></i> EDIT</a>
            </div>

        </div>

        <div class="card-body">

            <div class="list-group list-group-flush my-n3">

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Name</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->name}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Email Address</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                <a href="mailto:{{$host->email}}">{{$host->email}}</a>
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Phone Number</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                <a href="tel:{{$host->email}}">{{$host->email}}</a>
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Address</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->address}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Account Created</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->created_at}}
                            </time>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="card col-6">

        <div class="card-header">

            <h4 class="card-header-title">Business Information</h4>

        </div>

        <div class="card-body">

            <div class="list-group list-group-flush my-n3">

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">CAC Document</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                <a href="{{$host->cac_url}}" target="_blank">View Document <i class="fe fe-arrow-up-right"></i></a>
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Bank Account Name</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->bank_name}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Bank Account Number</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->bank_account_number}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Bank Account Name</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->bank_account_name}}
                            </time>

                        </div>
                    </div>
                </div>

                {{-- <div class="list-group-item">
					<div class="row align-items-center">
						<div class="col">
							<h5 class="mb-0">Photo</h5>
						</div>
						<div class="col-auto">

							<time class="small text-muted">
								<a href="{{$host->photo_url}}" target="_blank">View Photo</a>
                </time>

            </div>
        </div>
    </div> --}}

</div>

</div>
</div>

</div>

<div class="row">

    <div class="card col-6 m-r-5">

        <div class="card-header">

            <h4 class="card-header-title">Spaces Information</h4>

        </div>

        <div class="card-body">

            <div class="list-group list-group-flush my-n3">

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Spaces</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->spaces()->count()}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Check Ins</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->attendances()->count()}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Space Reviews</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->reviews()->count()}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Reservations</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->reservations()->count()}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Locations</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->locations()->count()}}
                            </time>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="card col-6">

        <div class="card-header">

            <h4 class="card-header-title">Contact Person</h4>

        </div>

        <div class="card-body">

            <div class="list-group list-group-flush my-n3">

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Name</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->users()->first()->name}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Email Address</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->users()->first()->email}}
                            </time>

                        </div>
                    </div>
                </div>

                <div class="list-group-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <h5 class="mb-0">Phone</h5>
                        </div>
                        <div class="col-auto">

                            <time class="small text-muted">
                                {{$host->users()->first()->phone}}
                            </time>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>

@endsection
