@extends('admin.layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('admin-assets/libs/dropify/css/dropify.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/libs/intl-tel-input-17.0.0/build/css/intlTelInput.css')}}">
@endsection

@section('title')
<div class="d-flex">
    {{$host->id > 0 ? 'Update '.$host->name : 'Add New Host'}}
    <a href="/admin/hosts" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to All Hosts</a>
</div>
@endsection

@section('content')

<div class="col-8 offset-2">

    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $row)
            <li>{{$row}}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form class="mb-4" action="{{url('admin/host')}}" method="POST" enctype="multipart/form-data">

        @csrf

        <h3>HOST DETAILS</h3>
        <hr>

        <div class="form-group">
            <label>Host Name</label>
            <input name="name" value="{{old('name', $host->name)}}" type="text" class="form-control" required>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
            @enderror
        </div>


        <div class="form-group">
            <label class="mb-1">Description</label>
            <textarea name="description" class="form-control" rows="3">{{old('description', $host->description)}}</textarea>
            @error('description')
            <div class="invalid-feedback" role="alert">{{ $errors->first('description') }}</div>
            @enderror
        </div>

        <div class="row">

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>Email Address</label>
                    <input name="email" value="{{old('email', $host->email)}}" type="email" class="form-control" required @if($host->id > 1) disabled @endif>
                    @error('email')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('email') }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label class="d-block">Phone Number</label>
                    <input name="phone" id="phone" value="{{old('phone', $host->phone)}}" type="number" class="w-100 form-control" required>
                    @error('phone')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('phone') }}</div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <label>Main Address</label>
                <input name="address" value="{{old('address', $host->address)}}" type="text" class="form-control" required>
                @error('address')
                <div class="invalid-feedback" role="alert">{{ $errors->first('address') }}</div>
                @enderror
            </div>
        </div>

        <div class="row mt-4">
            <div class="form-group col-12">
                <label>Photo</label>
                <input type="file" class="dropify" name="photo_url" data-default-file="{{$host->getOriginal('photo_url')}}" data-height="200" data-allowed-file-extensions="jpg jpeg png" data-max-file-size="2M" />
                @error('photo_url')
                <div class="invalid-feedback" role="alert">{{ $errors->first('photo_url') }}</div>
                @enderror
            </div>
        </div>


        {{-- @if($host->id < 1) --}}
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="form-group">
                    <label>Country *</label>
                    <select name="country" id="country" class="form-control" required>
                    </select>
                    @error('country')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('country') }}</div>
                    @enderror
                </div>

            </div>
        </div>

        <div class="row">

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>State *</label>
                    <select name="state" id="state" class="form-control" required>
                    </select> @error('state')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('state') }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>City *</label>
                    <input name="city" value="" type="number" class="w-100 form-control" required>
                    @error('city')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('city') }}</div>
                    @enderror
                </div>
            </div>

        </div>

        {{-- @endif --}}
        <br>
        <h3>CONTACT PERSON *</h3>
        <hr class="mt-5 mb-5">

        <div class="row">
            <div class="col-12">
                <label>Name *</label>
                <input name="address" value="{{old('address', $host->address)}}" type="text" class="form-control" required>
                @error('address')
                <div class="invalid-feedback" role="alert">{{ $errors->first('address') }}</div>
                @enderror
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>Email Address *</label>
                    <input name="email" value="{{old('email', $host->email)}}" type="email" class="form-control" required>
                    @error('email')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('email') }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label class="d-block">Phone Number *</label>
                    <input name="phone" id="phone2" value="{{old('phone', $host->phone)}}" type="number" class="w-100 form-control" required>
                    @error('phone')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('phone') }}</div>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-md-12">

                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control" required>
                        @foreach(['active', 'inactive'] as $row)
                        <option value="{{$row}}" {{old('status', $host->status) == $row ? 'selected' : ''}}>
                            {{ucfirst($row)}}
                        </option>
                        @endforeach
                    </select>
                </div>

            </div>


        </div>

        <hr class="mt-5 mb-5">

        <input type="hidden" name="id" value="{{$host->id}}">

        <button type="submit" class="btn btn-block btn-primary">
            Save
        </button>

    </form>

    @endsection

    @section('scripts')
    <script type="text/javascript" src="{{asset('admin-assets/libs/dropify/js/dropify.min.js')}}"></script>
    <script type="text/javascript">
        $('.dropify').dropify();

    </script>
    @endsection
