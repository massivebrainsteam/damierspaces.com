<!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/admin-assets/fonts/feather/feather.min.css">

    <link rel="stylesheet" href="/admin-assets/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="/admin-assets/css/theme.min.css" id="stylesheetLight">
    <link rel="stylesheet" href="/admin-assets/css/theme-dark.min.css" id="stylesheetDark">
    <script src="/assets/libs/intl-tel-input-17.0.0/build/css/intlTelInput.css"></script>
    <style>
        body {
            display: none;
        }

        .iti {
            width: 100%;
        }

        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        
        .nav-tabs .nav-item a.active {
            padding-bottom: .8rem;
    border-bottom: 2px solid white;
}
        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

    </style>
    @yield('styles')
    <title>Damier Spaces</title>
</head>
<body>


    @include('admin.components.sidebar')


    <nav id="sidebarSmall"></nav>
    <nav id="topnav"></nav>


    <div class="main-content">


        @include('admin.components.topbar')


        <div class="header">
            <div class="container-fluid">


                @hasSection('title')

                <div class="header-body">
                    <div class="row align-items-end">
                        <div class="col">

                            <h1 class="header-title">
                                @yield('title')
                            </h1>

                        </div>
                    </div>
                </div>
                @endif

            </div>
        </div>


        <div class="container-fluid">

            @include('admin.components.alert')

            @yield('content')
        </div>



        <script src="/admin-assets/libs/jquery/dist/jquery.min.js"></script>
        <script src="/admin-assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
        <script src="/assets/libs/intl-tel-input-17.0.0/build/js/intlTelInput.js"></script>

        {{-- <script src="/admin-assets/libs/%40shopify/draggable/lib/es5/draggable.bundle.legacy.js"></script> --}}
        {{-- <script src="/admin-assets/libs/autosize/dist/autosize.min.js"></script> --}}
        {{-- <script src="/admin-assets/libs/chart.js/dist/Chart.min.js"></script> --}}
        {{-- <script src="/admin-assets/libs/dropzone/dist/min/dropzone.min.js"></script> --}}
        {{-- <script src="/admin-assets/libs/flatpickr/dist/flatpickr.min.js"></script> --}}
        {{-- <script src="/admin-assets/libs/highlightjs/highlight.pack.min.js"></script> --}}
        {{-- <script src="/admin-assets/libs/jquery-mask-plugin/dist/jquery.mask.min.js"></script> --}}
        {{-- <script src="/admin-assets/libs/list.js/dist/list.min.js"></script> --}}
        {{-- <script src="/admin-assets/libs/quill/dist/quill.min.js"></script> --}}
        <script src="/admin-assets/libs/select2/dist/js/select2.min.js"></script>
        {{-- <script src="/admin-assets/libs/chart.js/Chart.extension.min.js"></script> --}}


        {{-- <script src='../api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.js'></script> --}}

        <script src="/admin-assets/js/theme.min.js"></script>
        <script src="/admin-assets/js/countries.js"></script>
        <script>
            var input = document.querySelector("#phone");
            if (input) {
                window.intlTelInput(input);
            }
            var input2 = document.querySelector("#phone2");
            if (input2) {
                window.intlTelInput(input2);
            }
        </script>

        <script language="javascript">
            populateCountries("country", "state"); 
            // first parameter is id of country drop-down and second parameter is id of state drop-down
            populateCountries("country2");
            populateCountries("country2");
        </script>

        @yield('scripts')
</body>
</html>
