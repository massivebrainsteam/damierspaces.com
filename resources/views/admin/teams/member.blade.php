@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
    {{$user->team->name}} -> {{$user->name}}
    <a href="{{url('admin/team/' .$user->team->id)}}" class="btn ml-auto btn-outline-primary"><i class="fe fe-chevron-left"></i>  Back to Team Details</a>
</div>
@endsection
{{-- @php
    var_dump($user)
@endphp --}}

@section('content')
<div class="text-center">
    <div class="mb-4">
        <img src="/" class="bg-primary mb-3 rounded-circle" height="150" width="150">
        <h2 class="mb-1">{{$user->name}}</h2>
        <p class="mb-1">Team Member</p>
        <td>{{_badge('Active')}}</td>
    </div>
</div>
<div class="card mb-5 col-12">
    <div class="card-body ">
        <div class="row">

            <div class="col ml-n2 ">
                <h2 class="card-title d-flex mb-1">
                    USER DETAILS

                    <a href="{{url('admin/users/user/' .$user->id)}}" class="btn-sm ml-auto btn btn-outline-primary"><i class="fe fe-edit"></i> EDIT</a>
                </h2>

                <div class="card-text  pt-3 mt-3 border-top">
                    <h5>About User</h5>
                    <p class="mb-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
                </div>
                <hr>
                <div class="row ">
                    <div class="col-lg-6">
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>Account Created</h4>
                            <p>{{$user->created_at}}</p>
                        </div>
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>Email Address</h4>
                            <p class="text-primary">{{$user->email}}</p>
                        </div>
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>Phone Number</h4>
                            <p class="text-primary">{{$user->phone}}</p>
                        </div>
                        <div class="border-bottom d-flex pt-3 align-items-center justify-content-between">
                            <h4>Sex</h4>
                            <p class="text-primary">Male</p>
                        </div>
                        <div class="d-flex pt-3 align-items-center justify-content-between">
                            <h4>Subscription Status</h4>
                            <p class="text-primary">Mini Package Expiring Jan 11, 2021</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>All Check-Ins</h4>
                            <p>5</p>
                        </div>
                        <div class="d-flex pt-3 border-bottom align-items-center justify-content-between">
                            <h4>All Reservations</h4>
                            <p>5</p>
                        </div>
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>All Reviews</h4>
                            <p>5</p>
                        </div>
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>Role (Team) or Occupation (Personal)</h4>
                            <p>Graphic Designer</p>
                        </div>
                        <div class=" d-flex pt-3 align-items-center justify-content-between">
                            <h4>Account Status</h4>
                            <p class="mb-0">Active</p>
                        </div>

                    </div>
                </div>



            </div>



        </div>
    </div>
</div>
<br><br><br>

@endsection
