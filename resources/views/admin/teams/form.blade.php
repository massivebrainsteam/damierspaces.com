@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
    {{$team->id > 0 ? 'Update '.$team->name : 'Add New Team'}}
    <a href="{{url('admin/team/1')}}" class="ml-auto btn btn-sm btn-outline-primary"><i class="fe fe-chevron-left"></i> Back to Team Details </a>
</div>
@endsection
@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/libs/intl-tel-input-17.0.0/build/css/intlTelInput.css')}}">
@endsection

@section('content')

<div class="col-8 offset-2">

    <form class="mb-4" method="post" action="{{url('admin/user')}}">

        @csrf

        <div class="form-group">
            <label>Name</label>
            <input name="name" type="text" class="form-control" value="{{old('name', $user->name)}}" required>
            @error('name')
            <div class="text-danger" role="alert">{{ $errors->first('name') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Email</label>
            <input name="email" type="email" class="form-control" value="{{old('email', $user->email)}}" required>
            @error('email')
            <div class="text-danger" role="alert">{{ $errors->first('email') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Phone</label>
            <input name="phone" id="phone" type="number" class="form-control" value="{{old('phone', $user->phone)}}" required>
            @error('phone')
            <div class="text-danger" role="alert">{{ $errors->first('phone') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Address</label>
            <input name="address" type="text" class="form-control" value="{{old('address', $user->address)}}" required>
            @error('address')
            <div class="text-danger" role="alert">{{ $errors->first('address') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Country</label>
            <input name="country" type="text" class="form-control" value="{{old('country', $user->country)}}" required>
            @error('country')
            <div class="text-danger" role="alert">{{ $errors->first('country') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>State</label>
            <input name="state" type="text" class="form-control" value="{{old('state', $user->state)}}" required>
            @error('state')
            <div class="text-danger" role="alert">{{ $errors->first('state') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>City</label>
            <input name="city" type="text" class="form-control" value="{{old('city', $user->city)}}" required>
            @error('city')
            <div class="text-danger" role="alert">{{ $errors->first('city') }}</div>
            @enderror
        </div>

        @if($user->id < 1) <div class="form-group">
            <label>Password</label>
            <div class="alert alert-light">User will get an email of a generated password.</div>
</div>

@endif

<div class="form-group">
    <label>Status</label>
    <select name="status" class="form-control" required>

        @foreach(['active', 'inactive'] as $row)
        <option value="{{$row}}" {{old('status', $user->status) == $row ? 'selected' : ''}}>
            {{ucfirst($row)}}
        </option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Sex</label>
    <select name="sex" class="form-control" required>

        @foreach(['male', 'female'] as $row)
        <option value="{{$row}}" {{old('sex', $user->sex) == $row ? 'selected' : ''}}>
            {{ucfirst($row)}}
        </option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <label>Role</label>
    <input name="occupation" type="text" class="form-control" value="{{old('occupation', $user->occupation)}}" required>
    @error('occupation')
    <div class="text-danger" role="alert">{{ $errors->first('occupation') }}</div>
    @enderror
</div>

<input type="hidden" name="id" value="{{$user->id}}">
<input type="hidden" name="type" value="{{request('type', 'user')}}">

<button type="submit" class="btn btn-block btn-primary">
    Save User
</button>

</form>
</div>

@endsection
