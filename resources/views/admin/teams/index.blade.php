@extends('admin.layouts.app')

@section('title')
Teams
@endsection

@section('content')

<div class="card">
  @include('admin.components.search')
  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Team Admin</th>
          <th>Email</th>
          <th>Users</th>
          <th>Package</th>
          <th>Subscription End Date</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach($teams as $row)

        @php
          $admin = $row->users()->whereType('team_admin')->first();
        @endphp
        <tr>
          <td><a href="/admin/team/{{$row->id}}">{{$row->name}}</a></td>
          <td>{{$admin->name}}</td>
          <td>{{$row->email}}</td>
          <td>{{$row->users->count()}}</td>
          <td>{{$admin ? optional($admin->subscription->package)->name : ''}}</td>
          <td>{{$admin ? _date(optional($admin->subscription)->end_at) : ''}}</td>
          <td>{{$admin ? _badge($row->status) : ''}}</td>
        </tr>
        @endforeach

        @if($teams->count() < 1)
        <tr>
          <td colspan="6" class="text-center text-muted">When new teams sign up. You will see them here.</td>
        </tr>
        @endif
      </tbody>
    </table>

  </div>

  <div class="mt-3 float-right">
    {{$teams->links() }}
  </div>
</div>

@endsection