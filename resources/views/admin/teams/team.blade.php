@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
Team: {{$team->name}}
<a href="/admin/teams" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to Team List</a>
</div>
<a href="/admin/team/{{$team->id}}?status=true" class="btn btn-dark btn-sm" onclick="return confirm('Are you sure?')">{{$team->status == 'active' ? 'Deactive' : 'Activate'}} Team</a>
@endsection

@section('content')
<div class="text-center">
    <div class="mb-4">
        <img src="/"   class="bg-primary mb-3 rounded-circle" height="150" width="150">
        <h2 class="mb-1">{{$team->name}}</h2>
        <p class="mb-1">Team Account</p>
        <td>{{_badge('Active')}}</td>
    </div>
</div>
<div class="card mb-5 col-12">
    <div class="card-body ">
        <div class="row">

            <div class="col ml-n2 ">
                <h2 class="card-title d-flex mb-1">
                    TEAM DETAILS

                    <a href="{{url('admin/teams/'.$team)}}" class="btn-sm ml-auto btn btn-outline-primary"><i class="fe fe-edit"></i> EDIT</a>
                </h2>

                <div class="card-text  pt-3 mt-3 border-top">
                    <h5>About User</h5>
                    <p class="mb-1">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
                </div>
                <hr>
                <div class="row ">
                    <div class="col-lg-6">
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>Account Created</h4>
                            <p>July 01, 2020</p>
                        </div>
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>Email Address</h4>
                            <p class="text-primary">innovativehub@gmail.com</p>
                        </div>
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>Phone Number</h4>
                            <p class="text-primary">08098765432</p>
                        </div>
                        <div class="d-flex pt-3 align-items-center justify-content-between">
                            <h4>All Members</h4>
                            <p class="text-primary">12</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>All Check-Ins</h4>
                            <p>5</p>
                        </div>
                        <div class="d-flex pt-3 border-bottom align-items-center justify-content-between">
                            <h4>All Reservations</h4>
                            <p>5</p>
                        </div>
                        <div class="border-bottom pt-3 d-flex align-items-center justify-content-between">
                            <h4>All Reviews</h4>
                            <p>5</p>
                        </div>
                        <div class=" d-flex pt-3 align-items-center justify-content-between">
                            <h4>Subscription Status</h4>
                            <p class="mb-0">{{_badge("ACTIVE - PACKAGE NAME")}}</p>
                        </div>

                    </div>
                </div>



            </div>



        </div>
    </div>
</div>

<h2>MEMBERS LIST</h2>
<div class="card">
    <div class="table-responsive mb-0">
        <table class="table table-sm table-nowrap card-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Role</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Slots</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($team->users as $row)

                <tr>
                    <td><a href="{{url('admin/user/'.$row->id)}}">{{$row->name}}</a></td>
                    <td>{{_badge($row->type)}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->phone}}</td>
                    <td>{{$row->slots}}</td>
                    <td>{{_badge($row->status)}}</td>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>

</div>
<br><br>

@endsection
