<!doctype html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/admin-assets/fonts/feather/feather.min.css">
    <link rel="stylesheet" href="/admin-assets/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="/admin-assets/css/theme.min.css" id="stylesheetLight">
    <link rel="stylesheet" href="/admin-assets/css/theme-dark.min.css" id="stylesheetDark">
    <style>
        body {
            display: none;
        }

    </style>
    <title>Damier Spaces Admin Login</title>
</head>

<body class="d-flex align-items-center bg-auth border-top border-top-2 border-primary">

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5 col-xl-4 my-5">

                <a class="mb-5 d-flex align-items-center justify-content-center" href="/">
                    <img src="/assets/img/brand/dark.svg" id="navbar-logo">
                </a>

                <h1 class="display-4 text-center mb-3">
                    Sign in
                </h1>

                <p class="text-muted text-center mb-5">
                    Login to your admin account
                </p>


                <form action="/auth/login" method="POST">

                    @csrf

                    @include('site.components._alert')

                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" name="email" class="form-control" placeholder="name@address.com" required>
                    </div>

                    <div class="form-group">

                        <div class="row">
                            <div class="col">

                                <label>Password</label>

                            </div>
                            <div class="col-auto">

                                <a href="#" class="form-text small text-muted">
                                    Forgot password?
                                </a>

                            </div>
                        </div>


                        <div class="input-group input-group-merge">

                            <input type="password" name="password" id="password" class="form-control form-control-appended" placeholder="Enter your password" required>


                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fe fe-eye togglePassword"></i>
                                </span>
                            </div>

                        </div>
                    </div>

                    <input type="hidden" name="type" value="admin">

                    <button class="btn btn-lg btn-block btn-primary mb-3">
                        Sign in
                    </button>

                </form>

            </div>
        </div>
    </div>


    <script>
        const toggle = document.querySelector('.togglePassword');
        const password = document.querySelector('#password');

        toggle.addEventListener('click', function(e) {
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            this.classList.toggle("fa-eye-splash");
            //this.classList.remove("fa-eye")
            //this.classList.add("fa-eye-splash")
        })
    </script>
    <script src="/admin-assets/js/theme.min.js"></script>

</body>
</html>
