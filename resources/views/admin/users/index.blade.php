@extends('admin.layouts.app')

@section('content')

<div class="header">
    <div class="header-body">
        <div class="row align-items-center">
            <div class="col">
                <h1 class="header-title">{{ucwords($title)}}</h1>
            </div>
        </div>
        <div class="row align-items-center">
            <div class="col">
                <ul class="nav nav-tabs nav-overflow header-tabs">
                    <li class="nav-item">
                        <a href="{{url('admin/users')}}" class="nav-link active">
                            All <span class="badge badge-pill badge-soft-secondary">{{\App\User::whereType($type)->count()}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('admin/users?q=active')}}" class="nav-link">
                            Active <span class="badge badge-pill badge-soft-secondary">{{\App\User::whereType($type)->whereStatus('active')->count()}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('admin/users?q=inactive')}}" class="nav-link">
                            Inactive <span class="badge badge-pill badge-soft-secondary">{{\App\User::whereType($type)->whereStatus('inactive')->count()}}</span>
                        </a>
                    </li>

                </ul>

            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h4 class="card-header-title">
            <a class="btn btn-outline-primary" href="{{url('admin/users/user/0')}}"><i class="fe fe-plus-circle"></i> Add New</a>
        </h4>
    </div>

    <div class="col mt-4">

        <form action="{{url('admin/users')}}" method="GET">

            <div class="input-group input-group-lg input-group-merge">
                <input type="text" name="q" class="form-control form-control-prepended search" placeholder="Search Users" required>
                <div class="input-group-prepend">
                    <div class="input-group-text">
                        <span class="fe fe-search"></span>
                    </div>
                </div>
            </div>

            @if(request('q'))
            <div class="alert alert-light mt-3">Your search returned {{$users->count()}} {{str_plural('Result', $users->count())}}.</div>
            @endif

        </form>

    </div>

    <div class="table-responsive mt-3">
        <table class="table table-sm table-nowrap card-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th>Subscription</th>
                    {{-- @if($row->type !== 'admin')
                    @endif --}}
                    <th>Date Joined</th>
                </tr>
            </thead>
            <tbody>

                @foreach($users as $row)
                <tr>
                    <td><a href="{{url('admin/user/'.$row->id)}}">{{$row->name}}</a></td>
                    <td><a href="mailto:{{$row->email}}">{{$row->email}}</a></td>
                    <td><a href="tel:{{$row->phone}}">{{$row->phone}}</a></td>
                    <td>{{_badge($row->status)}}</td>
                    @if($row->type !== 'admin')
                    <td>
                        @if($row->subscription->package->name)
                        {{$row->subscription->package->name}}. <small>Expiring {{_date($row->subscription->end_at)}}</small>
                        @else
                        <small> No active subcription</small>
                        @endif
                    </td>
                    <td>{{_date($row->created_at)}}</td>
                </tr>
                @endif
                @endforeach

                @if($users->count() < 1) <tr>
                    <td colspan="6" class="text-center text-muted">When you create a user, or someone registers on Damier Spaces, you will see them here.</td>
                    </tr>
                    @endif

            </tbody>
        </table>
    </div>

    <div class="mt-3 float-right">
        {{$users->links() }}
    </div>

</div>

@endsection
