@extends('admin.layouts.app')

@section('title')
Reviews
@endsection

@section('content')

<div class="card">
  @include('admin.components.search')
  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>User</th>
          <th>Space</th>
          <th>Review Date</th>
          <th>Review</th>
        </tr>
      </thead>
      <tbody>
        @foreach($reviews as $row)
        <tr>
          <td>{{$row->user->name}}</td>
          <td><a href="{{url('admin/space/'.$row->space_id)}}" target="_blank">{{$row->space->name}}</a></td>
          <td>{{_date($row->created_at)}}</td>
          <td><small>{{$row->review}}</small></td>
        </tr>
        @endforeach

        @if($reviews->count() < 1)
        <tr>
          <td colspan="4" class="text-center text-muted">When users create reviews, those reviews will show up here.</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>

  <div class="mt-3 float-right">
    {{$reviews->links() }}
  </div>

</div>

@endsection