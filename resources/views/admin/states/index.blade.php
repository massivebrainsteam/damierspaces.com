@extends('admin.layouts.app')

@section('title')
All States
@endsection

@section('content')
<div class="card">
    @include('admin.components.search')
    <div class="card-header">
        <h4 class="card-header-title">
            <a href="{{url('admin/states/0')}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-plus-circle"></i> Add new State</a>
        </h4>
    </div>

    <div class="table-responsive mb-0">
        <table class="table table-sm table-nowrap card-table">
            <thead>
                <tr>
                    <th>State Name</th>
                    <th>ALL CITIES</th>
                    <th>STATUS</th>
                    <th>Last Modified</th>
                    <th>ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $row)
                <tr>
                    <td><a href="{{url('admin/states/'.$row->id)}}">Abia</a></td>
                    <td>Lagos</td>
                    <td>{{_badge($row->status)}}</td>
                    <td>Sep 20, 2020</td>
                    <td>
                        <div class="d-">
                            <a href="{{url('admin/city')}}" class="btn btn-primary btn-sm">MANAGE CITIES</a>
                        </div>
                    </td>
                </tr>
                @endforeach

                @if($categories->count() < 1) <tr>
                    <td colspan="4" class="text-center text-muted">When you create States, you will see them here.</td>
                    </tr>
                    @endif
            </tbody>
        </table>

    </div>

    <div class="mt-3 float-right">
        {{$categories->links() }}
    </div>
</div>

@endsection
