@extends('admin.layouts.app')


@section('title')
<div class="d-flex">
    {{'Update Ikeja -> Lagos'}}
    <a href="/admin/states" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to All Cities</a>
</div>
@endsection

@section('content')

<div class="col-8 offset-2">

    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $row)
            <li>{{$row}}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form class="mb-4" action="{{url('admin/host')}}" method="POST" enctype="multipart/form-data">

        @csrf

        <h3>STATE DETAILS</h3>
        <hr>

        <div class="form-group">
            <label>City Name</label>
            <input name="name" value="" type="text" class="form-control" required>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
             @enderror
        </div>


        <div class="form-group">
            <label class="mb-1">Status</label>
            <select name="status" class="form-control">
                <option>Active </option>
                <option>Inactive </option>
            </select>
            {{-- @error('status')
            <div class="invalid-feedback" role="alert">{{ $errors->first('status') }}</div>
            @enderror --}}
        </div>

        <button type="submit" class="btn btn-block btn-primary">
            Save
        </button>

    </form>

</div>

@endsection
