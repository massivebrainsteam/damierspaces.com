@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
    Lagos > Cities
    <a href="/admin/states" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to All States</a>
</div>

@endsection

@section('content')
<div class="card">
    @include('admin.components.search')
    <div class="card-header">
        <h4 class="card-header-title">
            <a href="{{url('admin/cityform/0')}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-plus-circle"></i> Add new City</a>
        </h4>
    </div>

    <div class="table-responsive mb-0">
        <table class="table table-sm table-nowrap card-table">
            <thead>
                <tr>
                    <th>State Name</th>
                    <th>ALL CITIES</th>
                    <th>STATUS</th>
                    <th>Last Modified</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $row)
                <tr>
                    <td><a href="{{url('admin/category/'.$row->id)}}">Abia</a></td>
                    <td>Lagos</td>
                    <td>{{_badge($row->status)}}</td>
                    <td>Sep 20, 2020</td>
                </tr>
                @endforeach

            </tbody>
        </table>

    </div>

    <div class="mt-3 float-right">
        {{$categories->links() }}
    </div>
</div>

@endsection
