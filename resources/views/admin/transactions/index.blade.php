@extends('admin.layouts.app')

@section('title')
Transactions
@endsection

@section('content')

<div class="card">
  
  @include('admin.components.search')

  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>User</th>
          <th>Payment Reference</th>
          <th>Amount</th>
          <th>Transaction Reference</th>
          <th>Transaction Date</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach($transactions as $row)
        <tr>
          <td>{{$row->user->name}}</td>
          <td>{{$row->payment_reference}}</td>
          <td>{{_currency($row->amount)}}</td>
          <td>{{$row->transaction_reference}}</td>
          <td>{{_date($row->transaction_date)}}</td>
          <td>{{_badge($row->status)}}</td>
        </tr>
        @endforeach

        @if($transactions->count() < 1)
        <tr>
          <td colspan="6" class="text-center text-muted">Transactions will show up here.</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>

  <div class="mt-3 float-right">
    {{$transactions->links() }}
  </div>

</div>

@endsection