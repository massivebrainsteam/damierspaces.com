@extends('admin.layouts.app')

@section('content')

<div class="pt-7 pb-8 bg-dark bg-ellipses">
    <div class="row justify-content-center">
        <div class="col-md-10 col-lg-8 col-xl-12">

            <h1 class="display-3 text-center text-white">
                Damier Spaces Packages
            </h1>

            <div class="text-center">
                <a href="{{url('admin/package/0/basic-info')}}" class="btn btn-primary"><i class="fe fe-plus-circle"></i> Add New Package</a>
            </div>

            <ul class="nav nav-tabs justify-content-center display-5 mt-5 pb-3" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="text-white active" id="personal-tab" data-toggle="tab" href="#personal" role="tab" aria-controls="personal" aria-selected="true">Personal (10)</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="text-white" id="team-tab" data-toggle="tab" href="#team" role="tab" aria-controls="team" aria-selected="false">Team (20)</a>
                </li>

            </ul>

            {{-- <p>
                We have plans and prices that fit your business perfectly. Make your client site a success with our products.
            </p> --}}

        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="personal" role="tabpanel" aria-labelledby="nav-personal-tab">
            <div class="row mt-n7">

                @foreach($packages as $row)

                <div class="col-12 col-lg-4">


                    <div class="card">
                        <div class="card-body">


                            <h3 class="text-uppercase text-center text-muted my-4">{{$row->name}}</h3>


                            <div class="row no-gutters align-items-center justify-content-center">
                                <div class="col-auto">
                                    <div class="h2 mb-0">N</div>
                                </div>
                                <div class="col-auto">
                                    <div class="display-2 mb-0">{{number_format($row->amount)}}</div>
                                </div>
                            </div>


                            <div class="h3 text-uppercase text-center text-muted mb-5">
                                / {{_duration_description($row->duration)}}
                                <div class="mt-3">{{_badge($row->status)}}</div>
                            </div>

                            <div class="mb-3">
                                <ul class="list-group list-group-flush">

                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Slots</small> <small>{{$row->total_slots}}</small>
                                    </li>

                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Reservation</small> <small>{{ucwords($row->can_reserve_spaces)}}</small>
                                    </li>

                                    {{-- <li class="list-group-item d-flex align-items-center justify-content-between px-0">
								<small>Amenities</small> <small>{{$row->amenities()->count()}}</small>
                                    </li>

                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Monthly Reservations</small> <small>{{$row->monthly_reservations}}</small>
                                    </li>

                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Reservation Duration</small> <small>{{$row->reservation_duration}} Mins</small>
                                    </li>

                                    @if($row->type == 'team')
                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Maximum Team Users</small> <small>{{$row->max_team_users}}</small>
                                    </li>
                                    @endif --}}

                                </ul>
                            </div>


                            <a href="{{url('admin/package/'.$row->id.'/basic-info')}}" class="btn btn-block btn-primary">
                                CONFIGURE
                            </a>

                        </div>
                    </div>

                </div>
                @endforeach

            </div>

        </div>
        <div class="tab-pane fade" id="team" role="tabpanel" aria-labelledby="nav-team-tab">
            <div class="row mt-n7">

                @foreach($packages as $row)

                <div class="col-12 col-lg-4">

                    <div class="card">
                        <div class="card-body">


                            <h3 class="text-uppercase text-center text-muted my-4">{{$row->name}}</h3>


                            <div class="row no-gutters align-items-center justify-content-center">
                                <div class="col-auto">
                                    <div class="h2 mb-0">N</div>
                                </div>
                                <div class="col-auto">
                                    <div class="display-2 mb-0">{{number_format($row->amount)}}</div>
                                </div>
                            </div>


                            <div class="h3 text-uppercase text-center text-muted mb-5">
                                / {{_duration_description($row->duration)}}
                                <div class="mt-3">{{_badge($row->status)}}</div>
                            </div>

                            <div class="mb-3">
                                <ul class="list-group list-group-flush">

                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Slots</small> <small>{{$row->total_slots}}</small>
                                    </li>

                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Reservation</small> <small>{{ucwords($row->can_reserve_spaces)}}</small>
                                    </li>

                                    {{-- <li class="list-group-item d-flex align-items-center justify-content-between px-0">
								<small>Amenities</small> <small>{{$row->amenities()->count()}}</small>
                                    </li>

                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Monthly Reservations</small> <small>{{$row->monthly_reservations}}</small>
                                    </li>

                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Reservation Duration</small> <small>{{$row->reservation_duration}} Mins</small>
                                    </li>

                                    @if($row->type == 'team')
                                    <li class="list-group-item d-flex align-items-center justify-content-between px-0">
                                        <small>Maximum Team Users</small> <small>{{$row->max_team_users}}</small>
                                    </li>
                                    @endif --}}

                                </ul>
                            </div>


                            <a href="{{url('admin/package/'.$row->id.'/basic-info')}}" class="btn btn-block btn-primary">
                                CONFIGURE
                            </a>

                        </div>
                    </div>

                </div>
                @endforeach

            </div>
        </div>

    </div>



</div>

@endsection
