@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
{{$package->id > 0 ? 'Update '.$package->name : 'Add New Package'}}  - Other Settings
<a href="{{url('admin/packages/$package->id')}}" class="ml-auto btn btn-sm btn-outline-primary"><i class="fe fe-chevron-left"></i> Back to Basic Settings </a>
</div>
@endsection

@section('content')

<div class="col-8 offset-2">        

    <form class="mb-4" method="post" action="{{url('admin/package/'.$package->id.'/other-settings')}}">

        @csrf

        <h3>OTHER SETTINGS</h3>
        <hr>

        <div class="row">

            <div class="col-12 col-md-12">
                <div class="form-group">
                    <label>Package Rating</label>
                    <select name="rating_id" class="form-control" required>
                        @foreach(\App\Rating::get() as $row)
                        <option value="{{$row->id}}" {{$row->id == $package->rating_id ? 'selected' : ''}}>{{$row->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-12 col-md-12">
                <div class="form-group">
                    <label>Slots per month</label>
                    <input name="slots" type="number" class="form-control" value="{{$package->slots}}" required>
                </div>
                @error('slots')
                <div class="invalid-feedback" role="alert">{{ $errors->first('slots') }}</div>
                @enderror
            </div>

            <div class="col-12 col-md-12">
                <div class="form-group">
                    <label>Can users who choose this Package reserve spaces before confirmation?</label>
                    <select name="can_reserve_spaces" class="form-control" required>
                        <option value="yes" {{$package->can_reserve_spaces == 'yes' ? 'selected' : ''}}>YES</option>
                        <option value="no" {{$package->can_reserve_spaces == 'no' ? 'selected' : ''}}>NO</option>
                    </select>
                </div>
            </div>

            <div class="col-12 col-md-12">
                <div class="form-group">
                    <label>If users can reserve spaces, What is the duration in <strong>minutes</strong> for a reservation?</label>
                    <input name="reservation_duration" type="number" class="form-control" value="{{$package->reservation_duration}}">
                </div>
            </div>

            <div class="col-12 col-md-12">
                <div class="form-group">
                    <label>If users can reserve spaces, How many times within the month?</label>
                    <input name="monthly_reservations" type="number" class="form-control" value="{{$package->monthly_reservations}}">
                </div>
            </div>

            @if($package->type == 'team')
            <div class="col-12 col-md-12">
                <div class="form-group">
                    <label>What is the Maximum number of Users allowed in team?</label>
                    <input name="max_team_users" type="number" class="form-control" value="{{$package->max_team_users}}">
                </div>
                @error('max_team_users')
                <div class="invalid-feedback" role="alert">{{ $errors->first('max_team_users') }}</div>
                @enderror
            </div>
            @endif

        </div>

        <input type="hidden" name="id" value="{{$package->id}}">

        <button type="submit" class="btn btn-block btn-primary">
            Complete Package Configuration
        </button>

    </form>
</div>
<br><br>

@endsection