@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
{{$package->id > 0 ? 'Update '.$package->name : 'Add New Package'}}  - Basic Info
<a href="{{url('admin/packages')}}" class="ml-auto btn btn-sm btn-outline-primary"><i class="fe fe-chevron-left"></i> Back to All Packages </a>
</div>
@endsection

@section('content')

<div class="col-8 offset-2">        

    <form class="mb-4" method="post" action="{{url('admin/package/'.$package->id.'/basic-info')}}">

        @csrf

        <h3>PACKAGE DETAILS</h3>
        <hr>

        <div class="row">

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>Name</label>
                    <input name="name" type="text" class="form-control" value="{{old('name', $package->name)}}" placeholder="Package Name" required>
                    @error('name')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>Type</label>
                    <select name="type" class="form-control" required>
                        <option value="user" {{$package->type == 'user' ? 'selected' : ''}}>Individual</option>
                        <option value="team" {{$package->type == 'team' ? 'selected' : ''}}>Team</option>
                    </select>
                    @error('type')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('type') }}</div>
                    @enderror
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>Order</label>
                    <input name="order" type="number" class="form-control" value="{{old('order', $package->order)}}" placeholder="Order 1 means the 1st Package" required>
                    @error('order')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('order') }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>Duration</label>
                    <select name="duration" class="form-control" required>
                        @foreach(['monthly'] as $row)
                        <option value="{{$row}}" {{$row == old('duration', $package->duration) ? 'selected' : ''}}>
                            {{ucfirst($row)}}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-6">
                <div class="form-group">
                    <label>Price</label>
                    <input name="amount" type="number" class="form-control" value="{{old('amount', $package->amount)}}" required>
                    @error('amount')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('amount') }}</div>
                    @enderror
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control" required>

                        @foreach(['active', 'inactive'] as $row)
                        <option value="{{$row}}" {{old('status', $package->status) == $row ? 'selected' : ''}}>
                            {{ucfirst($row)}}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>

        </div> 

        <input type="hidden" name="id" value="{{$package->id}}">

        <button type="submit" class="btn btn-block btn-primary">
            Continue
        </button>

    </form>
</div>
<br><br>

@endsection