@extends('admin.layouts.app')


@section('title')
<div class="d-flex">
    {{'Add a New Filter'}}
    <a href="/admin/pricing" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to All Filters</a>
</div>
@endsection

@section('content')

<div class="col-8 offset-2">

    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $row)
            <li>{{$row}}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form class="mb-4" action="{{url('admin/host')}}" method="POST" enctype="multipart/form-data">

        @csrf

        <div class="form-group">
            <label>Name:</label>
            <input name="name" value="" type="text" class="form-control" required>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
             @enderror
        </div>

        <div class="form-group">
            <label>User Group:</label>
            <select name="status" class="form-control">
                <option>Personal </option>
                <option>Team </option>
            </select>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
             @enderror
        </div>

        <div class="form-group">
            <label>Minimum Price:</label>
            <input name="name" value="" type="text" class="form-control" required>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
             @enderror
        </div>

        <div class="form-group">
            <label>Maximum Price:</label>
            <input name="name" value="" type="text" class="form-control" required>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
             @enderror
        </div>

        <div class="form-group">
            <label>Icon URL:</label><br>
            <img src="" class="img img-thumbnail mt-2" style="width:50px">
            <input name="icon" type="file" class="form-control" required>
            @error('icon')
            <div class="invalid-feedback" role="alert">{{ $errors->first('icon') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label class="mb-1">Status</label>
            <select name="status" class="form-control">
                <option>Active </option>
                <option>Inactive </option>
            </select>
            {{-- @error('status')
            <div class="invalid-feedback" role="alert">{{ $errors->first('status') }}</div>
            @enderror --}}
        </div>

        <button type="submit" class="btn btn-block btn-primary">
            Save
        </button>

    </form>

</div>

@endsection
