@extends('admin.layouts.app')

@section('title')
Pricing Filters
@endsection

@section('content')
<div class="card">
    @include('admin.components.search')
    <div class="card-header">
        <h4 class="card-header-title">
            <a href="{{url('admin/pricing/0')}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-plus-circle"></i> Add new</a>
        </h4>
    </div>

    <div class="table-responsive mb-0">
        <table class="table table-sm table-nowrap card-table">
            <thead>
                <tr>
                    <th>NAME</th>
                    <th>USER GROUP</th>
                    <th>PRICE RANGE</th>
                    <th>STATUS</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $row)
                <tr>
                    <td><a href="{{url('admin/pricing/'.$row->id)}}">Oga Boss</a></td>
                    <td>User</td>
                    <td>5000 - 10000</td>
                    <td>{{_badge($row->status)}}</td>
                </tr>
                @endforeach

                @if($categories->count() < 1) <tr>
                    <td colspan="4" class="text-center text-muted">When you create Pricing Filters, you will see them here.</td>
                    </tr>
                    @endif
            </tbody>
        </table>

    </div>

    <div class="mt-3 float-right">
        {{$categories->links() }}
    </div>
</div>

@endsection
