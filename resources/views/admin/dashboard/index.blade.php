@extends('admin.layouts.app')

@section('content')

<div class="row">

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Total Users
                        </h6>
                        <span class="h2 mb-0">{{$users}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Active Users
                        </h6>
                        <span class="h2 mb-0">{{$active_users}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Inactive Users
                        </h6>
                        <span class="h2 mb-0">{{$users - $active_users}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Total Hosts
                        </h6>
                        <span class="h2 mb-0">{{$hosts}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Active Hosts
                        </h6>
                        <span class="h2 mb-0">{{$active_hosts}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Inactive Hosts
                        </h6>
                        <span class="h2 mb-0">{{$hosts - $active_hosts}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Total Teams
                        </h6>
                        <span class="h2 mb-0">{{$teams}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Active Teams
                        </h6>
                        <span class="h2 mb-0">{{$active_teams}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Inactive Teams
                        </h6>
                        <span class="h2 mb-0">{{$teams - $active_teams}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Attendances
                        </h6>
                        <span class="h2 mb-0">{{$attendances_count}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Reservations
                        </h6>
                        <span class="h2 mb-0">{{$reservations_count}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>

    <div class="col-12 col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="card-title text-uppercase text-muted mb-2">
                            Reviews
                        </h6>
                        <span class="h2 mb-0">{{$reviews_count}}</span>
                    </div>
                    <div class="col-auto">
                        <span class="h2 fe fe-users text-muted mb-0"></span>
                    </div>
                </div> 

            </div>
        </div>
    </div>


</div>

<div class="row">

    <div class="col-12 col-xl-12">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col">
                        <h4 class="card-header-title">
                         Spaces pending approval
                     </h4>
                 </div>
             </div> 

         </div>
         <div class="card-body">

            <div class="table-responsive mb-0">
                <table class="table table-sm table-nowrap card-table">
                    <thead>
                        <tr>
                            <th>Host</th>
                            <th>Location</th>
                            <th>Space</th>
                            <th>Capacity</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($spaces as $row)
                        <tr>
                            <td>{{$row->host->name}}</td>
                            <td>{{$row->location->name}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{$row->capacity}}</td>

                            <td><a href="/admin/space/{{$row->id}}">View Space</a></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>

<div class="col-12 col-xl-12">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col">
                        <h4 class="card-header-title">
                           Pending Settlements
                        </h4>
                    </div>
                </div> 

            </div>
            <div class="card-body">

                <div class="table-responsive mb-0">
                    <table class="table table-sm table-nowrap card-table">
                        <thead>
                            <tr>
                                <th>Host</th>
                                <th>SETTLEMENT AMOUNT</th>
                                <th>PENDING SINCE</th>
                                <th>ATTENDANCES</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Invention Hub</td>
                                <td>₦ 5,000.00</td>
                                <td>Jan 09, 2021</td>
                                <td>5</td>
                                <td><a href="#" class="btn btn-sm btn-primary">PAY</a></td>
                            </tr>
                            

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-12 col-xl-12">
        <div class="card">
            <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col">
                            <h4 class="card-header-title">
                            Current Attendance
                        </h4>
                    </div>
                </div> 
            </div>

            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Check Ins (10)</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Reservations (10)</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="table-responsive mb-0">
                        <table class="table table-sm table-nowrap card-table">
                            <thead>
                                <tr>
                                    <th>REFERENCE</th>
                                    <th>SPACE</th>
                                    <th>HOST</th>
                                    <th>USER</th>
                                    <th>CHECKIN AT</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>251502</td>
                                    <td>Up and Down Desk</td>
                                    <td>Invention Hub</td>
                                    <td>Olawale Jones	(Personal)</td>
                                    <td>Jan 09, 2021 1:33 PM</td>
                                </tr>
                                <tr>
                                    <td>251502</td>
                                    <td>Up and Down Desk</td>
                                    <td>Invention Hub</td>
                                    <td>Bosun Jones	(iFlux Team Member)</td>
                                    <td>Jan 09, 2021 1:33 PM</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <div class="table-responsive mb-0">
                        <table class="table table-sm table-nowrap card-table">
                            <thead>
                                <tr>
                                    <th>USER</th>
                                    <th>SPACE</th>
                                    <th>HOST</th>
                                    <th>DATE/TIME STARTED</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Bosun Jones (Personal)</td>
                                    <td>Dedicated Chair </td>
                                    <td>Workstation NG</td>
                                    <td>Jan 20, 2021 8:55 AM</td>
                                </tr>
                                <tr>
                                    <td>Olawale Jones (iFlux Team Member)</td>
                                    <td>Dedicated Chair </td>
                                    <td>Workstation NG</td>
                                    <td>Jan 20, 2021 8:55 AM</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="row">

    <div class="col-12 col-xl-12">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col">
                        <h4 class="card-header-title">
                         New Accounts
                     </h4>
                 </div>
             </div> 

         </div>
         <div class="card-body">

            <div class="table-responsive mb-0">
                <table class="table table-sm table-nowrap card-table">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Name</th>
                            <th>Date Registered</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($new_users as $row)
                        <tr>
                            <td>{{$row->type}}</td>
                            <td>{{$row->name}}</td>
                            <td>{{_date($row->created_at, true)}}</td>
                            <td>{{_badge($row->status)}}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>

</div>

<div class="row">

    <div class="col-12 col-xl-12">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col">
                        <h4 class="card-header-title">
                            Recently active attendances
                        </h4>
                    </div>
                </div> 

            </div>
            <div class="card-body">

                <div class="table-responsive mb-0">
                    <table class="table table-sm table-nowrap card-table">
                        <thead>
                            <tr>
                                <th>Reference</th>
                                <th>Space</th>
                                <th>Host</th>
                                <th>User</th>
                                <th>CheckIn At</th>
                                <th>CheckOut At</th>
                                <th>Time Used</th>
                                <th>Package</th>
                                <th>Settlement</th>
                                <th>Settlement Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($attendances as $row)
                            <tr>
                                <td>{{$row->reference}}</td>
                                <td>{{$row->space->name}}</td>
                                <td>{{$row->space->host->name}}</td>
                                <td>{{$row->user->name}}</td>
                                <td>{{_date($row->start_at, true)}}</td>
                                <td>{{_date($row->end_at, true)}}</td>
                                <td>{{$row->time_used}}</td>
                                <td>{{$row->user->subscription->package->name}}</td>
                                <td>{{_currency($row->settlement)}}</td>
                                <td>{{_badge($row->settlement_status)}}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>

</div>


@endsection