@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
    {{$amenity->id > 0 ? 'Update '.$amenity->name : 'Add New Amenity'}}
    <a href="/admin/amenities" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to All Amenities</a>
</div>
@endsection

@section('content')

<div class="col-8 offset-2">

    <form class="mb-4" method="post" action="{{url('admin/amenity')}}">

        @csrf

        <div class="form-group">
            <label>Name</label>
            <input name="name" type="text" class="form-control" value="{{old('name', $amenity->name)}}" required>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Category</label>
            <input name="category" type="text" class="form-control" value="{{old('category', $amenity->category)}}" required>
            @error('category')
            <div class="invalid-feedback" role="alert">{{ $errors->first('category') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Icon URL</label><br>
            <img src="{{$amenity->icon}}" class="img img-thumbnail mt-2" style="width:50px">
            <input name="icon" type="file" class="form-control" required>
            @error('icon')
            <div class="invalid-feedback" role="alert">{{ $errors->first('icon') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control" required>

                @foreach(['active', 'inactive'] as $row)
                <option value="{{$row}}" {{old('status', $amenity->status) == $row ? 'selected' : ''}}>
                    {{ucfirst($row)}}
                </option>
                @endforeach
            </select>
        </div>

        <input type="hidden" name="id" value="{{$amenity->id}}">

        <button type="submit" class="btn btn-block btn-primary">
            Save Amenity
        </button>

    </form>
</div>

@endsection
