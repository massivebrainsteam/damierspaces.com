@extends('admin.layouts.app')

@section('title')
Amenities
@endsection

@section('content')

<div class="card">
@include('admin.components.search')
  <div class="card-header">
    <h4 class="card-header-title">
      <a href="{{url('admin/amenity/0')}}"  class="ml-auto btn-outline-primary btn "><i class="fe fe-plus-circle"></i> Add new Amenity</a>
    </h4>
  </div>

  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Category</th>
          <th>Last Modified</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach($amenities as $row)
        <tr>
          <td><a href="{{url('admin/amenity/'.$row->id)}}">{{$row->name}}</a></td>
          <td>{{$row->category}}</td>
          <td>{{_date($row->updated_at)}}</td>
          <td>{{_badge($row->status)}}</td>
        </tr>
        @endforeach

        @if($amenities->count() < 1)
        <tr>
          <td colspan="4" class="text-center text-muted">When you create amenities, you will see them here.</td>
        </tr>
        @endif

      </tbody>
    </table>
  </div>

  <div class="mt-3 float-right">
    {{$amenities->links() }}
  </div>
  
</div>

@endsection