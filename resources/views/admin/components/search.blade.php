<div class="card-header">

    <form method="GET" action="">
      <div class="input-group input-group-flush">
        <div class="input-group-prepend">
          <span class="input-group-text">
            <i class="fe fe-search"></i>
          </span>
        </div>
        <input class="form-control search" type="search" placeholder="Search" name="q">
      </div>
    </form>

  </div>