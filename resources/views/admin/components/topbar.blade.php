<nav class="navbar navbar-expand-md navbar-light d-none d-md-flex" id="topbar">
  <div class="container-fluid">


    <form class="form-inline mr-4 d-none d-md-flex">
      <div class="input-group input-group-flush input-group-merge">
      </div>
    </form>


    <div class="navbar-user">

      <div class="dropdown">


        <a href="#" class="avatar avatar-sm dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="{{Auth::user()->photo_url}}" alt="..." class="avatar-img rounded-circle">
        </a>


        <div class="dropdown-menu dropdown-menu-right">
          <a href="{{url('admin/user/'.Auth::user()->id)}}" class="dropdown-item">Profile</a>
          <hr class="dropdown-divider">
          <a  data-toggle="modal" data-target="#forgot-password" class="dropdown-item">Change Password</a>
          <hr class="dropdown-divider">
          <a href="{{url('auth/logout')}}" class="dropdown-item">Logout</a>
        </div>

      </div>

    </div>

  </div>
</nav>


<div class="modal fade" id="forgot-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Chhange Password</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <form method="POST" action="/auth/password/forgot">

                  @csrf
                  <div class=form-group>
                      <label class=form-control-label>Current Password</label>
                      <input type=password class="form-control form-control-prepend" id=input-email placeholder="" required>
                  </div>
                  <div class=form-group>
                      <label class=form-control-label>New Password</label>
                      <input type=password class="form-control form-control-prepend" id=input-email placeholder="" required>
                  </div>
                  <div class=form-group>
                      <label class=form-control-label>Confirm New Password</label>
                      <input type=password class="form-control form-control-prepend" id=input-email placeholder="" required>
                  </div>
                  <div class="form-group">
                      <button type="submit" class="btn btn-primary w-100">Update Password</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>