@php 
$active = isset($active) ? $active : 'dashboard';
@endphp

<nav class="navbar navbar-vertical fixed-left navbar-expand-md " id="sidebar">
  <div class="container-fluid">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebarCollapse" aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>


    <a class="navbar-brand" href="/admin">
      <img src="/admin-assets/img/logo.svg" class="navbar-brand-img mx-auto" alt="...">
    </a>

    <div class="navbar-user d-md-none">

      <div class="dropdown">


        <a href="#" id="sidebarIcon" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="avatar avatar-sm avatar-online">
            <img src="{{Auth::user()->photo_url}}" class="avatar-img rounded-circle" alt="...">
          </div>
        </a>


        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sidebarIcon">
          <a href="/admin" class="dropdown-item">Profile</a>
          <hr class="dropdown-divider">
          <a href="/auth/logout" class="dropdown-item">Logout</a>
        </div>

      </div>

    </div>


    <div class="collapse navbar-collapse" id="sidebarCollapse">

      <ul class="navbar-nav">
        <li class="nav-item {{$active == 'dashboard' ? 'active' : ''}}">
          <a class="nav-link" href="/admin">
            <i class="fe fe-layers"></i> Dashboard
          </a>
        </li>
        <li class="nav-item {{$active == 'attendance' ? 'active' : ''}}">
          <a class="nav-link" href="{{url('admin/attendances')}}">
            <i class="fe fe-calendar"></i> Attendance
          </a>
        </li>
        <li class="nav-item {{$active == 'hosts' ? 'active' : ''}}">
          <a class="nav-link" href="#hosts" data-toggle="collapse" role="button" aria-controls="hosts">
            <i class="fe fe-home"></i> Hosts
          </a>
          <div class="collapse" id="hosts">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a href="{{url('/admin/hosts')}}" class="nav-link">All Hosts</a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/host/0')}}" class="nav-link">Add New Host</a>
              </li>
            </ul>
          </div>
        </li>
         <li class="nav-item {{$active == 'teams' ? 'active' : ''}}">
          <a class="nav-link" href="{{url('admin/teams')}}">
            <i class="fe fe-users"></i> Teams
          </a>
        </li>
        
        <li class="nav-item {{$active == 'spaces' ? 'active' : ''}}">
          <a class="nav-link" href="#spaces" data-toggle="collapse" role="button" aria-controls="hosts">
            <i class="fe fe-layout"></i> Spaces
          </a>
          <div class="collapse" id="spaces">
            <ul class="nav nav-sm flex-column">
              <li class="nav-item">
                <a href="{{url('/admin/spaces')}}" class="nav-link">All Spaces</a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/space/0')}}" class="nav-link">Add New Space</a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/categories')}}" class="nav-link">Categories</a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/amenities')}}" class="nav-link">Amenities</a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/ratings')}}" class="nav-link">Ratings</a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/states')}}" class="nav-link">States</a>
              </li>
              <li class="nav-item">
                <a href="{{url('admin/pricing')}}" class="nav-link">Pricing Filter</a>
              </li>
              
            </ul>
          </div>
        </li>

        <li class="nav-item {{$active == 'packages' ? 'active' : ''}}">
          <a class="nav-link" href="{{url('admin/packages')}}">
            <i class="fe fe-package"></i> Packages
          </a>
        </li>

        <li class="nav-item {{$active == 'users' ? 'active' : ''}}">
          <a class="nav-link" href="{{url('admin/users')}}">
            <i class="fe fe-users"></i> Users
          </a>
        </li>
        
        <li class="nav-item {{$active == 'subscriptions' ? 'active' : ''}}">
          <a class="nav-link" href="{{url('admin/subscriptions')}}">
            <i class="fe fe-grid"></i> Subscriptions
          </a>
        </li>
        <li class="nav-item {{$active == 'transactions' ? 'active' : ''}}">
          <a class="nav-link" href="{{url('admin/transactions')}}">
            <i class="fe fe-credit-card"></i> Transactions
          </a>
        </li>
        <li class="nav-item {{$active == 'settlements' ? 'active' : ''}}">
          <a class="nav-link" href="{{url('admin/settlements')}}">
            <i class="fe fe-check-circle"></i> Settlements
          </a>
        </li>
        
        <li class="nav-item {{$active == 'reviews' ? 'active' : ''}}">
          <a class="nav-link" href="{{url('admin/reviews')}}">
            <i class="fe fe-star"></i> Reviews
          </a>
        </li>

        <li class="nav-item {{$active == 'reports' ? 'active' : ''}}">
          <a class="nav-link" href="#" onclick="alert('Reports will be handled by Metabase.')">
            <i class="fe fe-bar-chart-2"></i> Reports
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="http://damier-spaces-blog.apprikaa.com/wp-admin" target="_blank">
            <i class="fe fe-file-text"></i> Content Management
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="https://app.drift.com"  target="_blank">
            <i class="fe fe-clipboard"></i> Drift Tickets
          </a>
        </li>

        <li class="nav-item {{$active == 'administrators' ? 'active' : ''}}">
          <a class="nav-link" href="/admin/users/admin">
            <i class="fe fe-users"></i> Administrators
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="/auth/logout">
            <i class="fe fe-log-out"></i> Logout
          </a>
        </li>

      </ul>

    </div> 

  </div>
</nav>