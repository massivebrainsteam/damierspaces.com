@extends('admin.layouts.app')

@section('title')
<div class="d-flex">
    Spaces
    @if(app('request')->input('host_id') > 0)
    <a href="/admin/view-host/{{app('request')->input('host_id')}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to Host Details</a>
    @else
    <a href="/admin/space/" class="ml-auto btn-outline-primary btn "><i class="fe fe-plus-circle"></i> Add New Space</a>
    @endif
</div>
@endsection

@section('content')


<div data-toggle="lists" data-options='{"valueNames": ["name"]}' class="mt-3">
    <div data-toggle="lists" data-options='{"valueNames": ["name"], "listClass": "listAlias"}'>
        <div class="row mb-4">
            <div class="col">

                <form action="{{url('admin/spaces')}}" method="GET">

                    <div class="input-group input-group-lg input-group-merge">
                        <input type="text" name="q" class="form-control form-control-prepended search" placeholder="Search Spaces" required>
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <span class="fe fe-search"></span>
                            </div>
                        </div>
                    </div>

                    @if(request('q'))
                    <div class="alert alert-light mt-3">Your search returned {{$spaces->count()}} {{str_plural('Result', $spaces->count())}}.</div>
                    @endif

                </form>

            </div>

            <div class="col-auto">

                <div class="nav btn-group" role="tablist">
                    <button class="btn btn-lg btn-white active" data-toggle="tab" data-target="#tabPaneOne" role="tab" aria-controls="tabPaneOne" aria-selected="true">
                        <span class="fe fe-grid"></span>
                    </button>
                    <button class="btn btn-lg btn-white" dusk="tab-pane-two" data-toggle="tab" data-target="#tabPaneTwo" role="tab" aria-controls="tabPaneTwo" aria-selected="false">
                        <span class="fe fe-list"></span>
                    </button>
                </div>

            </div>
        </div>


        <div class="tab-content">
            <div class="tab-pane fade active show" id="tabPaneOne" role="tabpanel">
                <div class="row listAlias">

                    @foreach($spaces as $row)

                    <div class="col-12 col-md-6 col-xl-4">

                        @component('admin.spaces.components.space-card', ['space' => $row]) @endcomponent

                    </div>

                    @endforeach

                </div>
            </div>
            <div class="tab-pane fade" id="tabPaneTwo" role="tabpanel">
                <div class="row list">

                    @foreach($spaces as $row)

                    <div class="col-12">

                        @component('admin.spaces.components.space-row', ['space' => $row]) @endcomponent

                    </div>
                    @endforeach

                </div>
            </div>
        </div>

        <div class="mt-3 float-right">
            {{$spaces->links() }}
        </div>

    </div>
</div>

@endsection
