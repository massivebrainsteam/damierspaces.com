<div class="card">
	<a href="{{url('admin/space/'.$space->id)}}">
		<img src="{{$space->photo_url}}" style="height:250px;" alt{{$space->name}} class="card-img-top">
	</a>
	<div class="card-body space-cards-card">
		<div class="row align-items-center">
			<div class="col">

				<h4 class="card-title mb-2 name">

					@if($space->featured == 'yes')
					<a href="{{url('admin/space/'.$space->id)}}" data-toggle="tooltip" data-placement="top" title="This is a featured Space." data-original-title="This is a featured Space.">
						{!!$space->featured == 'yes' ? '<i class="fe fe-star"></i>' : ''!!} {{$space->name}}
					</a>
					@else
					<a href="{{url('admin/space/'.$space->id)}}">
						{{$space->name}}
					</a>
					@endif
				</h4>
				{{_badge($space->space_status)}}
				<ul class="space-details-info">
					<li><strong>Category:</strong> {{$space->category->name}}</li>
					<li><strong>Host:</strong> {{$space->host->name}}</li>
					<li><strong>Rating:</strong> {{$space->rating->name}}</li>
				</ul>
			</div>
			<div class="col-auto">
				<div class="dropdown">
					<a href="javascript:;" class="dropdown-ellipses dropdown-toggle text-dark" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fe fe-more-vertical"></i>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="{{url('/admin/space/'.$space->id)}}" class="dropdown-item"><i class="fe fe-edit"></i> Edit Details</a>
						<a href="{{url('admin/space/'.$space->id.'/photos')}}" class="dropdown-item"><i class="fe fe-image"></i> Space Photos ({{$space->photos()->count()}})</a>

						<a href="{{url('admin/space/'.$space->id.'/photos')}}" class="dropdown-item text-danger"><i class="fe fe-x-circle"></i> Mark as Closed </a>
					</div>
				</div>
			</div>
		</div> 

	</div> 
</div>