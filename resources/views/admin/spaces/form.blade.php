@extends('admin.layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('admin-assets/libs/dropify/css/dropify.min.css')}}">
@endsection

@section('title')
<div class="d-flex">
    {{$space->id > 0 ? 'Update '.$space->name : 'Add New Space'}}
    @if($space->id > 0 )
    <a href="/admin/spaces" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to Space Detail</a>
    @else
    <a href="/admin/spaces" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to All Spaces</a>
    @endif
    

</div>
@endsection

@section('content')

<div class="col-8 offset-2">

    <form class="mb-4" method="POST" action="{{url('admin/space')}}" enctype="multipart/form-data">

        @csrf

        <h3>SPACE DETAILS</h3>
        <hr>

        <div class="form-group">
            <label>Host</label>
            <select class="form-control" id="host_id" name="host_id" data-toggle="select" required>
                <option>--select--</option>
                @foreach($hosts as $row)
                <option value="{{$row->id}}" {{$row->id == old('host_id', $host->id) ? 'selected' : ''}}>{{$row->name}} - {{$row->address}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label>Location</label>
            <select class="form-control" name="location_id" data-toggle="select" required>
                @foreach($locations as $row)
                <option value="{{$row->id}}" {{$row->id == old('location_id', $host->id) ? 'selected' : ''}}>{{$row->name}} - {{$row->address}}</option>
                @endforeach
            </select>
            @error('location_id')
            <div class="invalid-feedback" role="alert">{{ $errors->first('location_id') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Rating</label>
            <select class="form-control" name="rating_id" data-toggle="select" required>
                @foreach($ratings as $row)
                <option value="{{$row->id}}" {{$row->id == old('rating_id', $space->rating_id) ? 'selected' : ''}}>{{$row->name}}</option>
                @endforeach
            </select>
            @error('rating_id')
            <div class="invalid-feedback" role="alert">{{ $errors->first('rating_id') }}</div>
            @enderror
        </div>


        <div class="form-group">
            <label>Name</label>
            <input name="name" type="text" class="form-control" value="{{old('name', $space->name)}}" required>
            @error('name')
            <div class="invalid-feedback" role="alert">{{ $errors->first('name') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label class="mb-1">Space Description</label>
            <small class="form-text text-muted">
                This is how customers will know about this space, so make it good!
            </small>
            <textarea name="description" class="form-control" rows="3" required>{{old('description', $space->description)}}</textarea>
        </div>


        <div class="row">
            <div class="col-12 col-md-6">

                <div class="form-group">
                    <label>Category</label>
                    <select name="category_id" class="form-control" required>
                        @foreach($categories as $row)
                        <option value="{{$row->id}}" {{old('category_id', $space->category_id) == $row->id ? 'selected' : ''}}>
                            {{$row->name}}
                        </option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="col-12 col-md-6">

                <div class="form-group">
                    <label>Capacity</label>
                    <input name="capacity" type="number" class="form-control" value="{{old('capacity', $space->capacity)}}" required>
                    @error('capacity')
                    <div class="invalid-feedback" role="alert">{{ $errors->first('capacity') }}</div>
                    @enderror
                </div>

            </div>

        </div>

        <div class="form-group">
            <label>Space Quantity: (Amount available) *</label>
            <input name="quality" type="text" class="form-control" value="{{old('quantity', $space->quantity)}}" required>
            @error('quantity')
            <div class="invalid-feedback" role="alert">{{ $errors->first('quantity') }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label>Cover Photo</label>
            <input type="file" class="dropify" name="photo_url" data-default-file="{{$space->getOriginal('photo_url')}}" data-height="200" data-allowed-file-extensions="jpg jpeg png" data-max-file-size="2M" />
        </div>

        <br>
        <h3>SPACE AMENITIES</h3>
        <hr>

        <div class="row">
            @foreach($amenities as $row)

            @php
            $checked = $space->amenities()->where(['space_amenities.amenity_id' => $row->id])->count() > 0 ? 'checked' : '';
            @endphp

            <div class="col-md-6">
                <div class="form-group">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" name="amenities[{{$row->id}}]" class="custom-control-input" id="amenity_{{$row->id}}" {{$checked}}>
                        <label class="custom-control-label" for="amenity_{{$row->id}}">{{$row->name}}</label>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <hr class="mt-5 mb-5">

        <div class="row">
            <div class="col-12 col-md-12">

                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control" required>

                        @foreach(['active', 'inactive'] as $row)
                        <option value="{{$row}}" {{old('status', $space->status) == $row ? 'selected' : ''}}>
                            {{ucfirst($row)}}
                        </option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="col-12 col-md-12">

                <div class="form-group">
                    <label>Featured</label>
                    <select name="featured" class="form-control" required>

                        @foreach(['yes', 'no'] as $row)
                        <option value="{{$row}}" {{old('status', $space->featured) == $row ? 'selected' : ''}}>
                            {{ucfirst($row)}}
                        </option>
                        @endforeach
                    </select>
                </div>

            </div>

        </div>

        <hr class="mt-5 mb-5">
        <input type="hidden" name="id" value="{{$space->id}}">
        <button type="submit" class="btn btn-block btn-primary">
            Save Space
        </button>

    </form>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('admin-assets/libs/dropify/js/dropify.min.js')}}"></script>
<script type="text/javascript">
    $('.dropify').dropify();

    $('#host_id').change(() => {

        let host_id = $('#host_id').val();
        window.location = `{{url('/admin/space')}}/{{$space->id ?? 0}}?host_id=${host_id}`
    })

</script>
@endsection
