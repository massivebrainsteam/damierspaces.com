@extends('admin.layouts.app')

@section('title')
<div class="d-flex">

    {{$space->name}} - Photos
    <a href="/admin/space/{{$space->id}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to Space Details</a>
</div>

@endsection

@section('content')

<h4 class="card-header-title">
    <a href="{{url('admin/space/'.$space->id.'/photo/0')}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-plus-circle"></i> Add New Photo</a>
</h4>
<br>
</div>
<div class="card">

    <div class="card-header">


        <div class="card-body">

            <div class="row align-items-center">
                @foreach($photos as $row)
                <div class="col-lg-3">
                    <a href="{{$row->url}}" target="_blank">
                        <img src="{{$row->url}}" alt="{{$space->name}}" class="img-fluid rounded">
                    </a>
                    <div class="d-flex align-items-center justify-content-between mt-3">
                        <p class="card-text small text-muted mb-0">{{_badge($row->status)}}</p>
                        <a href="{{url('admin/space/'.$space->id.'/photo/'.$row->id)}}">
                            Edit Photo
                        </a>
                    </div>
                </div>
                @endforeach
            </div>

        </div>
    </div>

    @endsection
