@extends('admin.layouts.app')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('admin-assets/libs/dropify/css/dropify.min.css')}}">
@endsection

@section('title')
<div class="d-flex">
    {{$space->name}} - Photo
    <a href="/admin/space/{{$space->id}}/photos" class="ml-auto btn-outline-primary btn "><i class="fe fe-chevron-left"></i> Back to All Photos</a>
</div>
@endsection

@section('content')

<div class="col-8 offset-2">

    <form class="mb-4" method="POST" action="{{url('admin/space/'.$space->id.'/save-photo')}}" enctype="multipart/form-data">

        @csrf

        <div class="form-group">
            <label>Image</label>
            <input type="file" class="dropify" name="photo_url" data-height="200" data-default-file="{{$photo->url}}" data-allowed-file-extensions="jpg jpeg png" data-max-file-size="2M" />
        </div>

        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control" required>

                @foreach(['active', 'inactive'] as $row)
                <option value="{{$row}}" {{old('status', $photo->status) == $row ? 'selected' : ''}}>
                    {{ucfirst($row)}}
                </option>
                @endforeach
            </select>
        </div>

</div>

<input type="hidden" name="id" value="{{$space->id}}">

<div class="row">
    <div class="col-6 offset-3">
        <button type="submit" class="btn btn-block btn-primary">
            Upload
        </button>
    </div>
</div>

</form>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('admin-assets/libs/dropify/js/dropify.min.js')}}"></script>
<script type="text/javascript">
    $('.dropify').dropify();

</script>
@endsection
