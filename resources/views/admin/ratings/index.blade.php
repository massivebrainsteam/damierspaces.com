@extends('admin.layouts.app')

@section('title')
Ratings
@endsection

@section('content')

<div class="card">
  <div class="card-header">
    <h4 class="card-header-title">
      <a href="{{url('admin/rating/0')}}" class="ml-auto btn-outline-primary btn "><i class="fe fe-plus-circle"></i> Add new Rating</a>
    </h4>
  </div>

  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Last Modified</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach($ratings as $row)
        <tr>
          <td><a href="{{url('admin/rating/'.$row->id)}}">{{$row->name}}</a></td>
          <td>{{_date($row->updated_at)}}</td>
          <td>{{_badge($row->status)}}</td>
        </tr>
        @endforeach

        @if($ratings->count() < 1)
        <tr>
          <td colspan="4" class="text-center text-muted">When you create ratings, you will see them here.</td>
        </tr>
        @endif

      </tbody>
    </table>
  </div>
</div>

@endsection