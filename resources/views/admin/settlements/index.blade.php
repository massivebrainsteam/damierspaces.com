@extends('admin.layouts.app')

@section('title')
Settlements
@endsection

@section('content')

<div class="card">
 @include('admin.components.search')
  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>Host</th>
          <th>Settlement Date</th>
          <th>Settlement Amount</th>
          <th>Attendances</th>
          <th>Approved By</th>
        </tr>
      </thead>
      <tbody>
        @foreach($settlements as $row)

        <tr>
          <td>{{$row->host->name}}</td>
          <td>{{_date($row->created_at)}}</td>
          <td>{{_currency($row->total)}}</td>
          <td>{{$row->attendances()->count()}}</td>
          <td>{{$row->approver->name}}</td>
        </tr>
        @endforeach

        @if($settlements->count() < 1)
        <tr>
          <td colspan="6" class="text-center text-muted">When you create Settlements, you will see them here.</td>
        </tr>
        @endif

      </tbody>
    </table>
  </div>
</div>

@endsection