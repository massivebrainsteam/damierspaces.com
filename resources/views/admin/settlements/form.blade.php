@extends('admin.layouts.app')

@section('title')
Create Settlement for {{$host->name}}
@endsection

@section('content')

<div class="col-8 offset-2">

    <form class="mb-4" method="post" action="{{url('admin/settlement/save')}}">

        @csrf

        <div class="form-group">
            <h1>Host: {{$host->name}}</h1>
        </div>

        <div class="form-group">
            <label>Attendances</label>

            @php $sum = 0 @endphp

            <div class="row">

                @foreach($attendances as $row)

                <div class="card">
                    <div class="card-body">
                      <h3 class="card-title">{{$row->user->name}}</h3>
                      <p class="card-text">
                        Space: {{$row->space->name}}, Location: {{$row->location->name}},
                        Start At: {{_date($row->start_at, true)}}, End At: {{_date($row->end_at, true)}},
                        Package: {{$row->user->subscription->package->name}}, Settlement: {{_currency($row->settlement)}},
                        Cummulative: {{_currency($sum)}}
                    </p>

                </div>
            </div>

            @php $sum+= $row->settlement @endphp

            @endforeach


            <input type="hidden" name="host_id" value="{{$host->id}}">

        </div>

    </div>

    <div class="form-group">
        <label>Total Settlement</label>
        <input type="text" class="form-control disabled" style="background:#eee" value="{{_currency($sum)}}" disabled/>
    </div>

    <button type="submit" class="btn btn-block btn-primary" onclick="return confirm('Are you sure?')">
        Approve Settlement and Pay
    </button>

</form>
</div>

@endsection