@extends('admin.layouts.app')

@section('title')
Subscriptions
@endsection

@section('content')

<div class="card">
  
  @include('admin.components.search')
  
  <div class="table-responsive mb-0">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th>User</th>
          <th>Package</th>
          <th>Transaction Reference</th>
          <th>Amount Paid</th>
          <th>Start Date</th>
          <th>End Date</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        @foreach($subscriptions as $row)
        <tr>
          <td>{{$row->user->name}}</td>
          {{-- <td><a href="{{url('/admin/package/'.$row->package_id.'/basic-info')}}">{{$row->package->name}}</a></td> --}}
          <td>{{$row->package->name}}</td>
          <td>{{$row->transaction->transaction_reference}}</td>
          <td>{{_currency($row->total)}}</td>
          <td>{{_date($row->start_at)}}</td>
          <td>{{_date($row->end_at)}}</td>
          <td>{{_badge($row->status)}}</td>
        </tr>
        @endforeach

        @if($subscriptions->count() < 1)
        <tr>
          <td colspan="7" class="text-center text-muted">When users subscribe to a package, those subscriptions will show up here.</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>

  <div class="mt-3 float-right">
    {{$subscriptions->links() }}
  </div>

</div>

@endsection