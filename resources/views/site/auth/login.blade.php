@php
$nav = 'light';
$footer = '_footer_min';
@endphp

@extends('site.layout.app')

@section('content')
<div class="container-fluid ">
    <div class="col-12 d-flex justify-content-center align-items-center min-vh-90">
        <div class="form-wrapper">
            <div class="mb-5 text-center">
                <h6 class="h3 mb-1">Welcome back!</h6>
                <p class="text-muted mb-0">Sign in to your account to continue.</p>
            </div><span class=clearfix></span>

            <form action="/auth/login" method="POST">

                @csrf

                @include('site.components._alert')

                <div class=form-group>
                    <label class=form-control-label>Email address</label>
                    <div class="input-group input-group-merge">
                        <input type=email class="form-control form-control-prepend" placeholder="Example: bosunjones@mail.com" name="email" required>
                        <div class=input-group-prepend><span class=input-group-text><i data-feather=user></i></span></div>
                    </div>
                </div>
                <div class="form-group mb-0">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <label class=form-control-label>Password</label>
                        </div>
                        <div class=mb-2>
                            <a   data-toggle="modal" data-target="#forgot-password" class="small text-muted text-underline--dashed border-primary">Forgot Password?</a>
                        </div>
                    </div>
                    <div class="input-group input-group-merge">
                        <div class=input-group-prepend><span class=input-group-text><i data-feather=key></i></span></div>
                        <input type="password" class="form-control form-control-prepend" placeholder=Password name="password" required>
                        <i class="far fa-eye togglePassword"></i>
                    </div>
                </div>
                <div class=mt-4>
                    <input type="hidden" name="type" value="user">
                    <button type="submit" class="btn btn-block btn-primary">Sign in</button>
                </div>
            </form>
            <div class="mt-4 text-center"><small>Not registered?</small> <a href="/page/choose-account" class="small font-weight-bold">Create account</a></div>
        </div>
    </div>
</div>


<div class="modal fade" id="forgot-password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Forgot Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="/auth/password/forgot">

                    @csrf

                    <p class="text-center">Please enter your email address and we will send you a password reset instruction.</p>
                    <div class=form-group>
                        <label class=form-control-label>Email address</label>
                        <div class="input-group input-group-merge">
                            <input type=email class="form-control form-control-prepend" id=input-email placeholder="Example: bosunjones@mail.com" name="email" required>
                            <div class=input-group-prepend><span class=input-group-text><i data-feather=mail></i></span></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary w-100">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
