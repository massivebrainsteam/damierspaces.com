@extends('site.layout.app')

@section('content')

<section>
	<div class="container d-flex flex-column">
		<div class="row align-items-center justify-content-center min-vh-100">
			<div class="col-md-6 col-lg-5 col-xl-4 py-6 py-md-0">
				<div>
					<div class="mb-5 text-center">
						@if($status == 'success')
						<img class="mb-3" src="/assets/img/icon-checked.svg" alt="">
						<h6 class="h3 mb-1">Your Account Has Been Verified!</h6>
						<p class="text-muted mb-0">Welcome to Damier Spaces, your account has been verified and activated.</p>
						@else
						<p class="text-muted">Could not confirm your email address.</p>
						@endif

						@if($user->type == 'team_admin')
						<p class="text-muted">We are still reviewing your team account. We will be in touch.</p>
						@endif
						
					</div><span class=clearfix></span>
					<div>
						<a href="{{$url}}" class="btn btn-block btn-primary btn-fill">Login</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection