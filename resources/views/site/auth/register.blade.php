@extends('site.layout.app')

@section('styles')
<style type="text/css">
  .text-danger{

    color:red;
  }
</style>
@endsection

@section('content')

<div class="ui full modal scrolling transition visible active" data-for="modal01">
  <div class="modal-full-background">
    <img src="/site-assets/images/modal/modal_background_001.png" alt="">
  </div>

  <a href="/"><i class="icon icon-close close-modal"></i></a>

  <div class="header center"> Create an Account</div>

  <div class="content">

    <form action="/auth/register" method="POST">

      @csrf

      @include('site.components._alert')

      <div class="div-c">
        <div class="divided-column">
          <input type="text" name="name" placeholder="Full Name" value="{{old('name')}}" required>
          @if($errors->has('name'))
          <span class="text-danger">{{$errors->first('name')}}</span>
          @endif
        </div>
      </div>

      <div class="div-c">
        <div class="divided-column">
          <input type="email" name="email" placeholder="E-mail Adress" value="{{old('email')}}">
          @if($errors->has('email'))
          <span class="text-danger">{{$errors->first('email')}}</span>
          @endif
        </div>

        <div class="divided-column">
          <input type="number" name="phone" placeholder="Phone Number" value="{{old('phone')}}">
          @if($errors->has('phone'))
          <span class="text-danger">{{$errors->first('phone')}}</span>
          @endif
        </div>

      </div>

      <div class="div-c inline-2">
        <div class="divided-column">
          <input type="password" name="password" placeholder="Password" required>
          @if($errors->has('password'))
          <span class="text-danger">{{$errors->first('password')}}</span>
          @endif
        </div>
        <div class="divided-column">
          <input type="password" name="password_confirmation" placeholder="Confirm Password" required>
        </div>
      </div>

      <button type="submit" class="button-sq btn-dark fullwidth-sq">
        Sign Up
      </button>

    </form>
  </div>

  <div class="actions">
    <div class="border-container">
      <div class="button-sq link-sq modal-ui-trigger" data-trigger-for="modal02">Already a member?</div>

      <a class="button-sq link-sq login-sq" href="/auth/login">
        Log In
        <i class="icon icon-person-lock-2"></i>
      </a>
    </div>
  </div>
</div>

@endsection