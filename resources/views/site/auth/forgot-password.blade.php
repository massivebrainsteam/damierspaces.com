@php
$nav = 'light';
$footer = '_footer_min';
@endphp

@extends('site.layout.app')

@section('content')

<section>
    {{-- <div class="bg-primary position-absolute h-100 top-0 left-0 zindex-100 col-lg-6 col-xl-6 zindex-100 d-none d-lg-flex flex-column justify-content-end" data-bg-size=cover data-bg-position=center><img src=/assets/img/login-bg.png alt=Image class=img-as-bg>
    </div> --}}
    {{-- <div class="container-fluid d-flex flex-column">
        <div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
            
        </div>
    </div> --}}

    <div class="container">
        <div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
            <div class="col-sm-12 col-lg-12 col-xl-12 py-6 py-md-0">
                <div class="row justify-content-center">
                    <div class="col-11 col-lg-10  col-xl-6">
                        <div class="max-width-500">
                            <div class="mb-4 text-center">
                                <h6 class="h3 mb-1">Forgot Password</h6>
                                <p class="text-dark mb-0">Enter your email address below</p>
                            </div><span class=clearfix></span>

                            <form action="/auth/password/forgot" method="POSt">

                                @csrf

                                @include('site.components._alert')

                                <div class=form-group>
                                    <label class=form-control-label>Email address</label>
                                    <div class="input-group input-group-merge">
                                        <input type=email name="email" class="form-control form-control-prepend" placeholder="name@example.com" required>
                                        <div class=input-group-prepend>
                                            <span class=input-group-text><i data-feather=mail></i></span>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class=mt-4>
                                    <button type="submit" class="btn btn-block btn-primary">Continue</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>


@endsection