<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Damier Spaces - Enjoy frictionless daily access to the best workspaces near you!">
  <meta name="author" content="Nativebrands">
  <title>Host Login</title>
  <link rel="stylesheet" href="/host-assets/fontawesome5/css/all.min.css">
  <link rel="stylesheet" href="/host-assets/css/style.css" id="stylesheet">
</head>

<body class="">


    <div class="min-vh-100 py-5 d-flex align-items-center bg-white">
      <div class="w-100">
        <div class="row justify-content-center">
          <div class="col-sm-8 col-lg-4">
            <div class=" px-md-5 py-5">
              <div class="mb-5">
                <div class="text-center">
                <a class="navbar-brand mb-5" href="/">
                  <img src="/assets/img/brand/dark.svg" id="navbar-logo" style="width:250px"> 
                </a>
                <h6 class="h3">Host Login</h6>
                <p class="text-dark mb-0">Sign in to your host account to continue.</p>
              </div>
              </div>
              <span class="clearfix"></span>
              <form role="form" action="/auth/login" method="POST">

                @csrf

                @include('site.components._alert')

                <div class="form-group">
                  <label class="form-control-label">Email address</label>
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-user"></i>
                      </span>
                    </div>
                    <input type="email" name="email" class="form-control" id="input-email" placeholder="name@example.com" required>
                  </div>
                </div>
                <div class="form-group mb-4">
                  <div class="d-flex align-items-center justify-content-between">
                    <div>
                      <label class="form-control-label">Password</label>
                    </div>
                    <div class="mb-2">
                      <a href="/auth/password/forgot" class="small text-primary2 text-underline--dashed">Forget password?</a>
                    </div>
                  </div>
                  <div class="input-group input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="fa fa-key"></i>
                      </span>
                    </div>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                    <div class="input-group-append">
                      <span class="input-group-text">
                          <i class="fa fa-eye togglePassword"></i>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="mt-4">
                  <input type="hidden" name="type" value="host">
                  <button type="submit" class="btn btn-primary w-100 full-width">
                    Sign in
                  </button>
                </div>
                <p class="text-center text-dark mt-3">Got a Space? <a href="/become-a-host" class="text-primary text-bold">Become a host</a></p>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>


      <script>
        const toggle = document.querySelector('.togglePassword');
        const password = document.querySelector('#password');

        toggle.addEventListener('click', function(e) {
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            this.classList.toggle("fa-eye-splash");
            //this.classList.remove("fa-eye")
            //this.classList.add("fa-eye-splash")
        })
    </script>


  </body>
  </html>