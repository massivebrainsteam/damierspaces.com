@php
	$nav = 'light';
	$footer = '_footer_min';
@endphp

@extends('site.layout.app')

@section('content')

<section>
	<div class="bg-primary position-absolute h-100 top-0 left-0 zindex-100 col-lg-6 col-xl-6 zindex-100 d-none d-lg-flex flex-column justify-content-end" data-bg-size=cover data-bg-position=center><img src=../../assets/img/host-cac.png alt=Image class=img-as-bg>
	</div>
	<div class="container-fluid d-flex flex-column">
		<div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
			<div class="col-sm-7 col-lg-6 col-xl-6 py-6 py-md-0">
				<div class="row justify-content-center">
					<div class="col-11 col-lg-10 col-xl-6">
						<div>
							<div class=mb-5>
								<div class="text-center">
									<h6 class="h3 mb-1">Account Verfications</h6>
									<p class="text-muted mb-30">Upload your Certificate of Incorporation (CAC):</p>
								</div>

								<span class="badge badge-primary"><strong>Max file size:</strong> 2mb (PDF, JPG, PNG)</span>
								
							</div>
							<form method="POST" action="/become-a-host/cac" enctype="multipart/form-data">

								@csrf

								@include('site.components._alert')

								<div class=form-group>
									<label class=form-control-label>Upload File:</label>
									<div class="input-group input-group-merge">
										<input type="file" class="form-control form-control-prepend" name="cac" required>
										<div class=input-group-prepend><span class=input-group-text><i data-feather=user></i></span></div>
									</div>

									@error('cac')
                                    <span class="text-danger">File too large or invalid file type. Please upload an image or PDF file less than 1MB.</span>
                                    @enderror

								</div>

								<div class=mt-4>
									<button type="submit" class="btn btn-primary">Upload</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
@endsection
