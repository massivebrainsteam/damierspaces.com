@php
	$nav = 'light';
	$footer = '_footer_min';
@endphp

@extends('site.layout.app')

@section('content')

<section>
	<div class="container d-flex flex-column">
		<div class="row align-items-center justify-content-center min-vh-100">
			<div class="col-md-6 col-lg-5 col-xl-4 py-6 py-md-0">
				<div>
					<div class="mb-5 text-center">
						<img class="mb-3" src=/assets/img/created-success.svg alt="">
						<h6 class="h3 mb-1">Your account has been created successfully!</h6>
						<p class="text-muted mb-0">Your signup was successful, please check your email to verify your account.</p>
					</div><span class=clearfix></span>
				</div>
			</div>
		</div>
	</div>

</section>

@endsection