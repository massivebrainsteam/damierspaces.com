@php
	$nav = 'light';
	$footer = '_footer_min';
@endphp

@extends('site.layout.app')

@section('content')

<section>
	<div class="bg-primary position-absolute h-100 top-0 left-0 zindex-100 col-lg-6 col-xl-6 zindex-100 d-none d-lg-flex flex-column justify-content-end" data-bg-size=cover data-bg-position=center>
		<img src=/assets/img/host-bank.png alt=Image class=img-as-bg>
	</div>
	<div class="container-fluid d-flex flex-column">
		<div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
			<div class="col-sm-7 col-lg-6 col-xl-6 py-6 py-md-0">
				<div class="row justify-content-center">
					<div class="col-11 col-lg-10 col-xl-6">
						<div>
							<div class=mb-5>
								<div class="text-center">
									<h6 class="h3 mb-1">Bank Information</h6>
									<p class="text-muted mb-30">Enter your bank details.<br>This is where payments will be made to you.</p>
								</div>

							</div>
							<form method="POST" action="/become-a-host/bank-info">

								@csrf

							
								<div class=form-group>
									<label class=form-control-label>Bank Name:</label>
									<div class="input-group input-group-merge">
										<input type="text" name="bank_name" class="form-control" required>
									</div>
								</div>

								<div class=form-group>
									<label class=form-control-label>Account Name:</label>
									<div class="input-group input-group-merge">
										<input type="text" name="bank_account_name" class="form-control" required>
									</div>
								</div>

								<div class=form-group>
									<label class=form-control-label>Account Number:</label>
									<div class="input-group input-group-merge">
										<input type="number" name="bank_account_number" class="form-control" required>
									</div>
								</div>
								<div class=mt-4>
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

@endsection