@php
$nav = 'light';
$footer = '_footer_min';
@endphp

@extends('site.layout.app')

@section('content')

    <section class="damier-grey mt-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h2 class="damier-color border-bottom pb-3 mt-5">Become a Host</h2>

                    <div class="row mb-5">
                        <div class="col-12">
                            <p>For over 5 years, we pride ourselves on our commitment to excellence, as well as our ability to deliver for our customers.</p>
                            <a href="/page/host-how-it-works" class="btn btn-fill">See How it Works <i data-feather=chevron-right></i></a>
                        </div>
                    </div>

                    <form action="/become-a-host" method="POST">

                        @csrf

                        @include('site.components._alert')

                        <div class="mt-4">

                            <h6 class="text-primary pb-2 mt-5">COMPANY DETAILS</h6>

                            <div class="mt-4">
                                <div class="form-group">
                                    <label class=form-control-label>Company Name:</label>
                                    <div class="input-group  input-group-merge">
                                        <input type="text" class="form-control form-control-prepend" placeholder="e.g: WorkStation NG" name="name" required>
                                        <div class=input-group-prepend>
                                            <span class=input-group-text>
                                                <i data-feather=user></i>
                                            </span>
                                        </div>
                                    </div>
                                    @error('name')
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label class=form-control-label>Email Address:</label>
                                    <div class="input-group  input-group-merge">
                                        <input type="email" class="form-control form-control-prepend" placeholder="e.g: workstatin@gmail.com" name="email" value="{{ old('email') }}" required>
                                        <div class=input-group-prepend>
                                            <span class=input-group-text>
                                                <i data-feather=mail></i>
                                            </span>
                                        </div>
                                    </div>
                                    @error('email')
                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                    @enderror
                                </div>
                                <div class="alert alert-damier text-white bg-primary2">
                                    You will login with this email
                                </div>

                                <div class="form-group">
                                    <label class=form-control-label>Phone Number:</label>
                                    <div class="input-group w-100">
                                        <input type="number" id="phone" class="form-control w-100 form-control-prepend" value="{{ old('phone') }}" placeholder="Phone Number" name="phone" required>
                                    </div>
                                    @error('phone')
                                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                                    @enderror
                                </div>
                                <div class="form-group ">
                                    <label class="form-control-label">About Business / Description</label>
                                    <textarea rows="5" class="form-control" aria-label="Brief Description of about your company." placeholder="Brief Description of about your company." name="description" required>{{ old('description') }}</textarea>
                                </div>

                                <div class="form-group">
                                    <label class=form-control-label>Country:</label>
                                    <div class="input-group  input-group-merge">
                                        <div class=input-group-prepend>
                                            <span class=input-group-text>
                                                <i data-feather=globe></i>
                                            </span>
                                        </div>
                                        <select name="country" id="country" class="form-control form-control-prepend">
                                            <option value="nigeria">Nigeria</option>
                                            <option value="ghana">Ghana</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col">
                                        <label class=form-control-label>State:</label>
                                        <div class="input-group  input-group-merge">
                                            <div class=input-group-prepend>
                                                <span class=input-group-text>
                                                    <i data-feather=map></i>
                                                </span>
                                            </div>
                                            <select name="state" id="state" class="form-control form-control-prepend">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col">
                                        <label class=form-control-label>City:</label>
                                        <div class="input-group  input-group-merge">
                                            <input type="text" class="form-control form-control-prepend" placeholder="e.g: Ikeja" name="city" required>
                                            <div class=input-group-prepend>
                                                <span class=input-group-text>
                                                    <i data-feather=map-pin></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="form-control-label">Company Main Address:</label>
                                    <textarea rows="5" class="form-control" aria-label="Company's Address." placeholder="e.g: 123, Lagos Street, Lagos, Nigeria." name="address" required>{{ old('address') }}</textarea>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-lg-6">
                                    <div class=form-group>
                                        <label class=form-control-label>Password:</label>
                                        <div class="input-group input-group-merge">
                                            <div class=input-group-prepend>
                                                <span class=input-group-text>
                                                    <i data-feather="key"></i>
                                                </span>
                                            </div>
                                            <input type="password" name="password" class="form-control form-control-prepend" placeholder="******" required>
                                            <i class="far fa-eye togglePassword"></i>
                                        </div>
                                        @error('password')
                                            <span class="text-danger">{{ $errors->first('password') }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class=form-group>
                                        <label class=form-control-label>Confirm Password:</label>
                                        <div class="input-group input-group-merge">
                                            <div class=input-group-prepend>
                                                <span class=input-group-text>
                                                    <i data-feather="key"></i>
                                                </span>
                                            </div>
                                            <input type="password" name="password_confirmation" class="form-control form-control-prepend" placeholder="******" required>
                                            <i class="far fa-eye togglePassword"></i>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <h6 class="text-primary pb-2 mt-5"> CONTACT PERSON DETAILS</h6>
                            <div class="form-group">
                                <label class=form-control-label>Company Name::</label>
                                <div class="input-group  input-group-merge">
                                    <input type="text" class="form-control form-control-prepend" placeholder="e.g: Bosun Jones" name="contact_name" required>
                                    <div class=input-group-prepend>
                                        <span class=input-group-text>
                                            <i data-feather=user></i>
                                        </span>
                                    </div>
                                </div>
                                @error('name')
                                    <span class="text-danger">{{ $errors->first('contact_name') }}</span>
                                @enderror
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label class=form-control-label>Phone Number:</label>
                                    <div class="input-group ">
                                        <input type="number" id="phone" class="form-control w-100 form-control-prepend" placeholder="Phone Number" name="contact_phone" required>
                                    </div>
                                    @error('contact_phone')
                                        <span class="text-danger">{{ $errors->first('contact_phone') }}</span>
                                    @enderror
                                </div>
                                <div class="form-group col">
                                    <label class=form-control-label>Email Address:</label>
                                    <div class="input-group  input-group-merge">
                                        <div class=input-group-prepend>
                                            <span class=input-group-text>
                                                <i data-feather=mail></i>
                                            </span>
                                        </div>
                                        <input type="email" name="contact_email" placeholder="e.g: bosun@mail.com" class="form-control form-control-prepend" value="{{ old('contact_email') }}" required>
                                    </div>
                                    @error('contact_email')
                                        <span class="text-danger">{{ $errors->first('contact_email') }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group mb-5">
                                <button type="submit" class="btn btn-primary w-100">Continue</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-lg-4 mt-5">
                    <div class="card">
                        <div class="card-header border-bottom pb-2">
                            <i data-feather=message-circle></i> Got a question?
                        </div>
                        <div class="card-body">
                            <p>In case of any question or enquiries or glitches while filling the form</p>
                            <ul>
                                <li><span class="damier-color">Please call:</span> +234 809 876 5432</li>
                                <li><span class="damier-color">Email:</span> support@damierspaces.com</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
    </section>

@endsection
