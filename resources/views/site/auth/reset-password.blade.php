@php
$nav = 'light';
$footer = '_footer_min';
@endphp

@extends('site.layout.app')

@section('content')

<section>
    <div class="bg-primary position-absolute h-100 top-0 left-0 zindex-100 col-lg-6 col-xl-6 zindex-100 d-none d-lg-flex flex-column justify-content-end" data-bg-size=cover data-bg-position=center><img src=/assets/img/auth/login-bg.png alt=Image class=img-as-bg>
    </div>
    <div class="container-fluid d-flex flex-column">
        <div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
            <div class="col-sm-7 col-lg-6 col-xl-6 py-6 py-md-0">
                <div class="row justify-content-center">
                    <div class="col-11 col-lg-10 col-xl-6">
                        <div>
                            <div class=mb-5>
                                <h6 class="h3 mb-1">Reset Password!</h6>
                                <p class="text-muted mb-0">Enter your new secure password below</p>
                            </div><span class=clearfix></span>

                            <form action="/auth/password/reset" method="POSt">

                                @csrf

                                @if(request('message') && request('message') != 'undefined')
                                <div class="alert alert-danger">
                                    {{request('message')}}
                                </div>
                                @endif

                                @include('site.components._alert')

                                <div class=form-group>
                                    <label class=form-control-label>Email address</label>
                                    <div class="input-group input-group-merge">
                                        <input type=email class="form-control form-control-prepend disabled" value="{{$user->email}}" disabled required>
                                        <div class=input-group-prepend><span class=input-group-text><i data-feather=user></i></span></div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div>
                                            <label class=form-control-label>New Password</label>
                                        </div>
                                    </div>
                                    <div class="input-group input-group-merge">
                                        <input type="password" class="form-control form-control-prepend" placeholder=Password name="password" required>
                                        <div class=input-group-prepend><span class=input-group-text><i data-feather=key></i></span></div>
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <div>
                                            <label class=form-control-label>Retype Password</label>
                                        </div>
                                    </div>
                                    <div class="input-group input-group-merge">
                                        <input type="password" class="form-control form-control-prepend" placeholder=Password name="password_confirmation" required>
                                        <div class=input-group-prepend><span class=input-group-text><i data-feather=key></i></span></div>
                                    </div>
                                </div>

                                <div class=mt-4>
                                    <input type="hidden" name="token" value="{{$user->api_token}}">
                                    <button type="submit" class="btn btn-block btn-primary">Change Password</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>


@endsection