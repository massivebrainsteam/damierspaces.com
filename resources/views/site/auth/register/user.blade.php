@php
$nav = 'light';
$footer = '_footer_min';
@endphp

@extends('site.layout.app')

@section('content')

<section>

    <div class="container-fluid d-flex flex-column">
        <div class="col-12 d-flex justify-content-center align-items-center mt_large">
            <div>
                <div class="mb-5 mt-5">
                    <div class="text-center">
                        <h6 class="h3 mb-1">Create an account</h6>
                        <p class="text-muted mb-0">Complete your information in the form below.</p>
                    </div>
                </div>
                <span class=clearfix>
                </span>

                <form action="{{route('auth.user.register', ['code' =>  $code])}}" method="POST">

                    @csrf

                    @include('site.components._alert')

                    @if($team)
                    <div class=form-group>
                        <label class=form-control-label>Team:</label>
                        <input type="text" class="form-control form-control-prepend" placeholder="{{$team->name}}" value="{{$team->name}}" disabled readonly>
                    </div>
                    @endif

                    <div class="form-row">
                        <div class="form-group col">
                            <label class=form-control-label>Full Name:</label>
                            <div class="input-group  input-group-merge">
                                <input type="text" class="form-control form-control-prepend" placeholder="e.g: Bosun Jones" name="name" required>
                                <div class=input-group-prepend>
                                    <span class=input-group-text>
                                        <i data-feather=user></i>
                                    </span>
                                </div>
                            </div>
                            @error('name')
                            <span class="text-danger">{{$errors->first('name')}}</span>
                            @enderror
                        </div>

                        <div class="form-group col">
                            <label class=form-control-label>Email Address:</label>
                            <div class="input-group  input-group-merge">
                                <div class=input-group-prepend>
                                    <span class=input-group-text>
                                        <i data-feather=mail></i>
                                    </span>
                                </div>
                                <input type="email" name="email" placeholder="e.g: bosun@mail.com" class="form-control form-control-prepend" value="{{isset($email) ? $email : ''}}" required>
                            </div>
                            @error('email')
                            <span class="text-danger">{{$errors->first('email')}}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col">
                            <label class=form-control-label>Phone Number:</label>
                            <div class="input-group ">
                                
                                <input type="number" id="phone" class="form-control form-control-prepend" placeholder="Phone Number" name="phone" required>
                            </div>
                            @error('name')
                            <span class="text-danger">{{$errors->first('name')}}</span>
                            @enderror
                        </div>

                        <div class="form-group col">
                            <label class=form-control-label>Sex:</label>
                            <div class="input-group  input-group-merge">
                                <div class=input-group-prepend>
                                    <span class=input-group-text>
                                        <i data-feather=user></i>
                                    </span>
                                </div>
                                <select name="name" class="form-control form-control-prepend">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                            @error('sex')
                            <span class="text-danger">{{$errors->first('sex')}}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col">
                            <label class=form-control-label>Occupation:</label>
                            <div class="input-group  input-group-merge">
                                <input type="text" class="form-control form-control-prepend" placeholder="e.g: Web Developer" name="occupation" required>
                                <div class=input-group-prepend>
                                    <span class=input-group-text>
                                        <i data-feather=key></i>
                                    </span>
                                </div>
                            </div>
                            @error('name')
                            <span class="text-danger">{{$errors->first('name')}}</span>
                            @enderror
                        </div>

                        <div class="form-group col">
                            <label class=form-control-label>Country:</label>
                            <div class="input-group  input-group-merge">
                                <div class=input-group-prepend>
                                    <span class=input-group-text>
                                        <i data-feather=globe></i>
                                    </span>
                                </div>
                                <select name="country" id="country" class="form-control form-control-prepend">
                                </select>
                            </div>
                            @error('country')
                            <span class="text-danger">{{$errors->first('country')}}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col">
                            <label class=form-control-label>State:</label>
                            <div class="input-group  input-group-merge">
                                <div class=input-group-prepend>
                                    <span class=input-group-text>
                                        <i data-feather=map></i>
                                    </span>
                                </div>
                                <select name="state" id="state" class="form-control form-control-prepend">
                                </select>
                            </div>
                            @error('state')
                            <span class="text-danger">{{$errors->first('state')}}</span>
                            @enderror
                        </div>

                        <div class="form-group col">
                            <label class=form-control-label>City:</label>
                            <div class="input-group  input-group-merge">
                                <input type="text" class="form-control form-control-prepend" placeholder="e.g: Ikeja" name="city" required>
                                <div class=input-group-prepend>
                                    <span class=input-group-text>
                                        <i data-feather=map-pin></i>
                                    </span>
                                </div>
                            </div>
                            @error('city')
                            <span class="text-danger">{{$errors->first('city')}}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="col-lg-6">
                            <div class=form-group>
                                <label class=form-control-label>Password:</label>
                                <div class="input-group input-group-merge">
                                    <div class=input-group-prepend>
                                        <span class=input-group-text>
                                            <i data-feather="key"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password" class="form-control form-control-prepend" placeholder="******" required>
                                    <i class="far fa-eye togglePassword"></i>
                                </div>
                                @error('password')
                                <span class="text-danger">{{$errors->first('password')}}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class=form-group>
                                <label class=form-control-label>Confirm Password:</label>
                                <div class="input-group input-group-merge">
                                    <div class=input-group-prepend>
                                        <span class=input-group-text>
                                            <i data-feather="key"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="password_confirmation" class="form-control form-control-prepend" placeholder="******" required>
                                    <i class="far fa-eye togglePassword"></i>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class=mt-4>
                        <button type="submit" class="btn btn-block btn-primary">Continue</button>
                    </div>

                </form>

                <div class="mt-4 text-center">
                    <small>Not registered?</small> <a href="/auth/login" class="small font-weight-bold">Create account</a>
                </div>
            </div>
        </div>
    </div>

</section>

@endsection
