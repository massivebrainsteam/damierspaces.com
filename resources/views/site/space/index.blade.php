@extends('site.layout.app')

@section('content')

<section class="slice pt-md-8 pb-md-0 bg-section-secondary">
	<div class="container">

		<div class="mb-3">
			<h3 class="damier-dark">{{$space->name}}</h3>
			<p class="mb-2"><strong>Location</strong>: <i class="fas fa-map-marker-alt "></i> {{$space->location->address}}</p>
			<p class="mb-2"><strong>Maximum Capacity</strong>: {{$space->avaliable_capacity}} Person(s)</p>
			<p class="mb-2"><strong>Host</strong>: {{$space->host->name}}</p>
			<div class="mr-2 mb-2">
				<span class="mr-2">The Space is:</span>
				<span class="badge-primary2-outline text-capitalize badge"><span class="circle"></span> {{$space->space_status}}</span>
			</div>
			<span class="badge badge-primary2">{{$space->rating_average}} <i class="fas fa-star"></i></span> - Ratings
		</div>
		
		<div class="container">
			<div class="row">
				<div class="col-lg-12 space-slider max-vh-90   swiper-container space-swiper"> 
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<img src="{{$space->photo_url}}" class="img-fluid w-100" alt="">
						</div>
						@foreach($space->photos as $row)
						<div class="swiper-slide">
							<img src="{{$row->url}}" class="img-fluid w-100"  alt="">
						</div>
						@endforeach
					</div>

					<!-- If we need pagination -->
					<div class="swiper-pagination"></div>

					<!-- If we need navigation buttons -->
					{{-- <div class="swiper-button-prev swiper-button"></div>
					<div class="swiper-button-next swiper-button"></div> --}}
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-lg-6">
				<ul class="extend mb-0">
					<li>
						<a href="/space/{{$space->id}}/checkin" class="btn-fill btn mr-2">Check-in Now <i data-feather=check-circle></i></a>
						<a href="/auth/login" class="btn-outline btn">Reserve space <i data-feather=clock></i></a>
					</li>
				</ul>
			</div>
			<div class="col-lg-6 text-right">
				<ul class="nav float-right mt-3">
					<li>Share:</li>
					<li class=nav-item><a class=nav-link href=# target=_blank><i class="fab fa-facebook"></i></a></li>
					<li class=nav-item><a class=nav-link href=# target=_blank><i class="fab fa-twitter"></i></a></li>
					<li class=nav-item><a class=nav-link href=# target=_blank><i class="fab fa-whatsapp"></i></a></li>
					<li class=nav-item><a class=nav-link href=# target=_blank><i class="fab fa-linkedin"></i></a></li>
				</ul>
			</div>
		</div>

		<hr>

		<div class="row space-tabs">
			<div class="col-3">
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><i data-feather=arrow-right-circle></i> Space Details</a>
					<a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><i data-feather=arrow-right-circle></i> Amenities</a>
					<a class="nav-link" id="v-pills-reviews-tab" data-toggle="pill" href="#v-pills-reviews" role="tab" aria-controls="v-pills-reviews" aria-selected="false"><i data-feather=arrow-right-circle></i> Reviews</a>
				</div>
			</div>
			<div class="col-9">
				<div class="tab-content" id="v-pills-tabContent">

					<div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
						<div class="mb-5">
							<h4>Description</h4>
							<p>{{$space->description}}</p> 
						</div>


					</div>

					<div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">

						<div class="single-space-amenities">
							<h4>Amenities</h4>
							<h6 class="text-primary2 font-weight-400">BASICS</h6>
							<ul>
								@foreach($space->amenities as $row)
								<li><img src="{{$row->icon}}" style="width:20px"> {{$row->name}}</li>
								@endforeach
							</ul>
						</div>
						{{-- <h2 class="damier-dark">Location</h2> --}}
						{{-- <p class="pl-2"><i class="fas fa-map-marker-alt "></i> {{$space->location->address}}</p>
						<iframe src="https://maps.google.com/maps?q='{{$space->location->latitude}},{{$space->location->longitude}}&hl=es&z=14&amp;output=embed&zoom=18`" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> --}}
					</div>


					<div class="tab-pane fade" id="v-pills-reviews" role="tabpanel" aria-labelledby="v-pills-reviews-tab">
						<h2>Reviews</h2>

						@foreach($space->reviews as $row)
						<div class="review-box mb-5">
							<ul class="mb-2 d-flex align-items-center ">
								<li>
									<img src={{$row ->user->image ?? '/assets/img/ellipse.svg'}} class="shadow rounded-circle" alt="">
								</li>
								<li class="pl-3">
									<span class="font-weight-bold"> {{$row->user->name}}</span> <br>
									<small>
										<span>{{date('M d, Y', strtotime($row->created_at))	}}</span>
										<span class="   badge badge-primary2 ml-2"> {{$row->rating}}.0  <i class="fas fa-star open text-warning"></i></span>
									</small>
								</li>
							</ul>
							<p>{{$row->review}}</p>

							{{-- <ul class="pl-5"> 
								<li>
									<small class="mr-2">Rated</small>
								</li>
							</ul> --}}
						</div>
						@endforeach

					</div>
				</div>
			</div>
		</div>

		<hr>

	</div>
</div>
</section>

@endsection