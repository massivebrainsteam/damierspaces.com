@extends('site.layout.app')

@php
$nav = 'light';
$footer = '_footer_min';
@endphp

@section('content')

<section>

    <div class="bg-primary position-absolute h-100 top-0 left-0 zindex-100 col-lg-6 col-xl-6 zindex-100 d-none d-lg-flex flex-column justify-content-end" data-bg-size=cover data-bg-position=center>
        <img src="/assets/img/host-cac.png" alt=Image class=img-as-bg>
    </div>

    <div class="container-fluid d-flex flex-column">
        <div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
            <div class="col-sm-7 col-lg-6 col-xl-6 py-6 py-md-0">
                <div class="row justify-content-center">
                    <div class="col-11 col-lg-10 col-xl-6">
                        <div>
                            <div class=mb-5>
                                <div class="text-center">
                                    <h6 class="h3 mb-1">Invite Members</h6>
                                    <p class="text-muted mb-30">Please enter your team member's email address separated by comma (,)</p>
                                </div>

                                <span class="badge badge-primary"><strong>Max Team:</strong> 3 - Flexi Team Low</span>
                                
                            </div>
                            <form>
                                <div class=form-group>
                                    <label class=form-control-label>Email Address(es):</label>
                                    <div class="input-group input-group-merge">
                                        <input type=email class="form-control form-control-prepend" id=input-email placeholder="e.g: bosun@email.com, sarah@email.com, jane@email.com">
                                        <div class=input-group-prepend><span class=input-group-text><i data-feather=users></i></span></div>
                                    </div>
                                </div>
                                <div class=mt-4>
                                    <a href="../../../damier-spaces-user-dashboard/application/team(active)/dashboard.html" class="btn btn-primary">Skip</a>
                                    <button type="button" class="btn btn-primary success">Invite</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection