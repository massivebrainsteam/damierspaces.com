<!DOCTYPE html>
<html lang=en>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="description" content="Damier Spaces - Enjoy frictionless daily access to the best workspaces near you!">
    <meta name="author" content="Nativebrands">
    <title>{{isset($title) ? $title : 'Homepage'}} – Damier Spaces</title>
    <link rel=icon href="/assets/img/brand/favicon.png" type=image/png>
    <link rel=stylesheet href="/assets/css/damier-spaces.css" id=stylesheet>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    @stack('styles')
    @include('site.components._drift')
</head>

<body>

    @include('site.components._header')

    @yield('content')

    @include('site.components.'.(isset($footer) ? $footer : '_footer'))

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
    <script src="/assets/libs/sticky-kit/dist/sticky-kit.min.js"></script>
    <script src="/assets/libs/owl-carousel/src/js/owl.carousel.js"></script>
    <script src="/assets/libs/swiper/dist/js/swiper.min.js"></script>
    <script src="/assets/libs/intl-tel-input-17.0.0/build/js/intlTelInput.js"></script>
    <script src="/assets/js/custom.js"></script>
    <script src="/assets/js/damier.js"></script>
    <script src="/assets/js/countries.js"></script>
    @stack('scripts')

    <script type="text/javascript">
        feather.replace();
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>

    <script language="javascript">
        populateCountries("country", "state"); 
        // first parameter is id of country drop-down and second parameter is id of state drop-down
        populateCountries("country2");
        populateCountries("country2");
    </script>
</body>

</html>
