@php
$theme = 'dark';
@endphp

@extends('site.layout.app')

@push('styles')
<link rel=stylesheet href="/assets/libs/pricing-table/css/style.css" id=stylesheet>
@endpush

@section('content')

<section class="slice slice-lg pb-4 grey-bg pb-4">
    <div data-offset-top=#navbar-main>
        <div class="container position-relative zindex-100">
            <div class="row mb-5 justify-content-center text-center">
                <div class="col-lg-7 my-3 col-md-9">
                    <h3 class="h1 header-color">Flexible Pricing</h3>
                    <p class="lead damier-dark font-weight-500 mb-0">We have plans for everyone. Whether you want a
                        simple setup or would rather have a flexible solution with automated contract.</p>
                </div>
            </div>
            <ul class="nav nav-tabs justify-content-center mb-1" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active text-dark" id="personal-tab" data-toggle="tab" href="#personal" role="tab" aria-controls="personal" aria-selected="true"><i data-feather=user></i>Personal</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-dark" id="team-tab" data-toggle="tab" href="#team" role="tab" aria-controls="profile" aria-selected="false"><i data-feather=grid></i>Teams</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="personal" role="tabpanel" aria-labelledby="personal-tab">
                    <div class="row">
                        <div class="col-lg-12 mx-auto">
                            <div class="pricing-container">
                                <div class="pricing row my-3 no-gutters">

                                    @foreach($packages as $row)

                                    <div class=col-lg-4>
                                        <div class="card bg-section-secondary card-pricing">
                                            <div class="card-header pt-4 pb-2 border-0 delimiter-bottom">
                                                <span class="d-block text-uppercase mb-1">{{$row->name}}</span>
                                                <div class="h3 mb-0" data-pricing-value=40>
                                                    <span class="price  text-primary">{{_currency($row->amount)}}</span>
                                                </div>
                                            </div>
                                            <div class=card-body>
                                                <ul class="list-unstyled mb-4">
                                                    <li class="space-item"> <span>Slots per month</span> {{$row->total_slots}}</li>
                                                    <li class="space-item"> <span>Reservation</span> No</li>

                                                    @foreach($row->amenities()->take(5)->get() as $amenity)
                                                    <li>{{$amenity->amenity->name}}</li>
                                                    @endforeach
                                                </ul>

                                                <a href="/package/{{$row->id}}/subscribe" class="btn w-100  btn-lg btn-primary2 mb-3">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>

                                    @endforeach



                                </div>
                                {{--
                        <div class="text-center mb-5 mt-5">
                            <a class="text-center font-weight-bold" href="#compare"><u>Compare all Pricing</u></a>
                        </div> --}}
                            </div>



                        </div>



                    </div>
                </div>
                <div class="tab-pane fade" id="team" role="tabpanel" aria-labelledby="team-tab">
                    <div class="row">
                        <div class="col-lg-12 mx-auto">
                            <div class="pricing-container">
                                <div class="pricing row my-3 no-gutters">

                                    @foreach($packages as $row)

                                    <div class=col-lg-4>
                                        <div class="card bg-section-secondary card-pricing">
                                            <div class="card-header pt-4 pb-2 border-0 delimiter-bottom">
                                                <span class="d-block text-uppercase mb-1">{{$row->name}}</span>
                                                <div class="h3 mb-0" data-pricing-value=40>
                                                    <span class="price  text-primary">{{_currency($row->amount)}}</span>
                                                </div>
                                            </div>
                                            <div class=card-body>
                                                <ul class="list-unstyled mb-4">
                                                    <li class="space-item"> <span>Slots per month</span> {{$row->total_slots}}</li>
                                                    <li class="space-item"> <span>Reservation</span> No</li>

                                                    @foreach($row->amenities()->take(5)->get() as $amenity)
                                                    <li>{{$amenity->amenity->name}}</li>
                                                    @endforeach
                                                </ul>

                                                <a href="/package/{{$row->id}}/subscribe" class="btn w-100  btn-lg btn-primary2 mb-3">Subscribe</a>
                                            </div>
                                        </div>
                                    </div>

                                    @endforeach



                                </div>
                                {{--
                        <div class="text-center mb-5 mt-5">
                            <a class="text-center font-weight-bold" href="#compare"><u>Compare all Pricing</u></a>
                        </div> --}}
                            </div>



                        </div>



                    </div>
                </div>
            </div>
        </div>
</section>

{{-- <section id="compare" class="cd-products-comparison-table pt-5"> --}}
{{-- <header>
        <h2>Compare Packages</h2>

        <div class="actions">
            <a href="#0" class="reset">Reset</a>
            <a href="#0" class="filter">Filter</a>
        </div>
    </header> --}}

{{-- <div class="cd-products-table">
        <div class="features">
            <div class="top-info">Package Features</div>
            <ul class="cd-features-list">
                <li>Price</li>
                <li>Space Reservation</li>
            </ul>
        </div> 

        <div class="cd-products-wrapper">
            <ul class="cd-products-columns">

                @foreach($packages as $row)

                <li class="product">
                    <div class="top-info">
                        <div class="check"></div>
                        <h5>{{$row->name}}</h5>
</div>

<ul class="cd-features-list">
    <li>{{_currency($row->amount)}}</li>
    <li><i class='fa fa-{{$row->can_reserve_spaces == 'yes' ? 'check' : 'times'}}'></i></li>
</ul>
</li>

@endforeach

</ul>
</div>

<ul class="cd-table-navigation">
    <li><a href="#0" class="prev inactive">Prev</a></li>
    <li><a href="#0" class="next">Next</a></li>
</ul>
</div> --}}
{{-- </section> --}}

{{-- <section class="damier-light-bugundy">
    <div class="container">
        <div class= "row pb-2 pt-6">
            <div class="col-lg-5 text-lg-left">
                <h3 class="damier-dark mb-2">How can Damier space revolutionize the way you work?</h3>
                <p>Customise how payments can be made or split for single items, and choose to collect payment for one or more sub-merchants.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row pt-5 pb-5">
            <div class="col-4 text-lg-left">
                <img src="/assets/img/Group 445.svg" alt="">
                <h5 class="mt-2 mb-2 damier-dark">Our Mission</h5>
                <p>When we started WeWork in 2010, we wanted to build more than beautiful, shared office spaces.</p>
            </div>
            <div class="col-4 text-lg-left">
                <img src="/assets/img/inspire.svg" alt="">
                <h5 class="mt-2 mb-2 damier-dark">What inspires us</h5>
                <p>When we started WeWork in 2010, we wanted to build more than beautiful, shared office spaces.</p>
            </div>
            <div class="col-4 text-lg-left">
                <img src="/assets/img/value.svg" alt="">
                <h5 class="mt-2 mb-2 damier-dark">Our Values</h5>
                <p>When we started WeWork in 2010, we wanted to build more than beautiful, shared office spaces.</p>
            </div>
        </div>

        <div class="container">
            <div class="row mt-0 pb-6 justify-content-start">
                <a href="/page/contact" class="btn btn-primary">Contact Us</a>
            </div>
        </div>
    </div>
</section> --}}

@endsection

@push('scripts')
<script src="/assets/libs/pricing-table/js/main.js"> </script>
@endpush
