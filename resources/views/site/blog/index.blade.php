@php
$theme = 'light';
@endphp

@extends('site.layout.app')

@section('content')

<section class="slice slice-lg py-7 bg-cover bg-size--cover" style=background-image:url(/assets/img/backgrounds/img-5.jpg)><span class="mask bg-dark opacity-8"></span>
    <div class="container d-flex align-items-center" data-offset-top=#navbar-main>
        <div class="col py-5">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-7 col-lg-7 text-center">
                    <h1 class="display-4 text-white mb-2">Damier Insider</h1><span class="text-white text-sm">We write for a better culture and work-life tips.</span></div>
                </div>
            </div>
        </div>
    </section>
    <section class=border-bottom>
        <div class="container py-3">
            <div class="row align-items-center">
                <div class="col-lg-7 mb-4 mb-lg-0">
                    <ul class=nav>
                        <li class=nav-item>
                            <a class="nav-link text- active" href=#>Business</a>
                        </li>
                        <li class=nav-item>
                            <a class=nav-link href="{{_cms_url()}}">Lifestyle</a>
                        </li>
                        <li class=nav-item>
                            <a class=nav-link href="{{_cms_url()}}">Technology</a>
                        </li>
                        <li class=nav-item>
                            <a class=nav-link href="{{_cms_url()}}">Entrepreneur</a>
                        </li>
                        <li class=nav-item>
                            <a class=nav-link href="{{_cms_url()}}">Work-Life</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="slice pt-5 pb-7 bg-section-secondary">
        <div class=container>
            <div class=row>
                @foreach($posts as $row)
                <div class="col-xl-4 col-md-6">
                    <div class="card hover-translate-y-n3 hover-shadow-lg overflow-hidden">
                        <div class="position-relative overflow-hidden">
                            <a href="{{_cms_url($row->link)}}" class="d-block">
                                <img alt="Image placeholder" src="{{$row->image}}" class="card-img-top">
                            </a>
                        </div>
                        <div class="card-body py-4">
                            <small class="d-block text-sm mb-2">{{$row->date}}</small> 
                            <a href="{{_cms_url($row->link)}}" class="h5 stretched-link lh-150">{{$row->title}}</a>
                            <p class="mt-3 mb-0 lh-170">{{$row->excerpt}}</p>
                        </div>
                        <div class="card-footer border-0 delimiter-top">
                            <div class="row align-items-center">
                                <div class=col-auto>
                                    <span class="avatar avatar-sm bg-primary rounded-circle">DS</span> 
                                    <span class="text-sm mb-0 avatar-content">{{ucwords($row->user)}}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>

        @endsection