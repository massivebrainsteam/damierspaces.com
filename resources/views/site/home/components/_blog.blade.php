<section>
    <div class="container">
        <div class="row pt-7 mb-4 justify-content-center text-center">
            <div class="col-lg-4">
                <p class="mb-0">DAMIER INSIDER</p>
                <h4>We write for a better culture and work-life tips.</h4>
            </div>
        </div>
        <div class="row">

            @foreach($posts->take(3) as $row)
            <div class="col-lg-4 h-50 ">
                <div class="card damier-light-bugundy border-0 p-0  ">
                    <img src="{{$row->image}}" class="card-img-top" alt="#">
                    <div class="card-body blog-card">
                        <p class="card-title mb-0"><small> BUSINESS</small></p>
                        <p class="card-text damier-color font-weight-bolder">
                            <strong>
                                <a href="{{_cms_url($row->link)}}">{{$row->title}}</a>
                            </strong>
                        </p>
                        <p class="excerpt">{{$row->excerpt}}</p>
                        <div class="row">
                            <div class="col-sm-7 pl-3">
                                <p><small>{{$row->date}}</small></p>
                            </div>
                            <div class="col-sm-5 p-0">
                                <a href="{{_cms_url($row->link)}}" class="text-dark font-weight-bold d-flex align-items-center">Read More <i data-feather="chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
        <div class="container">
            <div class="row mt-2 mb-6 justify-content-center">
                <a href="/blog" class="btn btn-primary">Read More</a>
            </div>
        </div>
    </div>

</section>
