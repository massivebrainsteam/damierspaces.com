<section class="slice-lg damier-bg">
    <div class=container>
        <div class="row">
            <div class="col-lg-8 offset-lg-2 mb-3 text-center">
                <h3 class="text-white">Let’s get a space that perfectly fits you.</h3>
                <p class="text-white">
                    Co-working, Private Offices, Custom Built, Dedicated Desk,<br>Hot Desk,
                    Virtual Offices, Meeting Room, etc.
                </p>
            </div>

            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="pricing-filter">

                        <h4 class="border-bottom pb-2 pt-3 text-center mb-4">Space Calculator</h4>

                        <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active text-dark" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i data-feather=user></i> Individual</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-dark" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i data-feather=grid></i> Team</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">

                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                                @component('site.home.components._calculator', ['id' => 'individual-calculator', 'url' => '/api/auth/find-packages/user', 'app' => 'individual']) @endcomponent

                            </div>

                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                                @component('site.home.components._calculator', ['id' => 'team-calculator', 'url' => '/api/auth/find-packages/team', 'app' => 'team']) @endcomponent

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>