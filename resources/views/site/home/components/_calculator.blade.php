<div class="row align-items-center" id="{{$id}}">
    <div class="col-lg-6">
        <div class="left-col">
            <div class="filter-form">

                <div class="animated fadeIn" v-if="error.length > 0">
                    <div class="alert alert-danger">
                        @{{error}}
                    </div>
                </div>

                {{-- <div class="form-group">
                    <label class=form-control-label>Hi, what’s your name?</label>
                    <div class="input-group input-group-merge">
                        <input type=email class="form-control form-control-prepend" placeholder="First name only: (e.g: Bosun)" v-model="name">
                        <div class=input-group-prepend>
                            <span class=input-group-text>
                                <i data-feather=user></i>
                            </span>
                        </div>
                    </div>
                </div> --}}

                <div class="form-group">
                    <label class=form-control-label>How many times would you need a space in a month?</label>
                    <div class="range-slider">
                        <input class="range-slider__range" type="range" value="10" min="0" max="31" v-model="spaces_per_month">
                        <span class="range-slider__value">@{{spaces_per_month}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <label class=form-control-label>How many spaces would you like access to?</label>
                    <div class="range-slider">
                        <input class="range-slider__range" type="range" value="10" min="0" max="31" v-model="locations_per_month">
                        <span class="range-slider__value">@{{locations_per_month}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class=form-control-label>What’s your budget? (₦)</label>
                    <div class="range-slider">
                        <input class="range-slider__range" type="range" value="10000" min="5000" max="100000" v-model="budget">
                        <span class="range-slider__value">@{{budget}}</span>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col d-flex">

                        <button type="button" v-if="calculating" class="btn btn-outline check-pricing disabled" disabled>
                            <i class="fa fa-spin fa-spinner"></i> Loading...
                        </button>

                        <button type="button mr-auto" v-else v-on:click="calculate" class="btn btn-outline check-pricing">
                            Calculate
                        </button>
                    </div>

                    <div class="col text-right">
                        <a class="reset-pricing" href="javascript:;" v-on:click="reset">
                            <i data-feather=refresh-cw></i> Reset Form
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="right-col animated fadeIn" v-if="packages.length == 0 && calculated == true">
            <h5 class="text-center">Opzz! Thre seem to be no packcages based on your filter. </h5>
        </div>

        <div class="right-col animated fadeIn" v-if="packages.length == 0 && calculated == false">
            <h5 class="text-center">Enter your space considerations and find out how much space you need!</h5>
            <ul>
                <li>
                    <img src="/assets/img/adjust.svg">
                    <p>Adjust the
                        <br>filters
                    </p>
                </li>
                <li>
                    <img src="/assets/img/find.svg">
                    <p>Get the
                        <br>package
                    </p>
                </li>
                <li>
                    <img src="/assets/img/start.svg">
                    <p>Let’s get
                        <br>started!
                    </p>
                </li>
            </ul>
        </div>
        <div class="right-col animated fadeIn" v-if="packages.length > 0 && calculated == true">
            <div class="text-center">
                <img :src="`/assets/img/${icon}`" alt="">
                <h5 class="text-center mb-0">You are a @{{title}}</h5>
                <p>Here are our top recommendations</p>
            </div>
            <div id=accordion-1 class="accordion accordion-spaced">

                <div class="card" v-for="row in packages">

                    <div class="card-header py-4" data-toggle=collapse role=button :data-target="'#collapse-'+row.id" aria-expanded=false aria-controls=collapse-1-1>
                        <h6 class=mb-0><i data-feather=layers class=mr-3></i>@{{row.name}}</h6>
                    </div>

                    <div :id="'collapse-'+row.id" class=collapse aria-labelledby=heading-1-1 data-parent=#accordion-1>
                        <div class=card-body>
                            <table class="table table-hover">
                                <tbody>
                                    <tr>
                                        <td>Monthly Subsription</td>
                                        <td class="text-right">@{{row.amount}}</td>
                                    </tr>
                                    <tr>
                                        <td>Slots Per Month</td>
                                        <td class="text-right">@{{row.total_slots}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row mt-4">
                                <div class="col-lg-12 d-block">
                                    <a :href="row.url" class="btn btn-fill">Subscribe</a><br><br>
                                    <small>Note: All packages last for 30 days.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="help-text" v-if="calculated">
        <p>If you can’t find a suitable pricing <a href="/packages">Click Here</a></p>
    </div>
</div>

@push('scripts')

<script type="text/javascript">

    var {{$app}} = new Vue({

        el: '#{{$id}}',

        data: {

            type: '{{$id}}',
            name: '',
            spaces_per_month: 10,
            locations_per_month: 10,
            budget: 10000,
            calculated: false,
            packages: [],
            title: '',
            icon: '',
            error: '',
            calculating: false
        },

        methods: {

            async calculate(){

                try{

                    this.error = '';
                    this.packages = [];
                    this.title = '';
                    this.calculated = false;
                    this.calculating = true;

                    let response = await axios.post('{{$url}}', this.$data);

                    this.packages = response.data.data;
                    this.title = response.data.title;
                    this.icon = response.data.icon;
                    this.calculated = true;
                    this.calculating = false;

                }catch(error){

                    this.error = error.response.data.data.message;
                    this.calculated = true;
                    this.calculating = false;
                    document.getElementById('{{$id}}').scrollIntoView()
                }
            },

            reset(){

                this.name = '';
                this.spaces_per_month = 10;
                this.locations_per_month = 10;
                this.budget = 5000;
                this.calculated = false;
                this.packages = [];
                this.error = '';

            }
        }
    })
</script>

@endpush