    <!-- Slider main container -->
    <div class="card-deck mx-0 swiper-container featured_spaces w-100 ">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            @foreach($featured as $row)
            <div class="swiper-slide">
                <a href="/space/{{$row->id}}">
                <div class="card bg-none featured-card" style="background-image: url({{$row->photo_url}})">

                    <div class="card-img-overlay">
                        <div class="container">
                            <ul class="d-flex">
                                <li><span class="badge badge-white text-capitalize"><span class="circle"></span> {{$row->space_status}}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body bg-white">
                        <h5 class="card-title mb-0 text-primary2 ">{{$row->name}}</h5>
                        <p class="mb-0"><strong>Host:</strong> {{$row->host->name}}</p>
                        <p class="mb-0"> <strong class="font-weight-bold">Location:</strong> {{$row->location->address}}</p>
                        <div class="d-flex justify-content-between border-top border-bottom py-2 mt-3">
                            <ul class="mb-0 d-flex">
                                @foreach($row->simple_amenities as $icon)
                                <li data-toggle="tooltip" data-placement="top" class="mr-3" title="{{$icon['name']}}">
                                    <img src="{{_image($icon['icon'], 20, 20)}}" style="width:20px; height:20px" alt="{{$icon['name']}}">
                                </li>
                                @endforeach
                                <li data-toggle="tooltip" data-placement="top" title="More Amenities"><img src="/assets/img/more-horizontal.png" alt=""></li>
                            </ul>
                            <div class="d-flex align-items-center" data-toggle="tooltip" data-placement="top" title="Maximum Persons (Capacity)">
                                <i data-feather=user class=mr-1></i>
                                <p class="mb-0">{{$row->avaliable_capacity}}</p>
                            </div>
                        </div>

                        <div class="d-flex align-items-center pt-2">
                            <i data-feather=map-pin class="mr-1 text-primary2"></i>
                            <p class=mb-0>Ikeja, Lagos</p>
                        </div>

                        <div class="d-flex mt-3 pb-3 border-bottom">  <!-- align-items-center justify-content-between  -->
                            <div class=" text-sm-right p-0">
                                <a href="/space/{{$row->id}}" class="btn tx-12 px-2 btn-outline btn-sm">View details</a>
                            </div>
                            <div class="text-right rating-text">
                                <span class="badge badge-primary2 tx-12 font-weight-400 text-center">5.0 <i style="color: #fff" class="fas fa-star"></i></span>
                                <span class="tx-12"> - {{count($row->reviews)}} Reviews</span>
                            </div>

                        </div>
                        <div class="p-3 text-center">
                            <p>Space Package: <strong>{{$row->package ?? 'Flexi Low'}}</strong></p>
                        
                        </div>

                    </div>
                </a>
                </div>
            </div>
            @endforeach
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev swiper-button">
            <i data-feather="chevron-left"></i>
        </div>
        <div class="swiper-button-next swiper-button">
            <i data-feather="chevron-right"></i>
        </div>
    </div>
