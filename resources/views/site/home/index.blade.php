@extends('site.layout.app')

@section('content')

<section class="slice py-5 damier-light-bugundy damier-hero">
    <div class="mt-5 container">
        <div class="row row-grid align-items-center">
            <div class="col-12 col-md-5 col-lg-6 order-md-2 text-right">
                <figure class=w-100><img alt="Image placeholder" src=/assets/img/bg-home.png class="img-fluid mw-md-120"></figure>
            </div>
            <div class="col-12 col-md-7 col-lg-6 order-md-1 pr-md-5">
                <div class="caption-left">
                </div>
                <h2 class=" text-center text-md-left mb-3 hero-text">Enjoy frictionless daily access to the best workspaces near you!</h2>
                <p class="text-center text-md-left">The ultimate workspace experience, with ultimate flexibility. Access more than 50 coworking spaces across 20 cities, with a single membership.</p>

                <form class="mt-5" action="/auth/user" method="GET">

                    <div class="input-group input-group-lg mb-3" id="hero-signup">
                        <div class="input-group input-group-merge">
                            <div class=input-group-prepend>
                                <span class="input-group-text border-0 pr-2">
                                    <i data-feather=mail></i>
                                </span>
                            </div>
                            <input type="email" class="form-control border-0 px-1" placeholder="Enter your email address" name="email" required>
                            <div class=input-group-append>
                                <span class="input-group-text border-0 py-0 pl-2 pr-1">
                                    <button type="submit" class="btn btn-sm btn-primary">
                                        Get Started
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>

                    @if(session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                    @endif

                </form>

                <div class="mt-4">
                    <a type="button"  data-toggle="modal" data-target="#video" class="d-flex align-items-center" data-fancybox="">
                        <span class="btn btn-white btn-icon-only rounded-circle shadow-sm hover-scale-110">
                            <span class="btn-inner--icon text-sm">
                                <i class="fa fa-play text-primary"></i>
                            </span>
                        </span>
                        <small class="text-dark ml-2">See how it works</small>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section>
<section class="slice slice-lg mt-4">
    <div class=container>
        <div class=row>
            <div class=col-lg-6>
                <h5 class="lh-180 mt-4 mb-6">
                    <img src="/assets/img/spaces-logo.png" class="img-fluid" alt="">
                </h5>
            </div>
        </div>
        <div class="row pb-6">
            <div class="col-lg-6 offset-lg-3 col-md-6 offset-md-3 col-sm-6 offset-sm-3">
                <h3 class="mb-4 text-center">Damier helps you connect with co-working space around you!</h3>
                <p class="text-center">The ultimate workspace experience, with ultimate flexibility. Access more than 50
                    coworking spaces across 20 cities.</p>
            </div>
        </div>
    </div>
</section>

<!-- Vertically centered scrollable modal -->
<div class="modal fade" id="video" tabindex="-1" aria-labelledby="videonLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-lg">
    <iframe id="video" class="w-100" height="450" src="https://www.youtube.com/embed/V5he1JXiQbg" frameborder="0" allowfullscreen>
    </iframe>
</div>
</div>


<section class="grey-bg">
    <div class="container">
        <div class="row  pb-4 justify-content-md-between">
            <div class="col-lg-7 top">
                <img src="/assets/img/home-space.svg" alt="" class="img-fluid">
            </div>
            <div class="col-lg-5 pt-2 pt-lg-10">
                <h4>Book any space at your own convenience</h4>
                <p>The ultimate workspace experience, with ultimate flexibility. Access more than 50 coworking
                    spaces across 20 cities, with a single membership.</p>
                <a class="btn btn-primary" href="/page/how-it-works">How it works <i class="fas fa-chevron-right pl-2"></i></a>
            </div>
        </div>
        <hr>
        <div class="row  py-4 justify-content-md-between">
            <div class="col-lg-5 pt-6 mb-5 mb-lg-0 pr-6">
                <h4>Bring your team on board!</h4>
                <p>The ultimate workspace experience, with ultimate flexibility. Access more than 50 spaces across 20 cities, with a single membership..</p>
                <a class="btn btn-primary" href="/auth/team">Get Started<i class="fas fa-chevron-right pl-2"></i></a>
            </div>
            <div class="col-lg-7 team">
                <img src="/assets/img/team-1.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>

@include('site.home.components._space_calculator')

<section class="grey-bg">
    <div class="container pb-7">
        <div class="row justify-content-md-between pt-10 align-items-center">
            <div class="col-lg-4  pt-5">
                <h4>Own a space?<br>Come on board!</h4>
                <p>The ultimate workspace experience, with ultimate flexibility. Access more than 50 coworking
                    spaces across 20 cities, with a single membership.</p>
                <a href="/become-a-host" class="btn btn-primary" href="#">Become a Host</a>
            </div>
            <div class="col-lg-6 pt-5">
                <img src="/assets/img/Group 828.png" class="img-fluid" alt="">
            </div>

        </div>

       
        @if($featured->count() > 0)
        <div class="container">
            <div class="row justify-content-md-between">
                <div class="col-lg-6 pt-7 mb-4">
                    <h4 class="mb-0">Featured Spaces</h4>
                    <p>Explore some of our top featured workspaces.</p>
                </div>
                <div class="col-lg-6 d-lg-block d-flex pt-1 pt-lg-7 mb-4">
                    <a href="/auth" class="btn btn-outline">See all Featured Spaces <i data-feather=chevron-right></i></a>
                </div>
            </div>
        </div>

        @include('site.home.components._featured_spaces')

        @endif

</section>

@include('site.home.components._blog')

{{-- <div class="space-lg"></div> --}}

@endsection
