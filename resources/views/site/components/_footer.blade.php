<footer class=position-relative id=footer-main>
    <a id="button">
        <i data-feather="arrow-up"></i>
    </a>

    <div class="footer-dark damier-footer">
        <div class="container pt-4">
            <div class="row justify-content-center">
                <div class=col-lg-12>
                    <div class="row align-items-center">
                        <div class=col-lg-7>
                            <h3 class="h3 font-weight-normal mb-0 opacity-8">Setup a free account and start booking any space at your own convenience!</h3>
                        </div>

                        <div class="d-flex col-lg-5">
                            <a href="/page/choose-account" class="btn btn-outline my-2 mt-4 mt-lg-0 ml-0 ml-lg-3 ml-lg-auto">
                                <i data-feather=user-plus></i> Create an Account
                            </a>
                        </div>
                        {{-- <div class="col-lg-5 mt-4 mt-lg-0">
                        </div> --}}
                    </div>
                </div>
            </div>
            <hr class="divider divider-fade divider-dark my-5">
            <div class="row">
                <div class="col-lg-8">
                    <a href=# class="float-left mr-3"><img src="/assets/img/app-stores.png" alt=""></a>
                    <p class="fw-on-mobile">Download out apps on Apple App Store or Google Play Store and book your spaces on the go!</p>
                </div>
                <div class="col">
                    <div class="divider-hr"></div>
                </div>
                <div class="col-lg-3">
                    <p>Give us a call <a href="tel:+2348098765432" class="font-weight-bold damier-color"><u>+234 809 876 5432</u></a> and we can set you up or check our <a href="/packages" class="font-weight-bold damier-color"><u>Package Plans</u></a>.</p>
                </div>
            </div>


            <hr class="divider divider-fade divider-dark mt-4 mb-5">
            <div class=row>
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <a href="/"><img alt="Image placeholder" src=/assets/img/brand/dark.svg id=footer-logo></a>
                    <p class="mt-4 text-sm opacity-8 pr-lg-4">For over 5 years, we pride ourselves on our commitment to excellence, as well as our ability to deliver for our customers.</p>
                    <ul class="nav mt-4">
                        <li class=nav-item><a class=nav-link href=# target=_blank><i class="fab fa-facebook"></i></a></li>
                        <li class=nav-item><a class=nav-link href=# target=_blank><i class="fab fa-twitter"></i></a></li>
                        <li class=nav-item><a class=nav-link href=# target=_blank><i class="fab fa-instagram"></i></a></li>
                        <li class=nav-item><a class=nav-link href=# target=_blank><i class="fab fa-linkedin"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-6 col-sm-4 ml-lg-auto mb-5 mb-lg-0">
                    <h6 class="heading mb-3">Company</h6>
                    <ul class=list-unstyled>
                        <li><a href="/page/about">About Us</a></li>
                        <li><a href="/page/how-it-works">How it Works</a></li>
                        <li><a href="/packages">Package Plans</a></li>
                        <li><a href="/page/careers">Careers</a></li>
                        <li><a href="/page/contact">Contact</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-6 col-sm-4 mb-5 mb-lg-0">
                    <h6 class="heading mb-3">Resources</h6>
                    <ul class=list-unstyled>
                        <li><a href="/blog">Blog</a></li>
                        <li><a href="/page/faq">FAQs</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-6 col-sm-4 mb-5 mb-lg-0">
                    <h6 class="heading mb-3">Contact</h6>
                    <p><strong>Address:</strong><br>3a,John Obasi Kalu Close, Behind Ocean Crest School, Oniru Lekki</p>
                    <p><strong>Email:</strong><br>hello@damierspaces.com</p>
                    <p><strong>Phone:</strong><br>+234 809 8765 432</p>
                </div>
            </div>
            <hr class="divider divider-fade divider-dark my-4">
            <div class="row align-items-center justify-content-md-between pb-4">
                <div class=col-md-6>
                    <div class="copyright text-sm  text-center text-md-left">&copy; {{date('Y')}} <a href="/" class="text-dark font-weight-bold" target=_blank>Damier Spaces</a>. All rights reserved | Built by <a href="https://nativebrands.co" class="damier-color font-weight-bold" target="_blank">Nativebrands</a></div>
                </div>
                <div class=col-md-6>
                    <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                        <li class=nav-item>
                            <a class=nav-link href="/page/terms">Legal and Terms</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
