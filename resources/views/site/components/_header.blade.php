@php
$theme = isset($theme) ? $theme : 'dark';
$nav = isset($nav) ? $nav : '';
@endphp

<header class="auth" id="header-main">
    <nav class="navbar navbar-main navbar-expand-xl navbar-{{$nav}}" id=navbar-main>
        <div class=container>

            <a class=navbar-brand href="/">
                <img src=/assets/img/brand/{{$theme}}.svg id=navbar-logo>
            </a>

            <button class=navbar-toggler type=button data-toggle=collapse data-target=#navbar-main-collapse aria-controls=navbar-main-collapse aria-expanded=false aria-label="Toggle navigation">
                {{-- <span class=navbar-toggler-icon></span> --}}
                <i data-feather="menu" class=navbar-toggler-icon></i>
            </button>

            <div class="collapse navbar-collapse navbar-collapse-overlay" id=navbar-main-collapse>

                <div class=position-relative>
                    <button class=navbar-toggler type=button data-toggle=collapse data-target=#navbar-main-collapse aria-controls=navbar-main-collapse aria-expanded=false aria-label="Toggle navigation"><i data-feather=x></i></button>
                </div>

                <ul class="navbar-nav ml-lg-auto {{$theme}}-navigation">

                    <li class="nav-item nav-item-spaced d-none d-lg-block">
                        <a class="nav-link" href="/page/about">About</a>
                    </li>

                    <li class="nav-item nav-item-spaced d-none d-lg-block">
                        <a class="nav-link" href="/page/how-it-works">
                            How it Works
                        </a>
                    </li>

                    <li class="nav-item nav-item-spaced d-none d-lg-block">
                        <a class="nav-link" href="/become-a-host">
                            Become a Hosts
                        </a>
                    </li>

                    <li class="nav-item nav-item-spaced d-none d-lg-block">
                        <a class=nav-link href="/packages">
                            Pricing
                        </a>
                    </li>

                    <li class="nav-item nav-item-spaced dropdown dropdown-animate" data-toggle=hover>
                        <div class="dropdown-menu dropdown-menu-md p-0">
                            <div class="list-group list-group-flush px-lg-4">

                                <a href="/page/about" class="list-group-item list-group-item-action" role=button>
                                    <div class=d-flex>
                                        <span class=h6>
                                            <i data-feather=chevron-right></i>
                                        </span>
                                        <div class=ml-3>
                                            <h6 class="heading mb-0">About</h6>
                                        </div>
                                    </div>
                                </a>

                                <a href="/page/how-it-works" class="list-group-item list-group-item-action" role=button>
                                    <div class=d-flex>
                                        <span class=h6>
                                            <i data-feather=chevron-right></i>
                                        </span>
                                        <div class=ml-3>
                                            <h6 class="heading mb-0">How it Works</h6>
                                        </div>
                                    </div>
                                </a>

                                <a href="/become-a-host" class="list-group-item list-group-item-action" role=button>
                                    <div class=d-flex><span class=h6>
                                            <i data-feather=chevron-right></i>
                                        </span>
                                        <div class=ml-3>
                                            <h6 class="heading mb-0">Become a Hosts</h6>
                                        </div>
                                    </div>
                                </a>

                                <a href="/packages" class="list-group-item list-group-item-action" role=button>
                                    <div class=d-flex>
                                        <span class=h6>
                                            <i data-feather=chevron-right></i>
                                        </span>
                                        <div class=ml-3>
                                            <h6 class="heading mb-0">Pricing</h6>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>

                <ul class="navbar-nav align-items-lg-center d-none d-lg-flex ml-lg-auto {{$theme}}-buttons">

                    @if(Auth::check())

                    <li class=nav-item>
                        <a href="/auth" class="btn btn-sm btn-primary btn-icon ml-3">
                            <span class=btn-inner--icon>
                                <i data-feather="user"></i>
                            </span>
                            <span class=btn-inner--text>My Account</span>
                        </a>
                    </li>

                    <li class=nav-item>
                        <a href="/auth/logout" class="btn btn-sm btn-outline-primary btn-icon ml-3">
                            <span class=btn-inner--icon>
                                <i data-feather="log-out"></i>
                            </span>
                            <span class=btn-inner--text>Logout</span>
                        </a>
                    </li>

                    @else

                    <li class=nav-item>
                        <a href="/page/choose-account" class="btn btn-sm btn-outline-primary btn-icon ml-3">
                            <span class=btn-inner--icon>
                                <i data-feather="user-plus"></i>
                            </span>
                            <span class=btn-inner--text>Create an Account</span>
                        </a>
                    </li>

                    <li class=nav-item>
                        <a href="/auth/login" class="btn btn-sm btn-primary btn-icon ml-3">
                            <span class=btn-inner--icon>
                                <i data-feather="log-in"></i>
                            </span>
                            <span class=btn-inner--text>Login</span>
                        </a>
                    </li>
                    @endif


                </ul>

                <div class="d-lg-none px-4 text-center">
                    
                    @if(Auth::check())

                    <a href="/auth" class="btn btn-sm btn-primary btn-block btn-icon">
                        <span class=btn-inner--icon>
                            <i data-feather="user"></i>
                        </span>
                        <span class=btn-inner--text>My Account</span>
                    </a>


                    <a href="/auth/logout" class="btn btn-sm btn-block  btn-outline-primary btn-icon">
                        <span class=btn-inner--icon>
                            <i data-feather="log-out"></i>
                        </span>
                        <span class=btn-inner--text>Logout</span>
                    </a>


                    @else

                    <a href="/page/choose-account" class="btn btn-block  btn-sm btn-outline-primary btn-icon">
                        <span class=btn-inner--icon>
                            <i data-feather="user-plus"></i>
                        </span>
                        <span class=btn-inner--text>Create an Account</span>
                    </a>

                    <a href="/auth/login" class="btn btn-sm btn-block mx-0 btn-primary btn-icon">
                        <span class=btn-inner--icon>
                            <i data-feather="log-in"></i>
                        </span>
                        <span class=btn-inner--text>Login</span>
                    </a>

                    @endif
                </div>



            </div>

        </div>
    </nav>
</header>
