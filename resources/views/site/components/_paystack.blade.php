<script type="text/javascript" src="https://js.paystack.co/v2/inline.js"></script>

<script type="text/javascript">

    button = $('.pay');

    @php
    $public_key = env('PAYSTACK_STATUS', 'test') == 'test' ? env('PAYSTACK_PUBLIC_KEY') : env('PAYSTACK_PUBLIC_KEY');
    @endphp

    function payWithPaystack(email = '', amount = 0, orderid = '', redirecturl = '')
    {
        button.prop('disabled', true).html("<i class='fa fa-spin fa-spinner'></i> Loading Gateway...");

        var handler = PaystackPop.setup({

            key         : '{{$public_key}}',
            email       : email,
            amount      : amount*100,
            metadata    : {orderid : orderid },

            callback    : response => {

                button.prop('disabled', true).html("<i class='fa fa-spin fa-spinner'></i> Verifying Payment...");
                
                var url = '{{url('paystack/verify/')}}'+'/'+orderid+'/'+response.reference+'/{{$type}}';

                fetch(url).then(resp => resp.json()).then(response => {

                    if(response.status == true){

                        window.location = redirecturl+'?reference='+response.payment_reference;

                    }else{

                        button.prop('disabled', false).html('Payment Failed. Click to try again');
                    }

                })
            },
            onClose : function(){

                button.prop('disabled', false).html('Payment Failed. Click to try again');
            }
        });

        handler.openIframe();
    }

</script>