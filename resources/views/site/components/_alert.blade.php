@if (session('message'))

    <div class="alert alert-success" role="alert">
        {{ session('message') }}
    </div>

@endif

@if (session('error'))

    <div class="alert alert-danger" role="alert">
        {{ session('error') }}
    </div>

@endif

@if ($errors->any())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $row)
            <p>{{ $row }}</p>
        @endforeach
    </div>
@endif
