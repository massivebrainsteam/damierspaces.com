 <footer class=relative-footer>
     <div class="container pt-5">
         <div class="row align-items-center justify-content-md-between pb-4">
             <div class=col-md-6>
                 <div class="copyright text-sm  text-center text-md-left">&copy; {{date('Y')}} <a href="/" class="text-dark font-weight-bold" target=_blank>Damier Spaces</a>. All rights reserved | Built by <a href="https://nativebrands.co" class="damier-color font-weight-bold" target="_blank">Nativebrands</a></div>

             </div>
             <div class=col-md-6>
                 <ul class="nav justify-content-center justify-content-md-end mt-3 mt-md-0">
                     <li class=nav-item><a class=nav-link href="/">Home</a></li>
                     <li class=nav-item><a class=nav-link href="/page/how-it-works">How It Works</a></li>
                     <li class=nav-item><a class=nav-link href="/host">Host Login</a></li>
                     <li class=nav-item><a class=nav-link href="/page/faq">FAQs</a></li>
                     <li class=nav-item><a class=nav-link href="page/contact">Contact</a></li>
                 </ul>
             </div>
         </div>
     </div>
 </footer>
