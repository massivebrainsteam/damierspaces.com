@extends('site.layout.app')

@section('content')

<section class="slice py-6 pt-lg-7 pb-lg-8 damier-grey">
    <div class="container d-flex align-items-center text-center text-lg-left">
        <div class="col px-0">
            <div class="row row-grid align-items-center">
                <div class=col-lg-7>
                    <h1 class="h1 damier-dark text-center text-lg-left mt-4 mb-2">We’re happy to help!</h1>
                    <p class="lead damier-dark text-center text-lg-left font-weight-500">Need a quick answer? Enter your question below for instant responses.</p>
                    <div class="mt-5 text-center text-lg-left"><a href=#sct-form-contact data-scroll-to class="btn btn-primary btn-lg btn-icon"><span class=btn-inner--icon> </span><span class="btn-inner--text text-white">Write Us<i class="fas fa-chevron-right pl-md-2"></i></span></a></div>
                    <div class="d-flex align-items-center justify-content-center justify-content-lg-left mt-5"></div>
                </div>
                <div class="row w-100">
                    <div class="col-6">
                        <div class="card text-left hover-translate-y-n10 hover-shadow-lg">
                            <div class="px-md-4 py-md-4">
                                <div class=>
                                    <div class="icon text-primary mx-auto"><img src="/assets/img/support.svg" alt=""></div>
                                </div>
                                <h1 class=" word damier-dark pt-1 mb-0">General Support</h1>
                                <p class="mb-0 tx-14">+234 809 876 5432</p>
                                <p class="tx-14">support@damierspaces.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card text-left hover-translate-y-n10 hover-shadow-lg">
                            <div class="px-md-4 py-md-4">
                                <div class=>
                                    <div class="icon text-primary mx-auto"><img src="/assets/img/partnership.svg
                                        " alt=""></div>
                                </div>
                                <h1 class=" word damier-dark pt-2 mb-0">Partnership, Host, Investors</h1>
                                <p class="mb-0 tx-14">+234 809 876 5432</p>
                                <p class="tx-14">enquiries@damierspaces.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="slice slice-lg damier-light-bugundy pt-5" id=sct-form-contact>
    <div class="container position-relative zindex-100">
        <div class="row justify-content-center">
            <div class=col-lg-6>

                <div class="mb-5">
                    <h3 class="damier-dark">Get In touch</h3>
                    <p class=lh-190>If there's something we can help you with, jut let us know. We'll be more than happy to offer you our help</p>

                </div>
                <form id=form-contact action="/contact" method="POST">

                    @csrf

                    @include('site.components._alert')

                    <div class="form-row">
                        <div class="col-lg-6  form-group ">
                            <input name="name" class="form-control  form-control-lg" type=text placeholder="Your name" required>
                        </div>
                        <div class="col-lg-6  form-group ">
                            <input name="email" class="form-control form-control-lg" type=email placeholder=email@example.com required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-lg-6 form-group ">
                            <input name="phone" id="phone" class="form-control form-control-lg" type="number" placeholder="Phone Number" required>
                        </div>
                        <div class="col-lg-6  form-group 0">
                            <select name="name" class="form-control w-100  form-control-lg">
                                <option selected>Contact Purpose:</option>
                                <option value="business-purposes">Business Purposes</option>
                                <option value="partnership">Partnership</option>
                                <option value="enquiries">Enquiries</option>
                                <option value="feedbacks">Feedbacks</option>
                                <option value="comments">Comments</option>
                                <option value="suggestion">Suggestion</option>
                            </select>
                        </div>
                    </div>

                    <div class=form-group>
                        <input name="subject" class="form-control form-control-lg" type=text placeholder="Subject" required>
                    </div>
                    <div class=form-group>
                        <textarea name="message" class="form-control form-control-lg" data-toggle=autosize placeholder="Your message..." rows=5 required></textarea>
                    </div>
                    <div class=text-center>
                        <button type=reset class="btn-reset d-none"></button>
                        <button type="submit" class="btn btn-block btn-lg btn-primary mt-4">Send your message</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-6 damier-dark mt-5">
                <h6 class="mb-0 text-color">OUR OFFICE</p>
                    <h3 class="damier-dark mt-1">3a,John Obasi Kalu Close, Behind Ocean Crest School, Oniru Lekki</h3>
                    <p class="lead font-weight-bold my-3">Email: <a href=# class="text-color">support@damierspaces.com</a>
                        <br>Phone: <span class="font-weight-bold text-primary">(+234) 809-876-5432</span><br><br></p>


                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1013.7674074658519!2d3.4654320458719816!3d6.427257490796381!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103bf5a5e38d9ead%3A0x188d52a4b1223278!2sTEM%20Academy%20Nigeria%20Limited.!5e0!3m2!1sen!2sng!4v1610375831107!5m2!1sen!2sng" width="550" class="img-flid" height="320" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

            </div>


        </div>
    </div>
</section>



@endsection
