@extends('site.layout.app')

@php
$nav = 'light';
$footer = '_footer_min';
@endphp

@section('content')
<section class="damier-grey">
	<div class="container">
		<div class="row">
			<div class="text-center col-12 mt-5 mb-5">
				<h2 class="damier-color" style="margin-top:100px !important">Create A Team</h2>
				<img src="/assets/img/damier-team.png" alt="Create a team">

				<div class="col-8 offset-2">
					<p class="mt-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam magnam accusamus voluptatum, quasi cupiditate iusto nulla autem. Dolores nulla eaque placeat distinctio, quis nobis molestias error culpa ad mollitia, aliquam.</p>
					<a href="/teams/invite" class="btn btn-fill float-none">Proceed</a>
				</div>

			</div>

		</div>
	</div>
</section>
@endsection
