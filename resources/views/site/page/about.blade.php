@extends('site.layout.app')

@section('content')

<section class="slice py-8 bg-white bg-cover bg-size--cover about-inro"><span class="mask bg-gradient-white opacity-9"></span>
        <div data-offset-top=#navbar-main>
            <div class="container d-flex align-items-center text-center text-lg-left py-5">
                <div class="col px-0">
                    <div class="row row-grid align-items-center justify-content-center">
                        <div class="col-lg-7 text-center text-lg-left">
                            <h1 class="mb-4 text-center damier-color">About Us</h1>
                            <p class="lead text-center mb-lg-2"><strong>Create a world where people work to make a life, not just a living.</strong></p>
                            <p class="text-center opacity-10">For over 5 years, we pride ourselves on our commitment to excellence, as well as our ability to deliver for our customers. To make the world’s spaces connected, accessible and productive.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="slice slice-lg pb-0 pb-lg-5 damier-light-bugundy">
        <div class=container>
            <div class=about-img>
               <img src="/assets/img/about-bg.png" alt="">
            </div>
        </div>
    </section>
    <section class="damier-light-bugundy">
        <div class="container">
            <div class="pt-3 pb-2 w-100">
                <img src="/assets/img/divider.png" alt="">
            </div>
            <div class= "row pb-2">
                <div class="col-lg-4 text-lg-left">
                    <h3 class="text-black mb-2">Building the next frontier</h3>
                    <p>Create a world where people work to make a life, not just a living.</p>
                </div>
            </div>
        </div>
        <div class="container  pb-6">
            <div class="row pt-5 pb-3">
                <div class="col-lg-4 text-lg-left">
                    {{-- <img src="/assets/img/Group 445.svg" alt=""> --}}
                    <img src="/assets/img/mission.svg" alt="">

                    <h5 class="mt-2 mb-2">Our Mission</h5>
                    <p>When we started WeWork in 2010, we wanted to build more than beautiful, shared office spaces.</p>
                </div>
                <div class="col-lg-4 text-lg-left">
                    <img src="/assets/img/inspire-1.svg" alt="">
                    <h5 class="mt-2 mb-2">What inspires us</h5>
                    <p>When we started WeWork in 2010, we wanted to build more than beautiful, shared office spaces.</p>
                </div>
                <div class="col-lg-4 text-lg-left">
                    <img src="/assets/img/value.svg" alt="">
                    <h5 class="mt-2 mb-2">Our Values</h5>
                    <p>When we started WeWork in 2010, we wanted to build more than beautiful, shared office spaces.</p>
                </div>
            </div>

            <a class="btn btn-primary" href="/">Contact Us <i data-feather="chevron-right"></i></a>
        </div>
    </section>
    <section class="rounded-lg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 mt-4 pt-4" >
                    <h3 class="text-center text-wrap mb-2">What we do</h3>
                    <p class="text-center">When we started Damier Spaces in 2010, we wanted to build more than beautiful, shared office spaces.
                        We wanted to build a community. A place you join as an individual, 'me', but where you become part 
                        of a greater 'we'. A place where we’re redefining success measured by personal fulfillment.
                    </p>
                </div>
            </div> 
        </div>
        <hr width="75%" class="opacity-1">
        <div class="row pt-3 pb-4 justify-content-center">
            <div class="col-lg-5">
                <h3 class="mb-2 text-center">We’re happy to find incredible offices</h3>
                <p class="text-center">Join the ranks of industry leaders turning to Damier Spaces for their commercial real estate needs.</p>
            </div>
        </div>
        <div class="container"> 
            <img src="/assets/img/Vector 62.svg" class="img-fluid justify-content-center">
        </div>
        <div class="container pt-4 pb-6">
            <div class="row justify-content-center">
                <img src="/assets/img/clients 1.png" class="img-fluid">
            </div>
        </div>
    </section>

    <section class="grey-bg">
        <div class="container">
            <div class="row pt-7 mb-3">
                <div class="col-lg-4 text-lg-left pr-lg-4">
                    <h2>Our Team</h2>
                    <p>When we started WeWork in 2010, we wanted to build more than beautiful, shared office spaces.</p>
                    <a class="btn btn-primary mb-4" href="/page/careers">Join Our Team</a>
                </div>
                <div class="col-lg-4">
                    <img src="/assets/img/Rectangle 319.png" class="img-fluid rounded-lg">
                    <p class="mb-0 mt-2"><strong>Bosun Jones</strong></p>
                    <p ><small>PRINCIPAL PARTNER</small></p>
                </div>
                <div class="col-lg-4">
                    <img src="/assets/img/Rectangle 320.png" class="img-fluid rounded-lg">
                    <p class="mb-0 mt-2"><strong>Kosara Okafor</strong></h5>
                    <p><small>HEAD, TECHNOLOGY</small></p>
                </div>
            </div>
            <div class="row mb-3 justify-content-md-end">
                <div class="col-lg-4 col-12">
                    <img src="/assets/img/Rectangle 321.png" class="w-100 img-fluid rounded-lg">
                    <p class="mb-0 mt-2"><strong>Mordi Chukwu</strong></p class="mb-0 mt-2">
                    <p><small>PEOPLE HR</small></p>
                </div>
                <div class="col-lg-4 col-12">
                    <img src="/assets/img/Rectangle 322.png" class="w-100 img-fluid rounded-lg">
                    <p class="mb-0 mt-2"><strong>Ngozi Udeagbara</strong></p class="mb-0 mt-2">
                    <p><small>CUSTOMER SUPPORT</small></p>
                </div>
            </div>
            <div class="row pb-5 justify-content-md-end">
                <div class="col-lg-4 col-12">
                    <img src="/assets/img/Rectangle 343.png" class="w-100 img-fluid rounded-lg">
                    <p class="mb-0 mt-2"><strong>Ijeoma Chukwu</strong></p class="mb-0 mt-2">
                    <p><small>PEOPLE HR</small></p>
                </div>
                <div class="col-lg-4 col-12">
                    <img src="/assets/img/Rectangle 344.png" class="w-100 img-fluid rounded-lg">
                    <p class="mb-0 mt-2"><strong>Ahmadu Haruna</strong></p class="mb-0 mt-2">
                    <p><small>CUSTOMER SUPPORT</small></p>
                </div>
            </div>
        </div>
    </section>

@endsection