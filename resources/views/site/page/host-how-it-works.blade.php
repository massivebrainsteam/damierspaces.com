@extends('site.layout.app')

@section('content')

<section class="slice pt-md-8 pb-md-0 ">
    <div data-offset-top=#navbar-main>
        <div class="container position-relative zindex-100">
            <div class="row align-items-center">
                <div class="col-12 col-md-6 order-md-2 mb-5 mb-md-0">
                    <div class="position-relative left-5 left-md-0">
                        <figure><img alt="Image placeholder" src=/assets/img/first-space.png class="img-fluid mw-md-130 mw-lg-100 rounded perspective-md-right"></figure>
                    </div>
                    <div class="card shadow-lg mb-3 col-8 col-md-6 col-lg-6 px-2 py-3 position-absolute bottom-n6 bottom-md-n5 left-4 left-md-n4 z-index-100">
                        <div class="text-center h-40">
                            <img alt="Image placeholder" src="/assets/img/workstationNG.svg" class="">

                            <div class="mb-3">
                                <span class="d-block text-sm text-dark opacity-8 mb-3">200 Spaces</span>
                                <a href="javascript:;" class="btn btn-sm btn-primary btn-icon rounded-pill shadow hover-translate-y-n3">
                                    <span class="btn-inner--icon text-white font-weight-normal"><i class="fas fa-map-marker-alt"></i></span>
                                    <span class="btn-inner--text text-white font-weight-normal">20 Locations</span>

                                </a>
                            </div>
                            <span class=mb-3>Our name is Workstation NG
                                The leading provider of spaces across Lagos. </span>


                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 order-md-1 pr-lg-5 mt-5 mt-md-0">
                    <h1 class="display-4 font-weight-bolder mb-3">How it Works for Hosts<strong class="d-block font-weight-normal h4">Create a world where people work to make a life, not just a living.</strong></h1>
                    <p class=" text-muted">For over 5 years, we pride ourselves on our commitment to excellence, as well as our ability to deliver for our customers. To make the world’s spaces connected, accessible and productive.</p>
                    <div class="">
                        <div class="d-flex mb-3">
                            <a href="/become-a-host" class="btn btn-fill">Sign Up Now <i data-feather=chevron-right></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="shape-container shape-line shape-position-bottom">
        <svg width=2560px height=100px xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink preserveAspectRatio=none x=0px y=0px viewBox="0 0 2560 100" style="enable-background:new 0 0 2560 100" xml:space=preserve class="">
            <polygon points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
</section>
<section class=" pb-md-0">
    <div class="damier-light-bugundy">
        <div class="container position-relative zindex-100">
            <div class="row align-items-center">
                <div class="col-12 col-md-6 order-md-1 mb-5 mb-md-0">
                    <div class="position-relative left-5 left-md-0 pt-8 pb-8">
                        <figure><img alt="Image placeholder" src=/assets/img/second-space.png class="img-fluid"></figure>
                    </div>
                    <div class="card shadow-lg mb-3 col-8 col-md-6 col-lg-6 px-0 position-absolute bottom-n6 bottom-md-n5 right-4 right-md-n4 z-index-100">
                        <div class="px-md-4 py-md-4  text-center h-40">
                            <div class="row align-items-center justify-content-center pt-3">
                                <div class="col-md-12 text-center">
                                    <h5 class="damier-dark">Payment <br> Settlement</h5>
                                    <div>
                                        <div class="sub px-md-3">
                                            <p class="mb-0 damier-dark font-weight-bold">N300,000</p>
                                            <span class="damier-color">Paid</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 mt-3">
                                    <i class="fas text-primary display-4  fa-check-circle"></i> </li>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 order-md-2 pl-lg-6 mt-5 mt-md-0">
                    <h1 class="display-6 font-weight-bolder mb-3">Earn from your available space(s)</h1>
                    <p class=" text-muted">For over 5 years, we pride ourselves on our commitment to excellence, as well as our ability to deliver for our customers. To make the world’s spaces connected, accessible and productive.</p>
                    <ul class="mb-3">
                        <li class="float-left">
                            <img src="/assets/img/wifi-multi-color.svg" class=mr-3>
                        </li>
                        <li class="pl-md-5">
                            <h1 class="word font-weight-bolder mb-0">Manage your reports</h1>
                            <p class=" text-muted">Make more money from spaces, the </p>
                        </li>
                    </ul>
                    <ul class="mb-3">
                        <li class="float-left">
                            <img src="/assets/img/space-icon.svg" class=mr-3>
                        </li>
                        <li class="pl-md-5">
                            <h1 class="word font-weight-bolder mb-0">Give your space more exposure</h1>
                            <p class=" text-muted">Make more money from spaces, the </p>
                        </li>
                    </ul>
                    <a href="/become-a-host" class="btn-fill mb-5 mt-4 btn btn-primary">Become a Host<i class="fas fa-chevron-right pl-md-2"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="">
        <div class="container">
            <div class="text-center">
                {{-- Features --}}
                <div>
                    <div class="pt-5">
                        <h1 class="display-6 font-weight-bolder mb-3 text-dark">Features</h1>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 mb-4  text-left">
                            <ul>
                                <li class="float-left">
                                    <img src="/assets/img/reserved-space-on-go.svg">
                                </li>
                                <li class="pl-md-6">
                                    <h1 class="word font-weight-bolder mb-0">Reserve space on the go</h1>
                                    <p class=" text-muted">By using damier spaces, this
                                        allows you to reserve spaces </p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 mb-4  text-left">
                            <ul>
                                <li class="float-left">
                                    <img src="/assets/img/find-spaces.svg">
                                </li>
                                <li class="pl-md-6">
                                    <h1 class="word font-weight-bolder mb-0">Find spaces seamlesslessly</h1>
                                    <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4  mb-4 text-left">
                            <ul>
                                <li class="float-left">
                                    <img src="/assets/img/sub-model.svg">
                                </li>
                                <li class="pl-md-6">
                                    <h1 class="word font-weight-bolder mb-0">Subscription based model</h1>
                                    <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4  mb-4 text-left">
                            <ul>
                                <li class="float-left">
                                    <img src="/assets/img/mark-or-wrong.svg">
                                </li>
                                <li class="pl-md-6">
                                    <h1 class="word font-weight-bolder mb-0">Reserve space on the go</h1>
                                    <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 text-left mb-4">
                            <ul>
                                <li class="float-left">
                                    <img src="/assets/img/sub-model.svg">
                                </li>
                                <li class="pl-md-6">
                                    <h1 class="word font-weight-bolder mb-0">Find spaces seamlesslessly</h1>
                                    <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 text-left mb-4">
                            <ul>
                                <li class="float-left">
                                    <img src="/assets/img/custom-user-icon.svg">
                                </li>
                                <li class="pl-md-6">
                                    <h1 class="word font-weight-bolder mb-0">Subscription based model</h1>
                                    <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mb-4 d-flex align-items-center justify-content-center">
                        <a href="/page/faq" class="btn-fill btn btn-primary">Frequently Asked Questions<i class="fas fa-chevron-right pl-md-2"></i></a>
                    </div>
                </div>

                <div class="text-center pt-6 billboard-image ">
                    <img src="/assets/img/billboard-host.png" class="img-fluid" alt="">
                </div>
                <div class="d-flex align-items-center">
                    <a href=# class="mr-3"><img src="/assets/img/get-it-on-google-play.png"></a>
                    <a href=# class="mr-3"><img src="/assets/img/get-on-app-store.png"></a>
                </div>
                <div class="pt-3 text-left">
                    <p>Cross Platform experience on any smartphone,<br> tablets and modern devices.</p>
                </div>
                <div class="pt-3 pb-3 w-100 text-left">
                    <img src="/assets/img/divider.png" alt="">
                </div>
                <div>
                    <div class="col-lg-8">
                        <h1 class="display-6 font-weight-bold mb-0 text-dark text-left">End to end booking system flows for multi-party workspaces</h1>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="position-relative pb-md-0">
    <div class="">
        <div class="container position-relative zindex-100">
            <div class="row align-items-center">
                <div class="col-12 col-md-6 order-md-1 mb-5 mb-md-0">
                    <div class="position-relative left-5 left-md-0 pt-5 pb-5">
                        <figure><img alt="Image placeholder" src=/assets/img/third-space.png class="img-fluid  mw-lg-100 rounded "></figure>
                    </div>
                    <div class="card shadow-lg mb-3 col-8 col-md-6 col-lg-6 px-0 position-absolute bottom-n6 bottom-md-n5 right-4 right-md-n4 z-index-100">
                        <div class="card-body text-center h-40">
                            <div class="metas">
                                <div class="row pb-md-3">
                                    <div class="col-8 basic">
                                        <span class="status">
                                            <i class="fas fa-circle open"></i>
                                            Open
                                        </span>
                                    </div>
                                    <div class="col-4 text-right ">
                                        <span class="save-unsave save-space">
                                            <img src="/assets/img/love.png" alt="">
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8 basic">
                                        <ul>
                                            <li data-toggle="tooltip" data-placement="top" title="High Speed Wifi"><img src="/assets/img/wifi.svg" alt=""></li>
                                            <li data-toggle="tooltip" data-placement="top" title="Free Coffee"><img src="/assets/img/coffee.svg" alt=""></li>
                                            <li data-toggle="tooltip" data-placement="top" title="Printer"><img src="/assets/img/printer.svg" alt=""></li>
                                            <li data-toggle="tooltip" data-placement="top" title="More Amenities"><img src="/assets/img/more-horizontal.png" alt=""></li>
                                        </ul>
                                    </div>
                                    <div class="col-4 text-right basic">
                                        <span class="user-count text-dark" data-toggle="tooltip" data-placement="top" title="Space Capacity"><i data-feather="user"></i> 12</span>
                                    </div>
                                    <div class="pt-md-2 pl-md-2 basic">
                                        <h5><a class="damier-color" href="javascript:;">Meeting Room</a></h5>
                                        <p>360 Ikorodu Rd, Maryland, Lagos</p>
                                        <ul class="float-left">
                                            <li><span class="space-rating">3.5 <i class="fas fa-star"></i></span></li>
                                            <li class="damier-size"><span>- 0 Reviews</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 order-md-2 pl-md-6 mt-5 mt-md-0">
                    <h1 class="display-6 font-weight-bolder mb-3">Gain more exposure
                        by listing your space</h1>
                    <p class="text-color">Customise how payments can be made or split for single items, and choose to collect payment for one or more sub-merchants.</p>
                    <p class="text-color">Multiple payment methods are accepted, and payouts can be made as they happen.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="position-relative pb-md-0">
    <div class="">
        <div class="container position-relative zindex-100">
            <div class="row align-items-center pt-lg-5">
                <div class="col-12 col-md-6 order-md-2 mb-5 mb-md-0">
                    <div class="position-relative left-5 left-md-0 pt-5 pb-5">
                        <figure><img alt="Image placeholder" src=/assets/img/subscription.png class="img-fluid  mw-lg-100 rounded "></figure>
                    </div>
                    <div class="card shadow-lg mb-3 col-8 col-md-6 col-lg-5 px-0 position-absolute bottom-n6 bottom-md-n5 right-4 right-md-n4 z-index-100">
                        <div class="px-md-3 py-md-4 text-center h-40">
                            <div class="metas">
                                <div class="row mt-3">
                                    <div class="col-md-12 sales">
                                        <ul>
                                            <li>
                                                <p class="damier-dark font-weight-bold">Dedicated Desk</p>
                                            </li>
                                            <li class="place">
                                                <p>Ikeja</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12 py-md-3">
                                        <div class="timeline timeline-one-side" data-timeline-content="axis" data-timeline-axis-style="dashed">
                                            <div class="timeline-block">
                                                <span id="timeline-step">
                                                    <i class="fas fa-circle damier-color"></i>
                                                </span>
                                                <div class="mr-md-5">
                                                    <small class="damier-color font-weight-bold">5pm - 6pm</small>
                                                </div>
                                            </div>
                                            <div class="timeline-block">
                                                <span id="timeline-step">
                                                    <i class="fas fa-circle damier-color"></i>
                                                </span>
                                                <div class="mr-md-5">
                                                    <small class="damier-color font-weight-bold">4pm - 5pm</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 order-md-1 mt-5 mt-md-0">
                    <h1 class="display-6 font-weight-bolder mb-3">Detailed reports about
                        every activity</h1>
                    <p class=" text-muted">Customise how payments can be made or split for single items, and choose to collect payment for one or more sub-merchants.</p>
                    <p class=" text-muted">Multiple payment methods are accepted, and payouts can be made as they happen.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<hr>
</div>
</section>

@endsection
