@php
$nav = 'light';
$footer = '_footer_min';
@endphp

@extends('site.layout.app')

@section('content')

<section>

    <div class="container-fluid">
        <div class="row align-items-center justify-content-center  min-vh-90">
            <div class="mt-3">
                <div class=mb-5>
                    <div class="text-center">
                        <h6 class="h3 mb-1" style="margin-top: 150px">Create an account</h6>
                        <p class="text-muted mb-0">Hey!, let’s know who you are.</p>
                    </div>

                </div><span class=clearfix></span>
                <form>

                    <div class="row">
                        <div class="account-selection">
                            <ul>
                                <li data-toggle="tooltip" data-placement="top" title="Single account for Individuals, Freelancers and Solopreneurs.">
                                    <a href="/auth/user">
                                        <div class="account-choose user">
                                            <div class="action">
                                                <i data-feather=user></i> Personal Account
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li data-toggle="tooltip" data-placement="top" title="Group/Corporate account for Companies or Team with multiple user accounts.">
                                    <div class="account-choose host">
                                        <a href="/auth/team">
                                            <div class="account-choose">
                                                <div class="action">
                                                    <i data-feather=grid></i> Team Account
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </form>
                <div class="mt-4 text-center mb-5"><small>Already a User?</small> <a href="/auth/login" class="small font-weight-bold">Continue to Login</a></div>
            </div>
        </div>
    </div>

</section>

@endsection
