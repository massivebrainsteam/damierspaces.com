@php
$theme = 'light';
@endphp

@extends('site.layout.app')

@section('content')

<section class="slice slice-lg bg-cover bg-size--cover" style=background-image:url(/assets/img/FAQ.svg)>
    <div class="container pt-5">
        <div class="row justify-content-center">
            <div class="col-lg-9 mb-5">
                <h2 class="mb-4 text-center text-white">Frequently Asked Questions</h2>
            </div>
        </div>
    </div>
    <div class="container position-relative zindex-100">
        <div class="row justify-content-center">
            <div class="col-lg-9 col-xl-12">
                <div class=row>
                    <div class="col-xl-3  col-lg-6 mb-xl-n8">
                        <div class="card damier-bg hover-translate-y-n10 hover-shadow-lg">
                            <div class="card-body p-5  text-center">
                                <img src="/assets/img/users.svg" alt="">
                                <div class=pt-2><a href=# class="h6 stretched-link text-white font-weight-normal mb-0">Users Questions</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 mb-xl-n8">
                        <div class="card hover-translate-y-n10 hover-shadow-lg">
                            <div class="card-body p-5 text-center">
                                <img src="/assets/img/grid.svg" alt="">
                                <div class=pt-2><a href=# class="h6 stretched-link damier-dark font-weight-normal mb-0">Team Questions</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 mb-xl-n8">
                        <div class="card hover-translate-y-n10 hover-shadow-lg">
                            <div class="card-body p-5 text-center">
                                <img src="/assets/img/home.svg" alt="">
                                <div class=pt-2><a href=# class="h6 stretched-link damier-dark font-weight-normal mb-0">Hosts Questions</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 mb-xl-n8">
                        <div class="card hover-translate-y-n10 hover-shadow-lg">
                            <div class="card-body p-5 text-center">
                                <img src="/assets/img/help-circle.svg" alt="">
                                <div class=pt-2><a href=# class="h6 stretched-link damier-dark font-weight-normal mb-0">Other Questions</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shape-container shape-position-bottom">
        <svg xmlns=http://www.w3.org/2000/svg viewBox="0 0 1000 100" preserveAspectRatio=none>
            <path d="M 0 0 c 0 0 200 50 500 50 s 500 -50 500 -50 v 101 h -1000 v -100 z"></path>
        </svg>
    </div>
</section>
<section class="slice slice-lg" id=sct-faq>
    <div class=container>
        <div class="row row-grid">
            <div class=col-lg-3>
                <div data-toggle=sticky data-sticky-offset=50>
                    <div class=card>
                        <div class="list-group list-group-flush">
                            <a href=#theme-integration data-scroll-to data-scroll-to-offset=50 class="list-group-item list-group-item-action d-flex justify-content-between">
                                <div><i data-feather=help-circle class=mr-2></i> <span>General Questions</span></div>
                                <div><i data-feather=chevron-right></i></div>
                            </a>
                            <a href=#customization data-scroll-to data-scroll-to-offset=50 class="list-group-item list-group-item-action d-flex justify-content-between">
                                <div><i data-feather=help-circle class=mr-2></i> <span>Registration</span></div>
                                <div><i data-feather=chevron-right></i></div>
                            </a>
                            <a href=#fonts-and-colors data-scroll-to data-scroll-to-offset=50 class="list-group-item list-group-item-action d-flex justify-content-between">
                                <div><i data-feather=help-circle class=mr-2></i> <span>Packages</span></div>
                                <div><i data-feather=chevron-right></i></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 ml-lg-auto">
                <div class="mb-5">
                    <h4 class="mb-4 damier-dark" id=theme-integration >General Questions</h4>
                    <div id=accordion-1 class="accordion accordion-spaced">
                        <div class=card>
                            <div class="card-header py-4" id=heading-1-1 data-toggle=collapse role=button data-target=#collapse-1-1 aria-expanded=false aria-controls=collapse-1-1>
                                <h6 class=mb-0><i data-feather=chevron-right class=mr-3></i>How can I reserve a space</h6></div>
                                <div id=collapse-1-1 class=collapse aria-labelledby=heading-1-1 data-parent=#accordion-1>
                                    <div class=card-body>
                                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                                    </div>
                                </div>
                            </div>
                            <div class=card>
                                <div class="card-header py-4" id=heading-1-2 data-toggle=collapse role=button data-target=#collapse-1-2 aria-expanded=false aria-controls=collapse-1-2>
                                    <h6 class=mb-0><i data-feather=chevron-right class=mr-3></i>Are there rules that come with spaces</h6></div>
                                    <div id=collapse-1-2 class=collapse aria-labelledby=heading-1-2 data-parent=#accordion-1>
                                        <div class=card-body>
                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class=card>
                                    <div class="card-header py-4" id=heading-1-3 data-toggle=collapse role=button data-target=#collapse-1-3 aria-expanded=false aria-controls=collapse-1-3>
                                        <h6 class=mb-0><i data-feather=chevron-right class=mr-3></i>What are the top spaces you will recommend</h6></div>
                                        <div id=collapse-1-3 class=collapse aria-labelledby=heading-1-3 data-parent=#accordion-1>
                                            <div class=card-body>
                                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right py-4"><a href=#theme-integration data-scroll-to data-scroll-to-offset=50 class="text-sm font-weight-bold">Back to top<i data-feather=chevron-up class=ml-2></i></a></div>
                            </div>
                            <div class=mb-md>
                                <h4 class=mb-4 id=customization>Registration</h4>
                                <div id=accordion-2 class="accordion accordion-spaced">
                                    <div class=card>
                                        <div class="card-header py-4" id=heading-2-1 data-toggle=collapse role=button data-target=#collapse-2-1 aria-expanded=false aria-controls=collapse-2-1>
                                            <h6 class=mb-0><i data-feather=chevron-right class=mr-3></i>Where will my registration link be sent</h6></div>
                                            <div id=collapse-2-1 class=collapse aria-labelledby=heading-2-1 data-parent=#accordion-2>
                                                <div class=card-body>
                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=card>
                                            <div class="card-header py-4" id=heading-2-2 data-toggle=collapse role=button data-target=#collapse-2-2 aria-expanded=false aria-controls=collapse-2-2>
                                                <h6 class=mb-0><i data-feather=chevron-right class=mr-3></i>Are there other ways to log in into my account</h6></div>
                                                <div id=collapse-2-2 class=collapse aria-labelledby=heading-2-2 data-parent=#accordion-2>
                                                    <div class=card-body>
                                                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=card>
                                                <div class="card-header py-4" id=heading-2-3 data-toggle=collapse role=button data-target=#collapse-2-3 aria-expanded=false aria-controls=collapse-2-3>
                                                    <h6 class=mb-0><i data-feather=chevron-right class=mr-3></i>What details will you be collecting upon registration</h6></div>
                                                    <div id=collapse-2-3 class=collapse aria-labelledby=heading-2-3 data-parent=#accordion-2>
                                                        <div class=card-body>
                                                            <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-right py-4"><a href=#customization data-scroll-to data-scroll-to-offset=50 class="text-sm font-weight-bold">Back to top<i data-feather=chevron-up class=ml-2></i></a></div>
                                        </div>
                                        <div class=mb-md>
                                            <h4 class=mb-4 id=fonts-and-colors>Packages</h4>
                                            <div id=accordion-3 class="accordion accordion-spaced">
                                                <div class=card>
                                                    <div class="card-header py-4" id=heading-3-1 data-toggle=collapse role=button data-target=#collapse-3-1 aria-expanded=false aria-controls=collapse-3-1>
                                                        <h6 class=mb-0><i data-feather=chevron-right class=mr-3></i>How long does it take for a package to expire</h6></div>
                                                        <div id=collapse-3-1 class=collapse aria-labelledby=heading-3-1 data-parent=#accordion-3>
                                                            <div class=card-body>
                                                                <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class=card>
                                                        <div class="card-header py-4" id=heading-3-2 data-toggle=collapse role=button data-target=#collapse-3-2 aria-expanded=false aria-controls=collapse-3-2>
                                                            <h6 class=mb-0><i data-feather=chevron-right class=mr-3></i>My new package is refusing to activate</h6></div>
                                                            <div id=collapse-3-2 class=collapse aria-labelledby=heading-3-2 data-parent=#accordion-3>
                                                                <div class=card-body>
                                                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class=card>
                                                            <div class="card-header py-4" id=heading-3-3 data-toggle=collapse role=button data-target=#collapse-3-3 aria-expanded=false aria-controls=collapse-3-3>
                                                                <h6 class=mb-0><i data-feather=chevron-right class=mr-3></i>Are there more flexible plans</h6></div>
                                                                <div id=collapse-3-3 class=collapse aria-labelledby=heading-3-3 data-parent=#accordion-3>
                                                                    <div class=card-body>
                                                                        <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="text-right py-4"><a href=#fonts-and-colors data-scroll-to data-scroll-to-offset=50 class="text-sm font-weight-bold">Back to top<i data-feather=chevron-up class=ml-2></i></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>

                                    @endsection