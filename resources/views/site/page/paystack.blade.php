@php
    $theme = 'light';
@endphp

@extends('site.layout.app')

@section('content')

<section class="slice pt-md-8 pb-md-0 bg-section-secondary mb-10">
    <span class="mask bg-gradient-dark opacity-9"></span>
    
    <div class="row mt-10">
        <div class="col-4"></div>
        <div class="col-4">
            @if(isset($error))
            <div class="alert alert-danger">
                {{$error}}
            </div>
            @endif

            @if(isset($message))
            <div class="alert alert-success">
                {{$message}}
            </div>
            @endif
        </div>
    </div>
</section>

@endsection