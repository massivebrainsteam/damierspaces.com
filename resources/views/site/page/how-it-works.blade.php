@extends('site.layout.app')

@section('content')

<section class="slice pt-md-8 pb-md-0 ">
    <div data-offset-top=#navbar-main>
        <div class="container position-relative zindex-100">
            <div class="row align-items-center">
                <div class="col-12 col-md-6 order-md-2 mb-5 mb-md-0">
                    <div class="position-relative left-5 left-md-0">
                        <figure><img alt="Image placeholder" src=/assets/img/how-it-works.png class="img-fluid mw-md-130 mw-lg-100 rounded perspective-md-right"></figure>
                    </div>
                    <div class="card shadow-lg mb-3 col-8 col-md-6 col-lg-5 px-0 position-absolute bottom-n6 bottom-md-n5 left-4 left-md-n4 z-index-100">
                        <div class="card-body px-lg-4 pt-3 text-center h-100">
                            <img src="/assets/img/checked-in.svg" />
                            <p class="mt-4 mb-0 font-weight-500">You are currently checked in at:</p>
                            <p class="damier-color font-weight-bold">Dedicated Desk</p>
                            <hr class="divider divider-fade my-3">
                            <p class="mb-0"><small><strong>Host:</strong> Workstation NG</small></p>
                            <p class="mb-0"><small><strong>Location:</strong> Maryland Mall, Lagos</small></p>
                            <a class="btn btn-primary mt-3 text-white">Check-out Now </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 order-md-1 pr-lg-5 mt-5 mt-md-0">
                    <h1 class="display-4 font-weight-bolder mb-3">How it Works for Individuals & Teams<strong class="d-block font-weight-normal text-color h4">Create a world where people work to make a life, not just a living.</strong></h1>
                    <p class="">For over 5 years, we pride ourselves on our commitment to excellence, as well as our ability to deliver for our customers. To make the world’s spaces connected, accessible and productive.</p>
                    <div class="">
                        <div class=d-flex>
                            <h5 class="lh-180 mt-4 mb-6"><img src="/assets/img/spaces-logo.png" class="img-fluid" alt=""></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="shape-container shape-line shape-position-bottom">
        <svg width=2560px height=100px xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink preserveAspectRatio=none x=0px y=0px viewBox="0 0 2560 100" style="enable-background:new 0 0 2560 100" xml:space=preserve class="">
            <polygon points="2560 0 2560 100 0 100"></polygon>
        </svg>
    </div>
</section>

<section>
    <div class="damier-light-bugundy py-5">
        <div class="container">
            <div class="text-center">
                <div class="pt-5">
                    <h1 class="display-6 font-weight-bolder mb-3 text-dark">Features</h1>
                </div>

                <ul class="nav nav-tabs justify-content-center mb-5" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active text-dark" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i data-feather=user></i> For Individuals</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i data-feather=grid></i> For Teams</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-lg-4 text-left">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/reserved-space-on-go.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Reserve space on the go</h1>
                                        <p class=" text-muted">By using damier spaces, this
                                            allows you to reserve spaces </p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 text-left">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/find-spaces.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Find spaces seamlesslessly</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 text-left">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/sub-model.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Subscription based model</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 text-left">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/mark-or-wrong.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Reserve space on the go</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 text-left my-4">
                                <ul>
                                    <li class="float-left">
                                      <img src="/assets/img/sub-model.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Find spaces seamlesslessly</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 text-left my-4">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/custom-user-icon.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Subscription based model</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="mb-4 d-flex align-items-center justify-content-center">
                            <a href="/page/faq" class="btn-fill btn btn-primary">Frequently Asked Questions<i class="fas fa-chevron-right pl-md-2"></i></a>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="row">
                            <div class="col-lg-4 text-left">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/reserved-space-on-go.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Reserve space on the go</h1>
                                        <p class=" text-muted">By using damier spaces, this
                                            allows you to reserve spaces </p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 text-left">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/find-spaces.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Find spaces seamlesslessly</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 text-left">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/sub-model.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Subscription based model</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 text-left">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/mark-or-wrong.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Reserve space on the go</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 text-left my-4">
                                <ul>
                                    <li class="float-left">
                                      <img src="/assets/img/sub-model.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Find spaces seamlesslessly</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 text-left my-4">
                                <ul>
                                    <li class="float-left">
                                        <img src="/assets/img/custom-user-icon.svg">
                                    </li>
                                    <li class="pl-md-6">
                                        <h1 class="word font-weight-bolder mb-0">Subscription based model</h1>
                                        <p class=" text-muted">By using damier spaces,<br>this allows you to reserve space</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="mb-4 d-flex align-items-center justify-content-center">
                            <a href="/page/faq" class="btn-fill btn btn-primary">Frequently Asked Questions<i class="fas fa-chevron-right pl-md-2"></i></a>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

<section class="pb-md-0">
    <div>
        <div class="container position-relative zindex-100">
            <div class="row align-items-center w-100">
                <div class="col-12 col-md-6 order-md-2 pl-lg-6 mt-5 mt-md-0">
                    <h1 class="display-6 font-weight-bolder mb-3">Explore nearby spaces</h1>
                    <p class=" text-muted">For over 5 years, we pride ourselves on our commitment to excellence, as well as our ability to deliver for our customers. To make the world’s spaces connected, accessible and productive.</p>
                    <ul class="mb-4">
                        <li class="float-left pr-4">
                            <img src="/assets/img/bookmark.svg">
                        </li>
                        <li class="pl-md-5">
                            <h1 class="word font-weight-bolder mb-0">Make more money from spaces</h1>
                            <p class=" text-muted">Make more money from spaces, the </p>
                        </li>
                    </ul>
                    <ul class="mb-4">
                        <li class="float-left pr-4">
                            <img src="/assets/img/wifi-multi-color.svg">
                        </li>
                        <li class="pl-md-5">
                            <h1 class="word font-weight-bolder mb-0">Make more money from spaces</h1>
                            <p class=" text-muted">Make more money from spaces, the </p>
                        </li>
                    </ul>
                    <ul class="mb-4">
                        <li class="float-left pr-4">
                            <img src="/assets/img/space-icon.svg">
                        </li>
                        <li class="pl-md-5">
                            <h1 class="word font-weight-bolder mb-0">Make more money from spaces</h1>
                            <p class=" text-muted">Make more money from spaces, the </p>
                        </li>
                    </ul>
                    <a href="/auth/login" class="btn-fill btn btn-primary">Explore spaces<i class="fas fa-chevron-right pl-md-2"></i></a>
                </div>
                <div class="col-12 col-md-6 order-md-1 mb-5 mb-md-0">
                    <div class="position-relative left-5 left-md-0 pt-8 pb-8">
                        <figure><img alt="Image placeholder" src=/assets/img/single-space.png class="img-fluid"></figure>
                    </div>
                    <div class="card shadow-lg mb-3 col-8 col-md-6 col-lg-6 px-0 position-absolute bottom-n6 bottom-md-n5 right-4 right-md-n4 z-index-100">
                        <div class="card-body  text-center h-40">
                            <div class="metas">
                                <div class="row pb-md-3">
                                    <div class="col-7 basic">
                                        <span class="status">
                                            <i class="fas fa-circle open"></i>
                                            Open
                                        </span>
                                    </div>
                                    <div class="col-5 text-right ">
                                        <span class="save-unsave save-space" data-toggle="tooltip" data-placement="top" title="Save Space for Later">
                                            <img src="/assets/img/love.png" alt="">
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-7 basic">
                                        <ul>
                                            <li data-toggle="tooltip" data-placement="top" title="High Speed Wifi"><img src="/assets/img/wifi.svg" alt=""></li>
                                            <li data-toggle="tooltip" data-placement="top" title="Free Coffee"><img src="/assets/img/coffee.svg" alt=""></li>
                                            <li data-toggle="tooltip" data-placement="top" title="Printer"><img src="/assets/img/printer.svg" alt=""></li>
                                            <li data-toggle="tooltip" data-placement="top" title="More Amenities"><img src="/assets/img/more-horizontal.png" alt=""></li>
                                        </ul>
                                    </div>
                                    <div class="col-5 text-right basic">
                                        <span class="user-count text-dark" data-toggle="tooltip" data-placement="top" title="Space Capacity"><i data-feather="user"></i> 12</span>
                                    </div>
                                    <div class="pt-md-2 pl-md-2 basic">
                                        <h5><a class="damier-color" href="single-space.html">Meeting Room</a></h5>
                                        <p>360 Ikorodu Rd, Maryland, Lagos</p>
                                        <ul class="float-left">
                                            <li><span class="space-rating">3.5 <i class="fas fa-star"></i></span></li>
                                            <li class="damier-size"><span>- 0 Reviews</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center pt-8">
                <img src="/assets/img/billboard.png" class="img-fluid" alt="">
            </div>
            <div class="py-3 text-left">
                <p>Cross Platform experience on any smartphone,<br> tablets and modern devices.</p>
            </div>

        </div>
    </div>
</section>

<section>
    <div class="damier-light-bugundy py-5">
        <div class="container">
            <div class="col-lg-8 px-0">
                <h1 class="display-6 font-weight-bold mb-0 text-dark text-left">End to end booking system flows for multi-party workspaces</h1>
            </div>

            <div class="row align-items-center w-100">
                <div class="col-12 col-md-6 order-md-2 pl-lg-6 mt-2 mt-md-0">
                    <h1 class="display-6 font-weight-bolder mb-3">Reserve a Space
                        ahead of time</h1>
                    <p class="mb-2">Customise how payments can be made or split for single items, and choose to collect payment for one or more sub-merchants.</p>
                    ‍
                    <p>Multiple payment methods are accepted, and payouts can be made as they happen.</p>

                </div>
                <div class="col-12 col-md-6 order-md-1 mb-5 mb-md-0">
                    <div class="position-relative left-5 left-md-0 pt-5 pb-8">
                        <figure><img alt="Image placeholder" src=/assets/img/agent.png class="img-fluid"></figure>
                    </div>
                    <div class="card shadow-lg mb-3 col-8 col-md-6 col-lg-7 px-0 position-absolute bottom-n6 bottom-md-n5 right-4 right-md-n4 z-index-100">
                        <div class="card-body  text-center h-40">
                            <div class="display-4 text-color">
                                <i data-feather="clock"></i>
                            </div>
                            <span>SPACE RESERVED</span>
                            <p class="h5 text-color">This space has been reserved for you!</p>
                            <div class=d-flex>
                                <a class="btn btn-primary btn-fill tx-12 btn-sm"> Check-in-now </a>
                                <a class="btn btn-outline-primary tx-12 btn-sm"> Cancel </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section>
    <div class="py-5">
        <div class="container">


            <div class="row align-items-center w-100">

                <div class="col-12 col-md-6 order-md-1 pl-lg-6 mt-5 mt-md-0">
                    <h1 class="display-6 font-weight-bolder mb-3">Flexible Subscription based model</h1>
                    <p class="mb-2">Customise how payments can be made or split for single items, and choose to collect payment for one or more sub-merchants.</p>
                    <p>Multiple payment methods are accepted, and payouts can be made as they happen.</p>
                </div>

                <div class="col-12 col-md-6 order-md-2 mb-5 mb-md-0">
                    <div class="position-relative left-5 left-md-0 pt-8 pb-8">
                        <figure><img alt="Image placeholder" src=/assets/img/office-staff.png class="img-fluid"></figure>
                    </div>
                    <div class="card shadow-lg mb-3 col-8 col-md-6 col-lg-7 px-0 position-absolute bottom-n6 bottom-md-n5 left-4 left-md-6 z-index-100">
                        <div class="card-body  text-center h-40">
                            <div class="mb-3">
                                <h4 class="text-primary mb-0">N15,000</h4>
                                <span>Mini Package</span>
                            </div>
                            <p><i data-feather="check" class="text-primary mr-1"></i> 5 slots per month </p>
                            <p><i data-feather="check" class="text-primary mr-1"></i> High and stable internet</p>

                            <p><i data-feather="check" class="text-primary mr-1"></i>Table space</p>

                            <p><i data-feather="check" class="text-primary mr-1"></i> Print, scan and copy</p>

                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</section>

@endsection
