@extends('site.layout.app')

@section('content')

<section class="slice py-5 py-lg-7 bg-light">
    <div class=container>
        <div class="row row-grid align-items-center">
            <div class="col-12">
                <div class="career__header">
                    <h1 class="mb-1 text-center text-primary">Join The Damier Spaces Team</h1>
                    <p class="tx-18 text-center">We are reinventing the way people work through designed space, flexibility, technology, and community.</p>
                </div>
            </div>
        </div>

        <div class="row career__cards">
        <div class="col-lg-3">
            <div class="card p-3 text-center">
                <img src="/assets/img/svg/career/flexible.svg" class="card__illus" alt="">
                <h4>Flexible work life</h4>
                <p>We trust you to know your schedule and work when you feel most productive.</p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card p-3 text-center">
                <img src="/assets/img/svg/career/interpersonal.svg" class="card__illus" alt="">
                <h4>Interpersonal Relationship</h4>
                <p>Attend talks, conferences, and professional development events that.</p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card p-3 text-center">
                <img src="/assets/img/svg/career/careerImprovement.svg" class="card__illus" alt="">
                <h4>Career Improvement</h4>
                <p>We empower our employees to use the product we build.</p>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card p-3 text-center">
                <img src="/assets/img/svg/career/amazing.svg" class="card__illus" alt="">
                <h4>Amazing Perks</h4>
                <p>Bring your dog to work, and you’ll find plenty of friends waiting to play with them.</p>
            </div>
        </div>
            
        </div>

        <p class="text-center">Send your cv to <span class="text-primary font-weight-bold">careers@damierspaces.com</span></p>
    </div>
</section>



@endsection
