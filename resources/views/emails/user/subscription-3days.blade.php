@extends('site.layout.email')

@section('body')

<h4>Hello {{$subscription->user->name}},</h4>
<p>Your subscription on Damier Spaces will expire in 3 Days!</p>
<p>To renew your subscription, click <a href="{{url('/')}}">here</a> to login to your account and select your desired package.</p>

<small>daimerspaces.com</small>

@endsection