@extends('site.layout.email')

@section('body')

<h4>Hello {{$user->name}},</h4>
<p>You are one step away from completing your Signup on Damier Spaces.</p>
<p>Please <a href="{{url('auth/activate/'.$user->api_token)}}">Click here</a> to confirm your account.</p>
<p>You can also copy the link below to your browser</p>

<p>{{url('auth/activate/'.$user->api_token)}}</p>

<small>daimerspaces.com</small>

@endsection