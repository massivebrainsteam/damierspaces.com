@extends('site.layout.email')

@section('body')

<h4>Hello {{$reservation->user->name}},</h4>
<p>You just successfully reserved {{$reservation->space->name}} at {{$reservation->location->address}}</p>
<p>This space will be avaliable for you for the next 30mins.</p>

<small>daimerspaces.com</small>

@endsection