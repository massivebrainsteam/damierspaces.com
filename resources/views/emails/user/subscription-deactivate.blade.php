@extends('site.layout.email')

@section('body')

<h4>Hello {{$subscription->user->name}},</h4>
<p>Your damier spaces account has been deactivated due to subscription expirty.</p>
<p>To reactivate your account, Kindly contact our customer care.</p>

<small>daimerspaces.com</small>

@endsection