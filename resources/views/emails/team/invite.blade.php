@extends('site.layout.email')

@section('body')

<h4>Hi There!</h4>
<p>{{$subject}}</p>
<p>Please <a href="{{url('/auth/team/accept/'.$invitation->code)}}">Click here</a> to accept this invitation.</p>
<p>You can also copy the link below to your browser</p>

<p>{{url('/auth/team/accept/'.$invitation->code)}}</p>

<small>daimerspaces.com</small>

@endsection