@extends('site.layout.email')

@section('body')

<h4>Hi There!</h4>
<p>{{$subject}}</p>

<small>daimerspaces.com</small>

@endsection