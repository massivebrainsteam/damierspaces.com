@extends('site.layout.email')

@section('body')

    <h4>Hello {{ $user->name }},</h4>
    <p>You are one step away from completing your Host Signup on Damier Spaces.</p>
    <p>Please <a href="{{ url('/auth/activate/' . $user->api_token) }}">Click here</a> to confirm your account.</p>
    <p>You can also copy the link below to your browser</p>
    <p>Use the credentials below to gain access to your account after you have verified your email</p>
    <table>
        <tr>
            <td>Email</td>
            <th>{{ $user->email }}</th>
        </tr>
    </table>
    <p>{{ url('/auth/activate/' . $user->api_token) }}</p>

    <small>daimerspaces.com</small>

@endsection
