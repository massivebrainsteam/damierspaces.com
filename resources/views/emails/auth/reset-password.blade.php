@extends('site.layout.email')

@section('body')

@php($url = '/auth/password/reset/'.$user->api_token)

<p><a href="{{$url}}">Click here</a> to reset your password</p>

<p>If you have difficulties using the link above, copy and paste the link below to your browser</p>

<a href="{{$url}}">
	{{url('/auth/password/reset/'.md5($user->api_token))}}
</a>

@endsection