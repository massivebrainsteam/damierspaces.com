@extends('site.layout.email')

@section('body')

<p>You are one step away from activating your account on Damier Spaces.</p>

<p><a href="{{url('/auth/activate/'.$user->api_token)}}">Click here</a> to activate your account now!</p>

<p>If you have difficulties using the link above, copy and paste the link below to your browser</p>

<a href="{{url('/auth/activate/'.$user->api_token)}}">
	{{url('/activate/'.md5($user->api_token))}}
</a>

@endsection