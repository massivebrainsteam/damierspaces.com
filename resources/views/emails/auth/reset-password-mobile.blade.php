@extends('site.layout.email')

@section('body')

<h1>{{$user->api_token}}</h1>

<p>Use the Token above to reset your password.</p>

@endsection