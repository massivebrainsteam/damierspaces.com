@extends('site.layout.email')

@section('body')

<p>A new contact message from damierspaces.com</p>

<table>
	<tr>
		<th>Name</th>
		<th>{{request('name')}}</th>
	</tr>
	<tr>
		<th>Email</th>
		<th>{{request('email')}}</th>
	</tr>
	<tr>
		<th>Phone</th>
		<th>{{request('phone')}}</th>
	</tr>
	<tr>
		<th>Message</th>
		<th>{{request('message')}}</th>
	</tr>
</table>

@endsection