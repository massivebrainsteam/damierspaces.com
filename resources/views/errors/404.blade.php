<!DOCTYPE html>
<html lang=en>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<head>
    <meta charset=utf-8>
    <meta name=viewport content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="description" content="Damier Spaces - Enjoy frictionless daily access to the best workspaces near you!">
    <meta name="author" content="Nativebrands">
    <title>404 - Page Not Found – Damier Spaces</title>
    <link rel=icon href=/assets/img/brand/favicon.png type=image/png>
    <link rel=stylesheet href=/assets/css/damier-spaces.css id=stylesheet>
</head>

<body>

    @component('site.components._header', ['theme' => 'dark']) @endcomponent

    <section class="vh-100 d-flex align-items-center">
        <div class="bg-danger position-absolute h-100 top-0 right-0 zindex-110 col-lg-6 col-xl-6 zindex-100 d-none d-lg-flex flex-column justify-content-end rounded-bottom-left" data-bg-size=cover data-bg-position=center><img src=/assets/img/theme/light/img-v-error.jpg alt=Image class="img-as-bg rounded-bottom-left"></div>
        <div class=container-fluid>
            <div class="row align-items-center">
                <div class="col-sm-7 col-lg-6 col-xl-6 mx-auto ml-lg-0">
                    <div class="row justify-content-center">
                        <div class="col-11 col-lg-10 col-xl-6 py-5">
                            <p class="font-weight-400">Achievement Unlocked!</p>
                            <h6 class="display-4 mb-3 font-weight-600 text-primary">You found "nothing"</h6>
                            <h2 class="text-xl mb-4 font-weight-400">Our QA didn't see it coming, but you did!</h2>
                            <a href=/ class="btn btn-fill btn-icon hover-translate-y-n3"><span class=btn-inner--icon><i data-feather=home></i></span> <span class=btn-inner--text>Return home</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src=/assets/libs/jquery/dist/jquery.min.js></script>
        <script src=/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js></script>
        <script src=/assets/libs/feather-icons/dist/feather.min.js></script>
        <script src=/assets/libs/sticky-kit/dist/sticky-kit.min.js></script>
        <script src=/assets/js/damier.js></script>
        <script>
            feather.replace({
                width: "1em",
                height: "1em"
            })
        </script>
    </body>

    </html>