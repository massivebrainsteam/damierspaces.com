<?php

Route::view('/backend', 'admin.auth.login');
Route::view('/host', 'site.auth.host-login');

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::any('/', 'AuthController@index');
    Route::get('/activate/{token?}', 'AuthController@activate');

    Route::get('/login', 'LoginController@index')->name('login');
    Route::post('/login', 'LoginController@login');
    Route::get('/logout', 'LoginController@logout');

    Route::get('/user/{code?}', 'UserController@index');
    Route::post('/user', 'UserController@register')->name('auth.user.register');

    Route::get('/team', 'TeamController@index');
    Route::post('/team', 'TeamController@register');
    Route::any('/team/accept/{code?}', 'TeamController@accept');

    Route::get('/host', 'HostController@index');
    Route::post('/host', 'HostController@register');

    Route::any('password/change', 'PasswordController@change');
    Route::get('password/forgot', 'PasswordController@forgotForm');
    Route::post('password/forgot', 'PasswordController@forgot');
    Route::any('password/reset/{token?}', 'PasswordController@reset');
});


Route::group(['namespace' => 'Site'], function () {
    Route::get('/', 'HomeController@index');
    Route::post('/contact', 'HomeController@contact');
    Route::get('/page', 'PageController@index');
    Route::get('/page/{page}', 'PageController@page');

    Route::any('/spaces/{place_id?}', 'SpacesController@index');
    Route::get('/space/{id}', 'SpaceController@index');
    Route::get('/space/{id}/save', 'SpaceController@saveSpace');
    Route::get('/space/{id}/reserve', 'SpaceController@reserveSpace');
    Route::get('/space/{id}/checkin', 'SpaceController@checkIn');
    Route::get('/packages', 'PackagesController@index');
    Route::get('/package/{id}/subscribe', 'PackagesController@subscribe');
    Route::get('/blog', 'BlogController@index');

    Route::group(['prefix' => 'become-a-host'], function () {
        Route::get('/', 'BecomeAHostController@index');
        Route::post('/', 'BecomeAHostController@save');
        Route::any('/cac', 'BecomeAHostController@cac')->middleware('auth');
        Route::any('/bank-info', 'BecomeAHostController@bank')->middleware('auth');
        Route::any('/logo', 'BecomeAHostController@logo')->middleware('auth');
    });

    Route::group(['prefix' => 'teams'], function () {
        Route::get('/invite', 'TeamsController@invite');
    });
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', 'DashboardController@index');
    Route::get('/hosts/{id?}', 'HostsController@index');
    Route::get('/host/{id?}', 'HostsController@form');
    Route::post('/host', 'HostsController@host');
    Route::get('/view-host/{id}', 'HostsController@single');

    Route::get('/spaces', 'SpacesController@index');
    Route::get('/space/{id?}', 'SpacesController@form');
    Route::post('/space', 'SpacesController@space');

    Route::get('/singlespace', 'SpacesController@singlespace');

    Route::get('/space/{id}/photos', 'SpacePhotosController@index');
    Route::get('/space/{space_id}/photo/{id}', 'SpacePhotosController@form');
    Route::post('/space/{space_id}/save-photo', 'SpacePhotosController@save');

    Route::get('/states', 'StatesController@index');
    Route::get('/states/{id?}', 'StatesController@form');
    Route::get('/city', 'StatesController@city');
    Route::get('/city/{id?}', 'StatesController@cityform');

    Route::get('/pricing', 'PricingFilterController@index');
    Route::get('/pricing/{id?}', 'PricingFilterController@form');

    Route::get('/categories', 'CategoriesController@index');
    Route::get('/category/{id?}', 'CategoriesController@form');
    Route::post('/category', 'CategoriesController@category');

    Route::get('/teams', 'TeamsController@index');
    Route::get('/team/{id?}', 'TeamsController@team');
    // Route::get('/teams/team/{id?}', 'UsersController@form');


    Route::get('/packages', 'PackagesController@index');
    Route::get('/subscriptions', 'SubscriptionsController@index');
    Route::get('/transactions', 'TransactionsController@index');
    Route::get('/reviews', 'ReviewsController@index');
    Route::get('/attendances', 'AttendancesController@index');

    Route::group(['prefix' => 'package'], function () {
        Route::post('/', 'PackagesController@package');
        Route::any('{id}/basic-info', 'PackageController@basicInfo');
        Route::any('{id}/other-settings', 'PackageController@otherSettings');
    });

    Route::get('/amenities', 'AmenitiesController@index');
    Route::get('/amenity/{id?}', 'AmenitiesController@form');
    Route::post('/amenity', 'AmenitiesController@amenity');

    Route::get('/users/{type?}', 'UsersController@index');
    // added this
    Route::get('/user/{id?}', 'UsersController@member');
    Route::get('/users/user/{id?}', 'UsersController@form');
    Route::post('/user', 'UsersController@user');

    Route::get('/locations/{host_id?}', 'LocationsController@index');
    Route::get('/location/{host_id?}/{id?}', 'LocationsController@form');
    Route::post('/location', 'LocationsController@location');

    Route::get('/ratings', 'RatingsController@index');
    Route::get('/rating/{id?}', 'RatingsController@form');
    Route::post('/rating', 'RatingsController@rating');

    Route::get('/settlements/{host_id?}', 'SettlementsController@index');
    Route::get('/settlement/{host_id}', 'SettlementsController@form');
    Route::post('/settlement/save', 'SettlementsController@save');
});

Route::group(['prefix' => 'cron', 'namespace' => 'Cron'], function () {
    Route::get('/attendances/expire', 'AttendancesController@markAttendancesAsExpired');
    Route::get('/subscriptions/notify7days', 'SubscriptionsController@notify7DaysExpiry');
    Route::get('/subscriptions/notify3days', 'SubscriptionsController@notify3DaysExpiry');
    Route::get('/subscriptions/deactivate', 'SubscriptionsController@deactivateUser');
});
