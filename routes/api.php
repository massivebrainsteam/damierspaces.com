<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'auth', 'namespace' => 'Api'], function(){

    Route::post('login', 'LoginController@login');
    Route::get('logout/{token}', 'LoginController@logout');
    Route::post('login/email', 'LoginController@loginEmail');
    Route::post('find-packages/{type?}', 'PackagesController@findPackages');
    Route::post('password/forgot', 'LoginController@forgotPassword');
    Route::post('password/reset', 'LoginController@resetPassword');

    Route::group(['middleware' => 'auth:api'], function(){

        Route::get('profile', 'LoginController@profile');
        Route::post('profile', 'LoginController@updateProfile');
        Route::post('change-password', 'LoginController@changePassword');
    });

});

Route::group(['namespace' => 'Api\Host', 'prefix' => 'host'], function(){

    Route::group(['prefix' => 'auth'], function(){

        Route::post('register', 'RegisterController@index');
    });

    Route::group(['middleware' => ['auth:api', 'host']], function(){

        Route::get('dashboard', 'DashboardController@index');
        Route::post('update-details', 'RegisterController@updateDetails');
        Route::get('location/{id}', 'LocationsController@location');
        Route::post('location', 'LocationsController@save');
        Route::get('locations', 'LocationsController@index');
        Route::get('places', 'LocationsController@places');
        Route::get('categories', 'CategoriesController@index');
        Route::get('amenities/{space_id?}', 'AmenitiesController@index');
        Route::get('days/{space_id?}', 'SpacesController@days');
        Route::post('space', 'SpacesController@save');
        
        Route::get('attendances', 'AttendancesController@attendances');
        Route::get('users', 'UsersController@index');
        Route::get('spaces', 'SpacesController@index');
        Route::get('space/{id?}', 'SpacesController@space');
        Route::post('space/{id?}/photo', 'SpacesController@photo');
        Route::get('delete-space-photo/{id}', 'SpacesController@deletePhoto');
        Route::get('reservations', 'ReservationsController@index');
        Route::get('reviews', 'ReviewsController@index');
        Route::post('attendance/get-user', 'AttendanceTokensController@getUser');
        Route::post('attendance/use-token', 'AttendanceTokensController@useToken');
        Route::get('attendance/check-out/{id}', 'AttendanceTokensController@checkOut');
        Route::get('settlements', 'SettlementsController@index');

    });

});


Route::group(['namespace' => 'Api\User', 'prefix' => 'user'], function(){

    Route::post('register', 'RegisterController@index');

    Route::group(['middleware' => ['auth:api']], function(){

        Route::get('dashboard', 'DashboardController@index');
        Route::get('subscriptions', 'SubscriptionsController@index');
        Route::get('spaces/{filter?}/{query?}', 'SpacesController@index');
        Route::get('space/{id}', 'SpacesController@single');
        Route::post('check-out', 'AttendanceController@checkOut');
        Route::get('attendances', 'AttendanceController@attendances');
        Route::get('attendance/{id}', 'AttendanceController@attendance');
        Route::get('reservations', 'ReservationsController@index');
        Route::get('reviews', 'ReviewsController@index');
        Route::get('transactions', 'TransactionsController@index');
        Route::post('attendance/tokens/create', 'AttendanceTokensController@create');
        Route::post('reserve-space', 'ReservationsController@reserveSpace');
        Route::get('space/{id}/save', 'SpacesController@saveSpace');
        Route::post('space/{id}/review', 'ReviewsController@save');
        Route::get('team', 'TeamController@index');

    });

    Route::group(['middleware' => 'auth:api'], function(){

        Route::post('subscription/create', 'SubscriptionsController@createSubscription');
        Route::post('subscription/activate', 'SubscriptionsController@activateSubscription');

    });

});

Route::group(['namespace' => 'Api\Team', 'prefix' => 'team'], function(){

    Route::post('register', 'RegisterController@index');

    Route::group(['middleware' => ['auth:api', 'team-admin']], function(){

        Route::get('dashboard', 'DashboardController@index');
        Route::get('users', 'UsersController@index');
        Route::get('user/{id?}/{status?}', 'UsersController@single');
        Route::get('user-status/{id?}', 'UsersController@changeStatus');
        Route::post('user-update-slots', 'UsersController@updateSlots');
        Route::get('reviews', 'ReviewsController@index');
        Route::get('subscriptions', 'SubscriptionsController@index');
        Route::get('attendances', 'AttendancesController@index');
        Route::get('reservations', 'ReservationsController@index');
        Route::get('invite/subscription', 'InviteController@subscription');
        Route::post('invite/email', 'InviteController@email');

    });

});


Route::group(['prefix' => 'generic', 'namespace' => 'Api'], function(){

    Route::get('packages/{type?}', 'PackagesController@index');
    Route::get('package/{id}', 'PackagesController@package');
    Route::get('/p/{reference}', 'PaystackController@paystack');
    Route::any('/paystack/verify', 'PaystackController@paystackCallback');
});