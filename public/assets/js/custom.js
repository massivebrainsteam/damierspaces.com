


$('.owl-carousel').owlCarousel({
    loop:true,
    nav:true,
    items: 1,
    dots: true,
    loop: true,
    autoplaySpeed: true,
    autoplay: true,
    autoplayTimeout: 5000
});

var mySwiper = new Swiper('.swiper-container', {
    // Default parameters
    slidesPerView: 3,
    spaceBetween: 30,
    // Responsive breakpoints
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            slidesPerView: 1,
            spaceBetween: 20
        },

        // when window width is >= 640px
        800: {
            slidesPerView: 1,
            spaceBetween: 40
        },

        // when window width is >= 480px
        1200: {
            slidesPerView: 2,
            spaceBetween: 30
        },

    }
});


var swiper = new Swiper('.swiper-container.space-swiper', {
    // Default parameters
    slidesPerView: 1,
    spaceBetween: 10,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
      },

  })
  




var btn = $('#button');

$(window).scroll(function () {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});



btn.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
});
const togglePasswords = document.querySelectorAll('.togglePassword');
const passwords = document.querySelectorAll('[type="password"]');
const numberInputs = document.querySelectorAll('[type="number"]');
console.log(togglePasswords)
// Toggle show password.
togglePasswords.forEach(toggle => {
    toggle.addEventListener('click', function (e) {
        console.log("Hello")
        passwords.forEach(password => {
            if (this.previousElementSibling === password) {
                const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
                password.setAttribute('type', type);
            }
        })
        // toggle the eye slash icon
        this.classList.toggle('fa-eye-slash');
    });

});

//  Prevent 'e' from the type number Inputs.
numberInputs.forEach(input => {
    input.addEventListener('keydown', function(e) {
        return e.keyCode !== 69;
    })
})

var input = document.querySelector("#phone");
if (input) {
    console.log(input)
    window.intlTelInput(input, {
        // any initialisation options go here
    });
}



