let gulp 		= require('gulp');
let concat 		= require('gulp-concat');
let minify 		= require('gulp-minify');
let cleanCSS 	= require('gulp-clean-css');

gulp.task('default', function(done){

	/* CSS */
	
	gulp.src([

		'./public/site-assets/less/base.min.css',
		'./public/site-assets/less/header.min.css',
		'./public/site-assets/less/theme.min.css',
		'./public/site-assets/less/damier.css',
		'./public/site-assets/icon/style.css',
	])
	.pipe(concat('app.css'))
	.pipe(cleanCSS({compatibility: 'ie8'}))
	.pipe(gulp.dest('./public/site-assets/dist/css'));

	gulp.src(['./public/site-assets/icon/fonts/**/*'])
	.pipe(gulp.dest('./public/site-assets/dist/css'));
	



	/* Javascript */

	gulp.src([

		'./public/site-assets/library/flexmenu.js',
		'./public/site-assets/library/nouislider.min.js',
		'./public/site-assets/library/wNumb.js',
		'./public/site-assets/library/jrespond.min.js',
		'./public/site-assets/library/scrollspy.min.js',
		'./public/site-assets/library/visibility.js',
		'./public/site-assets/library/accordion.js',
		'./public/site-assets/library/dropdown-custom.js',
		'./public/site-assets/library/sticky.js',
		'./public/site-assets/library/page-transition.js',
		'./public/site-assets/library/checkbox.js',
		'./public/site-assets/library/transition.js',
		'./public/site-assets/library/sidebar.js',
		'./public/site-assets/library/modal.js',
		'./public/site-assets/library/dimmer.js',
		'./public/site-assets/library/popup.js',
		'./public/site-assets/library/calendar.js',
		'./public/site-assets/library/slick.js',
		'./public/site-assets/library/header.js',
		'./public/site-assets/library/functions.js'
	])
	.pipe(concat('app.js'))
	.pipe(minify())
	.pipe(gulp.dest('./public/site-assets/dist/js'));

	done();
});