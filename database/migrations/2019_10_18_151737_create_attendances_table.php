<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendancesTable extends Migration
{
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('team_id')->nullable();
            $table->integer('space_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('host_id')->nullable();
            $table->integer('settlement_id')->nullable();
            $table->datetime('start_at')->nullable();
            $table->datetime('end_at')->nullable();
            $table->integer('approved_by')->nullable();
            $table->string('reference')->nullable();
            $table->enum('status', ['pending', 'active', 'expired', 'declined'])->default('active')->nullable();
            $table->decimal('settlement')->nullable()->default(0);
            $table->enum('settlement_status', ['pending', 'paid'])->nullable()->default('pending');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
