<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->string('payment_reference')->nullable();
            $table->float('amount')->nullable();
            $table->datetime('transaction_date')->nullable();
            $table->string('status')->nullable();
            $table->string('transaction_reference')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
