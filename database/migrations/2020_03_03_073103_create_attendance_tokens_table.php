<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceTokensTable extends Migration
{

    public function up()
    {
        Schema::create('attendance_tokens', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('space_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('host_id')->nullable();
            $table->string('token')->nullable();
            $table->enum('status', ['used', 'unused'])->default('unused');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('table_attendance_tokens');
    }
}
