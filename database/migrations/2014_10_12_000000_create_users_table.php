<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('host_id')->nullable();
            $table->integer('team_id')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->enum('type', ['admin', 'host', 'user', 'team_admin'])->default('user')->nullable();
            $table->string('address')->nullable();
            $table->string('occupation')->nullable();
            $table->string('sex')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive')->nullable();
            $table->string('photo_url')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('api_token')->nullable();
            $table->integer('place_id')->nullable();
            $table->integer('subscription_id')->nullable();
            $table->integer('slots')->nullable();
            $table->datetime('reviewed_at')->nullable();
            $table->rememberToken();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
