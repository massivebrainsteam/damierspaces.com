<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpacesTable extends Migration
{
    public function up()
    {
        Schema::create('spaces', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('host_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('rating_id')->nullable();
            $table->string('name')->nullable();
            $table->integer('capacity')->default(0)->nullable();
            $table->integer('avaliable_capacity')->default(0)->nullable();
            $table->text('description')->nullable();
            $table->text('rules')->nullable();
            $table->string('photo_url')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active')->nullable();
            $table->enum('featured', ['yes', 'no'])->default('no')->nullable();
            $table->timestamps();
        });

        Schema::create('space_days', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('space_id')->nullable();
            $table->string('day')->nullable();
            $table->string('start_at')->nullable();
            $table->string('end_at')->nullable();
            $table->timestamps();
        });

        Schema::create('space_photos', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('space_id')->nullable();
            $table->string('url')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active')->nullable();
            $table->timestamps();
        });

        Schema::create('space_amenities', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('space_id')->nullable();
            $table->integer('amenity_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('spaces');
        Schema::dropIfExists('space_hours');
        Schema::dropIfExists('space_photos');
        Schema::dropIfExists('space_amenities');
    }
}
