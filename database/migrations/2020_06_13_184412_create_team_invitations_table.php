<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamInvitationsTable extends Migration
{
    public function up()
    {
        Schema::create('team_invitations', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('team_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('email')->nullable();
            $table->string('code')->nullable();
            $table->enum('status', ['pending', 'accepted', 'expired'])->default('pending')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('team_invitations');
    }
}
