<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettlementsTable extends Migration
{

    public function up()
    {
        Schema::create('settlements', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('host_id')->nullable();
            $table->integer('approved_by')->nullable();
            $table->decimal('total')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('settlements');
    }
}
