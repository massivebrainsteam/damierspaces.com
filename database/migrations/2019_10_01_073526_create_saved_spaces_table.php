<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSavedSpacesTable extends Migration
{
    public function up()
    {
        Schema::create('saved_spaces', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('space_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('host_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('saved_spaces');
    }
}
