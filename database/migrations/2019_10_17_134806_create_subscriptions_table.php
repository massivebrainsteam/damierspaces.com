<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('team_id')->nullable();
            $table->integer('package_id')->nullable();
            $table->string('payment_reference')->nullable();
            $table->integer('transaction_id')->nullable();
            $table->float('total')->nullable();
            $table->datetime('start_at')->nullable();
            $table->datetime('end_at')->nullable();
            $table->enum('status', ['pending', 'cancelled', 'expired', 'active'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
