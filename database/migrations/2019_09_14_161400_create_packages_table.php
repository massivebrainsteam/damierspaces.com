<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('rating_id')->nullable();
            $table->enum('type', ['user', 'team'])->default('user')->nullable();
            $table->string('name')->nullable();
            $table->integer('order')->nullable();
            $table->enum('duration', ['daily', 'weekly', 'monthly'])->default('monthly')->nullable();
            $table->float('amount')->default(0)->nullable();
            $table->enum('status', ['active', 'inactive'])->default('inactive')->nullable();
            $table->integer('slots')->default(1)->nullable();
            $table->enum('can_reserve_spaces', ['yes', 'no'])->default('no')->nullable();
            $table->integer('reservation_duration')->default(0)->nullable();
            $table->integer('monthly_reservations')->default(0)->nullable();
            $table->integer('max_team_users')->default(0)->nullable();
            $table->timestamps();
        });

        Schema::create('package_amenities', function (Blueprint $table) {
            
            $table->bigIncrements('id');
            $table->integer('package_id')->nullable();
            $table->integer('amenity_id')->nullable();
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
