<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Amenity extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];

    public function setCategoryAttribute($value)
	{
		$this->attributes['category'] = strtolower($value);
	}

	public function getCategoryAttribute($value)
	{
		return ucwords($value);
	}

	public static function getGroupedSpaceAmenities($space_id)
	{
		$amenities = DB::table('space_amenities')
		->join('amenities', 'amenities.id', '=', 'space_amenities.amenity_id')
		->join('spaces', 'spaces.id', '=', 'space_amenities.space_id')
    	->select(DB::raw('max(amenities.id) as id, max(amenities.name) as name, max(amenities.category) as category'))
    	->where('spaces.id', $space_id)
    	->groupBy(DB::raw('category, amenities.id'))
    	->get();

    	return collect($amenities)->groupBy('category');

	}
}
