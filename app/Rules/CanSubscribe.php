<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Package;
use Auth;

class CanSubscribe implements Rule
{
    public $package;
    public $error;

    public function __construct()
    {
        $this->package = Package::find(request('package_id'));
    }


    public function passes($attribute, $value)
    {
        if(!$this->package){

            $this->error = 'Invalid Package Selected';
            return false;
        }

        $existingSubscription = Auth::user()->subscription;

        if($existingSubscription){

            if($existingSubscription->end_at > now()){

                $this->error = 'You are currently subscribed to '.$existingSubscription->package->name.'. You cannot migrate to another Package until your existing Subscription expires. Your Existing subscription will expire '._date($existingSubscription->end_at);
                return false;
            }
        }

        return true;
    }

    public function message()
    {
        return $this->error;
    }
}
