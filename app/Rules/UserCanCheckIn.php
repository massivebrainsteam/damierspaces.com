<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\User;
use App\Space;
use App\Attendance;
use Auth;

class UserCanCheckIn implements Rule
{
    public $user;
    public $space;
    public $error;

    public function __construct()
    {
        $this->user     = Auth::user();
        $this->space    = Space::where(['id' => request('space_id'), 'status' => 'active'])->first();
    }

    public function passes($attribute, $value)
    {
        if(!$this->space){

            $this->error = 'Invalid Space selected';
            return false;
        }

        if($this->user->subscription->end_at < now()){

            $this->error = 'You have no active subsciription. Please subscribe to one of our packages.';
            return false;
        }

        if($this->space->avaliable_capacity < 1){

            $this->error = 'This space is alreadly filled up at this time. Please try again later.';
            return false;
        }

        if($this->space->rating_id < $this->user->subscription->package->rating_id){

            $this->error = 'You cannot checkin to this space as it is rated '.$this->space->rating->name;
            return false;
        }
        
        
        $attendance = Attendance::where([

            'user_id'   => $this->user->id, 
            'space_id'  => $this->space->id,
            'status'    => 'active'

        ])
        ->first();

        if($attendance){

            $this->error = 'You are currently checked in to '.$attendance->space->name;
            return false;
        }

        $attendance = Attendance::where([

            'user_id'   => $this->user->id, 
            'space_id'  => $this->space->id,
            'status'    => 'pending'

        ])
        ->first();

        if($attendance){

            $this->error = 'You already have a pending request for '.$attendance->space->name.'. Please wait while your host approves your checkin request.';
            return false;
        }

        if($attendance && $attendance->status == 'active'){

            $this->error = 'You are currently checked into this space.';
            return false;
        }

        return true;
    }

 
    public function message()
    {
        return $this->error;
    }
}
