<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Auth;
use App\User;

class CanUpdateSlots implements Rule
{
    public $team;
    public $user;
    public $error;

    public function __construct()
    {
        $this->user = User::whereId(request('user_id'))->where('team_id', '>', 0)->first();
    }

    
    public function passes($attribute, $value)
    {
        if(!$this->user){

            $this->error = 'Invalid User Selected';
            return false;
        }

        $this->team  = $this->user->team;

        if(Auth::user()->team_id != $this->team->id){

            $this->error = 'You are not authorized to update this team\'s information.';
            return false;
        }

        $users_slots    = $this->team->users()->where('id', '!=', request('user_id'))->sum('slots');
        $slots_sum      = $users_slots + (int)request('slots');

        if($slots_sum > $this->team->slots){

            $this->error = 'Your total package slots is '.$this->team->slots.' You cannot allocate up to '.$slots_sum.' within your team';
            return false;
        }

        return true;
    }

    public function message()
    {
        return $this->error;
    }
}
