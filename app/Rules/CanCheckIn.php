<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\User;
use App\Space;
use App\Attendance;
use Auth;

class CanCheckIn implements Rule
{
    public $user;
    public $space;
    public $error;

    public function __construct()
    {
        $this->user     = User::where(['id' => request('user_id'), 'type' => 'user', 'status' => 'active'])->first();
        $this->space    = Space::where(['id' => request('space_id'), 'status' => 'active'])->first();
    }

    public function passes($attribute, $value)
    {
        if(!$this->user){

            $this->error = 'Invalid User account selected.';
            return false;
        }

        if(!$this->space){

            $this->error = 'Invalid Space selected';
            return false;
        }

        if($this->user->subscription->end_at < now()){

            $this->error = 'User has no active subsciription. CheckIn Cannot be done.';
            return false;
        }

        if($this->space->host_id != Auth::user()->host_id){

            $this->error = 'Invalid Space selected.';
            return false;
        }

        if($this->space->avaliable_capacity < 1){

            $this->error = 'This space is alreadly filled up. Please check someone out.';
            return false;
        }
        
        $attendance = Attendance::where([

            'user_id'   => $this->user->id, 
            'space_id'  => $this->space->id,
            'status'    => 'active'

        ])
        ->first();

        if($attendance){

            $this->error = 'User is currently checked in to this space';
            return false;
        }

        return true;
    }

 
    public function message()
    {
        return $this->error;
    }
}
