<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Auth;
use App\Space;
use App\User;
use App\Reservation;

class CanReserveSpace implements Rule
{
    public $user;
    public $space;
    public $error;

    public function __construct()
    {
        $this->user     = Auth::user();
        $this->space    = Space::find(request('space_id'));
    }

    public function passes($attribute, $value)
    {
        if(!$this->user){

            $this->error = 'Invalid User account selected.';
            return false;
        }

        if(!$this->space){

            $this->error = 'The space you are trying to reserve does not exist.';
            return false;
        }

        if($this->user->subscription_status == 'inactive'){

            $this->error = 'You currently have no active subscription.';
            return false;
        }

        $package = $this->user->subscription->package;

        if($package->can_reserve_spaces == 'no'){

            $this->error = 'Your package does not allow Space Reservations. Please upgrade your plan.';
            return false;
        }

        $reservations_this_month = $this->user->reservations()->whereRaw('MONTH(created_at) = ?', date('m'))->count();

        if($reservations_this_month >= $package->monthly_reservations){

            $this->error = 'You have exceeded the number of reservations allowed for your subscribed package this month.';
            return false;
        }

        if($this->space->space_status != 'open'){

            $this->error = 'You cannot reserve this space at the moment. it is currently '.$this->space->space_status;
            return false;
        }
        
        if($this->space->avaliable_capacity < 1){

            $this->error = 'This space is alreadly filled up. Please check someone out.';
            return false;
        }

        if($this->space->rating_id < $this->user->subscription->package->rating_id){

            $this->error = 'You cannot reserve to this space as it is rated '.$this->space->rating->name;
            return false;
        }
        
        $reservations = Reservation::where([

            'space_id'  => $this->space->id,
            'user_id'   => $this->user->id,
            'status'    => 'active'
        ])->count();

        if($reservations > 0){

            $this->error = 'You have an existing reservation. You can not reserve more than one space at a time.';
            return false;
        }

        return true;
    }

 
    public function message()
    {
        return $this->error;
    }
}
