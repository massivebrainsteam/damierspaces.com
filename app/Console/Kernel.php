<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [ ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->call('App\Http\Controllers\Cron\AttendancesController@markAttendancesAsExpired')->everyMinute();
        $schedule->call('App\Http\Controllers\Cron\SubscriptionsController@notify7DaysExpiry')->dailyAt('12:00');
        $schedule->call('App\Http\Controllers\Cron\SubscriptionsController@notify3DaysExpiry')->dailyAt('12:00');
        $schedule->call('App\Http\Controllers\Cron\SubscriptionsController@deactivateUser')->dailyAt('12:00');
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
