<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Scopes;

class TeamInvitation extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];

    use Scopes;

    public function team()
    {
        return $this->belongsTo('App\Team')->withDefault();
    }

    public function user()
    {
        return $this->belongsTo('App\User')->withDefault();
    }

    public function updateAsAccepted(User $user)
    {
        $this->update([

            'status'    => 'accepted', 
            'user_id'   => $user->id
        ]);

        $user->update(['team_id' => $this->team_id ]);

        $team_admin = $this->team->users()->whereType('team_admin')->first();

        if($team_admin){

            if($team_admin->subscription_id > 0)
                $user->update(['subscription_id' => $team_admin->subscription_id]);
        }

        $subject    = $user->name.' just accepted your invitation.';
        $body       = view('emails.team.accepted', ['subject' => $subject, 'user' => $user])->render();

        _email($user->email, $subject, $body);
    }
}
