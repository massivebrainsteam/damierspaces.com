<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceToken extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];

    public function space()
    {
    	return $this->belongsTo('App\Space')->withDefault(function(){

            return new Space();
        });
    }

    public function location()
    {
        return $this->belongsTo('App\Location')->withDefault(function(){

            return new Location();
        });
    }

    public function host()
    {
        return $this->belongsTo('App\Host')->withDefault(function(){

            return new Host();
        });
    }

    public function user()
    {
    	return $this->belongsTo('App\User')->withDefault(function(){

            return new User();
        });
    }
}
