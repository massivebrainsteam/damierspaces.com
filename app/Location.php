<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];

    public function getPhotoUrlAttribute($value)
    {
        if(!$value){

            $name = $this->name ?? 'Location-Photo';
            return "https://via.placeholder.com/340x250/ffcdc9/840c04?text={$name}";
        }

        return $value;
    }

    public function host()
    {
        return $this->belongsTo('App\Host')->withDefault(function(){

            return new Host();
        });
    }

    public function spaces()
    {
        return $this->hasMany('App\Space');
    }
}
