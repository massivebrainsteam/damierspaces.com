<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Traits\Scopes;

class Space extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];
    protected $appends   = ['photo_url_lg', 'reviews_count', 'ratings_total', 'rating_average', 'space_status', 'simple_amenities'];

    use Scopes;

    public function getPhotoUrlAttribute($value)
    {
        if(!$value){

            $name = $this->name ?? 'Space Photo';
            return "https://via.placeholder.com/340x250/ffcdc9/840c04?text={$name}";
        }

        return $value;
    }

    public function getPhotoUrlLgAttribute($value)
    {
        if(!$this->getOriginal('photo_url')){

            $name = $this->name ?? 'Space Photo';
            return "https://via.placeholder.com/322X429/ffcdc9/840c04?text={$name}";
        }

        return $this->photo_url;
    }

    public function getReviewsCountAttribute($value)
    {
        return (int)$this->reviews()->count();
    }

    public function getRatingsTotalAttribute($value)
    {
        return (int)$this->reviews()->sum('rating') ?? 0;
    }

    public function getRatingAverageAttribute($value)
    {        
        if($this->reviews_count > 0)
            return round($this->ratings_total / $this->reviews_count, 1);

        return '0.0';
    }

    public function getSimpleAmenitiesAttribute($value)
    {
        $data       = [];
        $amenities  = $this->amenities()->whereNotNull('icon')->take(3)->get();

        foreach($amenities as $row)
            $data[] = ['name' => $row->name, 'icon' => $row->icon];

        return collect($data);
    }

    public function getSpaceStatusAttribute($value)
    {    
        $status = 'open';

        $today = _days()[\Carbon\Carbon::now()->dayOfWeek];

        if(in_array(date('w'), [0, 6])){

            if($this->location->weekends == 'no')
                $status = 'closed';
        }
        

        if($this->avaliable_capacity < 1){

            if($this->reservations()->where(['status' => 'active'])->count() > 0) {

                $status = 'reserved';
            
            }else{

                $status = 'occupied';
            }

        }
        
        if($this->status == 'inactive')
            $status = 'pending review';
        
        return $status;
    }

    public function host()
    {
        return $this->belongsTo('App\Host')->withDefault(function(){

            return new Host();
        });
    }

    public function space()
    {
        return $this->belongsTo('App\Space')->withDefault(function(){

            return new Space();
        });
    }

    public function category()
    {
        return $this->belongsTo('App\Category')->withDefault(function(){

            return new Category();
        });
    }

    public function location()
    {
        return $this->belongsTo('App\Location')->withDefault(function(){

            return new Location();
        });
    }

    public function rating()
    {
        return $this->belongsTo('App\Rating')->withDefault(function(){

            return new Rating();
        });
    }

    public function saved_spaces()
    {
        return $this->hasMany('App\SavedSpace');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    public function amenities()
    {
        return $this->belongsToMany('App\Amenity', 'space_amenities');
    }

    public function days()
    {
        return $this->hasMany('App\SpaceDay');
    }

    public function photos()
    {
        return $this->hasMany('App\SpacePhoto');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public static function search($param = '')
    {
        $like = "%$param%";

        return self::query()
        ->where('status', 'active')
        ->where('name', 'like', $like)
        ->orWhere('description', 'like', $like)
        ->orWhere(function($query) use($param){

            $host = Host::whereName($param)->first();

            if($host){

                $query->whereHostId($host->id);
            }

            $category = Category::whereName($param)->first();

            if($category){

                $query->whereCategoryId($category->id);
            }

            $location = Location::whereName($param)->first();

            if($location){

                $query->whereLocationId($location->id);
            }

            if(strtolower($param) === 'active'){

                $query->whereStatus('active');
            }

            if(strtolower($param) === 'inactive'){

                $query->whereStatus('inactive');
            }

        })
        ->paginate(10);
    }

    public static function transform($spaces)
    {
        $data = [];

        foreach($spaces as $row){

            $saved = false;

            if(SavedSpace::where(['user_id' => Auth::user()->id, 'space_id' => $row->id])->count() > 0)
                $saved = true;

            $data[] = [

                'id'                => $row->id,
                'name'              => $row->name,
                'description'       => $row->description,
                'location'          => $row->location,
                'location_id'       => $row->location_id,
                'host'              => $row->host,
                'place'             => $row->place,
                'category'          => $row->category->name,
                'category_id'       => $row->category_id,
                'capacity'          => $row->capacity,
                'avaliable_capacity'=> $row->avaliable_capacity,
                'photo_url'         => $row->photo_url,
                'status'            => ucfirst($row->status),
                'space_status'      => $row->space_status,
                'url'               => url('/space/'.$row->id),
                'reviews_count'     => $row->reviews_count,
                'ratings_total'     => $row->ratings_total,
                'ratings_average'   => $row->rating_average,
                'saved'             => $saved,
                'simple_amenities'  => $row->simple_amenities,
                'photos'            => $row->photos
            ];
        }

        return collect($data);
    }
}
