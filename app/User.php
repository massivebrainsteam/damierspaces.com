<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use \Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

   
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];
    protected $casts     = ['email_verified_at' => 'datetime'];
    protected $appends   = ['subscription_status', 'checkin_status', 'slots_left'];

    public function getPhotoUrlAttribute($value)
    {
        return $value ?? url('/assets/img/avatar.svg');
    }

    public function getCheckinStatusAttribute()
    {
        return Attendance::whereUserId($this->id)->whereStatus('active')->count() > 0
        ? 'checked in'
        : 'checked out';
    }

    public function getSubscriptionStatusAttribute()
    {
        if($this->subscription_id > 0){

            if($this->subscription->status == 'active' && $this->subscription->end_at->greaterThan(Carbon::today()))
                return 'active';
        }

        return 'inactive';
    }

    public function getSlotsLeftAttribute()
    {
        if($this->subscription_status == 'inactive')
            return 0;
        
        $attendances_this_month = Attendance::whereUserId($this->id)
        ->whereIn('status', ['active', 'expired'])
        ->whereMonth('created_at', date('m'))
        ->count();

        if($attendances_this_month >= 3)
            return 0;

        return $this->slots - $attendances_this_month;
    }

    public function host()
    {
        return $this->belongsTo('App\Host')->withDefault(function(){

            return new Host();
        });
    }

    public function team()
    {
        return $this->belongsTo('App\Team')->withDefault(function(){

            return new Team();
        });
    }

    public function subscription()
    {
        return $this->belongsTo('App\Subscription')->withDefault(function(){

            return new Subscription();
        });
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    public function attendances()
    {
        return $this->hasMany('App\Attendance');
    }

    public function scopeAdmin($query)
    {
        return $query->where(['type' => 'admin']);
    }

    public function scopeHost($query)
    {
        return $query->where(['type' => 'host']);
    }

    public function scopeUser($query)
    {
        return $query->where(['type' => 'user']);
    }

    public function scopeActive($query)
    {
        return $query->where(['status' => 'active']);
    }

    public function scopeInactive($query)
    {
        return $query->where(['status' => 'inactive']);
    }

    public static function search($param = '', $type = 'user')
    {
        $like = "%$param%";

        return self::query()
        ->whereType($type)
        ->where('name', 'like', $like)
        ->orWhere('email', 'like', $like)
        ->orWhere('phone', 'like', $like)
        ->orWhere(function($query) use($param){

            if(strtolower($param) === 'active'){

                $query->whereStatus('active');
            }

            if(strtolower($param) === 'inactive'){
                
                $query->whereStatus('inactive');
            }
        })
        ->paginate(10);
    }
}
