<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $hidden    = ['updated_at'];
	protected $guarded   = ['updated_at'];

	public function space()
	{
		return $this->belongsTo('App\Space')->withDefault(function(){

            return new Space();
        });
	}

	public function user()
	{
		return $this->belongsTo('App\User')->withDefault(function(){

            return new User();
        });
	}
}
