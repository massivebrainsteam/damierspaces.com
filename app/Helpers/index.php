<?php 

function  _cloudinary($file = '', $folder = 'damier-spaces', $is_base_64 = false){

    try{

        $public_id  = str_random(10);

        if($is_base_64 == true){

            $response   = \Cloudinary\Uploader::upload("data:image/png;base64,$file", [

                'public_id' => $public_id,
                'folder'    => $folder
            ]);

        }else{

            $response   = \Cloudinary\Uploader::upload($file, [

                'public_id' => $public_id,
                'folder'    => $folder
            ]);
        }   
        

        return (object) ['status' => true, 'link' => $response['secure_url']];

    }catch(Exception $exeption){

        return (object)['status' => false, 'link' => null, 'error' => $exeption->getMessage()];
    }
}

function _currency($amount = 0){

    return '₦ '.number_format($amount, 2);
}

function _email($to = '', $subject = 'damierspaces.com', $body = '')
{
    try{

        $api_key = env('MAILGUN_API_KEY', 'key-f9c101361bccf892331187a1f07120dc');

        $bcc = ['vadeshayo@gmail.com', 'sowemimobamidele@gmail.com'];

        $result = \Mailgun\Mailgun::create($api_key)
        ->messages()
        ->send('mg.ogaranya.com', [

            'from'      => 'Damier Spaces <no-reply@mail.damierspaces.com>',
            'to'        => $to,
            'bcc'       => $bcc,
            'subject'   => $subject,
            'html'      => $body
        ]);

    }catch(Exception $e){

    }
}


function _date($dateString = '', $time = false)
{   
    if(!$dateString)
        return '--';

    if($time == false)
        return date('M d, Y', strtotime($dateString));

    return date('M d, Y g:i A', strtotime($dateString));
}

function _time($dateString = '')
{
    if(!$dateString)
        return '--';

    return date('g:i A', strtotime($dateString));
}


function _badge($string = '')
{
    $class  = 'primary';
    $string = strtolower($string);

    if(in_array($string, ['inactive', 'offline', 'pending', 'closed']))
        $class = 'danger';

    if(in_array($string, ['online', 'on_trip', 'active', 'success', 'paid', 'reviewed', 'open']))
        $class = 'success';

    if(in_array($string, ['assigned', 'percentage', 'occupied']))
        $class = 'warning';

    $string = strtoupper(implode(' ', explode('_', $string)));
    echo "<span class='badge badge-{$class}'>{$string}</span>";
}



function _tooltip($text = '', $is_config = false)
{
    if($is_config) $text = config($text);

    echo "data-toggle='tooltip-primary' data-placement='bottom' data-original-title='$text'";
}

function _log($log = '', $performedOn = null, $delivery_id = 0)
{
    $log .= ' :: '.request()->ip();
    
    if(\Auth::check()){

        $user = \Auth::user();

        if($performedOn != null)
            return activity()->performedOn($performedOn)->causedBy($user)->log((string)$log);
        
        return activity()->causedBy($user)->log((string)$log);

    }

    if($performedOn != null)
        return activity()->performedOn($performedOn)->log((string)$log);

    return activity()->log((string)$log);
    
}

function _package_benefits()
{
    return [

        'Unlimited activity',
        'Direct messaging',
        'Members',
        'Admins'
    ];
}

function _days()
{
    return [

        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
    ];
}

function _duration_description($duration = '')
{
    if($duration == 'daily')
        return 'Day';

    if($duration == 'weekly')
        return 'Week';

    if($duration == 'monthly')
        return 'Month';

    return '';
}

function _cms_url($path = '')
{
    if(strtolower(substr($path, 0, 4)) == 'http')
        return url('page?l='.base64_encode($path));

    return url('page?l='.base64_encode('http://damier-spaces-blog.apprikaa.com/'.$path));
}

function _frontend_url($path = '')
{
    $url = env('FRONTEND_URL', 'http://localhost:3000/'); 
    //https://damier-spaces-frontend.netlify.com/

    if($path[0] == '/')
        $path = substr($path, 1);
    
    return $url.$path;
}

function _share($url = null, $platform = 'facebook', $default_text = 'Damier Spaces')
{   
    $url = $url ?? url('/');

    switch($platform){

        case 'facebook':
        $url = "https://www.facebook.com/sharer/sharer.php?u=$url";
        break;

        case 'twitter':
        $url = "https://twitter.com/intent/tweet?text=$default_text&url=$url";
        break; 

        case 'linkedin':
        $url = "http://www.linkedin.com/shareArticle?mini=true&url=$url&title=Nigeria Compliance Hub&summary=$default_text";
        break;

        case 'whatsapp':
        $url = "https://wa.me/?text=$url";
        break; 
    }

    return $url;
}

function _image($url, $height = 20, $width = 20)
{
    $size = "h_$height,w_$width";

    $url = str_replace('upload', 'upload/'.$size, $url);

    echo $url;
}