<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Scopes;

class Attendance extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];
    protected $casts     = ['start_at' => 'datetime', 'end_at' => 'datetime'];
    protected $appends   = ['time_used'];

    use Scopes;
    
    public function space()
    {
    	return $this->belongsTo('App\Space')->withDefault(function(){

            return new Space();
        });
    }

    public function location()
    {
        return $this->belongsTo('App\Location')->withDefault(function(){

            return new Location();
        });
    }

    public function host()
    {
        return $this->belongsTo('App\Host')->withDefault(function(){

            return new Host();
        });
    }

    public function user()
    {
    	return $this->belongsTo('App\User')->withDefault(function(){

            return new User();
        });
    }

    public function team()
    {
        return $this->belongsTo('App\Team')->withDefault(function(){

            return new Team();
        });
    }

    public function approver()
    {
    	return $this->belongsTo('App\User', 'approved_by')->withDefault(function(){

            return new User();
        });
    }

    public function settlmenet()
    {
        return $this->belongsTo('App\Settlement')->withDefault(function(){

            return new Settlement();
        });
    }

    public function getTimeUsedAttribute()
    {
        if($this->status == 'pending')
            return '';

        return $this->end_at->diff($this->start_at)->format('%HHrs %IMins');
    }

    public static function search($param = '')
    {
        $like = "%$param%";

        return self::query()
        ->where('reference', 'like', $like)
        ->orWhere('status', 'like', $like)
        ->orWhere(function($query) use($param, $like){

            $user = User::where('name', 'like', $like)->first();

            if($user){

                $query->whereUserId($user->id);
            }

            $host = Host::where('name', 'like', $like)->first();

            if($host){

                $query->whereHostId($host->id);
            }

            $space = Space::where('name', 'like', $like)->first();

            if($space){

                $query->whereSpaceId($space->id);
            }

            $location = Location::where('name', 'like', $like)->first();

            if($location){

                $query->whereLocationId($location->id);
            }
        })
        ->paginate(10);
    }
}
