<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
	protected $hidden    = ['updated_at', 'password'];
	protected $guarded   = ['updated_at'];
	protected $appends	 = ['total_slots', 'amenities_names'];

	public function setAllowedHostsAttribute($value)
	{
		$this->attributes['allowed_hosts'] = json_encode($value ?? []);
	}

	public function getAllowedHostsAttribute($value)
	{
		return json_decode($value) ?? [];
	}

	public function getTotalSlotsAttribute()
	{
		return $this->slots;
	}

	public function getAmenitiesNamesAttribute()
	{	
		$names = [];

		foreach(collect($this->amenities) as $row)
			$names[] = $row->amenity->name;

		return $names;
	}

	public function rating()
	{
		return $this->belongsTo('App\Rating');
	}

	public function spaces()
	{
		return $this->hasMany('App\Space');
	}

	public function amenities()
	{
		return $this->hasMany('App\PackageAmenity');
	}

	public function scopeActive($query)
	{
		return $query->where(['status' => 'active']);
	}

	public function scopeInactive($query)
	{
		return $query->where(['status' => 'inactive']);
	}

	public static function getTitle($amount = 0, $type = 'individual-calculator')
	{
		if($type == 'individual-calculator'){

			if($amount <= 30000)
				return 'Nomad';

			if($amount <= 60000)
				return 'Freelancer';

			return 'Oga Boss';
		}

		if($amount <= 30000)
			return 'Startup';

		if($amount <= 60000)
			return 'Company';

		return 'Fortune to be';

	}

	public static function getIcon($amount = 0, $type = 'individual-calculator')
	{
		if($type == 'individual-calculator'){

			if($amount <= 30000)
				return 'nomad.svg';

			if($amount <= 60000)
				return 'freelancer.svg';

			return 'oga-boss.svg';
		}

		if($amount <= 30000)
			return 'startup.svg';

		if($amount <= 60000)
			return 'company.svg';

		return 'fortune-to-be.svg';
	}
}
