<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpaceDay extends Model
{
    protected $hidden   = ['updated_at', 'password'];
    protected $guarded  = ['updated_at'];

    public function space()
    {
    	return $this->belongsTo('App\Space');
    }
}
