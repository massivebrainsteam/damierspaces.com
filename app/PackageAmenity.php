<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageAmenity extends Model
{
    protected $hidden   = ['updated_at', 'password'];
    protected $guarded  = ['updated_at'];

    public function package()
    {
    	return $this->belongsTo('App\Package');
    }

    public function amenity()
    {
    	return $this->belongsTo('App\Amenity');
    }
}
