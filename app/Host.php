<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Traits\Scopes;

class Host extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];

    use Scopes;

    public function getPhotoUrlAttribute($value)
    {
        if(!$value)
            return "https://static1.squarespace.com/static/587925ac37c5818e2e45665d/t/5ad2abc403ce641bc88deb64/1523755974991/default-profile.png";

        return $value;
    }

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function spaces()
    {
        return $this->hasMany('App\Space');
    }

    public function locations()
    {
        return $this->hasMany('App\Location');
    }

    public function attendances()
    {
        return $this->hasMany('App\Attendance');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function settlements()
    {
        return $this->hasMany('App\Settlemnt');
    }

    public function getUsersWhoHaveUsedASpaceWhichBelongsToThisHost()
    {
        $users = DB::table('attendances')
        ->selectRaw('max(attendances.user_id) as user_id')
        ->join('spaces', 'spaces.id', 'attendances.space_id')
        ->join('hosts', 'hosts.id', 'spaces.host_id')
        ->where('hosts.id', $this->id)
        ->where('attendances.status', '!=', 'pending')
        ->groupBy('attendances.user_id')
        ->get();

        return User::whereIn('id', collect($users)->pluck('user_id'))->get();
    }
}
