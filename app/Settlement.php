<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Scopes;

class Settlement extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];

    use Scopes;
    
    public function host()
    {
    	return $this->belongsTo('App\Host')->withDefault();
    }

    public function approver()
    {
    	return $this->belongsTo('App\User', 'approved_by')->withDefault();
    }

    public function attendances()
    {
    	return $this->hasMany('App\Attendance');
    }
}
