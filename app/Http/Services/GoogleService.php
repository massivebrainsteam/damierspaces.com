<?php

namespace App\Http\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class GoogleService
{
    public static function getCoordinatesFromAddress($address = '')
    {
        $default = (object)['latitude' => 0, 'longitude' => 0];

        try{

            if(!$address){

                return $default;
            }

            $key        = env('GOOGLE_API_KEY');
            $endpoint   = "https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=$key";

            $response = (new Client())->request('GET', $endpoint)->getBody();
            $response = json_decode($response);

            if(count($response->results) < 1){

                return $default;
            }

            $data = $response->results[0];

            return (object)['latitude' => $data->geometry->location->lat, 'longitude' => $data->geometry->location->lng];

        }catch(Exception $e){

            return $default;
        }
    }
}