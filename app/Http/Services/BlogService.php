<?php

namespace App\Http\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

class BlogService
{
    public static function getPosts($perPage = 5)
    {
        try {
            return collect([]);

            if (Cache::has('blog_posts')) {
                return collect(Cache::get('blog_posts', []));
            }

            $response = (new Client())->get('https://blog.damierspaces.com/wp-json/wp/v2/posts?_embed&per_page='.$perPage.'&orderby=date&order=desc');

            if ($response->getStatusCode() !== 200) {
                return collect([]);
            }

            $response = json_decode($response->getBody()->getContents());

            $posts = [];

            foreach ($response as $row) {
                if ($row->type !== 'post') {
                    continue;
                }

                $excerpt = $row->excerpt->rendered;

                $excerpt = str_replace('<p>', '', $excerpt);
                $excerpt = str_replace('</p>', '', $excerpt);
                $excerpt = str_replace('\n', '', $excerpt);

                $featured_image = null;
                $category 		= null;

                if (isset($row->_embedded->{'wp:featuredmedia'})) {
                    $featured_image = $row->_embedded->{'wp:featuredmedia'};
                }

                if (isset($row->_embedded->{'wp:term'})) {
                    $category = $row->_embedded->{'wp:term'};
                }

                $posts[] = (object)[

                    'title'		=> $row->title->rendered,
                    'link'		=> $row->link,
                    'image'		=> $featured_image ? optional($featured_image[0])->source_url : 'https://api.placeholder.com/200',
                    'excerpt'	=> $excerpt,
                    'category'	=> $category ? optional($category[0][0])->name : 'Post',
                    'date'		=> _date($row->date),
                    'user'		=> optional($row->_embedded->author[0])->name,
                ];
            }

            //Cache::add('blog_posts', $posts, now()->addMinutes(30));

            return collect($posts);
        } catch (Exception $exception) {
            _log('BlogService::getPosts() '.$exception->getMessage());

            return collect([]);
        }
    }
}
