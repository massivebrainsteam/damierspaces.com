<?php

namespace App\Http\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use App\Space;
use DB;

class SpaceService
{
	public static function getSpacesNearYou()
	{
		return Space::whereStatus('active')->take(10)->inRandomOrder()->get();
	}

	public static function getFeaturedSpaces()
	{
		return Space::query()->active()->featured()->take(10)->inRandomOrder()->get();
	}

}
