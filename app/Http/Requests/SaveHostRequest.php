<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class SaveHostRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type === 'admin';
    }

    public function rules()
    {
        return [

            'name'          => 'required',
            'email'         => 'required|email',
            'phone'         => 'required|min:11',
            'address'       => 'required',
            'contact_name'  => '',
            'contact_email'  => 'email',
            'contact_phone'  => 'min:11',
        ];
    }
}
