<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class SaveUserRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type === 'admin';
    }

    public function rules()
    {
        return [

            'name'          => 'required',
            'email'         => 'required|email',
            'phone'         => 'required|min:11',
            'address'       => 'required',
            'status'        => 'required'
        ];
    }
}
