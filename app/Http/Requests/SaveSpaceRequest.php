<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class SaveSpaceRequest extends FormRequest
{

    public function authorize()
    {
        return Auth::check() && Auth::user()->type === 'admin';
    }

    public function rules()
    {
        return [
            
            'host_id'       => 'required',
            'location_id'   => 'required',
            'name'          => 'required',
            'description'   => 'required',
            'category_id'   => 'required',
            'rating_id'     => 'required',
            'capacity'      => 'required|numeric',
            'status'        => 'required',
        ];
    }
}
