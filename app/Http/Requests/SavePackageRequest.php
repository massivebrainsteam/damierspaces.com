<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class SavePackageRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type === 'admin';
    }

    public function rules()
    {
        return [
            
            'name'      => 'required',
            'type'      => 'required|in:daily,weekly,monthly',
            'amount'    => 'required|numeric',
            'status'    => 'required|in:active,inactive',
        ];
    }
}
