<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class SaveLocationRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type === 'admin';
    }

    public function rules()
    {
        return [

            'host_id'       => 'required',
            'name'          => 'required',
            'address'       => 'required',
            'status'        => 'required',
        ];
    }
}
