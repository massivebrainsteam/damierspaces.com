<?php

namespace App\Http\Requests\Api\Team;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Rules\CanUpdateSlots;

class UpdateSlotRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'team_admin';
    }


    public function rules()
    {
        return [
            
            'user_id'   => 'required|exists:users,id',
            'slots'     => ['required', 'numeric', new CanUpdateSlots()]
        ];
    }
}
