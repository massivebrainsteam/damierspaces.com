<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class ActivateSubscription extends FormRequest
{
    public function rules()
    {
        return [
            
            'subscription_id'       => 'required|exists:subscriptions,id',
            'transaction_reference' => 'required'
        ];
    }
}
