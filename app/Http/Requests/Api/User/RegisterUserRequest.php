<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{    
    public function rules()
    {
        return [
            
            'name'          => 'required',
            'email'         => 'required|email|unique:users,email',
            'phone'         => 'required|min:11|unique:users,phone',
            'address'       => 'required',
            'password'		=> 'required|confirmed'
        ];
    }
}
