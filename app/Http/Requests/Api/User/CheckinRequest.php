<?php

namespace App\Http\Requests\Api\User;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UserCanCheckIn;
use Auth;

class CheckinRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'user';
    }

    public function rules()
    {
        return [
            
            'space_id'   => ['required', new UserCanCheckIn()]
        ];
    }
}
