<?php

namespace App\Http\Requests\Api\Host;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use App\Rules\CanCheckIn;

class CheckinAttendanceRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'host';
    }

    public function rules()
    {
        return [
            
            'space_id'  => '',
            'token'     => '',
            'user_id'   => ['required', new CanCheckIn()]
        ];
    }
}
