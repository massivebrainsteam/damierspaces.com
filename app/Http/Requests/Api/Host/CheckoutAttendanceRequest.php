<?php

namespace App\Http\Requests\Api\Host;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CheckoutAttendanceRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check() && Auth::user()->type == 'host';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'space_id'  => 'required',
            'user_id'   => 'required'
        ];
    }
}
