<?php

namespace App\Http\Requests\Api\Host;

use Illuminate\Foundation\Http\FormRequest;

class RegisterHostRequest extends FormRequest
{
    public function rules()
    {
        return [

            'name'          => 'required',
            'email'         => 'required|email|unique:hosts',
            'phone'         => 'required|min:11|unique:hosts',
            'address'       => 'required',
            'contact_name'  => 'required',
            'contact_email'  => 'required|email|unique:users,email',
            'contact_phone'  => 'required|min:11|unique:users,phone'
        ];
    }
}
