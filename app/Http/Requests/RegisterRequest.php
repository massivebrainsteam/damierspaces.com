<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\GoogleReCaptcha;

class RegisterRequest extends FormRequest
{
   
    public function rules()
    {
        return [
            
            'name'      			=> 'required',
            'email'     			=> 'required|email|unique:users',
            'phone'     			=> 'required|min:11|unique:users',
            'password'  			=> 'required|confirmed'
        ];
    }
}
