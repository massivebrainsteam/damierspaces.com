<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\CanSubscribe;

class CreateSubscriptionRequest extends FormRequest
{
   
    public function rules()
    {
        return [
            
            'package_id' => ['required', 'numeric', new CanSubscribe],
            'quantity'   => 'required|numeric|min:0'
        ];
    }
}
