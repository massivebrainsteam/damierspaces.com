<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    public function handle($request, Closure $next)
    {
        if(!Auth::check() || optional(Auth::user())->type != 'admin'){

            Auth::logout();
            return redirect('/login');
        }

        return $next($request);
    }
}
