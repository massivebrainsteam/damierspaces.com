<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class User
{

    public function handle($request, Closure $next)
    {
        if(!Auth::check() || optional(Auth::user())->type != 'user')
            return response()->json(['status' => 'Failed', 'message' => 'Forbidden'], 403);

        return $next($request);
    }
}
