<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index()
    {
        if(!request('l')){
            
            return redirect('/');
        }
    
        $data = [];

        $data['iframe_url'] = base64_decode(request('l'));

        if(request('no_footer')){

            $data['no_footer'] = true;
        }
        
        return view('site.page.index', $data);
    }

    public function page($page = 'about')
    {
        $page = 'site.page.'.$page;

        abort_if(!view()->exists($page), 404);

        return view($page);
    }
}
