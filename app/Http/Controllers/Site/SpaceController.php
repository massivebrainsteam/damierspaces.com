<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Space;
use App\Amenity;
use App\SavedSpace;
use App\Reservation;
use Auth;

class SpaceController extends Controller
{
    public function index($id = 0)
    {
    	$space = Space::find($id);

    	if(!$space)
    		return redirect('/');
    	
        $saved = false;

        if(Auth::check()){

            if(SavedSpace::where(['space_id' => $space->id, 'user_id' => Auth::user()->id])->count() > 0){

                $saved = true;
            }
        }
    	$data['space'] 		= $space;
        $data['saved']      = $saved;

    	return view('site.space.index', $data);
    }

    public function checkIn($space_id = 0)
    {
        if(!Auth::check())
            return redirect('/auth/login')->with('error', 'Please login or create an account with us to check in to our spaces.');

        return redirect(_frontend_url('/user/checkin?space_id='.$space_id));
    }
}
