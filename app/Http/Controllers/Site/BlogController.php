<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\BlogService;

class BlogController extends Controller
{
    public function index()
    {
    	$data['title']	= 'Blog';
    	$data['posts']	= BlogService::getPosts(10);

    	return view('site.blog.index', $data);
    }
}
