<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\BlogService;
use App\Http\Services\SpaceService;
use App\Space;
use App\Package;
use App\Category;

class HomeController extends Controller
{
    public function index()
    {
    	$data['featured']	= SpaceService::getFeaturedSpaces();
    	$data['posts']		= BlogService::getPosts();
    	
    	return view('site.home.index', $data);
    }

    public function contact(Request $request)
    {
    	$email = view('emails.site.contact', request()->all())->render();

    	$subject    = 'New Contact from damierspaces.com';
		$body       = view('emails.site.contact', ['subject' => $subject])->render();
		
		_email('hello@damierspaces.com', $subject, $body);

		return redirect('/page/contact#form-contact')->with('message', 'Thank you for contacting us!. We will get back to you soon.');
    }
}
