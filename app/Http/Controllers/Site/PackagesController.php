<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use Auth;

class PackagesController extends Controller
{
    public function index()
    {
        $data['packages']   = Package::whereStatus('active')->orderBy('type')->orderBy('order')->get();
        $data['title'] = 'Packages';

        return view('site.packages.index', $data);
    }

    public function subscribe($id = 0)
    {
        if(!Auth::check())
            return redirect('/login')->with('error', 'Please Login or register to Subscribe.');
        
    	if(Package::whereId($id)->count() < 1)
    		return redirect('/');

        return redirect(_frontend_url('/user/cart?id='.$id));
    }

    
}
