<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Space;
use App\Host;
use App\Category;

class SpacesController extends Controller
{
    public function index()
    {
    	$data['no_footer']	= true;
    	
    	$data['hosts']	    = Host::whereStatus('active')->get();
    	$data['categories']	= Category::whereStatus('active')->get();
    	$data['spaces']		= request()->isMethod('post') ? $this->search() : Space::get();
    	
    	if(request()->isMethod('post')){

    		$spaces = $this->search();
            

    	}else{

    		$spaces = Space::get();
    	}

    	$data['spaces']	= $spaces;

    	return view('site.spaces.index', $data);
    }

    public function search()
    {
        return Space::query()
        ->where('status', 'active')
        ->where(function($query){

            if(request('host_id') > 0){
                
                $query->whereHostId(request('host_id'));
            }

            if(request('category_id') > 0){
                
                $query->whereCategoryId(request('category_id'));
            }
        })
        ->get();
    }
}
