<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Host\RegisterHostRequest;
use Illuminate\Http\Exceptions\PostTooLargeException;
use App\Host;
use App\User;
use Auth;

class BecomeAHostController extends Controller
{
    public function index()
    {
        $data['title'] = 'Become a Host on Damier Spaces';

        return view('site.auth.become-a-host.index', $data);
    }

    public function save(RegisterHostRequest $request)
    {
        $this->validate($request, ['password' => 'required|confirmed']);

        $host = Host::updateOrCreate(['email' => request('email')], [

            'name'          => request('name'),
            'email'         => request('email'),
            'phone'         => request('phone'),
            'address'       => request('address'),
            'description'   => request('description'),
            'country'   => request('country'),
            'state'   => request('state'),
            'city'   => request('city'),
            'contact_name'   => request('contact_name'),
            'contact_phone'   => request('contact_phone'),
            'contact_email'   => request('contact_email'),

        ]);

        $user = User::updateOrCreate(['email' => request('email')], [

            'host_id'   => $host->id,
            'name'      => request('name'),
            'email'     => request('email'),
            'phone'     => request('phone'),
            'api_token' => str_random(16),
            'type'      => 'host',
            'password'  => bcrypt(request('password'))
        ]);

        $subject    = 'Please confirm your account on damierspaces.com';
        $body       = view('emails.host.registration', ['subject' => $subject, 'user' => $user])->render();
        _email($user->email, $subject, $body);

        return view('site.auth.become-a-host.pending-verification');
    }

    public function cac(Request $request)
    {
        try {
            $this->validate($request, [

                'cac' => 'max:1024|mimes:jpeg,png,pdf'
            ]);

            if (request()->isMethod('get')) {
                return view('site.auth.become-a-host.cac');
            }

            if (request('cac')) {
                $response = _cloudinary($request->file('cac'));

                if ($response->link != null) {
                    Auth::user()->host->update(['cac_url' => $response->link]);
                } else {
                    return redirect()->back()->with('error', $response->error);
                }
            }

            return redirect('/become-a-host/bank-info');
        } catch (PostTooLargeException $e) {
            return redirect()->back()->with('error', 'File Size is too large or invalid file. Maximum of 1MB and must be an image or PDF.');
        }
    }

    public function bank(Request $request)
    {
        if (Auth::user()->host->bank_name != null) {
            return redirect(_frontend_url('?api_token='.Auth::user()->api_token));
        }

        if (request()->isMethod('get')) {
            return view('site.auth.become-a-host.bank-info');
        }

        Auth::user()->host->update([

            'bank_name'             => request('bank_name'),
            'bank_account_name'     => request('bank_account_name'),
            'bank_account_number'   => request('bank_account_number')
        ]);

        return redirect('/become-a-host/logo');
    }

    public function logo(Request $request)
    {
        try {
            $this->validate($request, [

                'cac' => 'max:1024|image'
            ]);

            if (request()->isMethod('get')) {
                return view('site.auth.become-a-host.logo');
            }

            if (request('logo')) {
                $response = _cloudinary($request->file('logo'));

                if ($response->link != null) {
                    Auth::user()->update(['photo_url' => $response->link]);
                    Auth::user()->host->update(['photo_url' => $response->link]);
                }
            }

            return redirect(_frontend_url('?api_token='.Auth::user()->api_token));
        } catch (PostTooLargeException $e) {
            return redirect()->back()->with('error', 'File Size is too large or invalid file type. Maximum of 1MB and must be an image.');
        }
    }
}
