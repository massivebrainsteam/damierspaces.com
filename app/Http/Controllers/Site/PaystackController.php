<?php

namespace App\Http\Controllers\Site;

require_once(app_path('Services/Paystack/Paystack.php'));

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscription;
use App\Transaction;

class PaystackController extends Controller
{
	public function verifyPayment($paymentReference = '', $transactionReference = '', $type = 'subscription')
	{		
		$order = null;

		switch($type){

			case 'subscription':
			$order = Subscription::wherePaymentReference($paymentReference)->whereStatus('pending')->first();
			break;
		}
		
		if(!$order)
			return response(['status' => false, 'payment_reference' => $paymentReference]);

		$private_key = env('PAYSTACK_STATUS') == 'test' ? env('PAYSTACK_SECRET_KEY') : env('PAYSTACK_SECRET_KEY');

		$paystack = new \Paystack($private_key);

		try{

			$paystackTransaction = $paystack->transaction->verify(['reference' => $transactionReference]);

			if((bool)$paystackTransaction->status == true){

				$transaction = Transaction::create([

					'user_id'               => $order->user_id,
					'payment_reference'     => $order->payment_reference,
					'amount'                => $order->total,
					'transaction_date'      => date('Y-m-d H:i:s'),
					'status'                => $paystackTransaction->data->status,
					'transaction_reference' => $paystackTransaction->data->reference
				]);

				if ($order instanceof \App\Subscription) {

					$order->update([

						'transaction_id'    => $transaction->id,
						'status'            => 'active',
					]);

				}

				return response()->json([

					'status'                => true, 
					'payment_reference'     => $paymentReference,
					'transaction_reference' => $paystackTransaction->data->reference

				]);

			}else{

				return response()->json(['status' => false]);
			}
			
		}catch(Exception $e){

			return response()->json(['status' => false]);
		}
	}
}
