<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;

class PackagesController extends Controller
{
    public function index($type = 'user')
    {
    	$packages = Package::whereType($type)->get();

    	$data = [];

    	foreach($packages as $row){

    		$row->total_slots;
    		$row->formatted_amount 	= _currency($row->amount);
    		$amenities 				= $row->amenities_names;
    	}

    	return response()->json([

    		'status'	=> 'Successful',
    		'message'	=> 'Packages retrieved successfully.',
    		'data'		=> $packages
    	]);
    }

    public function package($id = 0)
    {
    	$package = Package::find($id);

    	if(!$package){

    		return response()->json([

    			'status'	=> 'Failed',
    			'message'	=> 'Package not found.',
    			'data'		=> null

    		], 404);
    	}

    	$package->total_slots;
    	$package->amenities_names;
    	$package->formatted_amount 	= _currency($package->amount);

    	return response()->json([

    		'status'	=> 'Successful',
    		'message'	=> 'Package retrieved successfully.',
    		'data'		=> $package
    	]);
    }

    public function findPackages(Request $request, $type = 'user')
    {        
        $this->validate($request, [

            'name'                  => 'required',
            'phone'                 => 'min:11',
            'spaces_per_month'      => 'required|numeric|gt:0',
            'locations_per_month'   => 'required||numeric|gt:0',
            'budget'                => 'required|numeric|min:5000'
        ]);

        $budget = (float)request('budget');

        $packages = Package::where('amount', '<=', $budget)
        ->whereType($type)
        ->orderBy('amount', 'desc')
        ->take(3)
        ->get();

        $data   = [];
        $title  = Package::getTitle($budget, request('type'));
        $icon   = Package::getIcon($budget, request('type'));

        foreach($packages as $row){

            $data[] = [

                'id'            => $row->id,
                'name'          => $row->name,
                'duration'      => $row->duration,
                'amount'        => _currency($row->amount),
                'total_slots'   => $row->total_slots,
                'url'           => url('auth/user?package_id='.$row->id)
            ];
        }

        return response()->json([

            'status'    => 'Successful',
            'messgae'   => 'Packages fetched successfully',
            'data'      => $data,
            'title'     => $title,
            'icon'      => $icon
        ]);


    }
}
