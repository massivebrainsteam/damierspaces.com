<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {
    	$categories = Category::whereStatus('active')->get();

    	return response()->json([

    		'status'	=> 'Successful',
    		'message'	=> 'Categories retrieved successfully.',
    		'data'		=> $categories
    	]);
    }
}
