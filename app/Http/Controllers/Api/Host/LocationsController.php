<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;
use Auth;
use App\Http\Services\GoogleService;

class LocationsController extends Controller
{
    public function index()
    {
        $locations = Location::where([

            'host_id' => Auth::user()->host_id,
            'status'  => 'active'

        ])->get();

        foreach ($locations as $row) {
            $row->spaces_count = $row->spaces()->count();
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Locations retrieved successfully.',
            'data'      => $locations
        ]);
    }

    public function location($id = 0)
    {
        $location = Location::where([

            'host_id'   => Auth::user()->host_id,
            'id'        => $id

        ])->first();

        if (!$location) {
            return response()->json([

                'status'    => 'Failed',
                'message'   => 'Location not found',
                'data'      => null

            ]);
        }

        return response()->json([

                'status'    => 'Successful',
                'message'   => 'Location retrieved successfully.',
                'data'      => $location

            ]);
    }

    public function save(Request $request)
    {
        $this->validate($request, [

            'name'          => 'required',
            'address'       => 'required'
        ]);

        $location = Location::updateOrCreate(['id' => request('id')], [

            'host_id'       => Auth::user()->host_id,
            'name'          => request('name'),
            'address'       => request('address'),
            'photo_url'     => request('photo_url'),
            'status'        => 'active',
            'weekends'      => request('weekends'),
            'state'      => request('state'),
            'city'      => request('city')
        ]);

        $coordinates = GoogleService::getCoordinatesFromAddress(request('address'));

        if ($coordinates->latitude > 0) {
            $location->update(['latitude' => $coordinates->latitude, 'longitude' => $coordinates->longitude]);
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Location saved successfully.',
            'data'      => $location->refresh()

        ], 201);
    }
}
