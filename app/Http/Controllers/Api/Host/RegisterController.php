<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Host\RegisterHostRequest;
use App\Host;
use App\User;
use Auth;

class RegisterController extends Controller
{
	public function index(RegisterHostRequest $request)
	{
		$host = Host::updateOrCreate(['email' => request('email')], [

			'name'			=> request('name'),
			'email'			=> request('email'),
			'phone'			=> request('phone'),
			'address'		=> request('address'),
			'description'   => request('description')
		]);

		$password = str_random(6);

		$user = User::updateOrCreate(['email' => request('contact_email')], [

			'host_id'   => $host->id,
			'name'		=> request('contact_name'),
			'email'		=> request('contact_email'),
			'phone'		=> request('contact_phone'),
			'api_token'	=> str_random(16),
			'type'		=> 'host',
			'password'	=> bcrypt($password)
		]);

		$subject = 'Please confirm your account on damierspaces.com';
		$body = view('emails.host.registration', ['subject' => $subject, 'user' => $user, 'password' => $password])->render();
		_email($user->email, $subject, $body);

		$subject = 'Thank you for joining damierspaces.com';
		$body = view('emails.host.host-registration', ['subject' => $subject, 'host' => $host])->render();
		_email($host->email, $subject, $body);

		return response()->json([

			'status'	=> 'Successful',
			'message'	=> 'Registration Successful',
			'data'		=> ['host' => $host, 'user' => $user]

		], 201);
	}

	public function updateDetails(Request $request)
	{
		$host = Auth::user()->host;

		if(!$host)
			return response()->json(['status' => 'Failed', 'message' => 'Host not found.'], 404);
		
		$host->update([

			'cac_url'				=> request('cac_url', $host->cac_url),
			'bank_name'				=> request('bank_name', $host->bank_name),
			'bank_account_number'	=> request('bank_account_number', $host->bank_account_number),
			'bank_account_name'		=> request('bank_account_name', $host->bank_account_name),
		]);

		return response()->json(['status' => 'Successful', 'message' => 'Host Updated successfully.']);
	}
}
