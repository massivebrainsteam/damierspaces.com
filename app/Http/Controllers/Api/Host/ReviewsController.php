<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;
use Auth;

class ReviewsController extends Controller
{
	public function index()
	{
		$reviews = Review::whereHostId(Auth::user()->host_id)->get();

		$data = [];

		foreach($reviews as $row){

			$data[] = [

				'id'				=> $row->id,
				'space'				=> $row->space,
				'user'				=> $row->user,
				'review'			=> $row->review,
				'rating'			=> $row->rating,
				'created_at'		=> _date($row->created_at, true)
			];
		}

		return response()->json([

			'status'    => 'Successful',
			'message'   => 'Reviews retrieved successfully.',
			'data'      => $data
		]);
	}
}
