<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settlement;
use Auth;

class SettlementsController extends Controller
{
    public function index()
    {
    	$settlements = Settlement::whereHostId(Auth::user()->host_id)->get();

    	$data = [];

    	foreach($settlements as $row){

    		$data[] = [

    			'id'			=> $row->id,
    			'created_at'	=> _date($row->created_at),
    			'approved_by'	=> $row->approver->name,
    			'total'			=> _currency($row->total),
    			'attendances'	=> $row->attendances()->count()
    		];
    	}

    	return response()->json([

            'status'    => 'Successful',
            'message'   => 'Settlements retrieved successfully.',
            'data'      => $data
        ]);
    }
}
