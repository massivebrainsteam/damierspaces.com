<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Host\CheckinAttendanceRequest;
use App\Http\Requests\Api\Host\CheckoutAttendanceRequest;
use App\Attendance;
use App\AttendanceToken;
use App\Space;
use Auth;

class AttendancesController extends Controller
{

    public function attendances()
    {
        $host_id = Auth::user()->host->id;

        $all = Attendance::whereHas('space', function($query) use($host_id) {

            return $query->whereHostId($host_id);

        })
        ->get();

        $pending = Attendance::whereHas('space', function($query) use($host_id) {

            return $query->whereHostId($host_id);

        })
        ->whereStatus('pending')
        ->get();

        $active = Attendance::whereHas('space', function($query) use($host_id) {

            return $query->whereHostId($host_id);

        })
        ->whereStatus('active')
        ->get();

        $expired = Attendance::whereHas('space', function($query) use($host_id) {

            return $query->whereHostId($host_id);

        })
        ->whereStatus('expired')
        ->take(50)
        ->get();

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Attendances retrieved successfully.',
            'data'      => [

                'all'       => $this->formatAttendances($all),
                'pending'   => $this->formatAttendances($pending),
                'active'    => $this->formatAttendances($active),
                'expired'   => $this->formatAttendances($expired)
            ]
        ]);
    }

    private function formatAttendances($attendances)
    {
        $data = [];

        foreach($attendances as $row){

            $data[] = [

                'id'                => $row->id,
                'user_id'           => $row->user_id,
                'space_id'          => $row->space_id,
                'user'              => $row->user->name,
                'space'             => $row->space->name,
                'package'           => $row->user->subscription->package->name,
                'location'          => $row->space->location->address,
                'host'              => $row->space->host->name,
                'reference'         => $row->reference,
                'created_at'        => _date($row->created_at),
                'start_at'          => _date($row->start_at, true),
                'end_at'            => _date($row->end_at, true),
                'time_left'         => $row->end_at->diff(now())->format('%H Hrs %I Mins Left'),
                'status'            => strtolower($row->status),
                'settlement'        => _currency($row->settlement),
                'settlement_status' => $row->settlement_status
            ];
        }

        return collect($data);
    }
}
