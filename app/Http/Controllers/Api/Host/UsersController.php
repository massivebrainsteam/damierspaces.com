<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Space;
use App\Attendance;

class UsersController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
    	$users = $user->host->getUsersWhoHaveUsedASpaceWhichBelongsToThisHost();

    	$data = [];

    	foreach($users as $row){

    		$data[] = [

    			'id'						=> $row->id,
    			'name'						=> $row->name,
    			'email'						=> $row->email,
    			'phone'						=> $row->phone,
    			'package'					=> $row->subscription->package->name,
    			'subscription_expire_at'	=> _date($row->subscription->end_at),
    			'checkin_status'			=> $row->checkin_status
    		];

    	}

    	return response()->json([

    		'status'	=> 'Successful',
    		'message'	=> 'Users retrieved successfully.',
    		'data'		=> $data
    	]);
    }
}
