<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Space;
use App\Location;
use App\SpaceDay;
use App\Amenity;
use App\SpaceAmenity;
use App\SpacePhoto;
use App\Attendance;

class SpacesController extends Controller
{
    public function index()
    {
        $spaces = Space::whereHostId(Auth::user()->host_id)->get();

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Spaces retrieved successfully.',
            'data'      => Space::transform($spaces)
        ]);
    }

    public function space($id = 0)
    {
        $spaces = Space::where([

            'host_id'   => Auth::user()->host_id,
            'id'        => $id

        ])->get();

        if($spaces->count() < 1){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'Space not found.',
                'data'      => null
            ]);
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Spaces retrieved successfully.',
            'data'      => Space::transform($spaces)->first()
        ]);
    }

    public function save(Request $request)
    {
        $this->validate($request, [

            'name'          => 'required',
            'location_id'   => 'required',
            'category_id'   => 'required',
            'capacity'      => 'required|numeric'
        ]);

        $location = Location::firstOrNew(['id' => request('location_id')]);

        $space = Space::updateOrCreate(['id' => request('id')], [

            'host_id'               => Auth::user()->host_id,
            'category_id'           => request('category_id'),
            'location_id'           => $location->id,
            'name'                  => request('name'),
            'description'           => request('description'),
            'capacity'              => request('capacity'),
            'photo_url'             => request('photo_url'),
            'rules'                 => request('rules')
        ]);

        if(request('id') < 1)
            $space->update(['avaliable_capacity' => request('capacity'), 'status' => 'inactive']);

        $amenities = request('amenities') ?? [];

        foreach($amenities as $group){

            foreach($group['amenities'] as $row){

                $amenity = Amenity::find($row['id']);

                if($amenity){

                    $where = ['space_id' => $space->id, 'amenity_id' => $amenity->id];

                    $space_amenity = SpaceAmenity::where($where)->first();

                    if($row['switch'] == false){

                        SpaceAmenity::where($where)->delete();

                    }else{

                        SpaceAmenity::create($where);
                    }
                }
            }
        }
        

        foreach(request('photos') ?? [] as $url){

            SpacePhoto::create(['space_id' => $space->id, 'url' => $url]);
        }

        if(request('days')){

            foreach(request('days') as $row){

                SpaceDay::updateOrCreate(['space_id' => $space->id, 'day' => $row['day']], [

                    'space_id'  => $space->id, 
                    'day'       => $row['day'], 
                    'start_at'  => $row['start_at'],
                    'end_at'    => $row['end_at']
                ]);
            }
        }

        $message = 'Space updated successfully';

        if(request('id') < 1)
            $message = 'Your space has been successfully submited. Please not that spaces are subject to reviews before going live.';

        return response()->json([

            'status'    => 'Successful', 
            'message'   => $message,
            'data'      => $space->refresh()

        ], 201);

    }

    public function days($space_id = 0)
    {
        $space = Space::find($space_id);

        $days = [];

        if($space && $space->days->count() > 0){

            $days = $space->days;

        }else{

            foreach(_days() as $row){

                $day = new SpaceDay();
                $day->day = $row;
                $day->start_at = '8:00 AM';
                $day->end_at = '5:00 PM';
                $days[] = $day;
            }
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Space days retrieved successfully.',
            'data'      => collect($days)
        ]); 
    }

    public function photo($space_id = 0)
    {
        $space = Space::find($space_id);

        if(!$space){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'Space not found.',
                'data'      => null
            ]);
        }

        if(request('photo_url')){

            SpacePhoto::create([

                'space_id'  => $space->id,
                'url'       => request('photo_url')
            ]);
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Space photo saved successfully.',
            'data'      => null
        ]); 
    }

    public function deletePhoto($photo_id = 0)
    {
        $photo = SpacePhoto::find($photo_id);

        if($photo)
            $photo->delete();
        
        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Space photo deleted successfully.',
            'data'      => null
        ]); 
    }
}
