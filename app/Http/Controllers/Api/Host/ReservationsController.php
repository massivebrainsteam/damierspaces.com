<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reservation;
use Auth;

class ReservationsController extends Controller
{
    public function index()
    {
    	$reservations = Reservation::whereHostId(Auth::user()->host_id)->get();

    	$data = [];

    	foreach($reservations as $row){

    		$data[] = [

    			'id'			=> $row->id,
    			'space'			=> $row->space->name,
                'address'       => $row->location->address,
    			'user'			=> $row->user->name,
    			'start_at'		=> _date($row->start_at, true),
                'end_at'        => _date($row->end_at, true),
    			'reserved_at'	=> _date($row->created_at, true),
    		];
    	}

    	return response()->json([

            'status'    => 'Successful',
            'message'   => 'Reservations retrieved successfully.',
            'data'      => $data
        ]);
    }
}
