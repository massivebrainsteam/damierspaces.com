<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AttendanceToken;
use App\Attendance;
use App\User;
use Auth;

class AttendanceTokensController extends Controller
{
    public function getUser(Request $request)
    {
        $this->validate($request, [

            'email' => 'required',
            'token' => 'required'        
        ]);

        $user = User::whereEmail(request('email'))->active()->first();

        if(!$user){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'User not found',
                'data'      => null

            ], 404);
        }

        if($user->subscription_status == 'inactive'){

            return response()->json([

                'status'    => 'Failed',
                'message'   => $user->name.' is currently inactive',
                'data'      => null

            ], 400);
        }

        $attendance_token = AttendanceToken::where([

            'user_id'   => $user->id,
            'token'     => request('token'),
            'status'    => 'unused'

        ])->with(['user', 'space', 'location', 'host'])->first();

        if(!$attendance_token){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'Invalid Token',
                'data'      => null

            ], 404);
        }

        if(Auth::user()->host->id != $attendance_token->space->host_id){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'Invalid Location. This token cannot be used in selected location.',
                'data'      => null

            ], 400);
        }
        
        $attendance_token->user->subscription->package;

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'User retrieved successfully.',
            'data'      => $attendance_token
        ]);
    }

    public function useToken(Request $reqeust)
    {
    	$this->validate($reqeust, ['token' => 'required|numeric']);

    	$token = AttendanceToken::where(['token' => request('token'), 'status'	=> 'unused'])->first();

    	if(!$token){

    		return response()->json([

    			'status'	=> 'Failed',
    			'message'	=> 'Invalid Token',
    			'data'		=> null

    		], 400);
    	}

        if($token->space->space_status != 'open'){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'User cannot check in to this space at the moment. it is currently '.$token->space->space_status,
                'data'      => null

            ], 400);
        }
        
        $attendance = Attendance::create([

            'space_id'  	=> $token->space_id,
            'location_id'	=> $token->location_id,
            'host_id'		=> $token->host_id,
            'user_id'       => $token->user_id,
            'team_id'   	=> $token->user->team_id,
            'approved_by'	=> Auth::user()->id,
            'reference'		=> $token->token,
            'start_at'		=> now()->format('Y-m-d H:i:s'),
            'end_at'		=> now()->addHours(24)->format('Y-m-d H:i:s'),
            'status'    	=> 'active',
            'settlement'    => $token->user->subscription->settlement

        ]);

        $token->space->update(['avaliable_capacity' => $token->space->avaliable_capacity - 1]);
        $token->update(['status' => 'used']);

        return response()->json([

            'status'	=> 'Successful',
            'message'	=> 'User has been successfully checked In'
        ]);


    }

    public function checkOut($attendance_id = 0)
    {
        $attendance = Attendance::where([

            'id'        => $attendance_id,
            'status'    => 'active'

        ])->first();

        if(!$attendance){

            return response()->json([

                'status' => 'validation-failed',
                'message' => 'Invalid Attendance'

            ], 422);
        }

        $space = $attendance->space;

        $attendance->update(['status' => 'expired', 'end_at' => now()]);
        $space->update(['avaliable_capacity' => $space->avaliable_capacity + 1]);

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'User has been successfully checked Out'
        ]);
    }
}
