<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Amenity;
use App\Space;
use App\SpaceAmenity;

class AmenitiesController extends Controller
{
    public function index($space_id = 0)
    {
    	$groups = DB::table('amenities')
    	->select(DB::raw('max(category) as category'))
    	->groupBy('category')
    	->get();

    	$data = [];

    	foreach($groups as $row){

            $amenities = [];
            $all_amenities = Amenity::whereCategory($row->category)->get();

            foreach($all_amenities as $amenity){

                $space_amenity = SpaceAmenity::where(['space_id' => $space_id, 'amenity_id' => $amenity->id])->count();

                $amenity->switch = $space_amenity > 0 ? true : false;
            }
            
            $data[] = [

               'group'			=> strtoupper($row->category),
               'amenities'		=> $all_amenities
           ];
       }

       return response()->json([

          'status' 	=> 'Successful',
          'message'	=> 'Amenities retrieved successfully.',
          'data'		=> $data
      ]);
   }
}
