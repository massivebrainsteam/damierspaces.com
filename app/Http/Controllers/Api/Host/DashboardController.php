<?php

namespace App\Http\Controllers\Api\Host;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Attendance;
use App\Space;
use App\Reservation;
use App\Review;
use App\Location;
use App\Settlement;

class DashboardController extends Controller
{
    public function index()
    {
        $id 			= Auth::user()->host->id;
        $attendances 	= Attendance::whereHostId($id)->whereStatus('active')->count();
        $spaces      	= Space::whereHostId($id)->count();
        $reservations	= Reservation::whereHostId($id)->whereStatus('active')->count();
        $reviews		= Review::whereHostId($id)->count();
        $locations = Location::whereHostId($id)->count();
        $settlement = Settlement::whereHostId($id)->sum('total');

        return response()->json([

            'status'	=> 'Successful',
            'message'	=> 'Dashboard data retrieved successfully.',
            'data'		=> [

                'active_attendances'	=> $attendances,
                'spaces'				=> $spaces,
                'reservations'			=> $reservations,
                'reviews'				=> $reviews,
                'locations' => $locations,
                'settlement' => _currency($settlement)
            ]
        ]);
    }
}
