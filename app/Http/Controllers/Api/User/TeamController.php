<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Team;

class TeamController extends Controller
{
    public function index()
    {
    	return response()->json([

    		'status'	=> 'Successful',
    		'message'	=> 'Users retrieved successfully.',
    		'data'		=> Auth::user()->team->users
    	]);
    }
}
