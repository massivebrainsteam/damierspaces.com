<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AttendanceToken;
use App\Rules\UserCanCheckIn;
use Auth;
use App\Space;

class AttendanceTokensController extends Controller
{
    public function create(Request $request)
    {
    	$this->validate($request, [

        'space_id'  => ['required', 'exists:spaces,id', new UserCanCheckIn()]
    	]);

    	$space = Space::find(request('space_id'));

        if($space->space_status != 'open'){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'This is space is currently '.$space->space_status,
                'data'      => null
            ]);
        }

        $token = AttendanceToken::create([

          'user_id'		=> Auth::user()->id,
          'space_id'		=> $space->id,
          'location_id'	=> $space->location_id,
          'host_id'		=> $space->host_id,
          'token'			=> mt_rand(111111, 999999)
      ]);

        return response()->json([

          'status'	=> 'Successful',
          'message'	=> 'Token generated successfully',
          'data'		=> $token
      ]);
    }
}
