<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Space;
use App\SavedSpace;
use App\Http\Services\SpaceService;
use App\Reservation;
use App\Attendance;

class SpacesController extends Controller
{
    public function index($filter = '', $query = '')
    {
        if($filter == 'saved'){

            $spaces = Space::whereHas('saved_spaces', function($query){

                return $query->where(['user_id' => Auth::user()->id]);

            })
            ->whereStatus('active')
            ->get();

        }else if($filter == 'nearby'){

            $spaces = Space::whereStatus('active')->get();

        }else if($filter == 'search'){

            $spaces = Space::search($query);

        }else if($filter == 'reserved'){

            $spaces = Space::whereHas('reservations', function($query){

                return $query->where(['user_id' => Auth::user()->id]);

            })
            ->whereStatus('active')
            ->get();

        }else{

            $spaces = SpaceService::getSpacesNearYou(); 
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Spaces retrieved successfully.',
            'data'      => Space::transform($spaces)
        ]);
    }

    public function single($id = 0)
    {
        $space = Space::with(['host', 'category', 'location', 'rating', 'amenities', 'days', 'photos', 'reviews'])->where(['id' => $id])->first();

        if(!$space){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'Space not found.',
                'data'      => null

            ], 404);
        }


        $space->related = Space::where('id', '!=', $space->id)->inRandomOrder()->with(['location'])->take(3)->get();

        foreach($space->reviews as $row)
            $row->user;

        $space->saved       = SavedSpace::where(['user_id' => Auth::user()->id, 'space_id' => $space->id])->first();
        $space->reservation = Reservation::where(['user_id' => Auth::user()->id, 'space_id' => $space->id, 'status' => 'active'])->first();

        $attendance = Attendance::where([

            'user_id'   => Auth::user()->id,
            'space_id'  => $space->id,
            'status'    => 'active'

        ])->first();

        if($attendance){

            $attendance->space;
            $space->attendance = $attendance;
        
        }else{

            $space->attendance = null;
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Space retrieved successfully.',
            'data'      => $space
        ]);
    }

    public function saveSpace($space_id = 0)
    {

        $space = Space::find($space_id);

        if(!$space){

            return response()->json([

                'status'    => 'Failed', 
                'message'   => 'The space you are trying to save does not exist.',
                'data'      => null

            ], 400); 
        }

        $user = Auth::user();

        $savedSpace = SavedSpace::where(['space_id' => $space->id, 'user_id' => $user->id])->first();

        if($savedSpace){

            $savedSpace->delete();

            return response()->json([

                'status'    => 'Successful', 
                'message'   => 'Space has been successfully unsaved',
                'data'      => null
            ]);
        }

        $saved = SavedSpace::create([

            'space_id'      => $space->id, 
            'user_id'       => $user->id,
            'location_id'   => $space->location_id,
            'host_id'       => $space->host_id
        ]);

        return response()->json([

            'status'    => 'Successful', 
            'message'   => 'Space has been successfully Saved',
            'data'      => $saved
        ]);
    }
}
