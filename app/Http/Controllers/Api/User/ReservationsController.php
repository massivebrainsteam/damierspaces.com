<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reservation;
use Auth;
use App\Space;
use App\Rules\CanReserveSpace;

class ReservationsController extends Controller
{
    public function index()
    {
        $reservations = Reservation::whereUserId(Auth::user()->id)->get();

        $data = [];

        foreach($reservations as $row){

            $data[] = [

                'id'            => $row->id,
                'space'         => $row->space,
                'host'          => $row->host,
                'location'      => $row->space->location,
                'user'          => $row->user,
                'start_at'      => _date($row->start_at, true),
                'end_at'        => _date($row->end_at, true),
                'time_left'     => $row->end_at->diff($row->start_at)->format('%iMins %sSecs')
            ];
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Reservations retrieved successfully.',
            'data'      => $data
        ]);
    }

    public function reserveSpace(Request $request)
    {
        $this->validate($request, [

            'space_id' => ['required', new CanReserveSpace]
        ]);

        $space      = Space::find(request('space_id'));
        $user       = Auth::user();
        $package    = $user->subscription->package;

        $reservation = Reservation::create([

            'space_id'      => $space->id,
            'location_id'   => $space->location_id,
            'host_id'       => $space->host_id,
            'user_id'       => $user->id,
            'team_id'       => $user->team_id,
            'start_at'      => now()->format('Y-m-d H:i:s'),
            'end_at'        => now()->addMinutes($package->reservation_duration ?? 10)->format('Y-m-d H:i:s'),
            'status'        => 'active'
        ]);

        $space->update(['avaliable_capacity' => $space->avaliable_capacity - 1]);

        $subject = 'You just reserved a space on damierspaces.com';
        $body = view('emails.user.reservation', ['subject' => $subject, 'reservation' => $reservation])->render();
        
        _email($reservation->user->email, $subject, $body);

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Reservation Made successfully. This Space will be locked down for you for the next '.$package->reservation_duration.' minutes',
            'data'      => $reservation

        ]);
    }
}
