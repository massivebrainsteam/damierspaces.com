<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use Auth;

class TransactionsController extends Controller
{
    public function index()
    {
    	$transactions = Transaction::whereUserId(Auth::user()->id)->orderBy('id', 'desc')->get();

    	$data = [];

    	foreach($transactions as $row){

    		$data[] = [

    			'id'			=> $row->id,
    			'reference'		=> $row->payment_reference,
    			'date'			=> _date($row->created_at),
    			'amount'		=> _currency($row->amount),
    			'status'		=> $row->status
    		];
    	}

    	return response()->json([

            'status'    => 'Successful',
            'message'   => 'Transactions retrieved successfully.',
            'data'      => $data
        ]);
    }
}
