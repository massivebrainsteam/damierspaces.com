<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\Api\User\RegisterUserRequest;

class RegisterController extends Controller
{
    public function index(RegisterUserRequest $request)
	{
		$user = User::updateOrCreate(['email' => request('email')], [

			'name'			=> request('name'),
			'email'			=> request('email'),
			'phone'			=> request('phone'),
			'address'		=> request('address'),
			'api_token'		=> str_random(16),
			'type'			=> 'user',
			'status'		=> 'inactive',
			'password'		=> bcrypt(request('password'))
		]);

		$subject = 'Please confirm your account on damierspaces.com';
		$body = view('emails.user.registration', ['subject' => $subject, 'user' => $user])->render();
		_email($user->email, $subject, $body);

		return response()->json([

			'status'	=> 'Successful',
			'message'	=> 'Registration Successful.',
			'data'		=> User::find($user->id)

		], 201);
	}
}
