<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;
use Auth;
use App\Space;
use App\Attendance;

class ReviewsController extends Controller
{
    public function index()
	{
		$reviews = Review::whereUserId(Auth::user()->id)->get();

		$data = [];

		foreach($reviews as $row){

			$data[] = [

				'id'				=> $row->id,
				'space'				=> $row->space,
				'user'				=> $row->user->name,
				'user_photo_url'	=> $row->user->photo_url,
				'review'			=> $row->review,
				'rating'			=> $row->rating,
				'created_at'		=> _date($row->created_at, true)
			];
		}

		return response()->json([

			'status'    => 'Successful',
			'message'   => 'Reviews retrieved successfully.',
			'data'      => $data
		]);
	}

	public function save(Request $request, $space_id = 0)
	{
		$this->validate($request, [

			'review'	=> 'required'
		]);

		$space = Space::find($space_id);

		if(!$space){

			return response()->json([

				'status'	=> 'Failed',
				'message'	=> 'Space not found.',
				'data'		=> null

			], 404);
		}

		$user = Auth::user();

		if(Attendance::where(['space_id' => $space->id, 'user_id' => $user->id])->count() < 1){

			return response()->json([

				'status'	=> 'Failed',
				'message'	=> 'You can only review spaces that you have checked in to at least once.',
				'data'		=> null

			], 400);
		}

		$review = Review::create([

			'space_id'	=> $space->id,
			'host_id'	=> $space->host_id,
			'user_id'	=> $user->id,
			'team_id'	=> $user->team_id,
			'rating'	=> request('rating', 1),
			'review'	=> request('review'),
			'status'	=> 'active'
		]);

		return response()->json([

			'status'    => 'Successful',
			'message'   => 'Review has been saved successfully.',
			'data'      => $review
		]);
	}
}
