<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attenance;
use App\Http\Requests\Api\User\CheckinRequest;
use Auth;
use App\Attendance;

class AttendanceController extends Controller
{
    
    public function checkOut(Request $request)
    {
        $this->validate($request, ['space_id' => 'required' ]);
        
        $attendance = Attendance::where([

            'user_id'   => Auth::user()->id,
            'space_id'  => request('space_id'),
            'status'    => 'active'

        ])->first();

        if(!$attendance){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'Attendance not found',
                'data'      => null

            ], 404);
        }

        $attendance->update([

            'end_at'    => now()->format('Y-m-d H:i:s'),
            'status'    => 'expired'
        ]);

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Checkout successful.',
            'data'      => $attendance->refresh()
        ]);
    }

    public function attendances()
    {
        $attendances = Attendance::whereUserId(Auth::user()->id)->orderBy('id', 'desc')->get();

        $data = [];

        foreach($attendances as $row){


            $data[] = [

                'id'            => $row->id,
                'space'         => $row->space->name,
                'host'          => $row->space->host->name,
                'created_at'    => _date($row->created_at),
                'start_at'      => _date($row->start_at, true),
                'end_at'        => _date($row->end_at, true),
                'status'        => strtoupper($row->status),
                'reference'     => $row->reference
            ];
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Attendances retrieved successfully.',
            'data'      => $data
        ]);
    }

    public function attendance($id = 0)
    {
        $attendance = Attendance::where(['id' => $id, 'user_id' => Auth::user()->id])->with(['space', 'host', 'location', 'approver'])->first();

        if(!$attendance){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'Attendance not found',
                'data'      => null

            ], 404);
        }

        $attendance->time_used;

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Attendance retrieved successfully',
            'data'      => $attendance
        ]);
    }


}
