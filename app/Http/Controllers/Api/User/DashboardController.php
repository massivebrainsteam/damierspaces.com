<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Attendance;
use App\Http\Services\SpaceService;
use App\Space;

class DashboardController extends Controller
{
    public function index()
    {
    	$attendance = Attendance::where([

    		'user_id'	=> Auth::user()->id,
    		'status'	=> 'active'

    	])->with(['space', 'location', 'host'])
        ->orderBy('id', 'desc')
        ->first();

        $previous_attendance = Attendance::where([

            'user_id'   => Auth::user()->id,
            'status'    => 'active'

        ])
        ->where('id', '!=', optional($attendance)->id)
        ->with(['space', 'location', 'host'])
        ->orderBy('id', 'desc')
        ->first();

    	$subscription      = Auth::user()->subscription()->with('package')->first();
        $nearby_spaces      = SpaceService::getSpacesNearYou();
        $featured_spaces    = SpaceService::getFeaturedSpaces();

    	return response()->json([

    		'status'	=> 'Successful',
    		'message'	=> 'Dashboard info retrieved successfully.',
    		'data'		=> [

    			'attendance'	        => $attendance,
                'previous_attendance'   => $previous_attendance,
    			'subscription'	        => $subscription,
                'nearby_spaces'         => Space::transform($nearby_spaces),
                'featured_spaces'       => Space::transform($featured_spaces)
    		]
    	]);
    }
}
