<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscription;
use App\Transaction;
use App\Package;
use Auth;
use Illuminate\Support\Str;
use App\Http\Requests\CreateSubscriptionRequest;
use App\Http\Requests\Api\User\ActivateSubscription;

class SubscriptionsController extends Controller
{
    public function index()
    {
    	$user = Auth::user();

    	$subscriptions = Subscription::whereUserId($user->id)
    	->where('status', '!=', 'pending')
    	->orderBy('id', 'desc')
    	->get();

    	$data = [];

    	foreach($subscriptions as $row){

    		$data[] = [

    			'id'			=> $row->id,
    			'package'		=> $row->package->name,
    			'package_url'	=> url('package/'.$row->package->id),
    			'start_at'		=> _date($row->start_at),	
    			'end_at'		=> _date($row->end_at),	
    			'status'		=> $row->status,
    			'total'			=> _currency($row->total),
    			'created_at'	=> _date($row->created_at)
    		];

    	}

    	return response()->json([

    		'status'	=> 'Successful',
    		'message'	=> 'Subscriptoins retrieved successfully.',
    		'data'		=> [

    			'active_subscription'	=> $user->subscription,
    			'subscriptions'			=> $data
    		]
    	]);
    }

    public function createSubscription(CreateSubscriptionRequest $request)
    {
        $user                   = Auth::user();
        $quantity               = request('quantity');
        $package                = Package::find(request('package_id'));
        $existing_subscription  = $user->subscription;

        $start_at   = now();
        $end_at     = now();

        switch($package->duration){

            case 'daily':
            $end_at->addDays($quantity);
            break;

            case 'weekly':
            $end_at->addWeeks($quantity);
            break;

            case 'monthly':
            $end_at->addMonths($quantity);
            break;
        }

        $total = $package->amount * $quantity;

        $transaction = Transaction::create([

            'user_id'               => $user->id,
            'payment_reference'     => Str::random(8),
            'amount'                => $total,
            'status'                => 'pending'
        ]);

        $subscription = Subscription::create([

            'user_id'               => $user->id,
            'team_id'               => $user->team_id,
            'package_id'            => $package->id,
            'transaction_id'        => $transaction->id,
            'payment_reference'     => Str::random(12),
            'total'                 => $total,
            'start_at'              => $start_at,
            'end_at'                => $end_at,
            'status'                => 'pending'
        ]);

        $subscription->user;
        $subscription->payment_url = url('api/generic/p/'.$transaction->payment_reference);

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Subscription request successsful. Please proceed to payment.',
            'data'      => $subscription
        ]);
    }

    public function activateSubscription(ActivateSubscription $request)
    {
        $user = Auth::user();

        $subscription = Subscription::where([

            'id'        => request('subscription_id'),
            'user_id'   => $user->id,
            'status'    => 'pending'

        ])->first();

        if(!$subscription){

            return response()->json([

                'status'    => 'Failed',
                'message'   => 'There is no pending subscription for '.$user->name,
                'data'      => null
            ], 422);
        }

        $transaction = Transaction::create([

            'user_id'               => $user->id,
            'payment_reference'     => $subscription->payment_reference,
            'amount'                => $subscription->total,
            'transaction_date'      => date('Y-m-d H:i:s'),
            'status'                => 'success',
            'transaction_reference'  => request('transaction_reference')

        ]);

        $subscription->activate($transaction);
         
        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Subscription has been activated',
            'data'      => $subscription
        ]);
    }
}
