<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;

class LoginController extends Controller
{
    public function login()
    {
        if (request('api_token')) {
            $user = User::whereApiToken(request('api_token'))->first();

            if (!$user) {
                return response()->json([

                    'status'    => 'Failed',
                    'message'   => 'Invalid username or password',
                    'data'      => null

                ], 422);
            }

            if ($user->type == 'host') {
                $user->host;
                $user->host->locations_count = $user->host->locations()->count();
                $user->host->spaces_count = $user->host->spaces()->count();
            } else {
                $user->subscription;
                $user->subscription_status;
                $user->slots_left;
            }

            return response()->json([

                'status'    => 'Successful',
                'message'   => 'Login Successful',
                'data'      => $user
            ]);
        }
    }

    public function loginEmail(Request $request)
    {
        $this->validate($request, [

            'email'         => 'required|exists:users,email',
            'password'      => 'required'
        ]);

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            return response()->json([

                'status'    => 'Successful',
                'message'   => 'Login Successful',
                'data'      => Auth::user()
            ]);
        }

        return response()->json([

            'status'    => 'Failed',
            'message'   => 'Invalid username or password',
            'data'      => null

        ], 422);
    }

    public function profile()
    {
        $user = Auth::user();

        if ($user->type == 'host') {
            $user->host;
            $user->host->locations_count = $user->host->locations()->count();
            $user->host->spaces_count = $user->host->spaces()->count();
        } else {
            $user->subscription;
            $user->subscription_status;
            $user->slots_left;
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'User retrieved successfully',
            'data'      => $user
        ]);
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [

            'email' => 'email',
            'phone' => 'min:11'
        ]);

        $user = Auth::user();

        $user->update([

            'name'          => request('name', $user->name),
            'email'         => request('email', $user->email),
            'phone'         => request('phone', $user->phone),
            'photo_url'     => request('photo_url', $user->photo_url),
            'address'       => request('address', $user->address),
            'occupation'    => request('occupation', $user->occupation),
            'sex'           => request('sex', $user->sex),
        ]);

        if ($user->type == 'host') {
            $host = $user->host;

            $_host = (object)request('host');

            $host->update([

                'name'                  => $_host->name ?? $host->name,
                'email'                 => $_host->email ?? $host->email,
                'phone'                 => $_host->phone ?? $host->phone,
                'bank_name'             => $_host->bank_name ?? $host->bank_name,
                'bank_account_number'   => $_host->bank_account_number ?? $host->bank_account_number,
                'bank_account_name'     => $_host->bank_account_name ?? $host->bank_account_name,
            ]);

            if (request('cac_url')) {
                $host->update(['cac_url' => request('cac_url')]);
            }
        }

        $user = User::with('host')->find($user->id);

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Profile updated successfully.',
            'data'      => $user

        ]);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [

            'current_password' => 'required',
            'new_password' => 'required|min:5|confirmed'
        ]);

        if (!app('hash')->check(request('current_password'), Auth::user()->password)) {
            return response()->json([

                'status'    => 'Failed',
                'message'   => 'Current Password does not match new password',
                'data'      => null

            ], 400);
        }

        Auth::user()->update([

            'password'  => bcrypt(request('new_password'))
        ]);

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Password changed successfully',
            'data'      => null
        ]);
    }

    public function logout($token = '')
    {
        $user = User::whereApiToken($token)->first();

        if ($user) {
            $user->update(['api_token' => str_random(16)]);
        }

        Auth::logout();

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Logout Successful',
            'data'      => [

                'url'   => url('/auth/logout')
            ]
        ]);
    }

    public function forgotPassword(Request $request)
    {
        $this->validate($request, [

            'email' => 'required|exists:users,email'
        ]);

        $user = User::whereEmail(request('email'))->first();

        $user->update(['api_token' => mt_rand(000000, 999999)]);

        $subject    = 'Reset your Password on Damier Spaces';
        $body       = view('emails.auth.reset-password-mobile', ['subject' => $subject, 'user' => $user])->render();

        _email($user->email, $subject, $body);

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'A password reset token has been sent to your email address.',
            'data'      => [

                'token' => $user->api_token
            ]
        ]);
    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [

            'token'         => 'required|exists:users,api_token',
            'password'      => 'required|min:6|confirmed'
        ]);

        $user = User::whereApiToken(request('token'))->first();

        $user->update([

            'api_token' => str_random(16),
            'password'  => bcrypt(request('password'))
        ]);

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Password changed successfully.',
            'data'      => $user
        ]);
    }
}
