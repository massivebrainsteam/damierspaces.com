<?php

namespace App\Http\Controllers\Api\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Team;
use App\User;
use App\Attendance;
use App\TeamInvitation;

class DashboardController extends Controller
{
	public function index()
	{
		$user = Auth::user();
		$team = $user->team;
		
		return response()->json([

			'status'	=> 'Successful',
			'message'	=> 'Dashboard info retrieved successfully.',
			'data'		=> [

				'users'					=> $team->users->count(),
				'new_users'				=> $team->users()->orderBy('created_at', 'desc')->take(5)->get(),
				'subscription'			=> $user->subscription,
				'active_attendances'	=> $team->attendances()->whereStatus('active')->with('user')->get(),
				'active_reservations'	=> $team->reservations()->whereStatus('active')->with(['user', 'space', 'location'])->get(),
				'slots'					=> $team->slots,
				'slots_assigned'		=> $team->slots_assigned,
				'slots_used'			=> $team->slots_used,
				'invitations'			=> $team->invitations()->where('user_id', null)->get()
			]
		]);
	}
}
