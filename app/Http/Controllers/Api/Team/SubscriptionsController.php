<?php

namespace App\Http\Controllers\Api\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Subscription;

class SubscriptionsController extends Controller
{
    public function index()
    {
    	$user = Auth::user();

    	$subscriptions = Subscription::whereTeamId(Auth::user()->team_id)
    	->where('status', '!=', 'pending')
    	->orderBy('id', 'desc')
    	->get();

    	$data = [];

    	foreach($subscriptions as $row){

    		$data[] = [

    			'id'			=> $row->id,
    			'user'			=> $row->user->name,
    			'package'		=> $row->package->name,
    			'package_url'	=> url('package/'.$row->package->id),
    			'start_at'		=> _date($row->start_at),	
    			'end_at'		=> _date($row->end_at),	
    			'status'		=> $row->status,
    			'total'			=> _currency($row->total),
    			'created_at'	=> _date($row->created_at)
    		];

    	}

    	return response()->json([

    		'status'	=> 'Successful',
    		'message'	=> 'Subscriptions retrieved successfully.',
    		'data'		=> $data
    	]);
    }
}
