<?php

namespace App\Http\Controllers\Api\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\TeamInvitation;

class InviteController extends Controller
{
	public function subscription()
	{
		$subscription 		= Auth::user()->subscription;
		$subscription->package;

		return response()->json([

			'status'	=> 'Successful',
			'message'	=> 'Info retrieved successfully.',
			'data'		=> [

				'subscription'	=> $subscription,
				'users'			=> Auth::user()->team->users
			]
		]);
	}

	public function email(Request $request)
	{
		$this->validate($request, ['emails' => 'required']);

		$emails = collect(explode(',', request('emails')));
		$user 	= Auth::user();
		$error 	= null;

		foreach($emails as $row){

			$email_user = User::whereEmail($row)->first();

			if($email_user){

				$error = 'A user with '.$row.' already exists.';
				break;
			}
		}

		$remaining_users = $user->subscription->package->max_team_users - $user->team->users()->count();

		if($remaining_users < 1)
			$error = 'You have only '.$remaining_users.' user accounts left.';

		if($error){

			return response()->json([

				'status'	=> 'Failed',
				'message'	=> 'Validation Failed',
				'data'		=> $error

			], 422);
		}

		foreach($emails as $row){

			$invitation = TeamInvitation::create([

				'team_id'		=> $user->team->id,
				'email'			=> $row,
				'code'			=> str_random(16)
			]);

			$subject = $user->name.'  just invited you to '.$user->team->name.' team on Damier Spaces.';
			$body = view('emails.team.invite', ['subject' => $subject, 'user' => $user, 'invitation' => $invitation])->render();
			_email($row, $subject, $body);
		}

		$invitations = $user->team->invitations;

		if(!$invitations)
			$user->team->update(['invitations' => request('emails')]);

		return response()->json([

			'status'	=> 'Successful',
			'message'	=> 'Invitation emails sent Successfully.',
			'data'		=> $user->team->invitations

		], 201);
	}
}
