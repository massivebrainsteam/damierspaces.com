<?php

namespace App\Http\Controllers\Api\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Reservation;

class ReservationsController extends Controller
{
	public function index()
	{
		$reservations = Reservation::whereTeamId(Auth::user()->team_id)->get();

		$data = [];

		foreach($reservations as $row){

			$data[] = [

				'id'            => $row->id,
				'space'         => $row->space,
				'host'          => $row->host,
				'location'      => $row->space->location,
				'user'          => $row->user,
				'start_at'      => _date($row->start_at, true),
				'end_at'        => _date($row->end_at, true),
				'time_left'     => $row->end_at->diff($row->start_at)->format('%iMins %sSecs')
			];
		}

		return response()->json([

			'status'    => 'Successful',
			'message'   => 'Reservations retrieved successfully.',
			'data'      => $data
		]);
	}
}
