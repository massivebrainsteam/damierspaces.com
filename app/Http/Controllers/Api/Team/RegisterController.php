<?php

namespace App\Http\Controllers\Api\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Team;
use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{
    public function index(RegisterRequest $request)
	{
		$this->validate($request, ['team_name' => 'required']);

		$team = Team::create([

			'name'		=> request('team_name'),
			'email'		=> request('email'),
			'phone'		=> request('phone'),
			'status'	=> 'active'
		]);

		$user = User::create([

			'team_id'	 => $team->id,
			'name'       => request('name'),
			'email'      => request('email'),
			'phone'      => request('phone'),
			'password'   => bcrypt(request('password')),
			'type'       => 'team_admin',
			'api_token'	 => str_random(16),
			'status'     => 'inactive'
		]);		

		$subject    = 'Activate your team account on Damier Spaces!';
		$body       = view('emails.auth.register', ['subject' => $subject, 'user' => $user])->render();
		
		_email($user->email, $subject, $body);

		return response()->json([

			'status'	=> 'Successful',
			'message'	=> 'An email has been sent to '.$user->email.' to activate your account.',
			'data'		=> $team

		], 201);
	}
}
