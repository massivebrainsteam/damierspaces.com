<?php

namespace App\Http\Controllers\Api\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Attendance;

class AttendancesController extends Controller
{
    public function index()
    {
        $attendances = Attendance::whereTeamId(Auth::user()->team_id)->orderBy('id', 'desc')->get();

        $data = [];

        foreach($attendances as $row){


            $data[] = [

                'id'            => $row->id,
                'user'          => $row->user->name,
                'space'         => $row->space->name,
                'host'          => $row->space->host->name,
                'created_at'    => _date($row->created_at),
                'start_at'      => _date($row->start_at, true),
                'end_at'        => _date($row->end_at, true),
                'status'        => strtoupper($row->status),
                'reference'     => $row->reference
            ];
        }

        return response()->json([

            'status'    => 'Successful',
            'message'   => 'Attendances retrieved successfully.',
            'data'      => $data
        ]);
    }
}
