<?php

namespace App\Http\Controllers\Api\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Review;

class ReviewsController extends Controller
{
    public function index()
	{
		$reviews = Review::whereTeamId(Auth::user()->team_id)->get();

		$data = [];

		foreach($reviews as $row){

			$data[] = [

				'id'				=> $row->id,
				'space'				=> $row->space,
				'user'				=> $row->user->name,
				'user_photo_url'	=> $row->user->photo_url,
				'review'			=> $row->review,
				'rating'			=> $row->rating,
				'created_at'		=> _date($row->created_at, true)
			];
		}

		return response()->json([

			'status'    => 'Successful',
			'message'   => 'Reviews retrieved successfully.',
			'data'      => $data
		]);
	}
}
