<?php

namespace App\Http\Controllers\Api\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Http\Requests\Api\Team\UpdateSlotRequest;

class UsersController extends Controller
{
    public function index()
	{
		$users = User::whereTeamId(Auth::user()->team_id)->get();

		foreach($users as $row){

			$row->attendance 			= $row->attendances()->whereStatus('active')->with('space')->first();
			$row->reservation 			= $row->reservations()->whereStatus('active')->first();
		}

		return response()->json([

			'status'    => 'Successful',
			'message'   => 'Users retrieved successfully.',
			'data'      => $users
		]);
	}

	public function single($id = 0)
	{
		$user = User::where([

			'team_id'	=> Auth::user()->team_id,
			'id'		=> $id

		])->with(['team', 'subscription'])->first();

		if(!$user){

			return response()->json([

				'status'	=> 'Failed',
				'message'	=> 'Team member not found.',
				'data'		=> null
			]);
		}			
		
		
		$user->total_reservations 	= $user->reservations->count();
		$user->recent_reservations 	= $user->reservations()->with(['host', 'space', 'location'])->orderBy('id', 'desc')->take(10)->get();
		$user->total_attendances  	= $user->attendances->count();

		$user->recent_attendances 	= $user->attendances()->with(['host', 'space', 'location'])->orderBy('id', 'desc')->take(10)->get();
		$user->last_attendance    	= collect($user->recent_attendances)->last();
		
		$user->subscription->package;
		$user->subscription->package->amenities = null;


		return response()->json([

			'status'    => 'Successful',
			'message'   => 'User retrieved successfully.',
			'data'      => $user
		]);
	}

	public function changeStatus($id = 0)
	{
		$user = User::where([

			'team_id'	=> Auth::user()->team_id,
			'id'		=> $id

		])->with(['subscription'])->first();

		if(!$user){

			return response()->json([

				'status'	=> 'Failed',
				'message'	=> 'Team member not found.',
				'data'		=> null
			]);
		}

		$user->update(['status' => $user->status == 'active' ? 'inactive' : 'active']);

		return response()->json([

			'status'	=> 'Successful',
			'message'	=> $user->name.'\'s status has been successfuly updated',
			'data'		=> $user->status
		]);

	}

	public function updateSlots(UpdateSlotRequest $request)
	{
		$user = User::find(request('user_id'));

		$user->update([

			'slots' => request('slots') 
		]);

		return response()->json([

			'status'	=> 'Successful',
			'message'	=> 'Slots successfully updated.',
			'data'		=> $user->slots
		]);
	}
}
