<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Transaction;
use App\Subscription;
use \GuzzleHttp\Client;
use \GuzzleHttp\RequestOptions;
use Auth;

class PaystackController extends Controller
{
	public function paystack($reference = '')
	{
		$transaction = Transaction::wherePaymentReference($reference)->first();

		if(!$transaction){

			$data['error'] = 'Transaction not found.';

			return view('site.page.paystack', $data);
		}

		if($transaction->status == 'paid'){

			$data['error'] = 'Transaction has already been paid for.';

			return view('site.page.paystack', $data);
		}

		try{

			$transaction->update([ 'payment_reference' => Str::random(6) ]);

			$amount = $transaction->amount;

			if($amount >= 2500)
				$amount += 100;

			$amount *= 100;

			$amount += (1.5/100) * $amount;

			$payload = [

				'reference'         => $transaction->payment_reference,
				'callback_url'      => url('api/generic/paystack/verify'),
				'amount'            => $amount,
				'email'             => $transaction->user->email
			];

			$response = (new Client)->post('https://api.paystack.co/transaction/initialize', [

				'headers' => [

					'Accept'            => 'application/json',
					'Authorization'     => 'Bearer '.env('PAYSTACK_SECRET_KEY')
				],

				RequestOptions::JSON => $payload
			]);

			if((int)$response->getStatusCode() != 200)
				return $this->paystackError();

			$response = json_decode($response->getBody()->getContents());

			if((bool)$response->status == true)
				return redirect($response->data->authorization_url);

			return $this->paystackError();

		}catch(Exception $e){

			return $this->paystackError();
		}
	}

	public function paystackCallback()
	{
		try{

			$transaction = Transaction::wherePaymentReference(request('reference'))->first();

			if(!$transaction)
				return $this->paystackError('confirm');

			$response = (new Client)->get("https://api.paystack.co/transaction/verify/{$transaction->payment_reference}", [

				'headers' => [

					'Accept'            => 'application/json',
					'Authorization'     => 'Bearer '.env('PAYSTACK_SECRET_KEY')
				]
			]);

			if((int)$response->getStatusCode() != 200)
				return $this->paystackError('confirm');

			$response = json_decode($response->getBody()->getContents());

			if((bool)$response->status == true){

				$subscription = Subscription::where([

					'transaction_id'  	=> $transaction->id,
					'status'    		=> 'pending'

				])->first();

				if(!$subscription){

					$data['error'] = 'There is no pending subscription for '.$transaction->user->name;
					return view('site.page.paystack', $data);
				}

				$transaction->update([

					'status'				=> 'success',
					'transaction_date'      => date('Y-m-d H:i:s'),
					'transaction_reference'	=> $response->data->gateway_response
				]);
					
				$subscription->activate($transaction);
				
				if($transaction->user->type == 'team_admin')
					return redirect(_frontend_url('/team'));

				return redirect(_frontend_url('/user'));
			}

			return $this->paystackError('confirm');

		}catch(Exception $e){

			return $this->paystackError('confirm');
		}
	}

	public function paystackError($message = 'initalize')
	{
		$data['error']  = 'Could not '.$message.' your payment. Please try again.';

		return view('site.page.paystack', $data);
	}
}
