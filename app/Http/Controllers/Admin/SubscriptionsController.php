<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscription;

class SubscriptionsController extends Controller
{
    public function index()
    {
    	if(request('q')){

    		$subscriptions = Subscription::search(request('q'));
    		session()->flash('message', $subscriptions->count().' Subscriptions match your search query.');

    	}else{

    		$subscriptions = Subscription::where('status', '!=', 'pending')->orderBy('id', 'desc')->paginate(10);
    	}

    	$data['subscriptions']	= $subscriptions;
    	$data['active'] 		= 'subscriptions';

    	return view('admin.subscriptions.index', $data);
    }
}
