<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;

class ReviewsController extends Controller
{
    public function index()
    {
    	$data['reviews']	= Review::paginate(10);
    	$data['active']    = 'reviews';

    	return view('admin.reviews.index', $data);
    }
}
