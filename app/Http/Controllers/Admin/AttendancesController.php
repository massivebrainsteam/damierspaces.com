<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attendance;

class AttendancesController extends Controller
{
    public function index()
    {
    	if(request('q')){

    		$attendances = Attendance::search(request('q'));
    		session()->flash('message', $attendances->count().' Attendances match your search query.');

    	}else{

    		$attendances = Attendance::orderBy('id', 'desc')->take(100)->paginate(10);
    	}

    	$data['attendances']	= $attendances;
    	$data['query']			= request('q');
    	$data['active']    		= 'attendance';

    	return view('admin.attendances.index', $data);
    }
}
