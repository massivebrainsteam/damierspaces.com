<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use App\Host;
use App\Http\Requests\SavePackageRequest;

class PackagesController extends Controller
{
    public function index()
    {
    	$data['packages']	= Package::orderBy('type')->orderBy('order')->get();
    	$data['active']     = 'packages';

    	return view('admin.packages.index', $data);
    }
}
