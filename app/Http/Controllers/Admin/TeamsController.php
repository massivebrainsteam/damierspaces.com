<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;

class TeamsController extends Controller
{
    public function index()
    {
    	$data['teams']		= Team::paginate(10);
    	$data['active']    	= 'teams';

    	return view('admin.teams.index', $data);
    }

 

    public function team(Request $request, $id = 0)
    {    
    	$team = Team::find($id);

    	if(!$team)
    		return redirect('admin/teams')->with('error', 'Team not found');

        if(request('status')){

            $team->update(['status' => $team->status == 'active' ? 'inactive' : 'active']);

            return redirect('admin/teams')->with('message', 'Team Status changed successfully.');
        }

    	$data['team']	= $team;
    	$data['active']	= 'teams';

    	return view('admin.teams.team', $data);
    }

}
