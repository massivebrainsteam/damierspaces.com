<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Attendance;
use App\Reservation;
use App\Subscription;
use App\Review;
use App\Team;
use App\Space;

class DashboardController extends Controller
{
    public function index()
    {
    	$data['users'] 				= User::whereType('user')->count();
    	$data['active_users'] 		= User::where(['type' => 'user', 'status' => 'active'])->count();
    	$data['hosts'] 				= User::whereType('host')->count();
    	$data['active_hosts'] 		= User::where(['type' => 'host', 'status' => 'active'])->count();
    	$data['teams'] 				= Team::count();
    	$data['active_teams'] 		= Team::where(['status' => 'active'])->count();
    	$data['attendances_count']	= Attendance::count();
    	$data['attendances']		= Attendance::orderBy('id', 'desc')->take(20)->get();
    	$data['reservations_count']	= Reservation::count();
    	$data['reviews_count']		= Review::count();
    	$data['spaces']				= Space::whereStatus('inactive')->orderBy('id', 'desc')->take(10)->get();
    	$data['new_users']			= User::orderBy('id', 'desc')->take(10)->get();

    	return view('admin.dashboard.index', $data);
    }
}
