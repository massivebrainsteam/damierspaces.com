<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Host;
use App\Http\Requests\SaveHostRequest;

class HostsController extends Controller
{
    public function index($id = 0)
    {
    	$host = Host::firstOrNew(['id' => $id]);

    	$data['hosts']	= Host::orderBy('name')->paginate(10);
    	$data['host']	= $host;
    	$data['user']	= $host->users()->first() ?? User::firstOrNew(['id' => $id]);
        $data['active'] = 'hosts';

    	return view('admin.hosts.index', $data);
    }

    public function form($id = 0)
    {
        $host           = Host::firstOrNew(['id' => $id]);
        
        $data['host']   = $host;
        $data['user']   = $host ?? new User();
        $data['active'] = 'hosts';

        return view('admin.hosts.form', $data);
    }

    public function single($id = 0)
    {
        $host = Host::find($id);

        if(!$host)
            return redirect()->back()->with('error', 'Host not found');

        $data['host']   = $host;
        $data['active'] = 'hosts';

        return view('admin.hosts.host', $data);
    }

    public function host(SaveHostRequest $request)
    {    
        if(request('id') < 1){

            if(Host::whereEmail(request('email'))->count() > 0)
                return redirect()->back()->with('error', 'Host Email already exists.');

            if(Host::wherePhone(request('phone'))->count() > 0)
                return redirect()->back()->with('error', 'Host Phone Number already exists.');

            if(User::whereEmail(request('contact_email'))->count() > 0)
                return redirect()->back()->with('error', 'Contact Email already exists.');

            if(User::wherePhone(request('contact_phone'))->count() > 0)
                return redirect()->back()->with('error', 'Contact Phone Number already exists.');
        }

    	$host = Host::updateOrCreate(['id' => request('id')], [

    		'name'			=> request('name'),
    		'email'			=> request('email'),
    		'phone'			=> request('phone'),
    		'address'		=> request('address'),
            'description'   => request('description'),
    		'status'	    => request('status')
    	]);

        if(request('photo_url')){

            $response = _cloudinary($request->file('photo_url'));

            if($response->link != null)
                $host->update(['photo_url' => $response->link]);
        }

    	if($host->users()->count() < 1){

    		$password = str_random(6);

    		$user = User::create([

                'host_id'   => $host->id,
    			'name'		=> request('contact_name'),
    			'email'		=> request('contact_email'),
    			'phone'		=> request('contact_phone'),
                'type'      => 'host',
    			'password'	=> bcrypt($password)
    		]);

    		_email($user->email, 'You have been added as a contact person on damier.com', 'Hello there! You have been added as a contact person on damier.com. Your password is : '.$password);
    	}

    	return redirect('admin/hosts')->with('message', 'Host saved successfully.');
    }
}
