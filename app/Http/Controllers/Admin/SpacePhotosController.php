<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Space;
use App\SpacePhoto;

class SpacePhotosController extends Controller
{
    public function index($space_id = 0)
    {
    	$space 				= Space::find($space_id);

    	if(!$space)
    		return redirect('/spaces')->with('error', 'Space not found.');

    	$data['photos']		= $space->photos;
    	$data['space']		= $space;
    	$data['active']    	= 'spaces';

    	return view('admin.space.photos.index', $data);
    }

    public function form($space_id = 0, $id = 0)
    {
    	$space 	= Space::find($space_id);

    	if(!$space)
    		return redirect('/spaces')->with('error', 'Space not found.');

    	$data['space']		= $space;
    	$data['photo']		= SpacePhoto::firstOrNew(['id' => $id]);
        $data['active']     = 'spaces';
        
    	return view('admin.space.photos.form', $data);
    }

    public function save(Request $request, $space_id = 0)
    {    	
    	$space = Space::find($space_id);

    	if(!$space)
    		return redirect('admin/spaces')->with('error', 'Space not found. No uploads where made');

    	if(request('photo_url')){

            $response = _cloudinary($request->file('photo_url'));

            if($response->link != null){

            	SpacePhoto::create([

            		'space_id'	=> $space->id,
            		'url'		=> $response->link,
            		'status'	=> request('status')
            	]);
            }
        }

    	return redirect('admin/space/'.$space->id.'/photos')->with('message', 'photo saved successfully.');
    }
}
