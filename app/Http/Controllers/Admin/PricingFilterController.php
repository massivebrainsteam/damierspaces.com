<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class PricingFilterController extends Controller
{
    //
    public function index()
    {
    	$data['categories']	= Category::paginate(10);
    	$data['active']    = 'spaces';

    	return view('admin.pricing.index', $data);
    }

    public function form($id = 0)
    {
    	$data['category']	= Category::firstOrNew(['id' => $id]);
        $data['active']     = 'spaces';
        
    	return view('admin.pricing.form', $data);
    }


}
