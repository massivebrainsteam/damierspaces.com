<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\SaveUserRequest;

class UsersController extends Controller
{
    public function index($type = 'user')
    {
    	if(request('q')){

            $users = User::search(request('q'), $type);

        }else{

            $users = User::orderBy('name')->whereType($type)->paginate(10);
        }
        
        $data           = [];
        $data['users']  = $users;
    	$data['title']  = str_plural($type);
        $data['type']   = $type;
        $data['active'] = 'users';

    	return view('admin.users.index', $data);
    }

    public function form($id = 0)
    {
        $data           = [];
    	$data['user']	= User::firstOrNew(['id' => $id]);
        $data['active'] = 'users';
        
    	return view('admin.users.form', $data);
    }

    public function member($id = 0)
    {
        $data           = [];
    	$data['user']	= User::firstOrNew(['id' => $id]);
        $data['active'] = 'users';
        
    	return view('admin.teams.member', $data);
    }

    public function user(SaveUserRequest $request)
    {    
        if(request('id') < 1){

            if(User::whereEmail(request('email'))->count() > 0)
                return redirect()->back()->with('error', 'Email already exists.');

            if(User::wherePhone(request('phone'))->count() > 0)
                return redirect()->back()->with('error', 'Phone Number already exists.');
        }

    	$user = User::updateOrCreate(['id' => request('id')], [

    		'name'		  => request('name'),
    		'email'		  => request('email'),
    		'phone'		  => request('phone'),
            'address'     => request('address'),
            'sex'         => request('sex'),
    		'occupation'  => request('occupation'),
    		'status'	  => request('status'),
    	]);

    	if(request('id') < 1){

    		$password = str_random(8);

    		$user->update([

                'password'  => bcrypt($password), 
                'type'      => request('type', 'user')
            ]);

            $subject    = 'An account ('.ucfirst($user->type).') has been created for you on damierspaces.com';
            $body       = $subject.' <br><br> Your password is <strong>'.$password.'</strong>. Please login with this email address.';

            _email($user->email, $subject, $body);
    	}

    	return redirect('admin/users/'.$user->type)->with('message', 'User saved successfully.');
    }
}
