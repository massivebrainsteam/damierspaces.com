<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rating;

class RatingsController extends Controller
{
    public function index()
    {
    	$data['ratings']	= Rating::orderBy('name')->get();
    	$data['active']     = 'spaces';

    	return view('admin.ratings.index', $data);
    }

    public function form($id = 0)
    {
    	$data['rating']		= Rating::firstOrNew(['id' => $id]);
        $data['active']     = 'spaces';
        
    	return view('admin.ratings.form', $data);
    }

    public function rating(Request $request)
    {    	
    	$this->validate($request, ['name' => 'required']);

    	Rating::updateOrCreate(['id' => request('id')], [

            'name'      => request('name'),
    		'status' 	=> request('status')
    	]);

    	return redirect('admin/ratings')->with('message', 'Rating saved successfully.');
    }
}
