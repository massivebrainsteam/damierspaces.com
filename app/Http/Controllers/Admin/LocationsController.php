<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;
use App\Http\Requests\SaveLocationRequest;
use App\Host;
use App\Http\Services\GoogleService;

class LocationsController extends Controller
{
    public function index($id = 0)
    {
    	$data['host']		= Host::firstOrNew(['id' => $id]);
    	$data['locations']	= Location::whereHostId($id)->get();
    	$data['active']     = 'spaces';

    	return view('admin.locations.index', $data);
    }

    public function form($host_id = 0, $id = 0)
    {
        $host = Host::find($host_id);

        if(!$host)
        	return redirect('admin/hosts')->with('error', 'Host not found.');

        $data['host']		= $host;
    	$data['location'] 	= Location::firstOrNew(['id' => $id]);
        $data['active']     = 'spaces';
        
    	return view('admin.locations.form', $data);
    }

    public function location(SaveLocationRequest $request)
    {   
        $existing_address = Location::firstOrNew(['id' => request('id')])->address;

    	$location = Location::updateOrCreate(['id' => request('id')], [

            'host_id'    => request('host_id'),
    		'name'		 => request('name'),
            'address'    => request('address'),       
    		'latitude'   => request('latitude'),
    		'longitude'  => request('longitude'),
            'status'     => request('status'),
    		'weekends'   => request('weekends')
    	]);

        if(request('address') && $existing_address != request('address')){

            $coordinates = GoogleService::getCoordinatesFromAddress(request('address'));

            if($coordinates->latitude > 0){

                $location->update(['latitude' => $coordinates->latitude, 'longitude' => $coordinates->longitude]);
            }
        }

    	if(request('photo_url')){

            $response = _cloudinary($request->file('photo_url'));

            if($response->link != null)
                $location->update(['photo_url' => $response->link]);
        }

    	return redirect('admin/locations/'.request('host_id'))->with('message', 'Location saved successfully.');
    }


 
}
