<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Amenity;

class AmenitiesController extends Controller
{
    public function index()
    {
    	$data['amenities']	= Amenity::orderBy('category')->paginate(10);
    	$data['active']     = 'spaces';

    	return view('admin.amenities.index', $data);
    }

    public function form($id = 0)
    {
    	$data['amenity']	= Amenity::firstOrNew(['id' => $id]);
        $data['active']     = 'spaces';
        
        return view('admin.amenities.form', $data);
    }

    public function amenity(Request $request)
    {    	
    	$this->validate($request, ['name' => 'required']);

    	$amenity = Amenity::updateOrCreate(['id' => request('id')], [

    		'category' 	=> request('category'),
            'name'      => request('name'),
            'status' 	=> request('status')
        ]);

        if(request('icon')){

            $response = _cloudinary($request->file('icon'));

            if($response->link != null)
                $amenity->update(['icon' => $response->link]);
        }

        return redirect('admin/amenities')->with('message', 'Amenity saved successfully.');
    }
}
