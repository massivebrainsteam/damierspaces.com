<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoriesController extends Controller
{
    public function index()
    {
    	$data['categories']	= Category::paginate(10);
    	$data['active']    = 'spaces';

    	return view('admin.categories.index', $data);
    }

    public function form($id = 0)
    {
    	$data['category']	= Category::firstOrNew(['id' => $id]);
        $data['active']     = 'spaces';
        
    	return view('admin.categories.form', $data);
    }

    public function category(Request $request)
    {    	
    	$this->validate($request, ['name' => 'required']);

    	Category::updateOrCreate(['id' => request('id')], [

    		'name'		=> request('name'),
    		'status'	=> request('status')
    	]);

    	return redirect('admin/categories')->with('message', 'Category saved successfully.');
    }
}
