<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use App\Location;
use App\Amenity;
use App\PackageSlot;
use App\PackageAmenity;
use App\Amenities;
use App\Rating;

class PackageController extends Controller
{
    public function basicInfo($id = 0)
    {
        if (Rating::count() == 0) {
            return redirect('/admin/ratings')->with('message', 'Please setup a couple of ratings first before you create packages.');
        }

        if (request()->isMethod('post')) {
            $package = Package::updateOrCreate(['id' => request('id')], [

                'name'                 => request('name'),
                'type'                 => request('type'),
                'order'                => request('order'),
                'duration'             => request('duration'),
                'amount'               => request('amount'),
                'status'               => request('status'),
            ]);

            return redirect('admin/package/'.$package->id.'/other-settings')->with('message', 'Package saved successfully. Setup Other Settings');
        }

        $data['package'] = Package::firstOrNew(['id' => $id]);
        $data['active']  = 'packages';

        return view('admin.packages.form.basic-info', $data);
    }

    public function otherSettings(Request $request, $id = 0)
    {
        $package = Package::find($id);

        if (!$package) {
            return redirect('admin/package/0/basic-info');
        }

        if (request()->isMethod('post')) {
            if (request('slots') < 1) {
                return redirect()->back()->with('error', 'Slots must be more than zero.');
            }

            if (request('can_reserve_spaces') == 'yes' && request('reservation_duration') < 1) {
                return redirect()->back()->with('error', 'Please enter reservation duration if you allow users to reserve spaces.');
            }

            $package->update([

                'rating_id'             => request('rating_id'),
                'slots'                 => request('slots'),
                'can_reserve_spaces'    => request('can_reserve_spaces'),
                'reservation_duration'  => request('reservation_duration'),
                'monthly_reservations'  => request('monthly_reservations'),
                'max_team_users'        => request('max_team_users'),
            ]);

            return redirect('admin/packages')->with('message', 'Package configuration saved successfully.');
        }

        $data['package']    = $package;
        $data['active']     = 'packages';

        return view('admin.packages.form.other-settings', $data);
    }
}
