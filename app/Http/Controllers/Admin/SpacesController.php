<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveSpaceRequest;
use App\Space;
use App\Host;
use App\Category;
use App\SpaceDay;
use App\SpaceAmenity;
use App\Amenity;
use App\Location;
use App\Attendance;
use App\Rating;

class SpacesController extends Controller
{
    public function index()
    {
        if(request('q')){

            $spaces = Space::search(request('q'));

        }elseif(request('host_id')){

            $spaces = Space::whereHostId(request('host_id'))->orderBy('name')->paginate(10);

        }else{

            $spaces = Space::orderBy('name')->paginate(10);
        }
        
    	$data['spaces']	= $spaces;
        $data['active'] = 'spaces';

    	return view('admin.spaces.index', $data);
    }

    public function form($id = 0)
    {
        $space = Space::firstOrNew(['id' => $id]);

        if($space->days->count() < 1){

            $days = [];

            foreach(_days() as $row){

                $day = new SpaceDay();
                $day->day = $row;
                $day->start_at = '8:00 AM';
                $day->end_at = '5:00 PM';
                $days[] = $day;
            }

            $space->days = collect($days);
        }

        $host_id = $space->host_id;

        if(request('host_id')){

            $host_id = request('host_id');
        }
        
    	$data['space']	    = $space;
    	$data['hosts']	    = Host::orderBy('name')->get();
        $data['locations']  = Location::whereHostId($host_id)->get();
        $data['ratings']    = Rating::get();
        $data['categories'] = Category::get();
        $data['amenities']  = Amenity::orderBy('category')->get();
        $data['host']       = Host::firstOrNew(['id' => $host_id]);
        $data['active']     = 'spaces';
    
    	return view('admin.spaces.form', $data);
    }

    public function space(SaveSpaceRequest $request)
    {   
        $location   = Location::firstOrNew(['id' => request('location_id')]);
        $amenities  = [];

        foreach(request('amenities') ?? [] as $key => $value){

            if($value == 'on')
                $amenities[] = $key;
        }

        if(count($amenities) < 1)
            return redirect()->back()->with('error', 'Please select at least one amenity');

    	$space = Space::updateOrCreate(['id' => request('id')], [

            'host_id'        => request('host_id'),
            'location_id'    => $location->id,
            'rating_id'      => request('rating_id'),
            'category_id'    => request('category_id'),
    		'name'			 => request('name'),
    		'description'	 => request('description'),
    		'capacity'		 => request('capacity'),
            'status'         => request('status'),
    		'featured'	     => request('featured'),
    	]);

        if(request('id') < 1){

            $space->update(['avaliable_capacity' => request('capacity')]);
        }

    	if(request('photo_url')){

            $response = _cloudinary($request->file('photo_url'));

            if($response->link != null)
                $space->update(['photo_url' => $response->link]);
        }

        foreach(request('start_at') ?? [] as $key => $value){

            SpaceDay::updateOrCreate(['space_id' => $space->id, 'day' => $key], [

                'space_id'  => $space->id,
                'day'       => $key,
                'start_at'  => $value
            ]);
        }

        foreach(request('end_at') ?? [] as $key => $value){

            SpaceDay::updateOrCreate(['space_id' => $space->id, 'day' => $key], [

                'space_id'  => $space->id,
                'day'       => $key,
                'end_at'    => $value
            ]);
        }

        SpaceAmenity::whereSpaceId($space->id)->delete();

        foreach($amenities as $row)
            SpaceAmenity::create(['space_id' => $space->id, 'amenity_id' => $row]);
            
    	return redirect('admin/spaces')->with('message', 'Space saved successfully.');
    }

}
