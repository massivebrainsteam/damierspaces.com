<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class StatesController extends Controller
{
    //
    public function index()
    {
    	$data['categories']	= Category::paginate(10);
    	$data['active']    = 'spaces';

    	return view('admin.states.index', $data);
    }

    public function form($id = 0)
    {
    	$data['category']	= Category::firstOrNew(['id' => $id]);
        $data['active']     = 'spaces';
        
    	return view('admin.states.form', $data);
    }

    // 
    public function city()
    {
    	$data['categories']	= Category::paginate(10);
    	$data['active']    = 'spaces';

    	return view('admin.states.city', $data);
    }

    public function cityform($id = 0)
    {
    	$data['category']	= Category::firstOrNew(['id' => $id]);
        $data['active']     = 'spaces';
        
    	return view('admin.states.cityform', $data);
    }


}
