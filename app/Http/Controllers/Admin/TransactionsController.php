<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;

class TransactionsController extends Controller
{
    public function index()
    {
    	if(request('q')){

    		$transactions = Transaction::search(request('q'));
    		session()->flash('message', $transactions->count().' Transactions match your search query.');

    	}else{

    		$transactions = Transaction::orderBy('id', 'desc')->paginate(10);
    	}

    	$data['transactions']	= $transactions;
    	$data['active'] 		= 'transactions';

    	return view('admin.transactions.index', $data);
    }
}
