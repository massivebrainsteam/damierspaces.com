<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settlement;
use App\Attendance;
use App\Host;
use Auth;

class SettlementsController extends Controller
{
	public function index($id = 0)
	{
		if($id > 0){

			$settlements = Settlement::whereHostId($id)->orderBy('id')->paginate(10);
		
		}else{

			$settlements = Settlement::orderBy('id')->paginate(10);
		}

		$data['settlements']	= $settlements;
		$data['active'] 		= 'settlements';

		return view('admin.settlements.index', $data);
	}

	public function form($id = 0)
	{     
		$host = Host::find($id); 

		if(!$host)
			return redirect('/hosts')->with('error', 'Invalid Host selected.');

		$attendances = Attendance::whereSettlementStatus('pending')->whereHostId($host->id)->get();

		if($attendances->count() < 1)
			return redirect('/admin/hosts')->with('error', $host->name.' has no pending settlments.');

		$data['active'] 		= 'settlement';
		$data['host']			= $host;
		$data['attendances']	= $attendances;

		return view('admin.settlements.form', $data);
	}

	public function save(Request $request)
	{   
		$this->validate($request, ['host_id' => 'required|exists:hosts,id']);

		$host 		 = Host::find(request('host_id'));
		$total  	= $host->attendances()->whereSettlementStatus('pending')->sum('settlement');

		$settlement = Settlement::create([

			'host_id'		=> $host->id,
			'approved_by'	=> Auth::user()->id,
			'total'			=> $total,
			'notes'			=> 'Settlement #'.mt_rand(1111, 9999)

		]);

		$host->attendances()->whereSettlementStatus('pending')->update([

			'settlement_id'			=> $settlement->id,
			'settlement_status'		=> 'paid'
		]);

		return redirect('/admin/settlements')->with('message', 'Settlement Created Successfully.');
	}
}
