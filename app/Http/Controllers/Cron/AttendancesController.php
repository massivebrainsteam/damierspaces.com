<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attendance;
use Carbon\Carbon;

class AttendancesController extends Controller
{
    public function markAttendancesAsExpired()
    {
    	$attendances = Attendance::where('end_at', '<', Carbon::now())
    	->whereStatus('active')->update(['status' => 'expired']);
    }
}
