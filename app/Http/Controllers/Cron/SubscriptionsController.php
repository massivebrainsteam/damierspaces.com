<?php

namespace App\Http\Controllers\Cron;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscription;
use Carbon\Carbon;

class SubscriptionsController extends Controller
{
	public function notify7DaysExpiry()
	{    	
		$subcriptions = Subscription::whereStatus('active')
		->whereDate('end_at', Carbon::now()->addDays(7)->format('Y-m-d'))
		->get();

		if($subcriptions->count() < 1)
			return;

		$subject = 'Your Subscription will expire in 7 Days!';

		foreach($subcriptions as $row){

			$body = view('emails.user.subscription-7days', ['subject' => $subject, 'subscription' => $row])->render();
			
			_email($row->user->email, $subject, $body);
		}
	}

	public function notify3DaysExpiry()
	{    	
		$subcriptions = Subscription::whereStatus('active')
		->whereDate('end_at', Carbon::now()->addDays(3)->format('Y-m-d'))
		->get();

		if($subcriptions->count() < 1)
			return;

		$subject = 'Your Subscription will expire in 3 Days!';

		foreach($subcriptions as $row){

			$body = view('emails.user.subscription-3days', ['subject' => $subject, 'subscription' => $row])->render();
			
			_email($row->user->email, $subject, $body);
		}
	}

	public function deactivateUser()
	{    	
		$subcriptions = Subscription::whereStatus('active')
		->whereDate('end_at', '<', Carbon::now())
		->get();

		if($subcriptions->count() < 1)
			return;

		$subject = 'Your account has been deactivated';

		foreach($subcriptions as $row){

			$body = view('emails.user.subscription-deactivate', ['subject' => $subject, 'subscription' => $row])->render();
			
			_email($row->user->email, $subject, $body);

			$row->update(['status' => 'expired']);
		}
	}
}
