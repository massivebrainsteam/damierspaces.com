<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TeamInvitation;
use App\User;
use App\Http\Requests\RegisterRequest;
use Auth;

class UserController extends Controller
{
    public function index($code = '')
    {
        if (Auth::check()) {
            return redirect('auth');
        }

        if (request('package_id')) {
            session()->put('package_id', request('package_id'));
        }

        if (request('email')) {
            if (strlen(request('email')) < 10) {
                return redirect()->back()->with('error', 'Please enter a valid email address.');
            }
        }

        $team = null;

        if ($code) {
            $invitation = TeamInvitation::whereCode($code)->first();

            if ($invitation) {
                $team = $invitation->team;
            }
        }

        $data['email']	= request('email');
        $data['code']	= $code;
        $data['team']	= $team;

        return view('site.auth.register.user', $data);
    }

    public function register(RegisterRequest $request, $code = '')
    {
        $invitation = null;

        if (strlen($code) > 0) {
            $invitation = TeamInvitation::where([
                'code'		=> $code,
                'status'	=> 'pending'
            ])->first();

            if (!$invitation) {
                return redirect('/auth/user')->with('message', 'Invalid Team Invitation link.');
            }
        }

        $user = User::create([

            'name'       => request('name'),
            'email'      => request('email'),
            'phone'      => request('phone'),
            'occupation'      => request('occupation'),
            'country'      => request('country'),
            'state'      => request('state'),
            'city'      => request('city'),
            'password'   => bcrypt(request('password')),
            'type'       => 'user',
            'api_token'	 => str_random(16),
            'status'     => 'inactive'
        ]);

        $subject    = 'Activate your account on Damier Spaces!';
        $body       = view('emails.auth.register', ['subject' => $subject, 'user' => $user])->render();

        _email($user->email, $subject, $body);

        if ($invitation != null) {
            $invitation->updateAsAccepted($user);
        }

        return redirect('/auth/user')->with('message', 'An email has been sent to '.$user->email.' to activate your account.');
    }
}
