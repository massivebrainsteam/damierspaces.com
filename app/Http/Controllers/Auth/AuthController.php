<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class AuthController extends Controller
{
	public function index()
	{
		if(!Auth::check())
			return redirect('/login');

		$user = Auth::user();

		if($user->status == 'inactive'){

			Auth::logout();
			return redirect()->back()->with('error', 'Your account is still currently inactive.');
		}
		
		if($user->type == 'admin')
			return redirect('/admin');

		if($user->type == 'host'){

			if($user->host->cac_url == null)
				return redirect('/become-a-host/cac');

			if($user->host->status == 'inactive'){

				Auth::logout();
				return redirect('/host')->with('error', 'Your account is still under review.');
			}	
		}

		if($user->team_id > 0){

			if($user->team->status == 'inactive'){

				Auth::logout();
				return redirect('/auth/login')->with('error', 'Your account is still under review.');
			}	
		}
		
		return redirect(_frontend_url('?api_token='.Auth::user()->api_token));
	}

	public function activate($token = '')
	{
		$user = User::whereApiToken($token)->first();

		$data['status']	= 'success';
		$data['user']	= $user;
		
		if(!$user){

			$data['status']	= 'error';

		}else{

			$user->update(['status' => 'active']);
			
			if($user->type == 'host'){

				$user->host->update(['status' => 'active']);
			}
		}

		if(session('package_id')){

			$package_id = session()->pull('package_id');

			Auth::loginUsingId($user->id);
			
			return redirect('package/'.$package_id.'/subscribe');
		}

		$url = '/auth/login';

		if($user->type == 'host')
			$url = '/host';

		if($user->type == 'admin')
			$url = '/backend';

		$data['url'] 	= $url;
		$data['user']	= $user;
		
		return view('site.auth.activated', $data);
	}

	
}
