<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TeamInvitation;
use App\User;
use App\Team;
use App\Http\Requests\RegisterRequest;
use Auth;

class TeamController extends Controller
{
    public function index()
    {
        return view('site.auth.register.team');
    }

    public function register(RegisterRequest $request)
    {
        $team = Team::create([

            'name'		=> request('team_name'),
            'email'		=> request('email'),
            'phone'		=> request('phone'),
            'status'	=> 'inactive'
        ]);

        $user = User::create([

            'team_id'	 => $team->id,
            'name'       => request('name'),
            'email'      => request('email'),
            'phone'      => request('phone'),
            'password'   => bcrypt(request('password')),
            'type'       => 'team_admin',
            'api_token'	 => str_random(16),
            'country'      => request('country'),
            'state'      => request('state'),
            'city'      => request('city'),
            'status'     => 'inactive'
        ]);

        $subject    = 'Activate your team account on Damier Spaces!';
        $body       = view('emails.auth.register', ['subject' => $subject, 'user' => $user])->render();

        _email($user->email, $subject, $body);

        return redirect('/auth/user')->with('message', 'An email has been sent to '.$user->email.' to activate your account.');
    }

    public function accept($code = '')
    {
        $invitation = TeamInvitation::where([

            'code'		=> $code,
            'status'	=> 'pending'

        ])->first();

        if (!$invitation) {
            return view('site.errors.404');
        }

        return $this->registerUserForm($code);
    }
}
