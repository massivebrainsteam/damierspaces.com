<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\TeamInvitation;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use Auth;

class LoginController extends Controller
{
	public function index()
	{
		if(User::whereType('admin')->count() < 1){

			User::create([

				'name'		=> 'Administrator',
				'email'		=> 'admin@damier.com',
				'type'		=> 'admin',
				'password'	=> bcrypt('password'),
				'status'	=> 'active'
			]);
		}
		
		if(Auth::check()){

			Auth::user()->update(['api_token' => str_random(16)]);
			Auth::logout();
		}
		
		return view('site.auth.login');
	}

	public function login()
	{           
		if(Auth::check())
			return redirect('auth');

		if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

			if(request('type') != Auth::user()->type && Auth::user()->type != 'team_admin'){

				Auth::logout();
				return redirect()->back()->with('error', 'You are not allowed to login here.');
			}

			_log('New Sign In');
			
			return redirect('auth');

		}else{

			return redirect()->back()->with('error', 'Invalid Username or password');
		}
	}

	public function logout()
	{	
		$redirect = '/auth/login';

		if(Auth::check()){

			if(Auth::user()->type == 'admin')
				$redirect = '/backend';

			if(Auth::user()->type == 'host')
				$redirect = '/host';

			Auth::logout();
			_log('Logout Successful.');
		}
		

		return redirect($redirect)->with('message', 'You have been successfully logged out.');
	}
}
