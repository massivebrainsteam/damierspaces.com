<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class PasswordController extends Controller
{
	public function change(Request $request)
	{
		if(request()->isMethod('get'))
			return view('site.auth.change-password');

		$this->validate($request, [

			'current_password'	=> 'required',
			'password'			=> 'required|min:6|confirmed'
		]);

		if(!app('hash')->check(request('current_password'), Auth::user()->password))
			return redirect()->back()->with('error', 'Current Password is not correct.');

		Auth::user()->update([

			'password'	=> bcrypt(request('password'))
		]);

		return redirect()->back()->with('message', 'Password has been successfully changed.');
	}

	public function forgotForm()
	{
		$data['title']	= 'Forget Password';

		return view('site.auth.forgot-password', $data);
	}

	public function forgot(Request $request)
	{		
		$user = User::whereEmail(request('email'))->first();

		if(!$user)
			return redirect()->back()->with('error', 'We do not recognize this Email Address.');

		$user->update(['api_token' => str_random(16)]);

		$subject    = 'Reset your Password on Damier Spaces';
		$body       = view('emails.auth.reset-password', ['subject' => $subject, 'user' => $user])->render();
		
		_email($user->email, $subject, $body);

		return redirect()->back()->with('message', 'A password reset link has been sent to you.');
	}

	public function reset(Request $request, $token = '')
	{
		if(request()->isMethod('get')){

			$user = User::whereApiToken($token)->first();

			if(!$user)
				return redirect('/auth/login')->with('error', 'Invalid reset password link.');

			$data['title']	= 'Reset Password';
			$data['user']	= $user;

			return view('site.auth.reset-password', $data);
		}
		
		$this->validate($request, [

			'password'	=> 'required|min:6|confirmed'
		]);

		$user = User::whereApiToken(request('token'))->first();

		if(!$user)
			return redirect('/login');

		$user->update([

			'api_token'		=> str_random(16),
			'password' 		=> bcrypt(request('password'))
		]);	

		return redirect('/login')->with('message', 'Your password has been successfully changed. Please login below.');
	}
}
