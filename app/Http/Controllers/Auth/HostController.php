<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TeamInvitation;
use App\User;
use App\Http\Requests\RegisterRequest;
use Auth;

class HostController extends Controller
{
    public function index()
	{
		return view('site.auth.register.host');
	}
}
