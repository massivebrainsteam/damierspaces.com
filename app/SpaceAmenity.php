<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpaceAmenity extends Model
{
	protected $table 	= 'space_amenities';
    protected $hidden   = ['updated_at', 'password'];
    protected $guarded  = ['updated_at'];

    public function space()
    {
    	return $this->belongsTo('App\Space');
    }
}
