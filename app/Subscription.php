<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Scopes;

class Subscription extends Model
{
    protected $hidden    = ['updated_at'];
    protected $guarded   = ['updated_at'];
    protected $casts     = [ 'start_at' => 'datetime', 'end_at' => 'datetime' ];
    protected $appends   = ['settlement'];

    use Scopes;

    public function getSettlementAttribute()
    {
        return round($this->total / 3, 2);
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function package()
    {
        return $this->belongsTo('App\Package')->withDefault();
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction')->withDefault();
    }

    public static function search($param = '')
    {
        $like = "%$param%";

        return self::query()
        ->where('payment_reference', 'like', $like)
        ->orWhere('total', 'like', $like)
        ->orWhere(function($query) use($param, $like){

            $user = User::where('name', 'like', $like)->first();

            if($user){

                $query->whereUserId($user->id);
            }

            $package = Package::where('name', 'like', $like)->first();

            if($package){

                $query->wherePackageId($package->id);
            }

        })
        ->paginate(10);
    }

    public function activate($transaction)
    {
        $this->update([

            'status'            => 'active',
            'transaction_id'    => $transaction->id
        ]);

        $total_slots = $this->package->slots;

        $transaction->user->update([

            'subscription_id'   => $this->id,
            'slots'             => $total_slots
        ]);

        if($transaction->user->type == 'team_admin'){

            foreach($transaction->user->team->users as $row){

                $row->update(['subscription_id' => $this->id]);
            }
        }
    }
}
