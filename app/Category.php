<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Scopes;

class Category extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];

    use Scopes;
    
    public function spaces()
    {
    	return $this->hasMany('App\Space');
    }
}
