<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $hidden    = ['updated_at'];
    protected $guarded   = ['updated_at'];

    public function user()
    {
    	return $this->belongsTo('App\User')->withDefault(function(){

            return new User();
        });
    }
    
    public function subscription()
    {
    	return $this->hasMany('App\Subscription');
    }

    public static function search($param = '')
    {
        $like = "%$param%";

        return self::query()
        ->where('payment_reference', 'like', $like)
        ->orWhere('amount', 'like', $like)
        ->orWhere('status', 'like', $like)
        ->orWhere(function($query) use($param, $like){

            $user = User::where('name', 'like', $like)->first();

            if($user){

                $query->whereUserId($user->id);
            }

        })
        ->paginate(10);
    }
}
