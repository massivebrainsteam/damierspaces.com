<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $hidden    = ['updated_at', 'password'];
	protected $guarded   = ['updated_at'];

    public function packages()
	{
		return $this->hasMany('App\Package');
	}
}
