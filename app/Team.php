<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Scopes;

class Team extends Model
{
    protected $hidden    = ['updated_at', 'password'];
    protected $guarded   = ['updated_at'];
    protected $appends   = ['slots', 'slots_assigned', 'slots_used']; 

    use Scopes;

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function attendances()
    {
        return $this->hasMany('App\Attendance');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    public function subscriptions()
    {
        return $this->hasMany('App\Subscription');
    }

    public function invitations()
    {
        return $this->hasMany('App\TeamInvitation');
    }

    public function getSlotsAttribute()
    {
        $subscription = $this->subscriptions()->whereStatus('active')->first();

        if(!$subscription)
            return 0;

        return $subscription->package->slot;
    }

    public function getSlotsAssignedAttribute()
    {
        return $this->users->sum('slots');
    }

    public function getSlotsUsedAttribute()
    {
        return $this->attendances()->whereStatus('expired')->count();
    }
}
