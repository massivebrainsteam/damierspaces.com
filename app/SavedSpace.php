<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedSpace extends Model
{
    protected $hidden    = ['updated_at'];
	protected $guarded   = ['updated_at'];

	public function space()
	{
		$this->belongsTo('App\Space');
	}

	public function user()
	{
		$this->belongsTo('App\User');
	}
}
