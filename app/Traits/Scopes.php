<?php 

namespace App\Traits;

trait Scopes {

	public function scopeActive($query)
    {
        return $query->where(['status' => 'active']);
    }

    public function scopeInactive($query)
    {
        return $query->where(['status' => 'inactive']);
    }

    public function scopeFeatured($query)
    {
        return $query->where(['featured' => 'yes']);
    }

    public function scopeNotFeatured($query)
    {
        return $query->where(['featured' => 'no']);
    }

    public function scopePending($query)
    {
        return $query->where(['status' => 'pending']);
    }

    public function scopeExpired($query)
    {
        return $query->where(['status' => 'expired']);
    }

    public function scopeCancelled($query)
    {
        return $query->where(['status' => 'cancelled']);
    }

}