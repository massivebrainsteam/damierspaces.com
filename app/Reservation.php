<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Scopes;

class Reservation extends Model
{
    protected $hidden    = ['updated_at'];
	protected $guarded   = ['updated_at'];
	protected $casts     = ['start_at' => 'datetime', 'end_at' => 'datetime'];

	use Scopes;
	
	public function space()
	{
		return $this->belongsTo('App\Space')->withDefault();
	}

	public function host()
	{
		return $this->belongsTo('App\Host')->withDefault();
	}

	public function user()
	{
		return $this->belongsTo('App\User')->withDefault();
	}

	public function team()
	{
		return $this->belongsTo('App\Team')->withDefault();
	}

	public function location()
	{
		return $this->belongsTo('App\Location')->withDefault();
	}	
}
