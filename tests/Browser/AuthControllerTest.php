<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class AuthControllerTest extends DuskTestCase
{
    public function setUp() : void
    {
        parent::setUp();
    }

    /** @test */
    public function it_should_render_login_page_to_guest_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->visit('/admin')
            ->assertPathIs('/login')
            ->waitUntilMissing('.se-pre-con')
            ->assertSee('Welcome Back!');
        });
    }

    /** @test */
    public function it_should_fail_to_login_with_invalid_credentials()
    {
        $this->browse(function (Browser $browser){

            $browser->visit('/login')
            ->waitForText('Sign Up')
            ->waitUntilMissing('.se-pre-con')
            ->type('email', '')
            ->type('password', '')
            ->press('#submit_login')
            ->assertFocused('input[name=email]')
            ->assertPathIs('/login');
        });

        $this->browse(function (Browser $browser){

            $browser->visit('/login')
            ->waitForText('Sign Up')
            ->waitUntilMissing('.se-pre-con')
            ->type('email', 'invalid@gmail.com')
            ->type('password', '')
            ->press('#submit_login')
            ->assertFocused('input[name=password]')
            ->assertPathIs('/login');
        });

        $this->browse(function (Browser $browser){

            $browser->visit('/login')
            ->waitForText('Sign Up')
            ->waitUntilMissing('.se-pre-con')
            ->type('email', 'invalid@gmail.com')
            ->type('password', 'invalid-password')
            ->press('#submit_login')
            ->waitForText('Sign Up')
            ->assertPathIs('/login');
        });
    }

    /** @test */
    public function it_should_login_successfully()
    {
        $admin = User::create([

            'type'      => 'admin',
            'name'      => 'Administrator',
            'email'     => 'admin@test.com',
            'password'  => bcrypt('password'),
            'status'    => 'active'
        ]);
        
        $this->browse(function (Browser $browser){

            $browser->visit('/login')
            ->waitForText('Sign Up')
            ->waitUntilMissing('.se-pre-con')
            ->type('input[name=email]', 'admin@test.com')
            ->type('input[name=password]', 'password')
            ->press('#submit_login')
            ->assertPathIs('/admin')
            ->assertSee('Dashboard');
        });

    }

    /** @test */
    public function it_should_logout_successfully()
    {
        $this->browse(function (Browser $browser){

            $browser->visit('/logout')
            ->assertPathIs('/login');
        });

    }
}
