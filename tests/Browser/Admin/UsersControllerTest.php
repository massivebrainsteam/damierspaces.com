<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class UsersControllerTest extends DuskTestCase
{
    public $admin;

    public function setUp() : void
    {
        parent::setUp();

        $this->admin = User::whereType('admin')->first();
    }

    /** @test */
    public function it_should_create_user_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/user/0')
            ->assertSee('Add New User')
            ->type('name', 'Test User')
            ->type('email', 'test@user.com')
            ->type('phone', '08175989890')
            ->type('address', 'Lagos')
            ->select('type', 'user')
            ->select('status', 'active')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/users/user')
            ->assertSee('User saved successfully.');
        });
    }

    /** @test */
    public function it_should_update_user_successfully()
    {
        $user = User::wherePhone('08175989890')->first();

        $this->browse(function (Browser $browser) use ($user) {

            $browser->loginAs($this->admin)
            ->visit('/admin/user/'.$user->id)
            ->type('name', 'Updated')
            ->select('status', 'inactive')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/users/user')
            ->assertSee('Updated')
            ->assertSee('INACTIVE')
            ->assertSee('User saved successfully.');
        });
    }

    /** @test */
    public function it_should_render_users_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/users/user')
            ->assertPathIs('/admin/users/user')
            ->assertSee('Users')
            //Since we updated above we should see updated data
            ->assertSee('Updated')
            ->assertSee('INACTIVE');
        });
    }

    /** @test */
    public function it_should_create_admin_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/user/0')
            ->assertSee('Add New User')
            ->type('name', 'Test Admin')
            ->type('email', 'admin@user.com')
            ->type('phone', '08175989891')
            ->type('address', 'Lagos')
            ->select('type', 'admin')
            ->select('status', 'active')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/users/admin')
            ->assertSee('User saved successfully.');
        });
    }

    /** @test */
    public function it_should_update_admin_successfully()
    {
        $user = User::wherePhone('08175989891')->first();

        $this->browse(function (Browser $browser) use ($user) {

            $browser->loginAs($this->admin)
            ->visit('/admin/user/'.$user->id)
            ->type('name', 'UpdatedAdmin')
            ->select('status', 'inactive')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/users/admin')
            ->assertSee('UpdatedAdmin')
            ->assertSee('INACTIVE')
            ->assertSee('User saved successfully.');
        });
    }

    /** @test */
    public function it_should_render_admins_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/users/admin')
            ->assertPathIs('/admin/users/admin')
            ->assertSee('Admins')
            //Since we updated above we should see updated data
            ->assertSee('UpdatedAdmin')
            ->assertSee('INACTIVE');
        });
    }
}
