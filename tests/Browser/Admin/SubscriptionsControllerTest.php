<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class SubscriptionsControllerTest extends DuskTestCase
{
    public $admin;

    public function setUp() : void
    {
        parent::setUp();

        $this->admin = User::whereType('admin')->first();
    }

    /** @test */
    public function it_should_render_subscriptions_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/subscriptions')
            ->assertPathIs('/admin/subscriptions')
            ->assertSee('Subscriptions');
            
        });
    }
}
