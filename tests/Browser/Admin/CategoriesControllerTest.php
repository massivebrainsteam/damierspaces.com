<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class CategoriesControllerTest extends DuskTestCase
{
    public $admin;

    public function setUp() : void
    {
        parent::setUp();

        $this->admin = User::whereType('admin')->first();
    }

    /** @test */
    public function it_should_create_category_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/category/0')
            ->assertSee('Add New Category')
            ->type('name', 'Test Category')
            ->select('status', 'active')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/categories')
            ->assertSee('Category saved successfully.');
        });
    }

    /** @test */
    public function it_should_update_category_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/category/1')
            ->type('name', 'Updated Test Category')
            ->select('status', 'inactive')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/categories')
            ->assertSee('Category saved successfully.');
        });
    }

    /** @test */
    public function it_should_render_categories_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/categories')
            ->assertPathIs('/admin/categories')
            ->assertSee('Categories')
            //Since we updated above we should see updated data
            ->assertSee('Updated Test Category')
            ->assertSee('INACTIVE')
            //Since we updated above we shouldn't see active as it should have been updated
            ->assertDontSee('active');
        });
    }
}
