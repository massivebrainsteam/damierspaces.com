<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Host;
use App\Location;
use App\Place;

class LocationsControllerTest extends DuskTestCase
{
    public $admin;
    public $host;

    public function setUp() : void
    {
        parent::setUp();

        Place::create(['name' => 'Ikeja']);
        $this->admin = User::whereType('admin')->first();
        $this->host = Host::where(['phone' => '09060051239'])->first();
    }

    /** @test */
    public function it_should_create_location_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/location/'.$this->host->id.'/0')
            ->assertSee('Add New Location for '.$this->host->name)
            ->type('name', 'Test Location')
            ->select('place_id', '1')
            ->type('address', 'Ikeja Lagos')
            ->select('status', 'active')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/locations/'.$this->host->id)
            ->assertSee('Location saved successfully.');
        });
    }

    /** @test */
    public function it_should_update_location_successfully()
    {
        $location = Location::first();

        $this->browse(function (Browser $browser) use($location) {

            $browser->loginAs($this->admin)
            ->visit('/admin/location/'.$this->host->id.'/'.$location->id)
            ->type('name', 'Updated')
            ->select('status', 'inactive')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/locations/'.$this->host->id)
            ->assertSee('Location saved successfully.');
        });
    }

    /** @test */
    public function it_should_render_locations_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/locations/'.$this->host->id)
            ->assertPathIs('/admin/locations/'.$this->host->id)
            ->assertSee('Locations')
            //Since we updated above we should see updated data
            ->assertSee('Updated')
            ->assertSee('Ikeja');
        });
    }
}
