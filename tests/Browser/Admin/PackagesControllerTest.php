<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Package;
use App\LocationCategory;
use App\Amenity;

class PackagesControllerTest extends DuskTestCase
{
    public $admin;

    public function setUp() : void
    {
        parent::setUp();

        $this->admin = User::whereType('admin')->first();
    }

    /** @test */
    public function it_should_create_package_basic_info_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/package/0/basic-info')
            ->assertSee('Add New Package')
            ->type('input[name=name]', 'Test Package')
            ->type('input[name=order]', '1')
            ->select('duration', 'monthly')
            ->type('input[name=amount]', 10000)
            ->select('status', 'active')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/package/1/slots')
            ->assertSee('Package saved successfully. Setup Package Slots');
        });
    }

    /** @test */
    public function it_should_create_package_slots_successfully()
    {
        $package = Package::first();

        $location_category = LocationCategory::create(['name' => 'Lagos Island']);

        $this->browse(function (Browser $browser) use ($package, $location_category) {

            $browser->loginAs($this->admin)
            ->visit('/admin/package/'.$package->id.'/slots')
            ->assertSee('Slots')
            ->type('slots['.$location_category->id.']', '10')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/package/'.$package->id.'/amenities')
            ->assertSee('Package Slots saved successfully. Setup Amenities');
        });
    }

    /** @test */
    public function it_should_create_package_amenities_successfully()
    {
        $package = Package::first();
        $amenity = Amenity::create(['category' => 'category', 'name' => 'amenity']);

        $this->browse(function (Browser $browser) use ($package, $amenity) {

            $browser->loginAs($this->admin)
            ->visit('/admin/package/'.$package->id.'/amenities')
            ->assertSee('Amenities')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/package/'.$package->id.'/other-settings')
            ->assertSee('Package Slots saved successfully. Setup Other Settings');
        });
    }

    /** @test */
    public function it_should_create_package_other_settings_successfully()
    {
        $package = Package::first();

        $this->browse(function (Browser $browser) use ($package) {

            $browser->loginAs($this->admin)
            ->visit('/admin/package/'.$package->id.'/other-settings')
            ->assertSee('Other Settings')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/packages')
            ->assertSee('Package configuration saved successfully.');
        });
    }

    /** @test */
    public function it_should_render_packages_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/packages')
            ->assertPathIs('/admin/packages')
            ->assertSee('Damier Spaces Packages')
            ->assertSee('TEST PACKAGE');
        });
    }
}
