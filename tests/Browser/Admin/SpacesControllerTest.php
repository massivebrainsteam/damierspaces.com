<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Space;
use App\Location;
use App\Host;

class SpacesControllerTest extends DuskTestCase
{
    public $admin;
    public $host;
    public $location;

    public function setUp() : void
    {
        parent::setUp();

        $this->admin = User::whereType('admin')->first();
        $this->host = Host::where(['phone' => '09060051239'])->first();

        $this->location = Location::create([

            'location_category_id'   => 1, 
            'host_id'                => $this->host->id, 
            'name'                   => 'Location',
            'place_id'               => 1
        ]);
    }

    /** @test */
    public function it_should_create_space_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/space/0?host_id='.$this->host->id)
            ->assertSee('Add New Space')
            ->assertSee('Monday (From)')
            ->assertSee('Drag')
            ->select('host_id', $this->host->id)
            ->select('location_id', $this->location->id)
            ->type('name', 'Test Space')
            ->type('textarea[name=description]', 'Test Description')
            ->select('category_id', '1')
            ->type('capacity', '1')
            ->select('status', 'active')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/spaces')
            ->assertSee('Space saved successfully.');
        });
    }

    /** @test */
    public function it_should_render_spaces_successfully()
    {
        $space = Space::first();

        $this->browse(function (Browser $browser) use ($space) {

            $browser->loginAs($this->admin)
            ->visit('/admin/spaces')
            ->assertPathIs('/admin/spaces')
            ->assertSee('Spaces')
            //Since we updated above we should see updated data
            ->assertSee('Test')
            ->assertSee($space->category->name)
            ->click('@tab-pane-two')
            ->waitFor('div#tabPaneTwo')
            ->assertSee('Updated');
        });
    }

    /** @test */
    public function it_should_search_spaces_successfully()
    {
        $this->browse(function (Browser $browser){

            $browser->loginAs($this->admin)
            ->visit('/admin/spaces')
            ->assertPathIs('/admin/spaces')
            ->assertSee('Spaces')
            ->type('q', 'test')
            ->keys('input[name=q]', ['{enter}'])
            ->assertSee('Your search returned 1 Result.')
            ->assertSee('Updated');
        });
    }
}
