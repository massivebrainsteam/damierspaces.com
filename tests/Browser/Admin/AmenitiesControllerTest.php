<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class AmenitiesControllerTest extends DuskTestCase
{
    public $admin;

    public function setUp() : void
    {
        parent::setUp();

        $this->admin = User::create([

            'name'      => 'Administrator',
            'email'     => 'admin@damier.com',
            'type'      => 'admin',
            'status'    => 'active',
            'password'  => bcrypt('password')
        ]);
    }

    /** @test */
    public function it_should_create_amenity_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/amenity/0')
            ->assertSee('Add New Amenity')
            ->type('input[name=name]', 'Test Amenity')
            ->type('input[name=category]', 'Test Category')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/amenities')
            ->assertSee('Amenity saved successfully.');
        });
    }

    /** @test */
    public function it_should_update_amenity_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/amenity/1')
            ->type('input[name=name]', 'Updated')
            ->type('input[name=category]', 'Updated2')
            ->press('button[type=submit]')
            ->assertPathIs('/admin/amenities')
            ->assertSee('Amenity saved successfully.');
        });
    }

    /** @test */
    public function it_should_render_amenities_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->loginAs($this->admin)
            ->visit('/admin/amenities')
            ->assertPathIs('/admin/amenities')
            ->assertSee('Amenities')
            //Since we updated above we should see updated data
            ->assertSee('Updated')
            ->assertSee('Updated2')
            //Since we updated above we shouldn't see active as it should have been updated
            ->assertDontSee('Test Amenity');
        });
    }
}
