<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Host;

class HostsControllerTest extends DuskTestCase
{
	public $admin;

	public function setUp() : void
	{
		parent::setUp();

		$this->admin = User::whereType('admin')->first();
	}

	/** @test */
	public function it_should_create_host_successfully()
	{
		$this->browse(function (Browser $browser) {

			$browser->loginAs($this->admin)
			->visit('/admin/host/0')
			->type('name', 'Test Host')
			->type('description', 'Test Host')
			->type('email', 'host@test.com')
			->type('phone', '09060051239')
			->type('address', 'Test Host')
			->type('contact_name', 'Test Host')
			->type('contact_email', 'host@test.com')
			->type('contact_phone', '08175020329')
			->select('status', 'active')
			->press('button[type=submit]')
			->assertPathIs('/admin/hosts')
			->assertSee('Host saved successfully.');
		});
	}

	/** @test */
    public function it_should_update_host_successfully()
    {
    	$host = Host::where(['phone' => '09060051239'])->first();

        $this->browse(function (Browser $browser) use ($host) {

            $browser->loginAs($this->admin)
            ->visit('/admin/host/'.$host->id)
            ->assertDontSee('CONTACT PERSON')
            ->type('name', 'Updated')
            ->press('button[type=submit]')
            ->assertSee('Host saved successfully.')
            ->assertPathIs('/admin/hosts');
        });
    }

	/** @test */
	public function it_should_render_hosts_successfully()
	{
		$this->browse(function (Browser $browser) {

			$browser->loginAs($this->admin)
			->visit('/admin/hosts')
			->assertPathIs('/admin/hosts')
			->assertSee('Updated');
		});
	}
}
