<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Attendance;

class AttendancesControllerTest extends DuskTestCase
{
    public $admin;

    public function setUp() : void
    {
        parent::setUp();

        $this->admin = User::whereType('admin')->first();
    }

    /** @test */
    public function it_should_render_attendances_successfully()
    {
        $attendance = Attendance::create([

            'user_id'       => 1,
            'space_id'      => 1,
            'start_at'      => now(),
            'end_at'        => now(),
            'approved_by'   => 1,
            'reference'     => str_random(6),
            'status'        => 'active'
        ]);

        $this->browse(function (Browser $browser) use ($attendance) {

            $browser->loginAs($this->admin)
            ->visit('/admin/attendances')
            ->assertPathIs('/admin/attendances')
            ->assertSee('Attendances')
            ->assertSee($attendance->reference);
            
        });
    }
}
