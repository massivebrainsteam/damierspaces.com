<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Review;

class ReviewsControllerTest extends DuskTestCase
{
    public $admin;

    public function setUp() : void
    {
        parent::setUp();

        $this->admin = User::whereType('admin')->first();
    }

    /** @test */
    public function it_should_render_reviews_successfully()
    {
       $review = Review::create([

            'user_id'       => 1,
            'space_id'      => 1,
            'rating'        => 5,
            'review'        => 'review'
        ]);

        $this->browse(function (Browser $browser) use ($review) {

            $browser->loginAs($this->admin)
            ->visit('/admin/reviews')
            ->assertPathIs('/admin/reviews')
            ->assertSee('Reviews')
            ->assertSee($review->review);
            
        });
    }
}
