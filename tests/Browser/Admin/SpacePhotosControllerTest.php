<?php

namespace Tests\Browser\Admin;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Space;

class SpacePhotosControllerTest extends DuskTestCase
{
    public $admin;

    public function setUp() : void
    {
        parent::setUp();

        $this->admin = User::whereType('admin')->first();
    }

    /** @test */
    public function it_should_render_space_photos_successfully()
    {
        $space = Space::create([

            'host_id'       => 1,
            'location_id'   => 1,
            'place_id'      => 1,
            'category_id'   => 1,
            'name'          => 'space'
        ]);

        $this->browse(function (Browser $browser) use ($space) {

            $browser->loginAs($this->admin)
            ->visit('/admin/space/'.$space->id.'/photos')
            ->assertPathIs('/admin/space/'.$space->id.'/photos')
            ->assertSee('Photos')
            ->assertSee('Add New Photo');
            
        });
    }

    /** @test */
    public function it_should_render_space_photo_form_successfully()
    {
        $space = Space::first();

        $this->browse(function (Browser $browser) use ($space) {

            $browser->loginAs($this->admin)
            ->visit('/admin/space/'.$space->id.'/photo/0')
            ->assertPathIs('/admin/space/'.$space->id.'/photo/0')
            ->assertSee('Photo');
            
        });
    }
}
