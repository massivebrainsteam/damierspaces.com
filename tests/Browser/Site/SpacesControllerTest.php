<?php

namespace Tests\Browser\Site;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SpacesControllerTest extends DuskTestCase
{
    /** @test */
    public function it_should_render_space_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->visit('/spaces/')
            ->waitUntilMissing('.se-pre-con')
            ->assertSee('FILTER SPACES');
        });
    }
}
