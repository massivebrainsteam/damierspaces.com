<?php

namespace Tests\Browser\Site;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class HomeControllerTest extends DuskTestCase
{
    /** @test */
    public function it_should_render_homepage_successfully()
    {
        $this->browse(function (Browser $browser) {

            $browser->visit('/')
            ->waitUntilMissing('.se-pre-con')
            ->assertSee('Become a Host')
            ->assertSee('Locations')
            ->assertSee('Workspaces')
            ->assertSee('Spaces near you')
            ->assertSee('Contact');
        });
    }
}
