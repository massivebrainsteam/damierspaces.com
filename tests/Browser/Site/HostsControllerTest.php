<?php

namespace Tests\Browser\Site;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Host;

class HostsControllerTest extends DuskTestCase
{
    /** @test */
    public function it_should_render_host_successfully()
    {
        $this->browse(function (Browser $browser){

            $browser->visit('/host/1')
            ->assertSee('Description')
            ->assertSee('Spaces')
            ->assertSee('About')
            ->assertSee('Reviews');
        });
    }
}
