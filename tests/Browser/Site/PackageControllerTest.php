<?php

namespace Tests\Browser\Site;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Package;
use App\User;

class PackageControllerTest extends DuskTestCase
{
    /** @test */
    public function it_should_render_package_successfully()
    {
        $package = Package::whereStatus('active')->first();

        $this->browse(function (Browser $browser) use ($package) {

            $browser->visit('/package/'.$package->id)
            ->assertSee($package->name)
            ->assertSee('Subscribe');
        });
    }
}
