<?php

namespace Tests\Browser\Site;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Space;

class SpaceControllerTest extends DuskTestCase
{
    /** @test */
    public function it_should_render_space_successfully()
    {
        $space = Space::first();

        $this->browse(function (Browser $browser) use ($space) {

            $browser->visit('/space/'.$space->id)
            ->waitUntilMissing('.se-pre-con')
            ->assertSee('About This Space')
            ->assertSee($space->name);
        });
    }
}
