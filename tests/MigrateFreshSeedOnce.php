<?php
namespace Tests;
use Illuminate\Support\Facades\Artisan;
use App\Host;
use App\User;

trait MigrateFreshSeedOnce
{
    /**
    * If true, setup has run at least once.
    * @var boolean
    */
    protected static $setUpHasRunOnce = false;
    
    /**
    * After the first run of setUp "migrate:fresh --seed"
    * @return void
    */
    public function setUp(): void
    {
    	parent::setUp();

    	if(!static::$setUpHasRunOnce){

    		Artisan::call('migrate:fresh');

            $host = Host::create(['name' => 'Test Host', 'email' => 'host@gmail.com']);

            User::create([

                'host_id'   => $host->id,
                'name'      => 'Test',
                'email'     => 'test@gmail.com',
                'phone'     => '08145323432',
                'type'      => 'host',
                'address'   => 'Lagos',
                'status'    => 'active',
                'api_token' => str_random(16)
            ]);

    		static::$setUpHasRunOnce = true;
    	}
    }
}