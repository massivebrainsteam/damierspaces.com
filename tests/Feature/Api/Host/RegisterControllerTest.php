<?php

namespace Tests\Feature\Api\Host;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RegisterControllerTest extends TestCase
{
    /** @test */
    public function it_fails_to_register_on_invalid_payload()
    {
        $payloads = [

            [],
            [
                'email'   => 'user@damier.com', 
                'phone' => '08175020311', 
                'address' => 'Lagos Nigeria', 
                'contact_name' => 'Massive Brains', 
                'contact_email' => 'massive@damier.com', 
                'contact_phone' => '09060051233', 
            ],
            [
                'name'  => 'John Doe', 
                'phone' => '08175020311', 
                'address' => 'Lagos Nigeria', 
                'contact_name' => 'Massive Brains', 
                'contact_email' => 'massive@damier.com', 
                'contact_phone' => '09060051233', 
            ],
            [
                'name'  => 'John Doe', 
                'email'   => 'user@damier.com', 
                'address' => 'Lagos Nigeria', 
                'contact_name' => 'Massive Brains', 
                'contact_email' => 'massive@damier.com', 
                'contact_phone' => '09060051233', 
            ],
            [
                'name'  => 'John Doe', 
                'email'   => 'user@damier.com', 
                'phone' => '08175020311', 
                'contact_name' => 'Massive Brains', 
                'contact_email' => 'massive@damier.com', 
                'contact_phone' => '09060051233', 
            ],
            [
                'name'  => 'John Doe', 
                'email'   => 'user@damier.com', 
                'phone' => '08175020311', 
                'address' => 'Lagos Nigeria', 
                'contact_email' => 'massive@damier.com', 
                'contact_phone' => '09060051233', 
            ],
            [
                'name'  => 'John Doe', 
                'email'   => 'user@damier.com', 
                'phone' => '08175020311', 
                'address' => 'Lagos Nigeria', 
                'contact_name' => 'Massive Brains', 
                'contact_phone' => '09060051233', 
            ],
            [
                'name'  => 'John Doe', 
                'email'   => 'user@damier.com', 
                'phone' => '08175020311', 
                'address' => 'Lagos Nigeria', 
                'contact_name' => 'Massive Brains', 
                'contact_email' => 'massive@damier.com', 
            ],
            
        ];

        foreach($payloads as $payload){

            $response = $this->json('POST', '/api/host/auth/register', $payload)
            ->assertStatus(422)
            ->assertJson(['status' => 'validation-failed'])
            ->assertJsonStructure([

                'status',
                'data'
            ]);
        }
    }

    /** @test */
    public function it_registers_user_successfully()
    {
        $payload = [

            'name'  => 'John Doe', 
            'email'   => 'user@damier.com', 
            'phone' => '08175020311', 
            'address' => 'Lagos Nigeria', 
            'contact_name' => 'Massive Brains', 
            'contact_email' => 'massive@damier.com', 
            'contact_phone' => '09060051233', 
        ];

        $response = $this->json('POST', '/api/host/auth/register', $payload)
        ->assertStatus(201)
        ->assertJson([

            'status' => 'Successful',
            'message' => 'Registration Successful'
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }
}
