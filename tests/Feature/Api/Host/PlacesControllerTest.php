<?php

namespace Tests\Feature\Api\Host;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class PlacesControllerTest extends TestCase
{
    public $user;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = User::query()->host()->first();
    }
    
    /** @test */
    public function it_returns_categories_successfully()
    {
        $response = $this->actingAs($this->user, 'api')
        ->get('/api/host/places')
        ->assertStatus(200)
        ->assertJson(['status' => 'Successful'])
        ->assertJsonStructure([

            'status',
            'message',
            'data'
        ]);
    }
}
