<?php

namespace Tests\Feature\Api\Host;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class LocationsControllerTest extends TestCase
{
    public $user;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = User::query()->host()->first();
    }
    
    /** @test */
    public function it_returns_locations_successfully()
    {
        $response = $this->actingAs($this->user, 'api')
        ->get('/api/host/locations')
        ->assertStatus(200)
        ->assertJson(['status' => 'Successful'])
        ->assertJsonStructure([

            'status',
            'message',
            'data'
        ]);
    }

    /** @test */
    public function it_fails_to_create_location_on_invalid_payload()
    {
        $payloads = [

            [],
            [
                'place_id'  => 1, 
                'address'   => 'Ikeja, Lagos', 
                'photo_url' => 'https://example.com', 
            ],
            [
                'name' => 'Test Location', 
                'address'   => 'Ikeja, Lagos', 
                'photo_url' => 'https://example.com', 
            ],
            [
                'name' => 'Test Location', 
                'place_id'  => 1, 
                'photo_url' => 'https://example.com', 
            ]
        ];

        foreach($payloads as $payload){

            $response = $this->actingAs($this->user, 'api')
            ->json('POST', '/api/host/location', $payload)
            ->assertStatus(422)
            ->assertJson(['status' => 'validation-failed'])
            ->assertJsonStructure([

                'status',
                'data'
            ]);
        }
    }

    /** @test */
    public function it_creates_locations_successfully()
    {
        $payload = [

            'name' => 'Test Location', 
            'place_id'  => 1, 
            'address'   => 'Gbagada Lagos', 
            'photo_url' => 'https://example.com', 
        ];

        $response = $this->actingAs($this->user, 'api')
        ->json('POST', '/api/host/location', $payload)
        ->assertStatus(201)
        ->assertJson([

            'status' => 'Successful',
            'data'   => [

                'name'          => 'Test Location',
                'latitude'      => 6.558351699999999,
                'longitude'     => 3.3914817
            ] 
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }
}
