<?php

namespace Tests\Feature\Api\Host;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class SpacesControllerTest extends TestCase
{
    public $user;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = User::query()->host()->first();
    }
    
    /** @test */
    public function it_fails_to_create_space_on_invalid_payload()
    {
        $payloads = [

            [],
            [
                'location_id' => 1, 
                'package_id' => 1, 
                'name' => 'Test Space', 
                'capacity' => 1, 
                'photo_url' => 'https://example.com', 
                'amenities' => [1,2,4], 
                'photos' => ['https://example.com'], 
                'usage' => 'everyday', 
                'start_at' => '8:00AM', 
                'end_at' => '5:00PM'
            ],
            [
                'category_id' => 1, 
                'package_id' => 1, 
                'name' => 'Test Space', 
                'capacity' => 1, 
                'photo_url' => 'https://example.com', 
                'amenities' => [1,2,4], 
                'photos' => ['https://example.com'], 
                'usage' => 'everyday', 
                'start_at' => '8:00AM', 
                'end_at' => '5:00PM'
            ],
            [
                'category_id' => 1, 
                'location_id' => 1, 
                'name' => 'Test Space', 
                'capacity' => 1, 
                'photo_url' => 'https://example.com', 
                'amenities' => [1,2,4], 
                'photos' => ['https://example.com'], 
                'usage' => 'everyday', 
                'start_at' => '8:00AM', 
                'end_at' => '5:00PM'
            ],
            [
                'category_id' => 1, 
                'location_id' => 1, 
                'package_id' => 1, 
                'capacity' => 1, 
                'photo_url' => 'https://example.com', 
                'amenities' => [1,2,4], 
                'photos' => ['https://example.com'], 
                'usage' => 'everyday', 
                'start_at' => '8:00AM', 
                'end_at' => '5:00PM'
            ]
            
        ];

        foreach($payloads as $payload){

            $response = $this->actingAs($this->user, 'api')
            ->json('POST', '/api/host/space', $payload)
            ->assertStatus(422)
            ->assertJson(['status' => 'validation-failed'])
            ->assertJsonStructure([

                'status',
                'data'
            ]);
        }
    }

    /** @test */
    public function it_creates_spaces_successfully()
    {
        $payload = [

            'category_id' => 1, 
            'location_id' => 1, 
            'package_id' => 1, 
            'name' => 'Test Space', 
            'capacity' => 1, 
            'photo_url' => 'https://example.com', 
            'amenities' => [1,2,4], 
            'photos' => ['https://example.com'], 
            'usage' => 'everyday', 
            'start_at' => '8:00AM', 
            'end_at' => '5:00PM'
        ];

        $response = $this->actingAs($this->user, 'api')
        ->json('POST', '/api/host/space', $payload)
        ->assertStatus(201)
        ->assertJson([

            'status' => 'Successful',
            'data'   => [

                'name'  => 'Test Space'
            ] 
        ])
        ->assertJsonStructure([

            'status',
            'data'
        ]);
    }
}
