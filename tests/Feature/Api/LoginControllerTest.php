<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class LoginControllerTest extends TestCase
{
    public $user;

    public function setUp() : void
    {
        parent::setUp();

        $this->user = User::query()->host()->first();
    }

    /** @test */
    public function login_fails_on_invalid_api_tokens()
    {
        $response = $this->post('/api/auth/login', []);

        $response
        ->assertStatus(422)
        ->assertJson([

            'status'    => 'Failed',
            'message'   => 'Invalid username or password',
            'data'      => null
        ]);

        $response = $this->post('/api/auth/login', ['api_token' => 'invalid'], ['Accept' => 'application/json']);

        $response
        ->assertStatus(422)
        ->assertJson([

            'status'    => 'Failed',
            'message'   => 'Invalid username or password',
            'data'      => null
        ]);
        
    }

    /** @test */
    public function it_logsin_successfully_with_valid_api_token()
    {
        $user = $this->user;

        $this->json('POST', '/api/auth/login', ['api_token' => $user->api_token])
        ->assertStatus(200)
        ->assertJsonStructure([

            'status',
            'message',
            'data'
        ]);
    }

    /** @test */
    public function it_returns_users_profile()
    {
        $user = $this->user;

        $this->actingAs($this->user, 'api')
        ->json('GET', '/api/auth/profile')
        ->assertStatus(200)
        ->assertJson(['status' => 'Successful'])
        ->assertJsonStructure([

            'status',
            'message',
            'data'
        ]);
    }

    /** @test */
    public function it_updates_user_profile()
    {
        $user = $this->user;

        $this->actingAs($this->user, 'api')
        ->json('POST', '/api/auth/profile', ['name' => 'updated', 'place_id' => 1])
        ->assertStatus(200)
        ->assertJson([

            'data' => [

                'id'        => $user->id, 
                'name'      => 'updated',
                'place_id'  => 1
            ]
        ])
        ->assertJsonStructure([

            'status',
            'message',
            'data'
        ]);
    }
}
